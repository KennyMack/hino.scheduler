﻿using Hino.Scheduler.Utils;
using System;
using System.Windows.Forms;

namespace Hino.Scheduler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                // Forçando o idioma português para a aplicação
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");
                DevExpress.Skins.SkinManager.EnableFormSkins();
                DevExpress.UserSkins.BonusSkins.Register();

                System.Windows.Forms.Application.EnableVisualStyles();
                System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                Parameters.UserLogged = "***LOGIN_INVALIDO***";
                using (var login = new frmLogin())
                    login.ShowDialog();

                if (Parameters.UserLogged != "***LOGIN_INVALIDO***")
                    System.Windows.Forms.Application.Run(new frmMain());
            }
            catch (Exception e)
            {

                var mseg = "";


                mseg += e.Source + "\r\n";

                mseg += e.Message + "\r\n";
                mseg += e.StackTrace + "\r\n";
                MessageBox.Show(mseg, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
