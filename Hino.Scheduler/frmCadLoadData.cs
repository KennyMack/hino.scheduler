﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Hino.Scheduler.Application.ViewModel;
using Hino.Scheduler.Utils;

namespace Hino.Scheduler
{
    public partial class frmCadLoadData : frmCadPadrao
    {
        private readonly LoadDataVM _LoadDataVM;
        public frmCadLoadData()
        {
            InitializeComponent();
            _LoadDataVM = new LoadDataVM();

            deInitialDate.DateTime = DateTime.Now.FirstDay();
            deEndDate.DateTime = DateTime.Now.LastDay();

            fmtMascara.Data(deInitialDate);
            fmtMascara.Data(deEndDate);
        }

        protected async override void OnSaveClicked()
        {
            ssCadManager.ShowWaitForm();
            await Task.Delay(1000);
            if (!await _LoadDataVM.CreateScenario(deInitialDate.DateTime.FirstHour(), deEndDate.DateTime.LastHour()))
            {
                ssCadManager.CloseWaitForm();
                MessageBox.Show(_LoadDataVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }
            else
                ssCadManager.CloseWaitForm();
            base.OnSaveClicked();
        }

        ~frmCadLoadData()
        {
            _LoadDataVM.Dispose();
        }
    }
}