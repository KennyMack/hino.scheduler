﻿using Hino.Scheduler.Application.ViewModel.Engenharia;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Engenharia
{
    public partial class frmVsResourcesList : frmVsPadrao
    {
        private readonly ResouresVM _ResouresVM;

        public frmVsResourcesList()
        {
            InitializeComponent();
            _ResouresVM = new ResouresVM();
        }

        private async  void frmVsResourcesList_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _ResouresVM._IdScenario = Parameters.IdScenario;
            await _ResouresVM.LoadDataMachines();

            gcGrid.DataSource = _ResouresVM._MachinesDataSource;
            if (_Initialize)
            {
                var vsIndex = 0;
                gvVisao.Columns["codmaquina"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["descricao"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codcelula"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["quantidade"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["rendimento"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["observacao"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["status"].OrdenaColuna(vsIndex++);
                gvVisao.BestFitColumns();

                fmtGridView.OcultaColuna(gvVisao.Columns["codestab"]);
                fmtGridView.Observacao(gvVisao.Columns["observacao"]);

                gvVisao.Columns["codmaquina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion

        ~frmVsResourcesList()
        {
            _ResouresVM.Dispose();
        }

    }
}
