﻿using Hino.Scheduler.Application.ViewModel.Engenharia;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Engenharia
{
    public partial class frmVsResourcesDisp : frmVsPadrao
    {
        private readonly ResouresVM _ResouresVM;

        public frmVsResourcesDisp()
        {
            InitializeComponent();
            _ResouresVM = new ResouresVM();
        }

        private async void frmVsResourcesDisp_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _ResouresVM._IdScenario = Parameters.IdScenario;
            await _ResouresVM.LoadData();

            gcGrid.DataSource = _ResouresVM._MachineLoad;
            if (_Initialize)
            {
                var vsIndex = 0;
                gvVisao.Columns["iddemanda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["client"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codestab"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codpedvenda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codproduto"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["descricao"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dtinicio"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dttermino"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dataprogvenda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["qtdprogramada"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["sldprograma"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["diferenca"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["atendida"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["temops"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["StatusDemanda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["DiasDifProd"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["AtendeSaldoProg"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["IDJOBSIMU"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["IdLoadData"].OrdenaColuna(vsIndex++);

                gvVisao.BestFitColumns();

                fmtGridView.OcultaColuna(
                    gvVisao.Columns["iddemanda"],
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["IdLoadData"],
                    gvVisao.Columns["IDJOBSIMU"]
                );

                gvVisao.Columns["dataprogvenda"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion

        ~frmVsResourcesDisp()
        {
            _ResouresVM.Dispose();
        }
    }
}
