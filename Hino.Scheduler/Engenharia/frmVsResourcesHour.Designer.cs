﻿namespace Hino.Scheduler.Engenharia
{
    partial class frmVsResourcesHour
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVsResourcesHour));
            this.scMovimenta = new DevExpress.XtraEditors.SplitterControl();
            this.gcSchedule = new DevExpress.XtraGrid.GridControl();
            this.enMachineHoursScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.adgvSchedule = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDomingo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMachine = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourDom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourDom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourDom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourDom = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourSeg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourSeg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourTer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourTer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourQua = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourQua = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourQui = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourSex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourSex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourSab = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumSemana = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colMesAno = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHours = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHours = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkHourQui = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSegunda = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourSeg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourSeg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTerca = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourTer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourTer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQuarta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourQua = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourQua = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQuinta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourQui = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourQui = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSexta = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourSex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourSex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSabado = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescStopHourSab = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colStopHourSab = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDescWorkHourSab = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enMachineHoursScheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adgvSchedule)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(946, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 404);
            this.barDockControlBottom.Size = new System.Drawing.Size(946, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 378);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(946, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 378);
            // 
            // icImagens16
            // 
            this.icImagens16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImagens16.ImageStream")));
            this.icImagens16.Images.SetKeyName(0, "Novo16.png");
            this.icImagens16.Images.SetKeyName(1, "Editar 16.png");
            this.icImagens16.Images.SetKeyName(2, "Excluir_16.png");
            this.icImagens16.Images.SetKeyName(3, "Atualizar 16.png");
            this.icImagens16.Images.SetKeyName(4, "Imprimir 16.png");
            this.icImagens16.Images.SetKeyName(5, "Visualizar_Reg_16.png");
            this.icImagens16.Images.SetKeyName(6, "Configuração para Importação de Dados 16.png");
            this.icImagens16.Images.SetKeyName(7, "Gráfico 16.png");
            this.icImagens16.Images.SetKeyName(8, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(9, "Layout 16.png");
            this.icImagens16.Images.SetKeyName(10, "Filtro 16.png");
            this.icImagens16.Images.SetKeyName(11, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(12, "Definidos 16.png");
            this.icImagens16.Images.SetKeyName(13, "Primeiro16.png");
            this.icImagens16.Images.SetKeyName(14, "Anterior16.png");
            this.icImagens16.Images.SetKeyName(15, "Próximo16.png");
            this.icImagens16.Images.SetKeyName(16, "Último16.png");
            this.icImagens16.Images.SetKeyName(17, "Log 16.png");
            this.icImagens16.Images.SetKeyName(18, "Anexos 16.png");
            this.icImagens16.Images.SetKeyName(19, "Ajuda 16.png");
            this.icImagens16.Images.SetKeyName(20, "calculadora 16.png");
            // 
            // gcGrid
            // 
            this.gcGrid.Location = new System.Drawing.Point(6, 26);
            this.gcGrid.Size = new System.Drawing.Size(940, 378);
            // 
            // gvVisao
            // 
            this.gvVisao.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gvVisao.OptionsBehavior.ReadOnly = true;
            this.gvVisao.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gvVisao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvVisao.OptionsPrint.AutoWidth = false;
            this.gvVisao.OptionsPrint.ExpandAllDetails = true;
            this.gvVisao.OptionsPrint.PrintDetails = true;
            this.gvVisao.OptionsPrint.PrintFilterInfo = true;
            this.gvVisao.OptionsView.BestFitMaxRowCount = 100;
            this.gvVisao.OptionsView.ColumnAutoWidth = false;
            this.gvVisao.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gvVisao.OptionsView.ShowAutoFilterRow = true;
            this.gvVisao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gvVisao.OptionsView.ShowFooter = true;
            // 
            // bbiNovo
            // 
            this.bbiNovo.ImageOptions.ImageIndex = 0;
            this.bbiNovo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiEditar
            // 
            this.bbiEditar.ImageOptions.ImageIndex = 1;
            this.bbiEditar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiExcluir
            // 
            this.bbiExcluir.ImageOptions.ImageIndex = 2;
            this.bbiExcluir.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiAtualizar
            // 
            this.bbiAtualizar.ImageOptions.ImageIndex = 3;
            // 
            // bbiExportar
            // 
            this.bbiExportar.ImageOptions.ImageIndex = 6;
            // 
            // bbiImprimir
            // 
            this.bbiImprimir.ImageOptions.ImageIndex = 4;
            // 
            // bsiLayoutExcluir
            // 
            this.bsiLayoutExcluir.ImageOptions.ImageIndex = 2;
            // 
            // bbiFiltroSalvar
            // 
            this.bbiFiltroSalvar.ImageOptions.ImageIndex = 8;
            // 
            // bbiFiltroSalvarComo
            // 
            this.bbiFiltroSalvarComo.ImageOptions.ImageIndex = 11;
            // 
            // bsiLayoutDefinir
            // 
            this.bsiLayoutDefinir.ImageOptions.ImageIndex = 12;
            // 
            // bbiPrimReg
            // 
            this.bbiPrimReg.ImageOptions.ImageIndex = 13;
            // 
            // bbiRegAnt
            // 
            this.bbiRegAnt.ImageOptions.ImageIndex = 14;
            // 
            // bbiProxReg
            // 
            this.bbiProxReg.ImageOptions.ImageIndex = 15;
            // 
            // bbiUltReg
            // 
            this.bbiUltReg.ImageOptions.ImageIndex = 16;
            // 
            // bsiAnexos
            // 
            this.bsiAnexos.ImageOptions.ImageIndex = 18;
            // 
            // bbiAjuda
            // 
            this.bbiAjuda.ImageOptions.ImageIndex = 19;
            // 
            // bbiExpDadosXLSX
            // 
            this.bbiExpDadosXLSX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosXLSX.ImageOptions.Image")));
            // 
            // bbiExpDadosXLS
            // 
            this.bbiExpDadosXLS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosXLS.ImageOptions.Image")));
            // 
            // bbiExpDadosPDF
            // 
            this.bbiExpDadosPDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosPDF.ImageOptions.Image")));
            // 
            // bbiExpDadosHTML
            // 
            this.bbiExpDadosHTML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosHTML.ImageOptions.Image")));
            // 
            // bbiExpDadosTXT
            // 
            this.bbiExpDadosTXT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosTXT.ImageOptions.Image")));
            // 
            // bbiExpDadosRTF
            // 
            this.bbiExpDadosRTF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosRTF.ImageOptions.Image")));
            // 
            // scMovimenta
            // 
            this.scMovimenta.Location = new System.Drawing.Point(0, 26);
            this.scMovimenta.Name = "scMovimenta";
            this.scMovimenta.Size = new System.Drawing.Size(6, 378);
            this.scMovimenta.TabIndex = 9;
            this.scMovimenta.TabStop = false;
            this.scMovimenta.Visible = false;
            // 
            // gcSchedule
            // 
            this.gcSchedule.DataSource = this.enMachineHoursScheduleBindingSource;
            this.gcSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSchedule.Location = new System.Drawing.Point(6, 26);
            this.gcSchedule.MainView = this.adgvSchedule;
            this.gcSchedule.MenuManager = this.bmControleBarra;
            this.gcSchedule.Name = "gcSchedule";
            this.gcSchedule.Size = new System.Drawing.Size(940, 378);
            this.gcSchedule.TabIndex = 10;
            this.gcSchedule.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.adgvSchedule});
            this.gcSchedule.Visible = false;
            // 
            // enMachineHoursScheduleBindingSource
            // 
            this.enMachineHoursScheduleBindingSource.DataSource = typeof(Hino.Scheduler.Model.Engenharia.enMachineHoursSchedule);
            // 
            // adgvSchedule
            // 
            this.adgvSchedule.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13});
            this.adgvSchedule.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colMachine,
            this.colDomingo,
            this.colWorkHourDom,
            this.colStopHourDom,
            this.colDescWorkHourDom,
            this.colDescStopHourDom,
            this.colSegunda,
            this.colWorkHourSeg,
            this.colStopHourSeg,
            this.colDescWorkHourSeg,
            this.colDescStopHourSeg,
            this.colTerca,
            this.colWorkHourTer,
            this.colStopHourTer,
            this.colDescWorkHourTer,
            this.colDescStopHourTer,
            this.colQuarta,
            this.colWorkHourQua,
            this.colStopHourQua,
            this.colDescWorkHourQua,
            this.colDescStopHourQua,
            this.colQuinta,
            this.colWorkHourQui,
            this.colStopHourQui,
            this.colDescWorkHourQui,
            this.colDescStopHourQui,
            this.colSexta,
            this.colWorkHourSex,
            this.colStopHourSex,
            this.colDescWorkHourSex,
            this.colDescStopHourSex,
            this.colSabado,
            this.colWorkHourSab,
            this.colStopHourSab,
            this.colDescWorkHourSab,
            this.colDescStopHourSab,
            this.colNumSemana,
            this.colMesAno,
            this.colWorkHours,
            this.colStopHours});
            this.adgvSchedule.GridControl = this.gcSchedule;
            this.adgvSchedule.Name = "adgvSchedule";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Dom";
            this.gridBand1.Columns.Add(this.colDomingo);
            this.gridBand1.Columns.Add(this.colMachine);
            this.gridBand1.Columns.Add(this.colWorkHourDom);
            this.gridBand1.Columns.Add(this.colStopHourDom);
            this.gridBand1.Columns.Add(this.colDescWorkHourDom);
            this.gridBand1.Columns.Add(this.colDescStopHourDom);
            this.gridBand1.Columns.Add(this.colWorkHourSeg);
            this.gridBand1.Columns.Add(this.colStopHourSeg);
            this.gridBand1.Columns.Add(this.colWorkHourTer);
            this.gridBand1.Columns.Add(this.colStopHourTer);
            this.gridBand1.Columns.Add(this.colWorkHourQua);
            this.gridBand1.Columns.Add(this.colStopHourQua);
            this.gridBand1.Columns.Add(this.colStopHourQui);
            this.gridBand1.Columns.Add(this.colWorkHourSex);
            this.gridBand1.Columns.Add(this.colStopHourSex);
            this.gridBand1.Columns.Add(this.colWorkHourSab);
            this.gridBand1.Columns.Add(this.colNumSemana);
            this.gridBand1.Columns.Add(this.colMesAno);
            this.gridBand1.Columns.Add(this.colWorkHours);
            this.gridBand1.Columns.Add(this.colStopHours);
            this.gridBand1.Columns.Add(this.colWorkHourQui);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 100;
            // 
            // colDomingo
            // 
            this.colDomingo.Caption = "Domingo";
            this.colDomingo.FieldName = "Domingo";
            this.colDomingo.Name = "colDomingo";
            this.colDomingo.Visible = true;
            this.colDomingo.Width = 100;
            // 
            // colMachine
            // 
            this.colMachine.Caption = "Máquina";
            this.colMachine.FieldName = "Machine";
            this.colMachine.Name = "colMachine";
            this.colMachine.RowIndex = 1;
            this.colMachine.Width = 78;
            // 
            // colWorkHourDom
            // 
            this.colWorkHourDom.FieldName = "WorkHourDom";
            this.colWorkHourDom.Name = "colWorkHourDom";
            this.colWorkHourDom.RowIndex = 1;
            this.colWorkHourDom.Width = 78;
            // 
            // colStopHourDom
            // 
            this.colStopHourDom.FieldName = "StopHourDom";
            this.colStopHourDom.Name = "colStopHourDom";
            this.colStopHourDom.RowIndex = 1;
            this.colStopHourDom.Width = 78;
            // 
            // colDescWorkHourDom
            // 
            this.colDescWorkHourDom.Caption = "Trab Dom";
            this.colDescWorkHourDom.FieldName = "DescWorkHourDom";
            this.colDescWorkHourDom.Name = "colDescWorkHourDom";
            this.colDescWorkHourDom.RowIndex = 1;
            this.colDescWorkHourDom.Visible = true;
            this.colDescWorkHourDom.Width = 50;
            // 
            // colDescStopHourDom
            // 
            this.colDescStopHourDom.Caption = "Par Dom";
            this.colDescStopHourDom.FieldName = "DescStopHourDom";
            this.colDescStopHourDom.Name = "colDescStopHourDom";
            this.colDescStopHourDom.RowIndex = 1;
            this.colDescStopHourDom.Visible = true;
            this.colDescStopHourDom.Width = 50;
            // 
            // colWorkHourSeg
            // 
            this.colWorkHourSeg.FieldName = "WorkHourSeg";
            this.colWorkHourSeg.Name = "colWorkHourSeg";
            this.colWorkHourSeg.RowIndex = 1;
            this.colWorkHourSeg.Width = 78;
            // 
            // colStopHourSeg
            // 
            this.colStopHourSeg.FieldName = "StopHourSeg";
            this.colStopHourSeg.Name = "colStopHourSeg";
            this.colStopHourSeg.RowIndex = 1;
            this.colStopHourSeg.Width = 78;
            // 
            // colWorkHourTer
            // 
            this.colWorkHourTer.FieldName = "WorkHourTer";
            this.colWorkHourTer.Name = "colWorkHourTer";
            this.colWorkHourTer.RowIndex = 1;
            this.colWorkHourTer.Width = 78;
            // 
            // colStopHourTer
            // 
            this.colStopHourTer.FieldName = "StopHourTer";
            this.colStopHourTer.Name = "colStopHourTer";
            this.colStopHourTer.RowIndex = 1;
            this.colStopHourTer.Width = 78;
            // 
            // colWorkHourQua
            // 
            this.colWorkHourQua.FieldName = "WorkHourQua";
            this.colWorkHourQua.Name = "colWorkHourQua";
            this.colWorkHourQua.RowIndex = 1;
            this.colWorkHourQua.Width = 78;
            // 
            // colStopHourQua
            // 
            this.colStopHourQua.FieldName = "StopHourQua";
            this.colStopHourQua.Name = "colStopHourQua";
            this.colStopHourQua.RowIndex = 1;
            this.colStopHourQua.Width = 78;
            // 
            // colStopHourQui
            // 
            this.colStopHourQui.FieldName = "StopHourQui";
            this.colStopHourQui.Name = "colStopHourQui";
            this.colStopHourQui.RowIndex = 1;
            this.colStopHourQui.Width = 78;
            // 
            // colWorkHourSex
            // 
            this.colWorkHourSex.FieldName = "WorkHourSex";
            this.colWorkHourSex.Name = "colWorkHourSex";
            this.colWorkHourSex.RowIndex = 1;
            this.colWorkHourSex.Width = 78;
            // 
            // colStopHourSex
            // 
            this.colStopHourSex.FieldName = "StopHourSex";
            this.colStopHourSex.Name = "colStopHourSex";
            this.colStopHourSex.RowIndex = 1;
            this.colStopHourSex.Width = 78;
            // 
            // colWorkHourSab
            // 
            this.colWorkHourSab.FieldName = "WorkHourSab";
            this.colWorkHourSab.Name = "colWorkHourSab";
            this.colWorkHourSab.RowIndex = 1;
            this.colWorkHourSab.Width = 71;
            // 
            // colNumSemana
            // 
            this.colNumSemana.FieldName = "NumSemana";
            this.colNumSemana.Name = "colNumSemana";
            this.colNumSemana.RowIndex = 1;
            this.colNumSemana.Width = 78;
            // 
            // colMesAno
            // 
            this.colMesAno.FieldName = "MesAno";
            this.colMesAno.Name = "colMesAno";
            this.colMesAno.RowIndex = 1;
            this.colMesAno.Width = 78;
            // 
            // colWorkHours
            // 
            this.colWorkHours.FieldName = "WorkHours";
            this.colWorkHours.Name = "colWorkHours";
            this.colWorkHours.RowIndex = 1;
            this.colWorkHours.Width = 78;
            // 
            // colStopHours
            // 
            this.colStopHours.FieldName = "StopHours";
            this.colStopHours.Name = "colStopHours";
            this.colStopHours.RowIndex = 1;
            this.colStopHours.Width = 176;
            // 
            // colWorkHourQui
            // 
            this.colWorkHourQui.FieldName = "WorkHourQui";
            this.colWorkHourQui.Name = "colWorkHourQui";
            this.colWorkHourQui.RowIndex = 2;
            this.colWorkHourQui.Width = 78;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Seg";
            this.gridBand2.Columns.Add(this.colSegunda);
            this.gridBand2.Columns.Add(this.colDescWorkHourSeg);
            this.gridBand2.Columns.Add(this.colDescStopHourSeg);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 100;
            // 
            // colSegunda
            // 
            this.colSegunda.Caption = "Segunda";
            this.colSegunda.FieldName = "Segunda";
            this.colSegunda.Name = "colSegunda";
            this.colSegunda.Visible = true;
            this.colSegunda.Width = 100;
            // 
            // colDescWorkHourSeg
            // 
            this.colDescWorkHourSeg.Caption = "Trab Seg";
            this.colDescWorkHourSeg.FieldName = "DescWorkHourSeg";
            this.colDescWorkHourSeg.Name = "colDescWorkHourSeg";
            this.colDescWorkHourSeg.RowIndex = 1;
            this.colDescWorkHourSeg.Visible = true;
            this.colDescWorkHourSeg.Width = 50;
            // 
            // colDescStopHourSeg
            // 
            this.colDescStopHourSeg.Caption = "Par Seg";
            this.colDescStopHourSeg.FieldName = "DescStopHourSeg";
            this.colDescStopHourSeg.Name = "colDescStopHourSeg";
            this.colDescStopHourSeg.RowIndex = 1;
            this.colDescStopHourSeg.Visible = true;
            this.colDescStopHourSeg.Width = 50;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Ter";
            this.gridBand3.Columns.Add(this.colTerca);
            this.gridBand3.Columns.Add(this.colDescWorkHourTer);
            this.gridBand3.Columns.Add(this.colDescStopHourTer);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 100;
            // 
            // colTerca
            // 
            this.colTerca.Caption = "Terça";
            this.colTerca.FieldName = "Terca";
            this.colTerca.Name = "colTerca";
            this.colTerca.Visible = true;
            this.colTerca.Width = 100;
            // 
            // colDescWorkHourTer
            // 
            this.colDescWorkHourTer.Caption = "Trab Ter";
            this.colDescWorkHourTer.FieldName = "DescWorkHourTer";
            this.colDescWorkHourTer.Name = "colDescWorkHourTer";
            this.colDescWorkHourTer.RowIndex = 1;
            this.colDescWorkHourTer.Visible = true;
            this.colDescWorkHourTer.Width = 50;
            // 
            // colDescStopHourTer
            // 
            this.colDescStopHourTer.Caption = "Par Ter";
            this.colDescStopHourTer.FieldName = "DescStopHourTer";
            this.colDescStopHourTer.Name = "colDescStopHourTer";
            this.colDescStopHourTer.RowIndex = 1;
            this.colDescStopHourTer.Visible = true;
            this.colDescStopHourTer.Width = 50;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Qua";
            this.gridBand4.Columns.Add(this.colQuarta);
            this.gridBand4.Columns.Add(this.colDescWorkHourQua);
            this.gridBand4.Columns.Add(this.colDescStopHourQua);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 100;
            // 
            // colQuarta
            // 
            this.colQuarta.FieldName = "Quarta";
            this.colQuarta.Name = "colQuarta";
            this.colQuarta.Visible = true;
            this.colQuarta.Width = 100;
            // 
            // colDescWorkHourQua
            // 
            this.colDescWorkHourQua.Caption = "Trab Qua";
            this.colDescWorkHourQua.FieldName = "DescWorkHourQua";
            this.colDescWorkHourQua.Name = "colDescWorkHourQua";
            this.colDescWorkHourQua.RowIndex = 1;
            this.colDescWorkHourQua.Visible = true;
            this.colDescWorkHourQua.Width = 50;
            // 
            // colDescStopHourQua
            // 
            this.colDescStopHourQua.Caption = "Par Qua";
            this.colDescStopHourQua.FieldName = "DescStopHourQua";
            this.colDescStopHourQua.Name = "colDescStopHourQua";
            this.colDescStopHourQua.RowIndex = 1;
            this.colDescStopHourQua.Visible = true;
            this.colDescStopHourQua.Width = 50;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "Qui";
            this.gridBand5.Columns.Add(this.colQuinta);
            this.gridBand5.Columns.Add(this.colDescWorkHourQui);
            this.gridBand5.Columns.Add(this.colDescStopHourQui);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 4;
            this.gridBand5.Width = 100;
            // 
            // colQuinta
            // 
            this.colQuinta.FieldName = "Quinta";
            this.colQuinta.Name = "colQuinta";
            this.colQuinta.Visible = true;
            this.colQuinta.Width = 100;
            // 
            // colDescWorkHourQui
            // 
            this.colDescWorkHourQui.Caption = "Trab Qui";
            this.colDescWorkHourQui.FieldName = "DescWorkHourQui";
            this.colDescWorkHourQui.Name = "colDescWorkHourQui";
            this.colDescWorkHourQui.RowIndex = 1;
            this.colDescWorkHourQui.Visible = true;
            this.colDescWorkHourQui.Width = 50;
            // 
            // colDescStopHourQui
            // 
            this.colDescStopHourQui.Caption = "Par Qui";
            this.colDescStopHourQui.FieldName = "DescStopHourQui";
            this.colDescStopHourQui.Name = "colDescStopHourQui";
            this.colDescStopHourQui.RowIndex = 1;
            this.colDescStopHourQui.Visible = true;
            this.colDescStopHourQui.Width = 50;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Sex";
            this.gridBand6.Columns.Add(this.colSexta);
            this.gridBand6.Columns.Add(this.colDescWorkHourSex);
            this.gridBand6.Columns.Add(this.colDescStopHourSex);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 100;
            // 
            // colSexta
            // 
            this.colSexta.FieldName = "Sexta";
            this.colSexta.Name = "colSexta";
            this.colSexta.Visible = true;
            this.colSexta.Width = 100;
            // 
            // colDescWorkHourSex
            // 
            this.colDescWorkHourSex.Caption = "Trab Sex";
            this.colDescWorkHourSex.FieldName = "DescWorkHourSex";
            this.colDescWorkHourSex.Name = "colDescWorkHourSex";
            this.colDescWorkHourSex.RowIndex = 1;
            this.colDescWorkHourSex.Visible = true;
            this.colDescWorkHourSex.Width = 50;
            // 
            // colDescStopHourSex
            // 
            this.colDescStopHourSex.Caption = "Par Sex";
            this.colDescStopHourSex.FieldName = "DescStopHourSex";
            this.colDescStopHourSex.Name = "colDescStopHourSex";
            this.colDescStopHourSex.RowIndex = 1;
            this.colDescStopHourSex.Visible = true;
            this.colDescStopHourSex.Width = 50;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Sab";
            this.gridBand7.Columns.Add(this.colSabado);
            this.gridBand7.Columns.Add(this.colDescStopHourSab);
            this.gridBand7.Columns.Add(this.colStopHourSab);
            this.gridBand7.Columns.Add(this.colDescWorkHourSab);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 100;
            // 
            // colSabado
            // 
            this.colSabado.FieldName = "Sabado";
            this.colSabado.Name = "colSabado";
            this.colSabado.Visible = true;
            this.colSabado.Width = 100;
            // 
            // colDescStopHourSab
            // 
            this.colDescStopHourSab.Caption = "Par Sex";
            this.colDescStopHourSab.FieldName = "DescStopHourSab";
            this.colDescStopHourSab.Name = "colDescStopHourSab";
            this.colDescStopHourSab.RowIndex = 1;
            this.colDescStopHourSab.Visible = true;
            this.colDescStopHourSab.Width = 50;
            // 
            // colStopHourSab
            // 
            this.colStopHourSab.FieldName = "StopHourSab";
            this.colStopHourSab.Name = "colStopHourSab";
            this.colStopHourSab.RowIndex = 1;
            this.colStopHourSab.Width = 71;
            // 
            // colDescWorkHourSab
            // 
            this.colDescWorkHourSab.Caption = "Trab Sex";
            this.colDescWorkHourSab.FieldName = "DescWorkHourSab";
            this.colDescWorkHourSab.Name = "colDescWorkHourSab";
            this.colDescWorkHourSab.RowIndex = 1;
            this.colDescWorkHourSab.Visible = true;
            this.colDescWorkHourSab.Width = 50;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Totais";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Visible = false;
            this.gridBand8.VisibleIndex = -1;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Visible = false;
            this.gridBand9.VisibleIndex = -1;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "gridBand10";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.Visible = false;
            this.gridBand10.VisibleIndex = -1;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "gridBand11";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Visible = false;
            this.gridBand11.VisibleIndex = -1;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "gridBand12";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.Visible = false;
            this.gridBand12.VisibleIndex = -1;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Visible = false;
            this.gridBand13.VisibleIndex = -1;
            // 
            // frmVsResourcesHour
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 404);
            this.Controls.Add(this.gcSchedule);
            this.Controls.Add(this.scMovimenta);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("frmVsResourcesHour.IconOptions.Icon")));
            this.Name = "frmVsResourcesHour";
            this.Text = "(EN) Maquina tempo turno x dia";
            this.Load += new System.EventHandler(this.frmVsResourcesHour_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.scMovimenta, 0);
            this.Controls.SetChildIndex(this.gcSchedule, 0);
            this.Controls.SetChildIndex(this.gcGrid, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enMachineHoursScheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adgvSchedule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitterControl scMovimenta;
        private DevExpress.XtraGrid.GridControl gcSchedule;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView adgvSchedule;
        private System.Windows.Forms.BindingSource enMachineHoursScheduleBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDomingo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMachine;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourDom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourDom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourDom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourDom;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourSeg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourSeg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourSeg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourSeg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourTer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourTer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourTer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourTer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourQua;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourQua;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourQua;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourQua;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourQui;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourQui;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourQui;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourQui;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourSex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourSex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHourSab;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumSemana;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMesAno;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkHours;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHours;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSegunda;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTerca;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQuarta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQuinta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSexta;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourSex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourSex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSabado;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescStopHourSab;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStopHourSab;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDescWorkHourSab;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
    }
}