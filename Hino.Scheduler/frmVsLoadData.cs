﻿using Hino.Scheduler.Application.ViewModel;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler
{
    public partial class frmVsLoadData : frmVsPadrao
    {
        private readonly LoadDataVM _LoadDataVM;
        public frmVsLoadData()
        {
            InitializeComponent();
            _LoadDataVM = new LoadDataVM();
            gvDemands.DefinePadrao();
            gvJobs.DefinePadrao();
            gvResource.DefinePadrao();
        }

        private async void frmVsLoadData_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            gvDemands.MoveFirst();
            gvJobs.MoveFirst();
            gvResource.MoveFirst();
            _Initialize = false;
        }

        protected async override void BotaoNovo()
        {
            using (var frm = new frmCadLoadData())
            {
                frm.ShowDialog();
                await BotaoAtualizar();
            }
        }

        protected async override void BotaoExcluir()
        {
            var resp = MessageBox.Show("Confirma a exclusão ?", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (resp == DialogResult.Yes)
            {
                if (HaRegistroSelecionado())
                {
                    gvVisao.ShowLoadingPanel();
                    gvDemands.ShowLoadingPanel();
                    gvJobs.ShowLoadingPanel();
                    gvResource.ShowLoadingPanel();

                    if (!await _LoadDataVM.DeleteScenario(
                        Convert.ToInt64(gvVisao.GetFocusedRowCellValue("IdLoadData"))))
                        MessageBox.Show(_LoadDataVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                    await BotaoAtualizar();

                    gvVisao.HideLoadingPanel();
                    gvDemands.HideLoadingPanel();
                    gvJobs.HideLoadingPanel();
                    gvResource.HideLoadingPanel();
                }
            }
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            await _LoadDataVM.LoadDataSource();

            gcGrid.DataSource = _LoadDataVM._LoadDataDataSource;
            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                fmtGridView.OcultaColuna(
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["minsdisponiveis"],
                    gvVisao.Columns["minhoraprod"]
                );

                gvVisao.Columns["dtproducao"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codmaquina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            await LoadDetails();
            gvVisao.HideLoadingPanel();
        }
        #endregion

        #region Load Details
        private async Task LoadDetails()
        {
            gvDemands.ShowLoadingPanel();
            gvJobs.ShowLoadingPanel();
            gvResource.ShowLoadingPanel();

            long idLoadData = -1;
            if (gvVisao.FocusedRowHandle > -1)
                idLoadData = Convert.ToInt64(gvVisao.GetFocusedRowCellValue("IdLoadData"));

            await _LoadDataVM.LoadDetailDataSource(idLoadData);

            gcDemands.DataSource = _LoadDataVM._DemandDataSource;
            gcJobs.DataSource = _LoadDataVM._JobRotDataSource;
            gcResource.DataSource = _LoadDataVM._ResourceDataSource;


            if (_Initialize)
            {
                gvDemands.BestFitColumns();
                gvJobs.BestFitColumns();
                gvResource.BestFitColumns();
            }

            gvDemands.HideLoadingPanel();
            gvJobs.HideLoadingPanel();
            gvResource.HideLoadingPanel();
        }
        #endregion

        private async void gvVisao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) =>
            await LoadDetails();

        ~frmVsLoadData()
        {
            _LoadDataVM.Dispose();
        }
    }
}

