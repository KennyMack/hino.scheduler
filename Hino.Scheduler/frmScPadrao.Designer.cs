﻿namespace Hino.Scheduler
{
    partial class frmScPadrao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScPadrao));
            this.ssManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Hino.Scheduler.WaitForm1), true, true);
            this.SuspendLayout();
            // 
            // ssManager
            // 
            this.ssManager.ClosingDelay = 500;
            // 
            // frmScPadrao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 453);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScPadrao";
            this.Text = "Schedule Padrão";
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraSplashScreen.SplashScreenManager ssManager;
    }
}