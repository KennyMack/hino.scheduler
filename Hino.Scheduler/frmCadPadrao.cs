﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Hino.Scheduler
{
    public partial class frmCadPadrao : DevExpress.XtraEditors.XtraForm
    {
        public frmCadPadrao()
        {
            InitializeComponent();
        }

        private void bbiSalvar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            OnSaveClicked();

        protected virtual void OnSaveClicked() =>
            this.DialogResult = DialogResult.OK;

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            OnCancelClicked();

        protected virtual void OnCancelClicked() =>
            this.DialogResult = DialogResult.Cancel;
    }
}