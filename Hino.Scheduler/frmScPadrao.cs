﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Utils;

namespace Hino.Scheduler
{
    public partial class frmScPadrao : DevExpress.XtraEditors.XtraForm
    {
        protected bool _Initialize;
        protected event EventHandler OnRefreshScenario;

        #region Get Form
        protected Form GetForm(Type pFormType, string pForm)
        {
            if (System.Windows.Forms.Application.OpenForms[pForm] != null)
                return System.Windows.Forms.Application.OpenForms[pForm];

            return (Form)Activator.CreateInstance(pFormType);
        }
        #endregion

        public void RefreshScenario()
        {
            OnRefreshScenario?.Invoke(this, new EventArgs());
        }

        public frmScPadrao()
        {
            _Initialize = true;
            InitializeComponent();
        }
    }
}