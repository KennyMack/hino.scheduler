﻿using Hino.Scheduler.Application.Services.Auth;
using Hino.Scheduler.Application.ViewModel.Gerais;
using Hino.Scheduler.Data.Repositories.Auth;
using Hino.Scheduler.Utils;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Hino.Scheduler
{
    public partial class frmLogin : Form
    {
        #region Propriedades
        private DataSet _DtConexoes;
        private DataTable _DtConexao
        {
            get
            {
                try
                {
                    return _DtConexoes.Tables[0];
                }
                catch (Exception)
                {

                }
                return null;
            }
        }
        #endregion

        #region Construtor
        public frmLogin()
        {
            InitializeComponent();
            _DtConexoes = new DataSet();
            CarregarDados();
        }
        #endregion

        #region Carregar Dados
        private void CarregarDados()
        {
            // Buscando a versão
            lblVersao.Text = "Versão: " + System.Windows.Forms.Application.ProductVersion;

            // Lendo as configurações do Hino.Conexoes.XML
            cbxAlias.Properties.Items.Clear();
            try
            {
                _DtConexoes.ReadXml("Hino.Conexoes.xml");
                
                _DtConexao.Select("1 = 1").ToList()
                    .ForEach(r => cbxAlias.Properties.Items.Add(r["Alias"]));
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Arquivo 'Hino.Conexoes.xml' não localizado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Não foi possível carregar os dados do banco de dados. Motivo: {e.Message}", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                MessageBox.Show(e.InnerException.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                MessageBox.Show(e.StackTrace, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            if (cbxAlias.Properties.Items.Count > 0)
            {
                cbxAlias.Properties.Sorted = true;
                cbxAlias.SelectedIndex = 0;
            }

            this.PrUsuAut();
            Parameters.UserLogged = "***LOGIN_INVALIDO***";
        }
        #endregion

        #region Ao carregar o formulário
        private void frmLogin_Load(object sender, EventArgs e)
        {
            
        }
        #endregion

        #region Realizando o login
        public async void btnEntrar_Click(object sender, EventArgs e)
        {
            // Validando informações digitadas
            bool validado = true;
            if (txtUsuario.Text == "")
            {
                MessageBox.Show("Informe o usuário.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                txtUsuario.Focus();
                validado = false;
            }
            else if (txtSenha.Text == "")
            {
                MessageBox.Show("Informe a senha.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                txtSenha.Focus();
                validado = false;
            }
            else if (cbxAlias.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione uma conexão.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                cbxAlias.Focus();
                validado = false;
            }

            // Lendo as configurações do alias escolhido
            if (validado)
            {
                Criptografia cripto = new Criptografia();
                try
                {
                    var conexao = _DtConexao.Select(string.Format("Alias = '{0}'", cbxAlias.SelectedItem.ToString()))
                        .FirstOrDefault();
                    Parameters.dbUser = cripto.Decrypt(conexao["UserName"].ToString());
                    Parameters.alias = cbxAlias.SelectedItem.ToString();

                    // Validando usuário e senha
                    validado = await ValidateUserPass();

                    if (validado && sender != null)
                    {
                        
                        Close();
                    }

                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Arquivo 'Hino.Conexoes.xml' não localizado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return;
                }
                catch (Exception ex)
                {
                    if (ex.Source == "BANCO")
                        MessageBox.Show("Problema ao estabelecer conexão com o servidor de banco de dados. Motivo: " + ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                    else
                        MessageBox.Show("Não foi possível carregar os dados do banco de dados.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
        }
        #endregion

        #region Ao pressionar a tecla enter no formulário
        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            // A propriedade KeyPreview do form deve estar com o valor "True"
            if (e.KeyValue == 13)
                this.btnEntrar_Click(sender, e);
        }
        #endregion

        #region Validando usuário e senha
        private async Task<bool> ValidateUserPass()
        {
            try
            {
                using (var repo = new UsersRepository())
                using (var login = new LoginService(repo))
                using (var userVM = new UserDataVM())
                {
                    var UserLogged = await login.ValidateUserPass(txtUsuario.Text, txtSenha.Text);
                    if (UserLogged == null)
                        throw new Exception(login.Errors[0]);

                    Parameters.UserLogged = UserLogged.codUsuario;

                    await userVM.LoadData();

                    Parameters.IdScenario = 0;
                    if (userVM._geUserData.IdLoadData > 0)
                        Parameters.IdScenario = (long)userVM._geUserData.IdLoadData;
                }
                return true;
            }
            catch (Exception e)
            {
                Parameters.UserLogged = "***LOGIN_INVALIDO***";
                MessageBox.Show(e.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return false;
            }
        }
        #endregion        

        #region Botao sair
        private void btnSair_Click(object sender, EventArgs e)
        {
            Parameters.UserLogged = "***LOGIN_INVALIDO***";
            Close();
        }
        #endregion

        #region Preencher usuário automático
        private void PrUsuAut()
        {
            try
            {
                System.Net.NetworkInformation.NetworkInterface[] adapters = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                foreach (System.Net.NetworkInformation.NetworkInterface adapter in adapters)
                {
                    if ((adapter.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up) && (adapter.GetPhysicalAddress().ToString() != ""))
                    {
                        switch (adapter.GetPhysicalAddress().ToString().ToUpper())
                        {
                            case "5CC9D35DB7AF":
                            case "1C3947535B4B":
                            case "4CCC6AF95FB5":
                                txtUsuario.Text = "HINO.JONATHAN";
                                txtSenha.Text = "22176";
                                break;
                            case "5CC9D314D719":
                            case "DC0EA1C7DE82":
                                txtUsuario.Text = "HINO.CAIO";
                                break;
                            case "5CC9D35EDC09":
                            case "1C3947538C6A":
                                txtUsuario.Text = "HINO.CESAR";
                                break;
                            case "142D277D83E7":
                            case "A02BB851B2DA":
                                txtUsuario.Text = "HINO.JULIO";
                                break;
                            default:
                                break;
                        }

                        if (txtUsuario.Text != "")
                            break;
                    }
                }
            }
            catch
            {
                //
            }
        }
        #endregion

        #region Ao exibir o formulário
        private void frmLogin_Shown(object sender, EventArgs e)
        {
            if (txtUsuario.Text != "")
                txtSenha.Focus();
        }
        #endregion
    }
}
