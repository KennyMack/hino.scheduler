﻿using Hino.Scheduler.Application.ViewModel;
using Hino.Scheduler.Application.ViewModel.Gerais;
using Hino.Scheduler.Engenharia;
using Hino.Scheduler.Production;
using Hino.Scheduler.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        public frmMain()
        {
            InitializeComponent();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
        }

        private async void frmMain_Load(object sender, EventArgs e)
        {
            bsiUsuario.Caption += " " + Parameters.UserLogged;
            bsiVersao.Caption += System.Windows.Forms.Application.ProductVersion;
            
            beiScenario.EditValue = Parameters.IdScenario;
            await LoadScenario();
        }

        #region Get Form
        private Form GetForm(Type pFormType, string pForm)
        {
            if (System.Windows.Forms.Application.OpenForms[pForm] != null)
                return System.Windows.Forms.Application.OpenForms[pForm];

            return (Form)Activator.CreateInstance(pFormType);
        }
        #endregion

        #region ShowFormMdi
        private void ShowFormMdi(Form pForm)
        {
            pForm.MdiParent = this;
            pForm.Show();
            pForm.BringToFront();
        }
        #endregion

        #region Scenario

        #region Load Scenario
        private async Task LoadScenario()
        {
            using (var _LoadDataVM = new LoadDataVM())
            {
                long value = 0;
                if (!long.TryParse((beiScenario.EditValue ?? "").ToString(), out value))
                    value = 0;

                ricbScenario.Items.Clear();
                await _LoadDataVM.LoadDataSource();

                foreach (var item in _LoadDataVM._LoadDataDataSource.OrderBy(r => r.IdLoadData))
                    ricbScenario.Items.Add(item.IdLoadData);

                if (_LoadDataVM._LoadDataDataSource.Any(r => r.IdLoadData == value))
                    beiScenario.EditValue = value;
                else if (_LoadDataVM._LoadDataDataSource.Any())
                {
                    value = _LoadDataVM._LoadDataDataSource.Min(r => r.IdLoadData);
                    beiScenario.EditValue = value;
                }
                else
                    beiScenario.EditValue = null;
            }
        }
        #endregion

        #region Showing Scenario options
        private async void beiScenario_ShowingEditor(object sender, DevExpress.XtraBars.ItemCancelEventArgs e) =>
            await LoadScenario();
        #endregion

        #region Changed Scenario value
        private async void beiScenario_EditValueChanged(object sender, EventArgs e)
        {
            long value = 0;
            if (!long.TryParse((beiScenario.EditValue ?? "").ToString(), out value))
                value = 0;

            Parameters.IdScenario = value;

            using (var _UserDataVM = new UserDataVM())
            {
                await _UserDataVM.LoadData();

                if (value > 0)
                    _UserDataVM._geUserData.IdLoadData = value;
                else
                    _UserDataVM._geUserData.IdLoadData = null;

                await _UserDataVM.UpdateUserData();
            }

            foreach (var item in System.Windows.Forms.Application.OpenForms)
            {
                if (item is frmVsPadrao)
                    await (item as frmVsPadrao).RefreshScenario();

                if (item is frmScPadrao)
                    (item as frmScPadrao).RefreshScenario();
            }
        }
        #endregion

        #endregion

        private void bbiProgramacaoOP_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi(
                (frmScScheduleOrdersSimulate)GetForm(typeof(frmScScheduleOrdersSimulate), "frmScScheduleOrdersSimulate"));

        private void bbiSchedulerOP_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowFormMdi(
                (frmScScheduledOrders)GetForm(typeof(frmScScheduledOrders), "frmScScheduledOrders"));

            var frmVS = System.Windows.Forms.Application.OpenForms["frmVsScheduleOrders"];
            if (frmVS != null)
                frmVS.Close();
        }

        private void bbiCargaMaquina_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsMachineLoad)GetForm(typeof(frmVsMachineLoad), "frmVsMachineLoad"));

        private void bbiMachineLoadCell_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) => 
            ShowFormMdi((frmVsMachineLoadTime)GetForm(typeof(frmVsMachineLoadTime), "frmVsMachineLoadTime"));

        private void bbiMachineLoadSimulate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsMachineLoadSimulated)GetForm(typeof(frmVsMachineLoadSimulated), "frmVsMachineLoadSimulated"));

        private void bbiProductionSimulated_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmScProductionSimulated)GetForm(typeof(frmScProductionSimulated), "frmScProductionSimulated"));

        private void bbiOrderBottleneck_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsOrderBottleneck)GetForm(typeof(frmVsOrderBottleneck), "frmVsOrderBottleneck"));

        private void bbiJobShop_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsJobShop)GetForm(typeof(frmVsJobShop), "frmVsJobShop"));

        private void bbiRoutinesSequence_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmScScheduledOrdersRotinas)GetForm(typeof(frmScScheduledOrdersRotinas), "frmScScheduledOrdersRotinas"));

        private void bbiLoadData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsLoadData)GetForm(typeof(frmVsLoadData), "frmVsLoadData"));

        private void bbiDemands_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsDemands)GetForm(typeof(frmVsDemands), "frmVsDemands"));

        private void bbiMachineDisp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsResourcesDisp)GetForm(typeof(frmVsResourcesDisp), "frmVsResourcesDisp"));

        private void bbiScheduleOPMachine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmScScheduledOrdersRot)GetForm(typeof(frmScScheduledOrdersRot), "frmScScheduledOrdersRot"));

        private void bbiResourceHours_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsResourcesHour)GetForm(typeof(frmVsResourcesHour), "frmVsResourcesHour"));

        private void bbiResources_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsResourcesList)GetForm(typeof(frmVsResourcesList), "frmVsResourcesList"));

        private void bbiSalesDemand_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            ShowFormMdi((frmVsSalesProgram)GetForm(typeof(frmVsSalesProgram), "frmVsSalesProgram"));
    }
}
