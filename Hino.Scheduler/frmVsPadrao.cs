﻿using DevExpress.Export;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting;
using Hino.Scheduler.Application.ViewModel;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler
{
    public partial class frmVsPadrao : XtraForm
    {
        #region Propriedades
        protected string _formName;
        protected string _formText;
        protected bool _Initialize;
        #endregion

        #region Set Valor Grid Filtro
        protected void SetValorFiltro<T>(DevExpress.XtraVerticalGrid.VGridControl pGrid,
            DevExpress.XtraVerticalGrid.Rows.BaseRow pRow, T pValor)
        {
            try
            {
                pGrid.SetCellValue(pRow, 0, pValor);
                pGrid.PostEditor();
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Get Valor Grid Filtro
        protected T GetValorFiltro<T>(DevExpress.XtraVerticalGrid.VGridControl pGrid,
            DevExpress.XtraVerticalGrid.Rows.BaseRow pRow)
        {
            try
            {
                pGrid.PostEditor();
                return (T)pGrid.GetCellValue(pRow, 0);

            }
            catch (Exception)
            {
                return default(T);
            }

        }
        #endregion

        #region Construtor
        public frmVsPadrao()
        {
            _Initialize = true;
            InitializeComponent();
            _formName = this.Name;
            _formText = this.Text;
            bsiItemScenario.Caption = $"Cenário: {Parameters.IdScenario}";
        }
        #endregion

        #region Tratar carregar formulário
        protected async virtual Task TratarCarregarFormulario()
        {
            await BotaoAtualizar();
            bsiAbrirDados.Visibility = bbiExportar.Visibility;
        }
        #endregion

        #region Botao novo
        private void bbiNovo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoNovo();
            gvVisao.CloseEditor();
            gvVisao.UpdateCurrentRow();
        }

        protected virtual void BotaoNovo()
        {
            // Botão novo
        }
        #endregion

        #region Botão editar
        private void bbiEditar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int focada = gvVisao.FocusedRowHandle;
            bool expand = gvVisao.GetMasterRowExpanded(focada);

            BotaoEditar();
            gvVisao.FocusedRowHandle = focada;
            if (expand)
                gvVisao.SetMasterRowExpanded(focada, !gvVisao.GetMasterRowExpanded(focada));

            gvVisao.CloseEditor();
            gvVisao.UpdateCurrentRow();
        }

        protected virtual void BotaoEditar()
        {
            // Botão editar
        }
        #endregion

        #region Botão excluir
        private void bbiExcluir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoExcluir();
        }

        protected virtual void BotaoExcluir()
        {
            // Botão excluir
        }
        #endregion

        #region Botão atualizar
        private void bbiAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int focada = gvVisao.FocusedRowHandle;
            bool expand = gvVisao.GetMasterRowExpanded(focada);

            BotaoAtualizar();

            gvVisao.FocusedRowHandle = focada;
            if (expand)
                gvVisao.SetMasterRowExpanded(focada, !gvVisao.GetMasterRowExpanded(focada));
        }

        protected virtual Task BotaoAtualizar()
        {
            return Task.FromResult<object>(null);
            // Botão atualizar
        }

        public Task RefreshScenario()
        {
            bsiItemScenario.Caption = $"Cenário: {Parameters.IdScenario}";
            return BotaoAtualizar();
        }
        #endregion

        #region Botão imprimir
        private void bbiImprimir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoImprimir();
        }

        protected virtual void BotaoImprimir()
        {
            pclPrintLink.CreateDocument();
            pclPrintLink.ShowPreview();
        }

        private void pclPrintLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            string reportHeader = _formText;
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
            e.Graph.Font = new Font("Tahoma", 14, FontStyle.Bold);
            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50);
            e.Graph.DrawString(reportHeader, Color.Black, rec, BorderSide.None);
        }
        #endregion

        #region Botão exportar
        private void bbiExportar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoExportar();
        }

        protected virtual void BotaoExportar()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "XLSX|*.xlsx|XLS|*.xls|TXT|*.txt|RTF|*.rtf|PDF|*.pdf|HTML|*.html";
            sfd.Title = "Exportar o grid";
            sfd.ShowDialog();

            if (sfd.FileName != "")
            {
                if (gvVisao.GridControl.ViewCollection.Count > 1)
                {
                    var resp = MessageBox.Show("Exportar detalhes?", "Exportar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (resp == System.Windows.Forms.DialogResult.Yes)
                        gvVisao.OptionsPrint.PrintDetails = true;
                    else
                        gvVisao.OptionsPrint.PrintDetails = false;
                }

                switch (sfd.FileName.Substring(sfd.FileName.LastIndexOf(".")))
                {
                    case ".html":
                        gvVisao.ExportToHtml(sfd.FileName);
                        break;
                    case ".pdf":
                        gvVisao.ExportToPdf(sfd.FileName);
                        break;
                    case ".rtf":
                        gvVisao.ExportToRtf(sfd.FileName);
                        break;
                    case ".txt":
                        gvVisao.ExportToText(sfd.FileName);
                        break;
                    case ".xls":
                        {
                            XlsExportOptionsEx xlsxOptionsExp = new XlsExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            gvVisao.ExportToXls(sfd.FileName, xlsxOptionsExp);
                        }
                        break;
                    case ".xlsx":
                        {
                            XlsxExportOptionsEx xlsxOptionsExp = new XlsxExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            gvVisao.ExportToXlsx(sfd.FileName, xlsxOptionsExp);
                        }
                        break;
                }
            }
            sfd.Dispose();
        }
        #endregion

        #region Botoes selecionar layout
        private void bckiLayoutPadSistema_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoSelecionarLayout(e.Item.Tag.ToString());
        }

        protected virtual void BotaoSelecionarLayout(string tag)
        {
            if (tag == "1")
            {
                RestauraLayout("global");
                bckiLayoutPadGlobal.Checked = true;
            }
            else if (tag == "2")
            {
                RestauraLayout(Parameters.codEstab.ToString());
                bckiLayoutPadEstab.Checked = true;
            }
            else if (tag == "3")
            {
                RestauraLayout(Parameters.UserLogged);
                bckiLayoutPadUsuario.Checked = true;
            }
            else
            {
                RestauraLayout("sistema");
                bckiLayoutPadSistema.Checked = true;
            }
        }
        #endregion

        #region Retorna todos os grids
        public IEnumerable<Control> GetAllGrids(Control control)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllGrids(ctrl))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == typeof(DevExpress.XtraGrid.GridControl));
        }
        #endregion

        #region Botão definir layout
        private void bbiLayoutDefinirGlobal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoDefinirLayout_ItemClick(e.Item.Caption);
        }

        protected virtual void BotaoDefinirLayout_ItemClick(string pTag)
        {
            String tagBtn = "1";
            if (pTag == "Como Padrão do Estabelecimento")
                tagBtn = "2";
            else if (pTag == "Como Padrão do Usuário")
                tagBtn = "3";
            BotaoDefinirLayout(tagBtn);

        }

        protected virtual void BotaoDefinirLayout(string tag)
        {
            if (tag == "1")
            {
                SalvarLayout("global");
                bckiLayoutPadGlobal.Checked = true;
            }
            else if (tag == "2")
            {
                SalvarLayout(Parameters.codEstab.ToString());
                bckiLayoutPadEstab.Checked = true;
            }
            else if (tag == "3")
            {
                SalvarLayout(Parameters.UserLogged);
                bckiLayoutPadUsuario.Checked = true;
            }
            HabDesOpcoesLayouts();
        }
        #endregion        

        #region Botão excluir layout
        private void bbiLayoutExcluirGlobal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String tagBtn = "1";
            if (e.Item.Caption == "Padrão do Estabelecimento")
                tagBtn = "2";
            else if (e.Item.Caption == "Padrão do Usuário")
                tagBtn = "3";
            BotaoExcluirLayout(tagBtn);
        }

        protected virtual void BotaoExcluirLayout(string tag)
        {
            if (tag == "1")
            {
                ExcluirLayout("global");
                if (bckiLayoutPadGlobal.Checked)
                {
                    bckiLayoutPadSistema.Checked = true;
                    RestauraLayout("sistema");
                }
            }
            else if (tag == "2")
            {
                ExcluirLayout(Parameters.codEstab.ToString());
                if (bckiLayoutPadEstab.Checked)
                {
                    bckiLayoutPadSistema.Checked = true;
                    RestauraLayout("sistema");
                }
            }
            else if (tag == "3")
            {
                ExcluirLayout(Parameters.UserLogged);
                if (bckiLayoutPadUsuario.Checked)
                {
                    bckiLayoutPadSistema.Checked = true;
                    RestauraLayout("sistema");
                }
            }
            HabDesOpcoesLayouts();
        }
        #endregion

        #region Restaurando o layout
        protected virtual void RestauraLayout(string layout)
        {
            if (layout == "sistema")
                RestauraLayoutSistema();
            else
                RestauraLayoutXML(layout);
        }

        protected virtual void RestauraLayoutSistema()
        {
            // Restaura o layout do sistema
        }

        protected virtual void RestauraLayoutXML(string layout)
        {
            var nome = _formName + "_" + layout + ".xml";

            /*if (nomeArquivo.ExisteArquivo(nome))
                gvVisao.RestoreLayoutFromXml(nome);
            else
            {
                var grids = GetAllGrids(this);
                if (grids.Count() > 1)
                {
                    foreach (DevExpress.XtraGrid.GridControl item in grids)
                    {
                        foreach (DevExpress.XtraGrid.Views.Grid.GridView view in item.Views)
                        {
                            if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + "_" + view.Name + ".xml"))
                                view.RestoreLayoutFromXml(_formName + "_" + layout + "_" + view.Name + ".xml");
                        }


                    }
                }
                else
                {
                    if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + "_" + gvVisao.Name + ".xml"))
                        gvVisao.RestoreLayoutFromXml(_formName + "_" + layout + "_" + gvVisao.Name + ".xml");
                }
            }*/
        }
        #endregion

        #region Habilitar ou desabilitar as opções de layouts existentes
        protected virtual void HabDesOpcoesLayouts()
        {
           /* bckiLayoutPadGlobal.Enabled = (System.IO.File.Exists(_formName + "_global_" + gvVisao.Name + ".xml")) ||
                                          (System.IO.File.Exists(_formName + "_global.xml"));
            bckiLayoutPadEstab.Enabled = (System.IO.File.Exists(_formName + "_" + _par.codEstab.ToString() + "_" + gvVisao.Name + ".xml")) ||
                                         (System.IO.File.Exists(_formName + "_" + _par.codEstab.ToString() + ".xml"));
            bckiLayoutPadUsuario.Enabled = (System.IO.File.Exists(_formName + "_" + _par.codUsuario + "_" + gvVisao.Name + ".xml") ||
                                            System.IO.File.Exists(_formName + "_" + _par.codUsuario + ".xml"));

            bbiLayoutExcluirGlobal.Enabled = bckiLayoutPadGlobal.Enabled;
            bbiLayoutExcluirEstab.Enabled = bckiLayoutPadEstab.Enabled;
            bbiLayoutExcluirUsuario.Enabled = bckiLayoutPadUsuario.Enabled;*/
        }
        #endregion

        #region Salvar o layout
        protected virtual void SalvarLayout(string layout)
        {
            var grids = GetAllGrids(this);
            if (grids.Count() > 1)
            {
                foreach (DevExpress.XtraGrid.GridControl item in grids)
                {
                    foreach (DevExpress.XtraGrid.Views.Grid.GridView view in item.Views)
                    {
                        view.SaveLayoutToXml(_formName + "_" + layout + "_" + view.Name + ".xml");
                    }
                }
            }
            else
                gvVisao.SaveLayoutToXml(_formName + "_" + layout + ".xml");
        }
        #endregion

        #region Excluir o layout
        protected virtual void ExcluirLayout(string layout)
        {
            /*var grids = GetAllGrids(this);
            if (grids.Count() > 1)
            {

                foreach (DevExpress.XtraGrid.GridControl item in grids)
                {
                    foreach (DevExpress.XtraGrid.Views.Grid.GridView view in item.Views)
                    {
                        if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + "_" + view.Name + ".xml"))
                            nomeArquivo.ExcluirArquivo(_formName + "_" + layout + "_" + view.Name + ".xml");
                    }

                    if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + ".xml"))
                        nomeArquivo.ExcluirArquivo(_formName + "_" + layout + ".xml");
                }
            }
            else
            {
                if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + ".xml"))
                    nomeArquivo.ExcluirArquivo(_formName + "_" + layout + ".xml");

                if (nomeArquivo.ExisteArquivo(_formName + "_" + layout + "_" + gvVisao.Name + ".xml"))
                    nomeArquivo.ExcluirArquivo(_formName + "_" + layout + "_" + gvVisao.Name + ".xml");
            }*/
        }
        #endregion

        #region Tratar layout padrão
        protected virtual void TratarLayoutPadrao()
        {

            /*HabDesOpcoesLayouts();
            if (System.IO.File.Exists(_formName + "_" + _par.codUsuario + ".xml") ||
                System.IO.File.Exists(_formName + "_" + _par.codUsuario + "_" + gvVisao.Name + ".xml"))
            {
                RestauraLayout(_par.codUsuario);
                bckiLayoutPadUsuario.Checked = true;
            }
            else if (System.IO.File.Exists(_formName + "_" + _par.codEstab.ToString() + ".xml") ||
                System.IO.File.Exists(_formName + "_" + _par.codEstab.ToString() + "_" + gvVisao.Name + ".xml"))
            {
                RestauraLayout(_par.codEstab.ToString());
                bckiLayoutPadEstab.Checked = true;
            }
            else if (System.IO.File.Exists(_formName + "_global.xml") ||
                System.IO.File.Exists(_formName + "_global_" + gvVisao.Name + ".xml"))
            {
                RestauraLayout("global");
                bckiLayoutPadGlobal.Checked = true;
            }
            else
            {
                RestauraLayout("sistema");
                bckiLayoutPadSistema.Checked = true;
            }*/
        }
        #endregion

        #region Botão editar filtro
        private void bbiFiltroEditar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoEditarFiltro();
        }

        protected virtual void BotaoEditarFiltro()
        {
            gvVisao.ShowFilterEditor(null);
        }
        #endregion

        #region Botão salvar filtro
        private void bbiFiltroSalvar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String tagBtn = "0";
            if (e.Item.Caption == "Salvar Como")
                tagBtn = "1";
            BotaoSalvarFiltro(tagBtn);
        }

        protected virtual void BotaoSalvarFiltro(string tag)
        {
           /* if (gvVisao.ActiveFilterString == "")
                MessageBox.Show("Não há filtro à ser salvo.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else if ((_gFiltros.padrao == "C") && (tag == "0"))
                MessageBox.Show("Não se pode sobrepor um filtro definido como padrão para a coligada.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else if ((_gFiltros.padrao == "G") & (tag == "0"))
                MessageBox.Show("Não se pode sobrepor um filtro definido como padrão global.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else if ((_gFiltros.padrao == "U") && (tag == "0"))
                MessageBox.Show("Não se pode sobrepor um filtro definido como padrão de um usuário.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else if ((_gFiltros.padrao == "S") && (tag == "0"))
                MessageBox.Show("Não se pode sobrepor um filtro definido como padrão do sistema.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            else
            {
                _gFiltros.formulario = _formName;
                _gFiltros.sentenca = gvVisao.ActiveFilterString;

                // Salvar como
                if (tag == "1")
                {
                    _gFiltros.padrao = "N";
                    _gFiltros.idFiltro = 0;
                }

                frmSalvarFiltro frm = new frmSalvarFiltro(_gFiltros);
                frm.ShowDialog();
                if (_gFiltros.idFiltro > 0)
                    bstiFiltroAtivo.Caption = "[" + _gFiltros.descricao + "]";
            }*/
        }
        #endregion

        #region Botão gerenciar filtro
        private void bbiFiltroGerenciar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoGerenciarFiltro();
        }

        protected virtual void BotaoGerenciarFiltro()
        {
           /* frmFiltros frm = new frmFiltros(_formName, ref _gFiltros);
            frm.ShowDialog();
            if (_gFiltros.idFiltro > 0)
            {
                gvVisao.ActiveFilterString = _gFiltros.sentenca;
                bstiFiltroAtivo.Caption = "[" + _gFiltros.descricao + "]";
            }*/
        }
        #endregion

        #region Botão cancelar filtro
        private void bbiFiltroCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoCancelarFiltro();
        }

        protected virtual void BotaoCancelarFiltro()
        {
            /*gvVisao.ActiveFilter.Clear();
            bstiFiltroAtivo.Caption = "[Nenhum]";
            _gFiltros.idFiltro = 0;
            _gFiltros.descricao = "";
            _gFiltros.formulario = "";
            _gFiltros.codUsuario = "";
            _gFiltros.publico = "N";
            _gFiltros.sentenca = "";
            _gFiltros.codEstab = -1;
            _gFiltros.padrao = "N";*/
        }
        #endregion

        #region Verifica se há algum registro selecionado
        protected virtual bool HaRegistroSelecionado()
        {
            bool retorno = false;
            if (gvVisao.IsEmpty)
                MessageBox.Show("Não há registro.", "Advertência", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            else if (gvVisao.FocusedRowHandle < 0)
                MessageBox.Show("Nenhum registro selecionado.", "Advertência", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            else
                retorno = true;
            return retorno;
        }
        #endregion

        #region Botão primeiro registro
        private void bbiPrimReg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoPrimeiroRegistro();
        }

        protected virtual void BotaoPrimeiroRegistro()
        {
            gvVisao.MoveFirst();
        }
        #endregion

        #region Botão registro anterior
        private void bbiRegAnt_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoRegistroAnterior();
        }

        protected virtual void BotaoRegistroAnterior()
        {
            gvVisao.MoveBy(-1);
        }
        #endregion

        #region Botão próximo registro
        private void bbiProxReg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoProximoRegistro();
        }

        protected virtual void BotaoProximoRegistro()
        {
            gvVisao.MoveBy(1);
        }
        #endregion

        #region Botão último registro
        private void bbiUltReg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoUltimoRegistro();
        }

        protected virtual void BotaoUltimoRegistro()
        {
            gvVisao.MoveLast();
        }
        #endregion

        #region Botão de log do registro
        private void bbiLogReg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoLogRegistro();
        }

        protected virtual void BotaoLogRegistro()
        {
            // BotaoLogRegistro
        }
        #endregion

        #region Botão de log do formulário
        private void bbiLogForm_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoLogForm();
        }

        protected virtual void BotaoLogForm()
        {
            // BotaoLogForm
        }
        #endregion

        #region Botao ajuda
        private void bbiAjuda_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BotaoAjuda();
        }

        protected virtual void BotaoAjuda()
        {
            // Botão ajuda
        }
        #endregion

        #region Botão calculadora
        private void bbiCalculadora_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("Calc");
        }
        #endregion

        #region Botão Abrir Dados
        private void AbrirDados_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.BotaoAbrirDados(e.Item.Name);
        }
        #endregion

        #region Função Abrir dados
        protected virtual void BotaoAbrirDados(string bbinome)
        {
            try
            {
                var ex = bbinome.Substring(11, (bbinome.Length - 10) - 1);

                var arq = string.Concat(Path.GetTempPath() + "\\", Guid.NewGuid(), ".", ex.ToLower());

                if (gvVisao.GridControl.ViewCollection.Count > 1)
                {
                    var resp = MessageBox.Show("Exportar detalhes?", "Exportar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (resp == System.Windows.Forms.DialogResult.Yes)
                        gvVisao.OptionsPrint.PrintDetails = true;
                    else
                        gvVisao.OptionsPrint.PrintDetails = false;
                }

                switch (arq.Substring(arq.LastIndexOf(".")))
                {
                    case ".html":
                        gvVisao.ExportToHtml(arq);
                        break;
                    case ".pdf":
                        gvVisao.ExportToPdf(arq);
                        break;
                    case ".rtf":
                        gvVisao.ExportToRtf(arq);
                        break;
                    case ".txt":
                        gvVisao.ExportToText(arq);
                        break;
                    case ".xls":
                        {
                            XlsExportOptionsEx xlsxOptionsExp = new XlsExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            gvVisao.ExportToXls(arq, xlsxOptionsExp);
                        }
                        break;
                    case ".xlsx":
                        {
                            XlsxExportOptionsEx xlsxOptionsExp = new XlsxExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            gvVisao.ExportToXlsx(arq, xlsxOptionsExp);
                        }
                        break;
                }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = arq;
                process.StartInfo.Verb = "Open";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                process.Start();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possivel gerar os dados.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }
        #endregion

        #region Popup Menu
        private void gvVisao_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            this.PopupMenu(sender, e);
        }

        #region Popup Menu
        protected virtual void PopupMenu(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Column)
            {
                DevExpress.XtraGrid.Menu.GridViewColumnMenu columnMenu = e.Menu as DevExpress.XtraGrid.Menu.GridViewColumnMenu;


                DevExpress.Utils.Menu.DXSubMenuItem subMenuExp = new DevExpress.Utils.Menu.DXSubMenuItem("Exportar Grid");
                subMenuExp.BeginGroup = true;

                DevExpress.Utils.Menu.DXMenuItem menuItemXLSX =
                            new DevExpress.Utils.Menu.DXMenuItem("XLSX",
                                new EventHandler(OnExportGrid), null);
                menuItemXLSX.Image = (System.Drawing.Image)Hino.Scheduler.Properties.Resources.excel_icone_16;
                menuItemXLSX.Tag = new object[] { e.Menu.View, "xlsx" };
                subMenuExp.Items.Add(menuItemXLSX);

                DevExpress.Utils.Menu.DXMenuItem menuItemXLS =
                            new DevExpress.Utils.Menu.DXMenuItem("XLS",
                                new EventHandler(OnExportGrid), null);
                menuItemXLS.Image = (System.Drawing.Image)Hino.Scheduler.Properties.Resources.excel_icone_16;
                menuItemXLS.Tag = new object[] { e.Menu.View, "xls" };
                subMenuExp.Items.Add(menuItemXLS);

                DevExpress.Utils.Menu.DXMenuItem menuItemTXT =
                            new DevExpress.Utils.Menu.DXMenuItem("TXT",
                                new EventHandler(OnExportGrid), null);
                menuItemTXT.Tag = new object[] { e.Menu.View, "txt" };
                menuItemTXT.Image = (System.Drawing.Image)Hino.Scheduler.Properties.Resources.notepad_icone_16;
                subMenuExp.Items.Add(menuItemTXT);
                columnMenu.Items.Add(subMenuExp);



                if (e.Menu.View.Columns.Count > 0 && e.HitInfo.Column != null)
                {
                    var caption = e.HitInfo.Column.Fixed == DevExpress.XtraGrid.Columns.FixedStyle.None ? "Travar Coluna" : "Destravar Coluna";
                    if (caption == "Destravar Coluna")
                    {
                        var item = new DevExpress.Utils.Menu.DXMenuItem();
                        item.Caption = caption;
                        item.BeginGroup = true;
                        item.Click += new EventHandler(click_destravar);
                        item.Tag = e.HitInfo.Column;
                        columnMenu.Items.Add(item);
                    }
                    else
                    {
                        var itemLeft = new DevExpress.Utils.Menu.DXMenuItem();
                        var itemRight = new DevExpress.Utils.Menu.DXMenuItem();
                        itemLeft.BeginGroup = true;
                        itemLeft.Caption = "Travar à Esquerda";
                        itemLeft.Click += new EventHandler(click_travaEsq);
                        itemLeft.Tag = e.HitInfo.Column;

                        columnMenu.Items.Add(itemLeft);

                        itemRight.Caption = "Travar à Direita";
                        itemRight.Click += new EventHandler(click_travaDir);
                        itemRight.Tag = e.HitInfo.Column;
                        columnMenu.Items.Add(itemRight);
                    }
                }

                var itemRodape = new DevExpress.Utils.Menu.DXMenuItem("Exibir/Ocultar Rodapé", new EventHandler(click_Footer), null, null);
                itemRodape.BeginGroup = true;

                columnMenu.Items.Add(itemRodape);


                if (e.Menu.View.Columns.Count > 0)
                {
                    DXSubMenuItem subMenu = new DXSubMenuItem("Foca Coluna");
                    subMenu.BeginGroup = true;


                    var cols = e.Menu.View.Columns.OrderBy(r => r.Caption);

                    foreach (GridColumn item in cols)
                    {
                        if (item.Visible && !string.IsNullOrWhiteSpace(item.Caption))
                        {
                            DXMenuItem menuItem = new DXMenuItem(item.Caption,
                                    new EventHandler(OnFocusedColumn), null);
                            menuItem.Tag = item.FieldName;
                            subMenu.Items.Add(menuItem);
                        }
                    }
                    columnMenu.Items.Add(subMenu);
                }



            }
        }

        private void click_travaEsq(object sender, EventArgs e)
        {
            if (gvVisao.Columns.Count > 0 && ((DXMenuItem)sender).Tag != null)
                ((GridColumn)((DXMenuItem)sender).Tag).Fixed = FixedStyle.Left;
        }

        private void click_travaDir(object sender, EventArgs e)
        {
            if (gvVisao.Columns.Count > 0 && ((DXMenuItem)sender).Tag != null)
                ((GridColumn)((DXMenuItem)sender).Tag).Fixed = FixedStyle.Right;
        }

        private void click_destravar(object sender, EventArgs e)
        {
            if (gvVisao.Columns.Count > 0 && ((DXMenuItem)sender).Tag != null)
                ((GridColumn)((DXMenuItem)sender).Tag).Fixed = FixedStyle.None;

        }

        private void OnFocusedColumn(object sender, EventArgs e)
        {
            if (gvVisao.Columns.Count > 0 && ((DXMenuItem)sender).Tag != null)
                gvVisao.FocusedColumn = gvVisao.Columns[((DXMenuItem)sender).Tag.ToString()];

        }

        private static void OnExportGrid(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;
            var grid = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));

            try
            {
                var ex = obj[1].ToString();

                var arq = string.Concat(Path.GetTempPath() + "\\", Guid.NewGuid(), ".", ex.ToLower());

                if (grid.GridControl.ViewCollection.Count > 1)
                {
                    var resp = MessageBox.Show("Exportar detalhes?", "Exportar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (resp == System.Windows.Forms.DialogResult.Yes)
                        grid.OptionsPrint.PrintDetails = true;
                    else
                        grid.OptionsPrint.PrintDetails = false;
                }

                switch (arq.Substring(arq.LastIndexOf(".")))
                {
                    case ".txt":
                        grid.ExportToText(arq);
                        break;
                    case ".xls":
                        {
                            XlsExportOptionsEx xlsxOptionsExp = new XlsExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            grid.ExportToXls(arq, xlsxOptionsExp);
                        }
                        break;
                    case ".xlsx":
                        {
                            XlsxExportOptionsEx xlsxOptionsExp = new XlsxExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            grid.ExportToXlsx(arq, xlsxOptionsExp);
                        }
                        break;
                }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = arq;
                process.StartInfo.Verb = "Open";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                process.Start();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possível gerar os dados.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

        }

        #endregion

        private void click_Footer(object sender, EventArgs e)
        {
            gvVisao.OptionsView.ShowFooter = !gvVisao.OptionsView.ShowFooter;
        }
        #endregion

        #region Get Form
        protected Form GetForm(Type pFormType, string pForm)
        {
            if (System.Windows.Forms.Application.OpenForms[pForm] != null)
                return System.Windows.Forms.Application.OpenForms[pForm];

            return (Form)Activator.CreateInstance(pFormType);
        }
        #endregion

        private void bbiScenario_EditValueChanged(object sender, EventArgs e)
        {
            if (!_Initialize)
                bbiAtualizar_ItemClick(sender, null);
        }
    }
}
