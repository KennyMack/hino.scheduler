﻿namespace Hino.Scheduler
{
    partial class frmVsPadrao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVsPadrao));
            this.bmControleBarra = new DevExpress.XtraBars.BarManager(this.components);
            this.barFerramentas = new DevExpress.XtraBars.Bar();
            this.bbiNovo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditar = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExcluir = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAtualizar = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImprimir = new DevExpress.XtraBars.BarButtonItem();
            this.bsiImpressao = new DevExpress.XtraBars.BarSubItem();
            this.bbiExportar = new DevExpress.XtraBars.BarButtonItem();
            this.bsiAbrirDados = new DevExpress.XtraBars.BarSubItem();
            this.bbiExpDadosXLSX = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExpDadosXLS = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExpDadosPDF = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExpDadosHTML = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExpDadosTXT = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExpDadosRTF = new DevExpress.XtraBars.BarButtonItem();
            this.bsiAnexos = new DevExpress.XtraBars.BarSubItem();
            this.bbiPrimReg = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRegAnt = new DevExpress.XtraBars.BarButtonItem();
            this.bbiProxReg = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUltReg = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCalculadora = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAjuda = new DevExpress.XtraBars.BarButtonItem();
            this.bsiItemScenario = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dmPrincipal = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.icImagens16 = new DevExpress.Utils.ImageCollection(this.components);
            this.bbiExcFiltroPadrao = new DevExpress.XtraBars.BarButtonItem();
            this.bckiLayoutPadSistema = new DevExpress.XtraBars.BarCheckItem();
            this.bckiLayoutPadGlobal = new DevExpress.XtraBars.BarCheckItem();
            this.bckiLayoutPadEstab = new DevExpress.XtraBars.BarCheckItem();
            this.bckiLayoutPadUsuario = new DevExpress.XtraBars.BarCheckItem();
            this.bsiLayoutExcluir = new DevExpress.XtraBars.BarSubItem();
            this.bbiLayoutExcluirGlobal = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutExcluirEstab = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutExcluirUsuario = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFiltroSalvar = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFiltroSalvarComo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFiltroGerenciar = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFiltroCancelar = new DevExpress.XtraBars.BarButtonItem();
            this.bsiLayoutDefinir = new DevExpress.XtraBars.BarSubItem();
            this.bbiLayoutDefinirGlobal = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutDefinirEstab = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayoutDefinirUsuario = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLogReg = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLogForm = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFiltroEditar = new DevExpress.XtraBars.BarButtonItem();
            this.gcGrid = new DevExpress.XtraGrid.GridControl();
            this.gvVisao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.prsImpressao = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.pclPrintLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).BeginInit();
            this.SuspendLayout();
            // 
            // bmControleBarra
            // 
            this.bmControleBarra.AllowCustomization = false;
            this.bmControleBarra.AllowQuickCustomization = false;
            this.bmControleBarra.AllowShowToolbarsPopup = false;
            this.bmControleBarra.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barFerramentas});
            this.bmControleBarra.DockControls.Add(this.barDockControlTop);
            this.bmControleBarra.DockControls.Add(this.barDockControlBottom);
            this.bmControleBarra.DockControls.Add(this.barDockControlLeft);
            this.bmControleBarra.DockControls.Add(this.barDockControlRight);
            this.bmControleBarra.DockManager = this.dmPrincipal;
            this.bmControleBarra.Form = this;
            this.bmControleBarra.Images = this.icImagens16;
            this.bmControleBarra.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNovo,
            this.bbiEditar,
            this.bbiExcluir,
            this.bbiAtualizar,
            this.bbiExcFiltroPadrao,
            this.bbiExportar,
            this.bbiImprimir,
            this.bckiLayoutPadSistema,
            this.bckiLayoutPadGlobal,
            this.bckiLayoutPadEstab,
            this.bckiLayoutPadUsuario,
            this.bsiLayoutExcluir,
            this.bbiLayoutExcluirGlobal,
            this.bbiLayoutExcluirEstab,
            this.bbiLayoutExcluirUsuario,
            this.bbiFiltroSalvar,
            this.bbiFiltroSalvarComo,
            this.bbiFiltroGerenciar,
            this.bbiFiltroCancelar,
            this.bsiLayoutDefinir,
            this.bbiLayoutDefinirGlobal,
            this.bbiLayoutDefinirEstab,
            this.bbiLayoutDefinirUsuario,
            this.bbiPrimReg,
            this.bbiRegAnt,
            this.bbiProxReg,
            this.bbiUltReg,
            this.bbiLogReg,
            this.bbiLogForm,
            this.bsiAnexos,
            this.bbiFiltroEditar,
            this.bbiAjuda,
            this.bsiImpressao,
            this.bbiCalculadora,
            this.bsiAbrirDados,
            this.bbiExpDadosXLSX,
            this.bbiExpDadosXLS,
            this.bbiExpDadosPDF,
            this.bbiExpDadosHTML,
            this.bbiExpDadosTXT,
            this.bbiExpDadosRTF,
            this.bsiItemScenario});
            this.bmControleBarra.MaxItemId = 95;
            this.bmControleBarra.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {});
            // 
            // barFerramentas
            // 
            this.barFerramentas.BarName = "Ferramentas";
            this.barFerramentas.DockCol = 0;
            this.barFerramentas.DockRow = 0;
            this.barFerramentas.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barFerramentas.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNovo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditar),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExcluir),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAtualizar, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiImprimir, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiImpressao),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportar),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiAbrirDados),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiAnexos, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrimReg, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRegAnt),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiProxReg),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUltReg),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCalculadora, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAjuda),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiItemScenario)});
            this.barFerramentas.Text = "Tools";
            // 
            // bbiNovo
            // 
            this.bbiNovo.Caption = "Novo";
            this.bbiNovo.Hint = "Novo";
            this.bbiNovo.Id = 0;
            this.bbiNovo.ImageIndex = 0;
            this.bbiNovo.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.bbiNovo.Name = "bbiNovo";
            this.bbiNovo.Tag = ((short)(1));
            this.bbiNovo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNovo_ItemClick);
            // 
            // bbiEditar
            // 
            this.bbiEditar.Caption = "Editar";
            this.bbiEditar.Hint = "Editar";
            this.bbiEditar.Id = 1;
            this.bbiEditar.ImageIndex = 1;
            this.bbiEditar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.bbiEditar.Name = "bbiEditar";
            this.bbiEditar.Tag = ((short)(2));
            this.bbiEditar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditar_ItemClick);
            // 
            // bbiExcluir
            // 
            this.bbiExcluir.Caption = "Excluir";
            this.bbiExcluir.Hint = "Excluir";
            this.bbiExcluir.Id = 2;
            this.bbiExcluir.ImageIndex = 2;
            this.bbiExcluir.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.bbiExcluir.Name = "bbiExcluir";
            this.bbiExcluir.Tag = ((short)(3));
            this.bbiExcluir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExcluir_ItemClick);
            // 
            // bbiAtualizar
            // 
            this.bbiAtualizar.Caption = "Atualizar";
            this.bbiAtualizar.Hint = "Atualizar";
            this.bbiAtualizar.Id = 4;
            this.bbiAtualizar.ImageIndex = 3;
            this.bbiAtualizar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.bbiAtualizar.Name = "bbiAtualizar";
            this.bbiAtualizar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAtualizar_ItemClick);
            // 
            // bbiImprimir
            // 
            this.bbiImprimir.Caption = "Imprimir";
            this.bbiImprimir.Hint = "Imprimir";
            this.bbiImprimir.Id = 30;
            this.bbiImprimir.ImageIndex = 4;
            this.bbiImprimir.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.bbiImprimir.Name = "bbiImprimir";
            this.bbiImprimir.Tag = ((short)(4));
            this.bbiImprimir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiImprimir_ItemClick);
            // 
            // bsiImpressao
            // 
            this.bsiImpressao.Id = 82;
            this.bsiImpressao.Name = "bsiImpressao";
            this.bsiImpressao.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiExportar
            // 
            this.bbiExportar.Caption = "Exportar";
            this.bbiExportar.Hint = "Exportar";
            this.bbiExportar.Id = 29;
            this.bbiExportar.ImageIndex = 6;
            this.bbiExportar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7);
            this.bbiExportar.Name = "bbiExportar";
            this.bbiExportar.Tag = ((short)(5));
            this.bbiExportar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportar_ItemClick);
            // 
            // bsiAbrirDados
            // 
            this.bsiAbrirDados.Id = 84;
            this.bsiAbrirDados.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosXLSX),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosXLS),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosPDF),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosHTML),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosTXT),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpDadosRTF)});
            this.bsiAbrirDados.Name = "bsiAbrirDados";
            this.bsiAbrirDados.Tag = "5";
            // 
            // bbiExpDadosXLSX
            // 
            this.bbiExpDadosXLSX.Caption = "XLSX";
            this.bbiExpDadosXLSX.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosXLSX.Glyph")));
            this.bbiExpDadosXLSX.Id = 85;
            this.bbiExpDadosXLSX.Name = "bbiExpDadosXLSX";
            this.bbiExpDadosXLSX.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bbiExpDadosXLS
            // 
            this.bbiExpDadosXLS.Caption = "XLS";
            this.bbiExpDadosXLS.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosXLS.Glyph")));
            this.bbiExpDadosXLS.Id = 86;
            this.bbiExpDadosXLS.Name = "bbiExpDadosXLS";
            this.bbiExpDadosXLS.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bbiExpDadosPDF
            // 
            this.bbiExpDadosPDF.Caption = "PDF";
            this.bbiExpDadosPDF.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosPDF.Glyph")));
            this.bbiExpDadosPDF.Id = 87;
            this.bbiExpDadosPDF.Name = "bbiExpDadosPDF";
            this.bbiExpDadosPDF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bbiExpDadosHTML
            // 
            this.bbiExpDadosHTML.Caption = "HTML";
            this.bbiExpDadosHTML.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosHTML.Glyph")));
            this.bbiExpDadosHTML.Id = 88;
            this.bbiExpDadosHTML.Name = "bbiExpDadosHTML";
            this.bbiExpDadosHTML.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bbiExpDadosTXT
            // 
            this.bbiExpDadosTXT.Caption = "TXT";
            this.bbiExpDadosTXT.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosTXT.Glyph")));
            this.bbiExpDadosTXT.Id = 89;
            this.bbiExpDadosTXT.Name = "bbiExpDadosTXT";
            this.bbiExpDadosTXT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bbiExpDadosRTF
            // 
            this.bbiExpDadosRTF.Caption = "RTF";
            this.bbiExpDadosRTF.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpDadosRTF.Glyph")));
            this.bbiExpDadosRTF.Id = 90;
            this.bbiExpDadosRTF.Name = "bbiExpDadosRTF";
            this.bbiExpDadosRTF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AbrirDados_ItemClick);
            // 
            // bsiAnexos
            // 
            this.bsiAnexos.Caption = "Anexos";
            this.bsiAnexos.Hint = "Anexos";
            this.bsiAnexos.Id = 79;
            this.bsiAnexos.ImageIndex = 18;
            this.bsiAnexos.Name = "bsiAnexos";
            this.bsiAnexos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiPrimReg
            // 
            this.bbiPrimReg.Caption = "Primeiro registro";
            this.bbiPrimReg.Hint = "Primeiro registro";
            this.bbiPrimReg.Id = 71;
            this.bbiPrimReg.ImageIndex = 13;
            this.bbiPrimReg.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Home));
            this.bbiPrimReg.Name = "bbiPrimReg";
            this.bbiPrimReg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrimReg_ItemClick);
            // 
            // bbiRegAnt
            // 
            this.bbiRegAnt.Caption = "Registro anterior";
            this.bbiRegAnt.Hint = "Registro anterior";
            this.bbiRegAnt.Id = 72;
            this.bbiRegAnt.ImageIndex = 14;
            this.bbiRegAnt.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Up));
            this.bbiRegAnt.Name = "bbiRegAnt";
            this.bbiRegAnt.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRegAnt_ItemClick);
            // 
            // bbiProxReg
            // 
            this.bbiProxReg.Caption = "Próximo registro";
            this.bbiProxReg.Hint = "Próximo registro";
            this.bbiProxReg.Id = 73;
            this.bbiProxReg.ImageIndex = 15;
            this.bbiProxReg.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Down));
            this.bbiProxReg.Name = "bbiProxReg";
            this.bbiProxReg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiProxReg_ItemClick);
            // 
            // bbiUltReg
            // 
            this.bbiUltReg.Caption = "Último registro";
            this.bbiUltReg.Hint = "Último registro";
            this.bbiUltReg.Id = 74;
            this.bbiUltReg.ImageIndex = 16;
            this.bbiUltReg.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.End));
            this.bbiUltReg.Name = "bbiUltReg";
            this.bbiUltReg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUltReg_ItemClick);
            // 
            // bbiCalculadora
            // 
            this.bbiCalculadora.Caption = "Calculadora";
            this.bbiCalculadora.Id = 83;
            this.bbiCalculadora.ImageIndex = 20;
            this.bbiCalculadora.Name = "bbiCalculadora";
            this.bbiCalculadora.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCalculadora_ItemClick);
            // 
            // bbiAjuda
            // 
            this.bbiAjuda.Caption = "Ajuda";
            this.bbiAjuda.Hint = "Ajuda";
            this.bbiAjuda.Id = 81;
            this.bbiAjuda.ImageIndex = 19;
            this.bbiAjuda.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.bbiAjuda.Name = "bbiAjuda";
            this.bbiAjuda.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAjuda_ItemClick);
            // 
            // bsiItemScenario
            // 
            this.bsiItemScenario.Caption = "Cenário";
            this.bsiItemScenario.Id = 94;
            this.bsiItemScenario.Name = "bsiItemScenario";
            this.bsiItemScenario.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(936, 28);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 397);
            this.barDockControlBottom.Size = new System.Drawing.Size(936, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 28);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 369);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(936, 28);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 369);
            // 
            // dmPrincipal
            // 
            this.dmPrincipal.Form = this;
            this.dmPrincipal.MenuManager = this.bmControleBarra;
            this.dmPrincipal.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // icImagens16
            // 
            this.icImagens16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImagens16.ImageStream")));
            this.icImagens16.Images.SetKeyName(0, "Novo16.png");
            this.icImagens16.Images.SetKeyName(1, "Editar 16.png");
            this.icImagens16.Images.SetKeyName(2, "Excluir_16.png");
            this.icImagens16.Images.SetKeyName(3, "Atualizar 16.png");
            this.icImagens16.Images.SetKeyName(4, "Imprimir 16.png");
            this.icImagens16.Images.SetKeyName(5, "Visualizar_Reg_16.png");
            this.icImagens16.Images.SetKeyName(6, "Configuração para Importação de Dados 16.png");
            this.icImagens16.Images.SetKeyName(7, "Gráfico 16.png");
            this.icImagens16.Images.SetKeyName(8, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(9, "Layout 16.png");
            this.icImagens16.Images.SetKeyName(10, "Filtro 16.png");
            this.icImagens16.Images.SetKeyName(11, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(12, "Definidos 16.png");
            this.icImagens16.Images.SetKeyName(13, "Primeiro16.png");
            this.icImagens16.Images.SetKeyName(14, "Anterior16.png");
            this.icImagens16.Images.SetKeyName(15, "Próximo16.png");
            this.icImagens16.Images.SetKeyName(16, "Último16.png");
            this.icImagens16.Images.SetKeyName(17, "Log 16.png");
            this.icImagens16.Images.SetKeyName(18, "Anexos 16.png");
            this.icImagens16.Images.SetKeyName(19, "Ajuda 16.png");
            this.icImagens16.Images.SetKeyName(20, "calculadora 16.png");
            // 
            // bbiExcFiltroPadrao
            // 
            this.bbiExcFiltroPadrao.Caption = "Excluir filtro padrão";
            this.bbiExcFiltroPadrao.Id = 28;
            this.bbiExcFiltroPadrao.Name = "bbiExcFiltroPadrao";
            // 
            // bckiLayoutPadSistema
            // 
            this.bckiLayoutPadSistema.Caption = "Padrão do Sistema";
            this.bckiLayoutPadSistema.GroupIndex = 1;
            this.bckiLayoutPadSistema.Id = 38;
            this.bckiLayoutPadSistema.Name = "bckiLayoutPadSistema";
            this.bckiLayoutPadSistema.Tag = "0";
            this.bckiLayoutPadSistema.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bckiLayoutPadSistema_CheckedChanged);
            // 
            // bckiLayoutPadGlobal
            // 
            this.bckiLayoutPadGlobal.Caption = "Padrão Global";
            this.bckiLayoutPadGlobal.GroupIndex = 1;
            this.bckiLayoutPadGlobal.Id = 39;
            this.bckiLayoutPadGlobal.Name = "bckiLayoutPadGlobal";
            this.bckiLayoutPadGlobal.Tag = "1";
            this.bckiLayoutPadGlobal.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bckiLayoutPadSistema_CheckedChanged);
            // 
            // bckiLayoutPadEstab
            // 
            this.bckiLayoutPadEstab.Caption = "Padrão do Estabelecimento";
            this.bckiLayoutPadEstab.GroupIndex = 1;
            this.bckiLayoutPadEstab.Id = 41;
            this.bckiLayoutPadEstab.Name = "bckiLayoutPadEstab";
            this.bckiLayoutPadEstab.Tag = "2";
            this.bckiLayoutPadEstab.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bckiLayoutPadSistema_CheckedChanged);
            // 
            // bckiLayoutPadUsuario
            // 
            this.bckiLayoutPadUsuario.Caption = "Padrão do Usuário";
            this.bckiLayoutPadUsuario.GroupIndex = 1;
            this.bckiLayoutPadUsuario.Id = 42;
            this.bckiLayoutPadUsuario.Name = "bckiLayoutPadUsuario";
            this.bckiLayoutPadUsuario.Tag = "3";
            this.bckiLayoutPadUsuario.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bckiLayoutPadSistema_CheckedChanged);
            // 
            // bsiLayoutExcluir
            // 
            this.bsiLayoutExcluir.Caption = "Excluir";
            this.bsiLayoutExcluir.Id = 48;
            this.bsiLayoutExcluir.ImageIndex = 2;
            this.bsiLayoutExcluir.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutExcluirGlobal),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutExcluirEstab),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutExcluirUsuario)});
            this.bsiLayoutExcluir.Name = "bsiLayoutExcluir";
            // 
            // bbiLayoutExcluirGlobal
            // 
            this.bbiLayoutExcluirGlobal.Caption = "Padrão Global";
            this.bbiLayoutExcluirGlobal.Id = 49;
            this.bbiLayoutExcluirGlobal.Name = "bbiLayoutExcluirGlobal";
            this.bbiLayoutExcluirGlobal.Tag = ((short)(9));
            this.bbiLayoutExcluirGlobal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutExcluirGlobal_ItemClick);
            // 
            // bbiLayoutExcluirEstab
            // 
            this.bbiLayoutExcluirEstab.Caption = "Padrão do Estabelecimento";
            this.bbiLayoutExcluirEstab.Id = 50;
            this.bbiLayoutExcluirEstab.Name = "bbiLayoutExcluirEstab";
            this.bbiLayoutExcluirEstab.Tag = ((short)(10));
            this.bbiLayoutExcluirEstab.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutExcluirGlobal_ItemClick);
            // 
            // bbiLayoutExcluirUsuario
            // 
            this.bbiLayoutExcluirUsuario.Caption = "Padrão do Usuário";
            this.bbiLayoutExcluirUsuario.Id = 51;
            this.bbiLayoutExcluirUsuario.Name = "bbiLayoutExcluirUsuario";
            this.bbiLayoutExcluirUsuario.Tag = ((short)(11));
            this.bbiLayoutExcluirUsuario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutExcluirGlobal_ItemClick);
            // 
            // bbiFiltroSalvar
            // 
            this.bbiFiltroSalvar.Caption = "Salvar";
            this.bbiFiltroSalvar.Id = 55;
            this.bbiFiltroSalvar.ImageIndex = 8;
            this.bbiFiltroSalvar.Name = "bbiFiltroSalvar";
            this.bbiFiltroSalvar.Tag = ((short)(12));
            this.bbiFiltroSalvar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFiltroSalvar_ItemClick);
            // 
            // bbiFiltroSalvarComo
            // 
            this.bbiFiltroSalvarComo.Caption = "Salvar Como";
            this.bbiFiltroSalvarComo.Id = 56;
            this.bbiFiltroSalvarComo.ImageIndex = 11;
            this.bbiFiltroSalvarComo.Name = "bbiFiltroSalvarComo";
            this.bbiFiltroSalvarComo.Tag = ((short)(13));
            this.bbiFiltroSalvarComo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFiltroSalvar_ItemClick);
            // 
            // bbiFiltroGerenciar
            // 
            this.bbiFiltroGerenciar.Caption = "Gerenciar";
            this.bbiFiltroGerenciar.Id = 57;
            this.bbiFiltroGerenciar.Name = "bbiFiltroGerenciar";
            this.bbiFiltroGerenciar.Tag = ((short)(14));
            this.bbiFiltroGerenciar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFiltroGerenciar_ItemClick);
            // 
            // bbiFiltroCancelar
            // 
            this.bbiFiltroCancelar.Caption = "Cancelar";
            this.bbiFiltroCancelar.Id = 58;
            this.bbiFiltroCancelar.Name = "bbiFiltroCancelar";
            this.bbiFiltroCancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFiltroCancelar_ItemClick);
            // 
            // bsiLayoutDefinir
            // 
            this.bsiLayoutDefinir.Caption = "Definir";
            this.bsiLayoutDefinir.Id = 63;
            this.bsiLayoutDefinir.ImageIndex = 12;
            this.bsiLayoutDefinir.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutDefinirGlobal),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutDefinirEstab),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayoutDefinirUsuario)});
            this.bsiLayoutDefinir.Name = "bsiLayoutDefinir";
            // 
            // bbiLayoutDefinirGlobal
            // 
            this.bbiLayoutDefinirGlobal.Caption = "Como Padrão Global";
            this.bbiLayoutDefinirGlobal.Id = 64;
            this.bbiLayoutDefinirGlobal.Name = "bbiLayoutDefinirGlobal";
            this.bbiLayoutDefinirGlobal.Tag = ((short)(6));
            this.bbiLayoutDefinirGlobal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutDefinirGlobal_ItemClick);
            // 
            // bbiLayoutDefinirEstab
            // 
            this.bbiLayoutDefinirEstab.Caption = "Como Padrão do Estabelecimento";
            this.bbiLayoutDefinirEstab.Id = 65;
            this.bbiLayoutDefinirEstab.Name = "bbiLayoutDefinirEstab";
            this.bbiLayoutDefinirEstab.Tag = ((short)(7));
            this.bbiLayoutDefinirEstab.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutDefinirGlobal_ItemClick);
            // 
            // bbiLayoutDefinirUsuario
            // 
            this.bbiLayoutDefinirUsuario.Caption = "Como Padrão do Usuário";
            this.bbiLayoutDefinirUsuario.Id = 66;
            this.bbiLayoutDefinirUsuario.Name = "bbiLayoutDefinirUsuario";
            this.bbiLayoutDefinirUsuario.Tag = ((short)(8));
            this.bbiLayoutDefinirUsuario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayoutDefinirGlobal_ItemClick);
            // 
            // bbiLogReg
            // 
            this.bbiLogReg.Caption = "Do registro";
            this.bbiLogReg.Id = 77;
            this.bbiLogReg.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F9);
            this.bbiLogReg.Name = "bbiLogReg";
            this.bbiLogReg.Tag = ((short)(15));
            this.bbiLogReg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLogReg_ItemClick);
            // 
            // bbiLogForm
            // 
            this.bbiLogForm.Caption = "Do formulário";
            this.bbiLogForm.Id = 78;
            this.bbiLogForm.Name = "bbiLogForm";
            this.bbiLogForm.Tag = ((short)(16));
            this.bbiLogForm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLogForm_ItemClick);
            // 
            // bbiFiltroEditar
            // 
            this.bbiFiltroEditar.Caption = "Editar";
            this.bbiFiltroEditar.Id = 80;
            this.bbiFiltroEditar.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F8);
            this.bbiFiltroEditar.Name = "bbiFiltroEditar";
            this.bbiFiltroEditar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFiltroEditar_ItemClick);
            // 
            // gcGrid
            // 
            this.gcGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcGrid.Location = new System.Drawing.Point(0, 28);
            this.gcGrid.MainView = this.gvVisao;
            this.gcGrid.MenuManager = this.bmControleBarra;
            this.gcGrid.Name = "gcGrid";
            this.gcGrid.Size = new System.Drawing.Size(936, 369);
            this.gcGrid.TabIndex = 4;
            this.gcGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVisao});
            // 
            // gvVisao
            // 
            this.gvVisao.GridControl = this.gcGrid;
            this.gvVisao.Name = "gvVisao";
            this.gvVisao.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gvVisao.OptionsBehavior.ReadOnly = true;
            this.gvVisao.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gvVisao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvVisao.OptionsPrint.AutoWidth = false;
            this.gvVisao.OptionsPrint.ExpandAllDetails = true;
            this.gvVisao.OptionsPrint.PrintDetails = true;
            this.gvVisao.OptionsPrint.PrintFilterInfo = true;
            this.gvVisao.OptionsView.BestFitMaxRowCount = 100;
            this.gvVisao.OptionsView.ColumnAutoWidth = false;
            this.gvVisao.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gvVisao.OptionsView.ShowAutoFilterRow = true;
            this.gvVisao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gvVisao.OptionsView.ShowFooter = true;
            this.gvVisao.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvVisao_PopupMenuShowing);
            // 
            // prsImpressao
            // 
            this.prsImpressao.Links.AddRange(new object[] {
            this.pclPrintLink});
            // 
            // pclPrintLink
            // 
            this.pclPrintLink.Component = this.gcGrid;
            this.pclPrintLink.Landscape = true;
            this.pclPrintLink.MinMargins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.pclPrintLink.PageHeaderFooter = new DevExpress.XtraPrinting.PageHeaderFooter(null, new DevExpress.XtraPrinting.PageFooterArea(new string[] {
                "[Data de Impressão] - [Horário de Impressão]",
                "",
                "[Página # de #]"}, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))), DevExpress.XtraPrinting.BrickAlignment.Near));
            this.pclPrintLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.pclPrintLink.PrintingSystemBase = this.prsImpressao;
            this.pclPrintLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.pclPrintLink_CreateMarginalHeaderArea);
            // 
            // frmVsPadrao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 397);
            this.Controls.Add(this.gcGrid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVsPadrao";
            this.Text = "Formulário de Visão Padrão";
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.XtraBars.BarManager bmControleBarra;
        protected DevExpress.Utils.ImageCollection icImagens16;
        protected DevExpress.XtraGrid.GridControl gcGrid;
        protected DevExpress.XtraGrid.Views.Grid.GridView gvVisao;
        protected DevExpress.XtraBars.Bar barFerramentas;
        protected DevExpress.XtraBars.BarButtonItem bbiNovo;
        protected DevExpress.XtraBars.BarButtonItem bbiEditar;
        protected DevExpress.XtraBars.BarButtonItem bbiExcluir;
        protected DevExpress.XtraBars.BarButtonItem bbiAtualizar;
        protected DevExpress.XtraBars.BarButtonItem bbiExportar;
        protected DevExpress.XtraBars.BarButtonItem bbiImprimir;
        protected DevExpress.XtraBars.BarCheckItem bckiLayoutPadSistema;
        protected DevExpress.XtraBars.BarCheckItem bckiLayoutPadGlobal;
        protected DevExpress.XtraBars.BarCheckItem bckiLayoutPadEstab;
        protected DevExpress.XtraBars.BarCheckItem bckiLayoutPadUsuario;
        protected DevExpress.XtraBars.BarSubItem bsiLayoutExcluir;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutExcluirGlobal;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutExcluirEstab;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutExcluirUsuario;
        protected DevExpress.XtraBars.BarButtonItem bbiFiltroSalvar;
        protected DevExpress.XtraBars.BarButtonItem bbiFiltroSalvarComo;
        protected DevExpress.XtraBars.BarButtonItem bbiFiltroGerenciar;
        protected DevExpress.XtraBars.BarButtonItem bbiFiltroCancelar;
        protected DevExpress.XtraBars.BarSubItem bsiLayoutDefinir;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutDefinirGlobal;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutDefinirEstab;
        protected DevExpress.XtraBars.BarButtonItem bbiLayoutDefinirUsuario;
        protected DevExpress.XtraBars.BarButtonItem bbiExcFiltroPadrao;
        protected DevExpress.XtraBars.BarButtonItem bbiLogReg;
        protected DevExpress.XtraBars.BarButtonItem bbiLogForm;
        protected DevExpress.XtraBars.BarButtonItem bbiPrimReg;
        protected DevExpress.XtraBars.BarButtonItem bbiRegAnt;
        protected DevExpress.XtraBars.BarButtonItem bbiProxReg;
        protected DevExpress.XtraBars.BarButtonItem bbiUltReg;
        protected DevExpress.XtraBars.BarSubItem bsiAnexos;
        private DevExpress.XtraBars.BarButtonItem bbiFiltroEditar;
        protected DevExpress.XtraPrinting.PrintingSystem prsImpressao;
        protected DevExpress.XtraPrinting.PrintableComponentLink pclPrintLink;
        protected DevExpress.XtraBars.BarSubItem bsiImpressao;
        protected DevExpress.XtraBars.BarButtonItem bbiAjuda;
        private DevExpress.XtraBars.BarButtonItem bbiCalculadora;
        protected DevExpress.XtraBars.BarSubItem bsiAbrirDados;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosXLSX;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosXLS;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosPDF;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosHTML;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosTXT;
        protected DevExpress.XtraBars.BarButtonItem bbiExpDadosRTF;
        protected DevExpress.XtraBars.Docking.DockManager dmPrincipal;
        private DevExpress.XtraBars.BarStaticItem bsiItemScenario;
    }
}