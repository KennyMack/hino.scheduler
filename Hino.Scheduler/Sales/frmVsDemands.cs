﻿using Hino.Scheduler.Application.ViewModel.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Sales
{
    public partial class frmVsDemands : frmVsPadrao
    {
        private readonly DemandsVM _DemandsVM;
        private readonly DemandsDetVM _DemandDetVM;
        public frmVsDemands()
        {
            InitializeComponent();
            _DemandsVM = new DemandsVM();
            _DemandDetVM = new DemandsDetVM();
            gvDetails.DefinePadrao();
        }

        private async void frmVsDemands_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _DemandsVM._IdScenario = Parameters.IdScenario;
            _DemandDetVM._IdScenario = Parameters.IdScenario;

            await _DemandsVM.LoadDataSource();
            await _DemandDetVM.LoadDemandaDetOP();

            gcGrid.DataSource = _DemandsVM._DemandDataSource;
            if (_Initialize)
            {
                var vsIndex = 0;
                gvVisao.Columns["iddemanda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["client"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codestab"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codpedvenda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["codproduto"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["descricao"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dtinicio"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dttermino"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["dataprogvenda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["qtdprogramada"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["sldprograma"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["diferenca"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["atendida"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["temops"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["StatusDemanda"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["DiasDifProd"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["AtendeSaldoProg"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["IDJOBSIMU"].OrdenaColuna(vsIndex++);
                gvVisao.Columns["IdLoadData"].OrdenaColuna(vsIndex++);

                gvVisao.BestFitColumns();

                fmtGridView.OcultaColuna(
                    gvVisao.Columns["iddemanda"],
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["IdLoadData"],
                    gvVisao.Columns["IDJOBSIMU"]
                );

                gvVisao.Columns["dataprogvenda"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion

        private void gvVisao_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            fmtGridView.ColoreColuna(e, "StatusDemanda", "SEM PRODUÇÃO", Color.Orange);
            fmtGridView.ColoreColuna(e, "StatusDemanda", "ATRASADA", Color.Salmon);
            fmtGridView.ColoreColuna(e, "StatusDemanda", "ADIANTADA", Color.PaleGreen);
            fmtGridView.ColoreColuna(e, "StatusDemanda", "NA DATA", Color.LightSkyBlue);

            fmtGridView.ColoreColuna(e, "AtendeSaldoProg", "ATENDE PARCIAL", Color.Orange);
            fmtGridView.ColoreColuna(e, "AtendeSaldoProg", "NÃO ATENDE", Color.Salmon);
            fmtGridView.ColoreColuna(e, "AtendeSaldoProg", "ATENDE TOTAL", Color.PaleGreen);
        }

        ~frmVsDemands()
        {
            _DemandsVM.Dispose();
            _DemandDetVM.Dispose();
        }

        #region Load Jobs Detail
        private void LoadJobsDetail()
        {
            gvDetails.ShowLoadingPanel();

            if (gvVisao.FocusedRowHandle > -1)
            {
                var IdDemand = Convert.ToInt64(gvVisao.GetFocusedRowCellValue("iddemanda"));

                gcDetails.DataSource = _DemandDetVM._DemandaDetOPDataSource.Where(r => r.iddemanda == IdDemand).ToList();

                if (_Initialize)
                {
                    gvDetails.BestFitColumns();
                    /*fmtGridView.OcultaColuna(
                        gvDetails.Columns["codestab"]
                    );
                    gvDetails.Columns["codordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                    gvDetails.Columns["nivelordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);*/
                }
            }
            gvDetails.HideLoadingPanel();
        }
        #endregion


        private void gvVisao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            LoadJobsDetail();
        }
    }
}
