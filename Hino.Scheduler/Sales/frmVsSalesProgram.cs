﻿using Hino.Scheduler.Application.ViewModel.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Sales
{
    public partial class frmVsSalesProgram : frmVsPadrao
    {
        private readonly DemandsVM _DemandsVM;

        public frmVsSalesProgram()
        {
            InitializeComponent();
            _DemandsVM = new DemandsVM();
        }

        private async void frmVsSalesProgram_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _DemandsVM._IdScenario = Parameters.IdScenario;
            await _DemandsVM.LoadSalesProgram();

            gcGrid.DataSource = _DemandsVM._SalesProgramDataSource;
            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                gvVisao.Columns["dataprograma"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codproduto"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codempresa"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion

        ~frmVsSalesProgram()
        {
            _DemandsVM.Dispose();
        }
    }
}
