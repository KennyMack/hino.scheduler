﻿namespace Hino.Scheduler
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.rcMenu = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bdcPrincipal = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.bbiProgramacaoOP = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSchedulerOP = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCargaMaquina = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMachineLoadCell = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMachineLoadSimulate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiProductionSimulated = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOrderBottleneck = new DevExpress.XtraBars.BarButtonItem();
            this.bbiJobShop = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRoutinesSequence = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLoadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDemands = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMachineDisp = new DevExpress.XtraBars.BarButtonItem();
            this.bbiScheduleOPMachine = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSalesDemand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiResources = new DevExpress.XtraBars.BarButtonItem();
            this.beiScenario = new DevExpress.XtraBars.BarEditItem();
            this.ricbScenario = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.bsiScenario = new DevExpress.XtraBars.BarStaticItem();
            this.bbiResourceHours = new DevExpress.XtraBars.BarButtonItem();
            this.rpSimulation = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgDataScenario = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgEngineering = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgProducao = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpProduction = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgProduction = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSales = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgEng = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.bsiUsuario = new DevExpress.XtraBars.BarStaticItem();
            this.bsiVersao = new DevExpress.XtraBars.BarStaticItem();
            this.dlfeSkin = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtdmdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.rcMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdcPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbScenario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtdmdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // rcMenu
            // 
            this.rcMenu.Controller = this.bdcPrincipal;
            this.rcMenu.ExpandCollapseItem.Id = 0;
            this.rcMenu.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rcMenu.ExpandCollapseItem,
            this.bbiProgramacaoOP,
            this.bbiSchedulerOP,
            this.bbiCargaMaquina,
            this.bbiMachineLoadCell,
            this.bbiMachineLoadSimulate,
            this.bbiProductionSimulated,
            this.bbiOrderBottleneck,
            this.bbiJobShop,
            this.bbiRoutinesSequence,
            this.bbiLoadData,
            this.bbiDemands,
            this.bbiMachineDisp,
            this.bbiScheduleOPMachine,
            this.bbiSalesDemand,
            this.bbiResources,
            this.beiScenario,
            this.bsiScenario,
            this.bbiResourceHours});
            this.rcMenu.Location = new System.Drawing.Point(0, 0);
            this.rcMenu.MaxItemId = 23;
            this.rcMenu.Name = "rcMenu";
            this.rcMenu.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpSimulation,
            this.rpProduction});
            this.rcMenu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ricbScenario});
            this.rcMenu.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.rcMenu.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.rcMenu.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.rcMenu.ShowFullScreenButton = DevExpress.Utils.DefaultBoolean.False;
            this.rcMenu.ShowQatLocationSelector = false;
            this.rcMenu.ShowToolbarCustomizeItem = false;
            this.rcMenu.Size = new System.Drawing.Size(1104, 117);
            this.rcMenu.StatusBar = this.ribbonStatusBar1;
            this.rcMenu.Toolbar.ShowCustomizeItem = false;
            this.rcMenu.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // bdcPrincipal
            // 
            this.bdcPrincipal.LookAndFeel.SkinName = "Blue";
            this.bdcPrincipal.PropertiesBar.AllowLinkLighting = false;
            this.bdcPrincipal.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.bdcPrincipal.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // bbiProgramacaoOP
            // 
            this.bbiProgramacaoOP.Caption = "Sequenciamento de rotinas";
            this.bbiProgramacaoOP.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiProgramacaoOP.Glyph")));
            this.bbiProgramacaoOP.Id = 1;
            this.bbiProgramacaoOP.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiProgramacaoOP.LargeGlyph")));
            this.bbiProgramacaoOP.Name = "bbiProgramacaoOP";
            this.bbiProgramacaoOP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiProgramacaoOP_ItemClick);
            // 
            // bbiSchedulerOP
            // 
            this.bbiSchedulerOP.Caption = "Calendário ordem de produção";
            this.bbiSchedulerOP.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSchedulerOP.Glyph")));
            this.bbiSchedulerOP.Id = 2;
            this.bbiSchedulerOP.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSchedulerOP.LargeGlyph")));
            this.bbiSchedulerOP.Name = "bbiSchedulerOP";
            this.bbiSchedulerOP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSchedulerOP_ItemClick);
            // 
            // bbiCargaMaquina
            // 
            this.bbiCargaMaquina.Caption = "Carga Máquina";
            this.bbiCargaMaquina.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCargaMaquina.Glyph")));
            this.bbiCargaMaquina.Id = 3;
            this.bbiCargaMaquina.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiCargaMaquina.LargeGlyph")));
            this.bbiCargaMaquina.Name = "bbiCargaMaquina";
            this.bbiCargaMaquina.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCargaMaquina_ItemClick);
            // 
            // bbiMachineLoadCell
            // 
            this.bbiMachineLoadCell.Caption = "Carga máquina x célula";
            this.bbiMachineLoadCell.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineLoadCell.Glyph")));
            this.bbiMachineLoadCell.Id = 5;
            this.bbiMachineLoadCell.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineLoadCell.LargeGlyph")));
            this.bbiMachineLoadCell.Name = "bbiMachineLoadCell";
            this.bbiMachineLoadCell.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiMachineLoadCell.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMachineLoadCell_ItemClick);
            // 
            // bbiMachineLoadSimulate
            // 
            this.bbiMachineLoadSimulate.Caption = "Carga máquina simulada";
            this.bbiMachineLoadSimulate.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineLoadSimulate.Glyph")));
            this.bbiMachineLoadSimulate.Id = 6;
            this.bbiMachineLoadSimulate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineLoadSimulate.LargeGlyph")));
            this.bbiMachineLoadSimulate.Name = "bbiMachineLoadSimulate";
            this.bbiMachineLoadSimulate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMachineLoadSimulate_ItemClick);
            // 
            // bbiProductionSimulated
            // 
            this.bbiProductionSimulated.Caption = "Simulação de produção";
            this.bbiProductionSimulated.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiProductionSimulated.Glyph")));
            this.bbiProductionSimulated.Id = 7;
            this.bbiProductionSimulated.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiProductionSimulated.LargeGlyph")));
            this.bbiProductionSimulated.Name = "bbiProductionSimulated";
            this.bbiProductionSimulated.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiProductionSimulated.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiProductionSimulated_ItemClick);
            // 
            // bbiOrderBottleneck
            // 
            this.bbiOrderBottleneck.Caption = "Operação gargalo";
            this.bbiOrderBottleneck.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiOrderBottleneck.Glyph")));
            this.bbiOrderBottleneck.Id = 8;
            this.bbiOrderBottleneck.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiOrderBottleneck.LargeGlyph")));
            this.bbiOrderBottleneck.Name = "bbiOrderBottleneck";
            this.bbiOrderBottleneck.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOrderBottleneck_ItemClick);
            // 
            // bbiJobShop
            // 
            this.bbiJobShop.Caption = "Calendário ordem de produção rotina (grid)";
            this.bbiJobShop.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiJobShop.Glyph")));
            this.bbiJobShop.Id = 9;
            this.bbiJobShop.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiJobShop.LargeGlyph")));
            this.bbiJobShop.Name = "bbiJobShop";
            this.bbiJobShop.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiJobShop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiJobShop_ItemClick);
            // 
            // bbiRoutinesSequence
            // 
            this.bbiRoutinesSequence.Caption = "Rotinas Sequenciadas";
            this.bbiRoutinesSequence.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRoutinesSequence.Glyph")));
            this.bbiRoutinesSequence.Id = 10;
            this.bbiRoutinesSequence.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRoutinesSequence.LargeGlyph")));
            this.bbiRoutinesSequence.Name = "bbiRoutinesSequence";
            this.bbiRoutinesSequence.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRoutinesSequence_ItemClick);
            // 
            // bbiLoadData
            // 
            this.bbiLoadData.Caption = "Carregar Cenários";
            this.bbiLoadData.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLoadData.Glyph")));
            this.bbiLoadData.Id = 11;
            this.bbiLoadData.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiLoadData.LargeGlyph")));
            this.bbiLoadData.Name = "bbiLoadData";
            this.bbiLoadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadData_ItemClick);
            // 
            // bbiDemands
            // 
            this.bbiDemands.Caption = "Demandas";
            this.bbiDemands.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDemands.Glyph")));
            this.bbiDemands.Hint = "Demandas atendias/atrasadas";
            this.bbiDemands.Id = 12;
            this.bbiDemands.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiDemands.LargeGlyph")));
            this.bbiDemands.Name = "bbiDemands";
            this.bbiDemands.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDemands_ItemClick);
            // 
            // bbiMachineDisp
            // 
            this.bbiMachineDisp.Caption = "Disponibilidade de máquinas";
            this.bbiMachineDisp.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineDisp.Glyph")));
            this.bbiMachineDisp.Id = 13;
            this.bbiMachineDisp.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiMachineDisp.LargeGlyph")));
            this.bbiMachineDisp.Name = "bbiMachineDisp";
            this.bbiMachineDisp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiMachineDisp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMachineDisp_ItemClick);
            // 
            // bbiScheduleOPMachine
            // 
            this.bbiScheduleOPMachine.Caption = "Calendário ordem de produção rotinas";
            this.bbiScheduleOPMachine.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiScheduleOPMachine.Glyph")));
            this.bbiScheduleOPMachine.Id = 14;
            this.bbiScheduleOPMachine.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiScheduleOPMachine.LargeGlyph")));
            this.bbiScheduleOPMachine.Name = "bbiScheduleOPMachine";
            this.bbiScheduleOPMachine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiScheduleOPMachine_ItemClick);
            // 
            // bbiSalesDemand
            // 
            this.bbiSalesDemand.Caption = "Programação de vendas";
            this.bbiSalesDemand.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSalesDemand.Glyph")));
            this.bbiSalesDemand.Id = 15;
            this.bbiSalesDemand.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSalesDemand.LargeGlyph")));
            this.bbiSalesDemand.Name = "bbiSalesDemand";
            this.bbiSalesDemand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSalesDemand_ItemClick);
            // 
            // bbiResources
            // 
            this.bbiResources.Caption = "Recursos";
            this.bbiResources.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiResources.Glyph")));
            this.bbiResources.Id = 16;
            this.bbiResources.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiResources.LargeGlyph")));
            this.bbiResources.Name = "bbiResources";
            this.bbiResources.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiResources_ItemClick);
            // 
            // beiScenario
            // 
            this.beiScenario.CaptionAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.beiScenario.Edit = this.ricbScenario;
            this.beiScenario.EditWidth = 65;
            this.beiScenario.Id = 17;
            this.beiScenario.Name = "beiScenario";
            this.beiScenario.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this.beiScenario.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.beiScenario.EditValueChanged += new System.EventHandler(this.beiScenario_EditValueChanged);
            this.beiScenario.ShowingEditor += new DevExpress.XtraBars.ItemCancelEventHandler(this.beiScenario_ShowingEditor);
            // 
            // ricbScenario
            // 
            this.ricbScenario.AutoHeight = false;
            this.ricbScenario.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricbScenario.Name = "ricbScenario";
            this.ricbScenario.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // bsiScenario
            // 
            this.bsiScenario.Caption = "Cenário atual";
            this.bsiScenario.Id = 21;
            this.bsiScenario.Name = "bsiScenario";
            this.bsiScenario.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bbiResourceHours
            // 
            this.bbiResourceHours.Caption = "Maquina turno x dia";
            this.bbiResourceHours.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiResourceHours.Glyph")));
            this.bbiResourceHours.Id = 22;
            this.bbiResourceHours.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiResourceHours.LargeGlyph")));
            this.bbiResourceHours.Name = "bbiResourceHours";
            this.bbiResourceHours.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiResourceHours_ItemClick);
            // 
            // rpSimulation
            // 
            this.rpSimulation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgDataScenario,
            this.rpgEngineering,
            this.rpgProducao});
            this.rpSimulation.Name = "rpSimulation";
            this.rpSimulation.Text = "Simulação";
            // 
            // rpgDataScenario
            // 
            this.rpgDataScenario.ItemLinks.Add(this.bbiLoadData);
            this.rpgDataScenario.ItemLinks.Add(this.bsiScenario, true);
            this.rpgDataScenario.ItemLinks.Add(this.beiScenario);
            this.rpgDataScenario.Name = "rpgDataScenario";
            this.rpgDataScenario.ShowCaptionButton = false;
            this.rpgDataScenario.Text = "Cenários";
            // 
            // rpgEngineering
            // 
            this.rpgEngineering.AllowTextClipping = false;
            this.rpgEngineering.ItemLinks.Add(this.bbiResourceHours);
            this.rpgEngineering.ItemLinks.Add(this.bbiMachineLoadSimulate);
            this.rpgEngineering.ItemLinks.Add(this.bbiMachineDisp);
            this.rpgEngineering.Name = "rpgEngineering";
            this.rpgEngineering.ShowCaptionButton = false;
            this.rpgEngineering.Text = "Engenharia";
            // 
            // rpgProducao
            // 
            this.rpgProducao.AllowTextClipping = false;
            this.rpgProducao.ItemLinks.Add(this.bbiSchedulerOP);
            this.rpgProducao.ItemLinks.Add(this.bbiScheduleOPMachine);
            this.rpgProducao.ItemLinks.Add(this.bbiRoutinesSequence);
            this.rpgProducao.ItemLinks.Add(this.bbiDemands);
            this.rpgProducao.ItemLinks.Add(this.bbiProgramacaoOP);
            this.rpgProducao.ItemLinks.Add(this.bbiJobShop);
            this.rpgProducao.ItemLinks.Add(this.bbiMachineLoadCell);
            this.rpgProducao.ItemLinks.Add(this.bbiProductionSimulated);
            this.rpgProducao.ItemLinks.Add(this.bbiOrderBottleneck);
            this.rpgProducao.Name = "rpgProducao";
            this.rpgProducao.Text = "Produção";
            // 
            // rpProduction
            // 
            this.rpProduction.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgProduction,
            this.rpgSales,
            this.rpgEng});
            this.rpProduction.Name = "rpProduction";
            this.rpProduction.Text = "Produção";
            // 
            // rpgProduction
            // 
            this.rpgProduction.AllowTextClipping = false;
            this.rpgProduction.ItemLinks.Add(this.bbiCargaMaquina);
            this.rpgProduction.Name = "rpgProduction";
            this.rpgProduction.Text = "Produção";
            // 
            // rpgSales
            // 
            this.rpgSales.AllowTextClipping = false;
            this.rpgSales.ItemLinks.Add(this.bbiSalesDemand);
            this.rpgSales.Name = "rpgSales";
            this.rpgSales.Text = "Vendas";
            // 
            // rpgEng
            // 
            this.rpgEng.AllowTextClipping = false;
            this.rpgEng.ItemLinks.Add(this.bbiResources);
            this.rpgEng.Name = "rpgEng";
            this.rpgEng.Text = "Engenharia";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.bsiUsuario);
            this.ribbonStatusBar1.ItemLinks.Add(this.bsiVersao);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 498);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.rcMenu;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1104, 23);
            // 
            // bsiUsuario
            // 
            this.bsiUsuario.Caption = "Usuário:";
            this.bsiUsuario.Id = 1;
            this.bsiUsuario.Name = "bsiUsuario";
            this.bsiUsuario.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bsiVersao
            // 
            this.bsiVersao.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiVersao.Caption = "Versão:";
            this.bsiVersao.Id = 2;
            this.bsiVersao.Name = "bsiVersao";
            this.bsiVersao.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dlfeSkin
            // 
            this.dlfeSkin.EnableBonusSkins = true;
            this.dlfeSkin.LookAndFeel.SkinName = "Blue";
            // 
            // xtdmdiManager
            // 
            this.xtdmdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPagesAndTabControlHeader;
            this.xtdmdiManager.Controller = this.bdcPrincipal;
            this.xtdmdiManager.MdiParent = this;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 521);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.rcMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = "Hino Scheduler";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rcMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdcPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbScenario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtdmdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl rcMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpSimulation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgProducao;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem bsiUsuario;
        private DevExpress.XtraBars.BarStaticItem bsiVersao;
        private DevExpress.LookAndFeel.DefaultLookAndFeel dlfeSkin;
        private DevExpress.XtraBars.BarButtonItem bbiProgramacaoOP;
        private DevExpress.XtraBars.BarButtonItem bbiSchedulerOP;
        private DevExpress.XtraBars.BarButtonItem bbiCargaMaquina;
        private DevExpress.XtraBars.BarAndDockingController bdcPrincipal;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtdmdiManager;
        private DevExpress.XtraBars.BarButtonItem bbiMachineLoadCell;
        private DevExpress.XtraBars.BarButtonItem bbiMachineLoadSimulate;
        private DevExpress.XtraBars.BarButtonItem bbiProductionSimulated;
        private DevExpress.XtraBars.BarButtonItem bbiOrderBottleneck;
        private DevExpress.XtraBars.BarButtonItem bbiJobShop;
        private DevExpress.XtraBars.BarButtonItem bbiRoutinesSequence;
        private DevExpress.XtraBars.BarButtonItem bbiLoadData;
        private DevExpress.XtraBars.BarButtonItem bbiDemands;
        private DevExpress.XtraBars.BarButtonItem bbiMachineDisp;
        private DevExpress.XtraBars.BarButtonItem bbiScheduleOPMachine;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpProduction;
        private DevExpress.XtraBars.BarButtonItem bbiSalesDemand;
        private DevExpress.XtraBars.BarButtonItem bbiResources;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgProduction;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSales;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgEng;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ricbScenario;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgDataScenario;
        protected DevExpress.XtraBars.BarEditItem beiScenario;
        private DevExpress.XtraBars.BarStaticItem bsiScenario;
        private DevExpress.XtraBars.BarButtonItem bbiResourceHours;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgEngineering;
    }
}