﻿namespace Hino.Scheduler
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.pnlFundoDev = new DevExpress.XtraEditors.PanelControl();
            this.btnSair = new DevExpress.XtraEditors.SimpleButton();
            this.btnEntrar = new DevExpress.XtraEditors.SimpleButton();
            this.cbxAlias = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtSenha = new DevExpress.XtraEditors.TextEdit();
            this.txtUsuario = new DevExpress.XtraEditors.TextEdit();
            this.lblVersao = new DevExpress.XtraEditors.LabelControl();
            this.lblAliasDev = new DevExpress.XtraEditors.LabelControl();
            this.lblSenhaDev = new DevExpress.XtraEditors.LabelControl();
            this.lblUsuarioDev = new DevExpress.XtraEditors.LabelControl();
            this.dlfSkin = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFundoDev)).BeginInit();
            this.pnlFundoDev.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxAlias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(157, 94);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(113, 106);
            this.picLogo.TabIndex = 26;
            this.picLogo.TabStop = false;
            // 
            // pnlFundoDev
            // 
            this.pnlFundoDev.Controls.Add(this.btnSair);
            this.pnlFundoDev.Controls.Add(this.btnEntrar);
            this.pnlFundoDev.Controls.Add(this.cbxAlias);
            this.pnlFundoDev.Controls.Add(this.txtSenha);
            this.pnlFundoDev.Controls.Add(this.txtUsuario);
            this.pnlFundoDev.Controls.Add(this.lblVersao);
            this.pnlFundoDev.Controls.Add(this.lblAliasDev);
            this.pnlFundoDev.Controls.Add(this.lblSenhaDev);
            this.pnlFundoDev.Controls.Add(this.lblUsuarioDev);
            this.pnlFundoDev.Controls.Add(this.picLogo);
            this.pnlFundoDev.Location = new System.Drawing.Point(0, 0);
            this.pnlFundoDev.Name = "pnlFundoDev";
            this.pnlFundoDev.Size = new System.Drawing.Size(270, 200);
            this.pnlFundoDev.TabIndex = 12;
            // 
            // btnSair
            // 
            this.btnSair.Location = new System.Drawing.Point(104, 140);
            this.btnSair.LookAndFeel.SkinName = "Blue";
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(75, 23);
            this.btnSair.TabIndex = 8;
            this.btnSair.Text = "&Sair";
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnEntrar
            // 
            this.btnEntrar.Location = new System.Drawing.Point(20, 140);
            this.btnEntrar.LookAndFeel.SkinName = "Blue";
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(75, 23);
            this.btnEntrar.TabIndex = 7;
            this.btnEntrar.Text = "&Entrar";
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // cbxAlias
            // 
            this.cbxAlias.Location = new System.Drawing.Point(20, 115);
            this.cbxAlias.Name = "cbxAlias";
            this.cbxAlias.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxAlias.Properties.LookAndFeel.SkinName = "Blue";
            this.cbxAlias.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxAlias.Size = new System.Drawing.Size(230, 20);
            this.cbxAlias.TabIndex = 6;
            // 
            // txtSenha
            // 
            this.txtSenha.EditValue = "";
            this.txtSenha.Location = new System.Drawing.Point(20, 75);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Properties.LookAndFeel.SkinName = "Blue";
            this.txtSenha.Properties.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(230, 20);
            this.txtSenha.TabIndex = 5;
            // 
            // txtUsuario
            // 
            this.txtUsuario.EditValue = "";
            this.txtUsuario.Location = new System.Drawing.Point(20, 35);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUsuario.Properties.LookAndFeel.SkinName = "Blue";
            this.txtUsuario.Size = new System.Drawing.Size(230, 20);
            this.txtUsuario.TabIndex = 4;
            // 
            // lblVersao
            // 
            this.lblVersao.Location = new System.Drawing.Point(20, 170);
            this.lblVersao.LookAndFeel.SkinName = "Blue";
            this.lblVersao.Name = "lblVersao";
            this.lblVersao.Size = new System.Drawing.Size(37, 13);
            this.lblVersao.TabIndex = 3;
            this.lblVersao.Text = "Versão:";
            // 
            // lblAliasDev
            // 
            this.lblAliasDev.Location = new System.Drawing.Point(20, 100);
            this.lblAliasDev.LookAndFeel.SkinName = "Blue";
            this.lblAliasDev.Name = "lblAliasDev";
            this.lblAliasDev.Size = new System.Drawing.Size(47, 13);
            this.lblAliasDev.TabIndex = 2;
            this.lblAliasDev.Text = "Conexão:";
            // 
            // lblSenhaDev
            // 
            this.lblSenhaDev.Location = new System.Drawing.Point(20, 60);
            this.lblSenhaDev.LookAndFeel.SkinName = "Blue";
            this.lblSenhaDev.Name = "lblSenhaDev";
            this.lblSenhaDev.Size = new System.Drawing.Size(34, 13);
            this.lblSenhaDev.TabIndex = 1;
            this.lblSenhaDev.Text = "Senha:";
            // 
            // lblUsuarioDev
            // 
            this.lblUsuarioDev.Location = new System.Drawing.Point(20, 20);
            this.lblUsuarioDev.LookAndFeel.SkinName = "Blue";
            this.lblUsuarioDev.Name = "lblUsuarioDev";
            this.lblUsuarioDev.Size = new System.Drawing.Size(40, 13);
            this.lblUsuarioDev.TabIndex = 0;
            this.lblUsuarioDev.Text = "Usuário:";
            // 
            // dlfSkin
            // 
            this.dlfSkin.LookAndFeel.SkinName = "Blue";
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 200);
            this.Controls.Add(this.pnlFundoDev);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hino ERP";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.Shown += new System.EventHandler(this.frmLogin_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmLogin_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFundoDev)).EndInit();
            this.pnlFundoDev.ResumeLayout(false);
            this.pnlFundoDev.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxAlias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSenha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogo;
        private DevExpress.XtraEditors.PanelControl pnlFundoDev;
        private DevExpress.XtraEditors.SimpleButton btnSair;
        private DevExpress.XtraEditors.SimpleButton btnEntrar;
        private DevExpress.XtraEditors.ComboBoxEdit cbxAlias;
        private DevExpress.XtraEditors.TextEdit txtSenha;
        private DevExpress.XtraEditors.TextEdit txtUsuario;
        private DevExpress.XtraEditors.LabelControl lblVersao;
        private DevExpress.XtraEditors.LabelControl lblAliasDev;
        private DevExpress.XtraEditors.LabelControl lblSenhaDev;
        private DevExpress.XtraEditors.LabelControl lblUsuarioDev;
        private DevExpress.LookAndFeel.DefaultLookAndFeel dlfSkin;
    }
}

