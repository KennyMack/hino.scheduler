﻿namespace Hino.Scheduler
{
    partial class frmVsLoadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVsLoadData));
            this.sccMove = new DevExpress.XtraEditors.SplitterControl();
            this.xtcDetail = new DevExpress.XtraTab.XtraTabControl();
            this.xtbpJobs = new DevExpress.XtraTab.XtraTabPage();
            this.gcJobs = new DevExpress.XtraGrid.GridControl();
            this.gvJobs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtbResources = new DevExpress.XtraTab.XtraTabPage();
            this.xtbpDemand = new DevExpress.XtraTab.XtraTabPage();
            this.gcDemands = new DevExpress.XtraGrid.GridControl();
            this.gvDemands = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcResource = new DevExpress.XtraGrid.GridControl();
            this.gvResource = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcDetail)).BeginInit();
            this.xtcDetail.SuspendLayout();
            this.xtbpJobs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvJobs)).BeginInit();
            this.xtbResources.SuspendLayout();
            this.xtbpDemand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDemands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDemands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvResource)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(936, 28);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 520);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 28);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 492);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(936, 28);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 492);
            // 
            // icImagens16
            // 
            this.icImagens16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImagens16.ImageStream")));
            this.icImagens16.Images.SetKeyName(0, "Novo16.png");
            this.icImagens16.Images.SetKeyName(1, "Editar 16.png");
            this.icImagens16.Images.SetKeyName(2, "Excluir_16.png");
            this.icImagens16.Images.SetKeyName(3, "Atualizar 16.png");
            this.icImagens16.Images.SetKeyName(4, "Imprimir 16.png");
            this.icImagens16.Images.SetKeyName(5, "Visualizar_Reg_16.png");
            this.icImagens16.Images.SetKeyName(6, "Configuração para Importação de Dados 16.png");
            this.icImagens16.Images.SetKeyName(7, "Gráfico 16.png");
            this.icImagens16.Images.SetKeyName(8, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(9, "Layout 16.png");
            this.icImagens16.Images.SetKeyName(10, "Filtro 16.png");
            this.icImagens16.Images.SetKeyName(11, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(12, "Definidos 16.png");
            this.icImagens16.Images.SetKeyName(13, "Primeiro16.png");
            this.icImagens16.Images.SetKeyName(14, "Anterior16.png");
            this.icImagens16.Images.SetKeyName(15, "Próximo16.png");
            this.icImagens16.Images.SetKeyName(16, "Último16.png");
            this.icImagens16.Images.SetKeyName(17, "Log 16.png");
            this.icImagens16.Images.SetKeyName(18, "Anexos 16.png");
            this.icImagens16.Images.SetKeyName(19, "Ajuda 16.png");
            this.icImagens16.Images.SetKeyName(20, "calculadora 16.png");
            // 
            // gcGrid
            // 
            this.gcGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcGrid.Location = new System.Drawing.Point(0, 28);
            this.gcGrid.Size = new System.Drawing.Size(936, 220);
            // 
            // gvVisao
            // 
            this.gvVisao.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gvVisao.OptionsBehavior.ReadOnly = true;
            this.gvVisao.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gvVisao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvVisao.OptionsPrint.AutoWidth = false;
            this.gvVisao.OptionsPrint.ExpandAllDetails = true;
            this.gvVisao.OptionsPrint.PrintDetails = true;
            this.gvVisao.OptionsPrint.PrintFilterInfo = true;
            this.gvVisao.OptionsView.BestFitMaxRowCount = 100;
            this.gvVisao.OptionsView.ColumnAutoWidth = false;
            this.gvVisao.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gvVisao.OptionsView.ShowAutoFilterRow = true;
            this.gvVisao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gvVisao.OptionsView.ShowFooter = true;
            this.gvVisao.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvVisao_FocusedRowChanged);
            // 
            // bbiEditar
            // 
            this.bbiEditar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // sccMove
            // 
            this.sccMove.Dock = System.Windows.Forms.DockStyle.Top;
            this.sccMove.Location = new System.Drawing.Point(0, 248);
            this.sccMove.Name = "sccMove";
            this.sccMove.Size = new System.Drawing.Size(936, 6);
            this.sccMove.TabIndex = 9;
            this.sccMove.TabStop = false;
            // 
            // xtcDetail
            // 
            this.xtcDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtcDetail.Location = new System.Drawing.Point(0, 254);
            this.xtcDetail.Name = "xtcDetail";
            this.xtcDetail.SelectedTabPage = this.xtbpJobs;
            this.xtcDetail.Size = new System.Drawing.Size(936, 266);
            this.xtcDetail.TabIndex = 10;
            this.xtcDetail.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtbpJobs,
            this.xtbResources,
            this.xtbpDemand});
            // 
            // xtbpJobs
            // 
            this.xtbpJobs.Controls.Add(this.gcJobs);
            this.xtbpJobs.Name = "xtbpJobs";
            this.xtbpJobs.Size = new System.Drawing.Size(931, 117);
            this.xtbpJobs.Text = "Ordens";
            // 
            // gcJobs
            // 
            this.gcJobs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcJobs.Location = new System.Drawing.Point(0, 0);
            this.gcJobs.MainView = this.gvJobs;
            this.gcJobs.MenuManager = this.bmControleBarra;
            this.gcJobs.Name = "gcJobs";
            this.gcJobs.Size = new System.Drawing.Size(931, 117);
            this.gcJobs.TabIndex = 1;
            this.gcJobs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvJobs});
            // 
            // gvJobs
            // 
            this.gvJobs.GridControl = this.gcJobs;
            this.gvJobs.Name = "gvJobs";
            // 
            // xtbResources
            // 
            this.xtbResources.Controls.Add(this.gcResource);
            this.xtbResources.Name = "xtbResources";
            this.xtbResources.Size = new System.Drawing.Size(931, 240);
            this.xtbResources.Text = "Recursos";
            // 
            // xtbpDemand
            // 
            this.xtbpDemand.Controls.Add(this.gcDemands);
            this.xtbpDemand.Name = "xtbpDemand";
            this.xtbpDemand.Size = new System.Drawing.Size(931, 119);
            this.xtbpDemand.Text = "Demandas";
            // 
            // gcDemands
            // 
            this.gcDemands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDemands.Location = new System.Drawing.Point(0, 0);
            this.gcDemands.MainView = this.gvDemands;
            this.gcDemands.MenuManager = this.bmControleBarra;
            this.gcDemands.Name = "gcDemands";
            this.gcDemands.Size = new System.Drawing.Size(931, 119);
            this.gcDemands.TabIndex = 0;
            this.gcDemands.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDemands});
            // 
            // gvDemands
            // 
            this.gvDemands.GridControl = this.gcDemands;
            this.gvDemands.Name = "gvDemands";
            // 
            // gcResource
            // 
            this.gcResource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcResource.Location = new System.Drawing.Point(0, 0);
            this.gcResource.MainView = this.gvResource;
            this.gcResource.MenuManager = this.bmControleBarra;
            this.gcResource.Name = "gcResource";
            this.gcResource.Size = new System.Drawing.Size(931, 240);
            this.gcResource.TabIndex = 0;
            this.gcResource.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvResource});
            // 
            // gvResource
            // 
            this.gvResource.GridControl = this.gcResource;
            this.gvResource.Name = "gvResource";
            // 
            // frmVsLoadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 520);
            this.Controls.Add(this.xtcDetail);
            this.Controls.Add(this.sccMove);
            this.Name = "frmVsLoadData";
            this.Text = "Dados do ERP/Cenários";
            this.Load += new System.EventHandler(this.frmVsLoadData_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gcGrid, 0);
            this.Controls.SetChildIndex(this.sccMove, 0);
            this.Controls.SetChildIndex(this.xtcDetail, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcDetail)).EndInit();
            this.xtcDetail.ResumeLayout(false);
            this.xtbpJobs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvJobs)).EndInit();
            this.xtbResources.ResumeLayout(false);
            this.xtbpDemand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDemands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDemands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvResource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitterControl sccMove;
        private DevExpress.XtraTab.XtraTabControl xtcDetail;
        private DevExpress.XtraTab.XtraTabPage xtbpJobs;
        private DevExpress.XtraTab.XtraTabPage xtbResources;
        private DevExpress.XtraGrid.GridControl gcJobs;
        private DevExpress.XtraGrid.Views.Grid.GridView gvJobs;
        private DevExpress.XtraTab.XtraTabPage xtbpDemand;
        private DevExpress.XtraGrid.GridControl gcDemands;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDemands;
        private DevExpress.XtraGrid.GridControl gcResource;
        private DevExpress.XtraGrid.Views.Grid.GridView gvResource;
    }
}