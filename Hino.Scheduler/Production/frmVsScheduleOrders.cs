﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsScheduleOrders : frmVsPadrao
    {
        private ScheduleOrdersVM _ScheduleOrdersVM;

        public frmVsScheduleOrders()
        {
            InitializeComponent();
            _ScheduleOrdersVM = new ScheduleOrdersVM();
        }

        private async void frmVsScheduleOrders_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            await _ScheduleOrdersVM.LoadData();
            gcGrid.DataSource = _ScheduleOrdersVM._DataSource;

            if (_ScheduleOrdersVM.Errors.Count > 0)
            {
                MessageBox.Show(_ScheduleOrdersVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                fmtGridView.OcultaColuna(
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["labelop"],
                    gvVisao.Columns["statusop"]
                 );
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }

        private async void bbiUpdateSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            await _ScheduleOrdersVM.UpdateScheduledOrders();
            if (_ScheduleOrdersVM.Errors.Count > 0)
            {
                MessageBox.Show(_ScheduleOrdersVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }
        }

        ~frmVsScheduleOrders()
        {
            _ScheduleOrdersVM.Dispose();
        }

        private void gvVisao_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            fmtGridView.ColoreColuna(e, "desclabelop", "Pendente", Color.Yellow);
            fmtGridView.ColoreColuna(e, "desclabelop", "Andamento", Color.LightSkyBlue);
            fmtGridView.ColoreColuna(e, "desclabelop", "Concluido", Color.PaleGreen);
            fmtGridView.ColoreColuna(e, "desclabelop", "Cancelado", Color.Yellow);

            fmtGridView.ColoreColuna(e, "descstatusop", "Sem Prioridade", Color.White);
            fmtGridView.ColoreColuna(e, "descstatusop", "Urgente", System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(0))))));
            fmtGridView.ColoreColuna(e, "descstatusop", "Andamento", System.Drawing.Color.SkyBlue);
            fmtGridView.ColoreColuna(e, "descstatusop", "Atrasado", System.Drawing.Color.Salmon);
            fmtGridView.ColoreColuna(e, "descstatusop", "Aguardando", System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))));
        }
    }
}

