﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Utils;
using DevExpress.XtraScheduler.Drawing;
using System.Collections.ObjectModel;

namespace Hino.Scheduler.Production
{
    public partial class frmScScheduleOrdersSimulate : frmScPadrao
    {
        private readonly ScheduleOrdersSimulVM _ScheduleOrdersSimulVM;
        public frmScScheduleOrdersSimulate()
        {
            InitializeComponent();
            _ScheduleOrdersSimulVM = new ScheduleOrdersSimulVM();
            beiTimeParts.EditValue = 60;
            beiDtInitial.EditValue = DateTime.Now;

            scSchedulerOP.DefaultProperties(SchedulerViewType.Gantt);
            this.OnRefreshScenario += FrmScScheduleOrdersSimulate_OnRefreshScenario;
            scSchedulerOP.AppointmentDrag += fmtSchedulerControl.AppointmentDrag;
            scSchedulerOP.AppointmentDrop += fmtSchedulerControl.AppointmentDrag;
        }

        private async void FrmScScheduleOrdersSimulate_OnRefreshScenario(object sender, EventArgs e) =>
            await BotaoAtualizar();

        private void frmScScheduleOrdersSimulate_Load(object sender, EventArgs e)
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";
        }

        private void MenuItemSelectOP_Click(object sender, EventArgs e)
        {
            if (scSchedulerOP.SelectedAppointments.Count > 0)
            {
                var selected = scSchedulerOP.SelectedAppointments[0];

                foreach (var item in ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() ==
                    selected.CustomFields["Codordprod"].ToString()))
                    scSchedulerOP.SelectedAppointments.Add(item);
            }
        }

        #region Botão Atualizar
        private async Task BotaoAtualizar()
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";

            _ScheduleOrdersSimulVM._IdScenario = Parameters.IdScenario;
            await _ScheduleOrdersSimulVM.LoadData();
            if (_ScheduleOrdersSimulVM.Errors.Count > 0)
                MessageBox.Show(_ScheduleOrdersSimulVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            pdResourceBindingSource.DataSource = null;
            pdResourceBindingSource.DataSource = null;

            pdResourceBindingSource.DataSource = _ScheduleOrdersSimulVM._ResourceDataSource;
            pdSimulJobsRotBindingSource.DataSource = _ScheduleOrdersSimulVM._JobRotDataSource;
            scSchedulerOP.Refresh();
            scSchedulerOP.AdjustRowHeight();
        }
        #endregion

        private void scSchedulerOP_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e) =>
            scSchedulerOP.AppointmentCustomizing(e,
                (beiHighlightOP.EditValue ?? "").ToString(),
                bciShowStatus.Checked);

        private async void bbiBotaoAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private async void bbiAplicarAlteracoes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var ask = MessageBox.Show("Confirma aplicar essa simulação no ERP ?\r\n*Obs.:Essa operação não pode ser desfeita", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (ask == DialogResult.Yes)
            {
                ssManager.ShowWaitForm();
                if (await _ScheduleOrdersSimulVM.SaveOPChangesToERP())
                    MessageBox.Show("Alterações salvas com sucesso.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                else
                    MessageBox.Show(_ScheduleOrdersSimulVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                scSchedulerOP.Refresh();
                ssManager.CloseWaitForm();
            }
        }

        private async void ssSchedulerOP_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            var Appointment = ((IList<Appointment>)e.Objects);

            await _ScheduleOrdersSimulVM.SaveChangesAppointments(Appointment);
            scSchedulerOP.Refresh();
        }

        private void bbiGridSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var frm = GetForm(typeof(frmVsScheduleOrders), "frmVsScheduleOrders");
            frm.MdiParent = this.MdiParent;
            frm.Show();
            frm.BringToFront();
            this.Close();
        }

        ~frmScScheduleOrdersSimulate()
        {
            pdResourceBindingSource.Dispose();
            pdSimulJobsRotBindingSource.Dispose();
            _ScheduleOrdersSimulVM.Dispose();
        }

        private async void frmScScheduledOrders_Shown(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private void scSchedulerOP_SelectionChanged(object sender, EventArgs e)
        {
            scSchedulerOP.Refresh();
            ssSchedulerOP.AppointmentDependencies.Clear();

            if (scSchedulerOP.SelectedAppointments.Any() && bciCheckDependencies.Checked)
            {
                var Ops = scSchedulerOP.SelectedAppointments.Select(r => r.CustomFields["Codordprod"].ToString()).Distinct();

                foreach (var item in Ops)
                {
                    var childsOP = ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() == item).OrderBy(r => r.Id);
                    if (childsOP.Count() > 1)
                    {
                        var FirstParent = childsOP.FirstOrDefault();
                        if (FirstParent != null)
                        {
                            foreach (var child in childsOP)
                            {
                                var dependencies = ssSchedulerOP.Appointments.Items.Where(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() == child.CustomFields["Nivelordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() != r.CustomFields["Nivelordprod"].ToString()
                                );

                                var parent = ssSchedulerOP.Appointments.Items.FirstOrDefault(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprod"].ToString() == child.CustomFields["Nivelordprod"].ToString()
                                );

                                if (parent != null && dependencies.Count() > 0)
                                {
                                    foreach (var dep in dependencies)
                                    {
                                        AppointmentDependency aptDependency = ssSchedulerOP.AppointmentDependencies.CreateAppointmentDependency(dep.Id, parent.Id);
                                        ssSchedulerOP.AppointmentDependencies.Add(aptDependency);
                                        aptDependency.Type = AppointmentDependencyType.FinishToStart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void beiHighlightOP_EditValueChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            Hino.Scheduler.Production.frmAppointmentDetailRot form = 
                new Hino.Scheduler.Production.frmAppointmentDetailRot(scheduler, e.Appointment, false);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }
        }

        private void bciShowStatus_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_CustomDrawAppointmentBackground(object sender, CustomDrawObjectEventArgs e) =>
            fmtSchedulerControl.CustomDraw(beiShowPriority.Checked, sender, e);

        private void scSchedulerOP_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)=>
            fmtSchedulerControl.PopupMenuShowing(e.Menu,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void Menu_CloseUp(object sender, EventArgs e) =>
            fmtSchedulerControl.CloseMenu(sender, e,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void beiShowPriority_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void ssSchedulerOP_AppointmentChanging(object sender, PersistentObjectCancelEventArgs e)
        {
            var resourceSelected = (Appointment)(e.Object);
            var resource = ssSchedulerOP.Resources.Items.Where(r => r.Id == resourceSelected.ResourceId).FirstOrDefault();

            if (resource != null &&
                resource.CustomFields["Codmaquina"] == null)
            {

                MessageBox.Show("Selecione uma máquina.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }

        private void beiTimeParts_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                _ScheduleOrdersSimulVM._TimeParts = Convert.ToInt32(beiTimeParts.EditValue);
            }
            catch (Exception)
            {
                beiTimeParts.EditValue = 60;
                _ScheduleOrdersSimulVM._TimeParts = 60;
            }
        }

        private void beiDtInitial_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                _ScheduleOrdersSimulVM._DtIntial = Convert.ToDateTime(beiDtInitial.EditValue);
            }
            catch (Exception)
            {
                beiDtInitial.EditValue = DateTime.Now;
                _ScheduleOrdersSimulVM._DtIntial = DateTime.Now;
            }

        }

        private async void bbiSimulateOperations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("Todos as alterações não salvas serão perdidas.\r\nDeseja continuar?", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) ==
                DialogResult.Yes)
            {
                ssManager.ShowWaitForm();
                _ScheduleOrdersSimulVM._DtIntial = Convert.ToDateTime(beiDtInitial.EditValue);
                _ScheduleOrdersSimulVM._TimeParts = Convert.ToInt32(beiTimeParts.EditValue);
                await _ScheduleOrdersSimulVM.GeneratedJobSchedule();
                if (_ScheduleOrdersSimulVM.Errors.Count > 0)
                    MessageBox.Show(_ScheduleOrdersSimulVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                await BotaoAtualizar();
                ssManager.CloseWaitForm();
            }
        }

        private void scSchedulerOP_AllowAppointmentConflicts(object sender, AppointmentConflictEventArgs e)
        {
            e.Conflicts.Clear();

            if (e.AppointmentClone.Duration.TotalMinutes < Convert.ToDouble(e.Appointment.CustomFields["Minimaltime"]) && 
                bciCheckDependencies.Checked)
            {
                e.Conflicts.Add(e.AppointmentClone);
                MessageBox.Show("Tempo da O.P. é menor que o mínimo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

            AppointmentDependencyBaseCollection depCollectionDep =
                ssSchedulerOP.AppointmentDependencies.Items.GetDependenciesByDependentId(e.Appointment.Id);
            if (depCollectionDep.Count > 0)
            {
                if (CheckForInvalidDependenciesAsDependent(depCollectionDep, e.AppointmentClone))
                    e.Conflicts.Add(e.AppointmentClone);
            }

            AppointmentDependencyBaseCollection depCollectionPar =
                ssSchedulerOP.AppointmentDependencies.Items.GetDependenciesByParentId(e.Appointment.Id);
            if (depCollectionPar.Count > 0)
            {
                if (CheckForInvalidDependenciesAsParent(depCollectionPar, e.AppointmentClone))
                    e.Conflicts.Add(e.AppointmentClone);
            }
        }

        private bool CheckForInvalidDependenciesAsDependent(AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.ParentId).End;
                    if (apt.Start < checkTime)
                        return true;
                }
            }
            return false;
        }

        private bool CheckForInvalidDependenciesAsParent(AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.DependentId).Start;
                    if (apt.End > checkTime)
                        return true;
                }
            }
            return false;
        }

        private void ttcMessages_BeforeShow(object sender, DevExpress.Utils.ToolTipControllerShowEventArgs e) =>
            fmtSchedulerControl.CustomToolTip(ttcMessages, sender, e);
    }
}