﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Utils;

namespace Hino.Scheduler.Production
{
    public partial class frmScScheduledOrdersRot : frmScPadrao
    {
        private readonly ScheduledOrdersRotViewVM _ScheduledOrdersRotViewVM;
        public frmScScheduledOrdersRot()
        {
            InitializeComponent();
            _ScheduledOrdersRotViewVM = new ScheduledOrdersRotViewVM();
            scSchedulerOP.DefaultProperties(SchedulerViewType.Gantt);
            this.OnRefreshScenario += FrmScScheduledOrdersRot_OnRefreshScenario;
            scSchedulerOP.AppointmentDrag += fmtSchedulerControl.AppointmentDrag;
            scSchedulerOP.AppointmentDrop += fmtSchedulerControl.AppointmentDrag;
        }

        private async void FrmScScheduledOrdersRot_OnRefreshScenario(object sender, EventArgs e) =>
            await BotaoAtualizar();

        private void frmScScheduledOrdersRot_Load(object sender, EventArgs e)
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";
        }

        private void MenuItemSelectOP_Click(object sender, EventArgs e)
        {
            if (scSchedulerOP.SelectedAppointments.Count > 0)
            {
                var selected = scSchedulerOP.SelectedAppointments[0];

                foreach (var item in ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() ==
                    selected.CustomFields["Codordprod"].ToString()))
                    scSchedulerOP.SelectedAppointments.Add(item);
            }
        }

        #region Botão Atualizar
        private async Task BotaoAtualizar()
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";

            _ScheduledOrdersRotViewVM._IdScenario = Parameters.IdScenario;
            await _ScheduledOrdersRotViewVM.LoadData();

            if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            pdResourceBindingSource.DataSource = _ScheduledOrdersRotViewVM._ResourceDataSource;
            pdJobRotBindingSource.DataSource = _ScheduledOrdersRotViewVM._JobRotDataSource;
        }
        #endregion

        private void scSchedulerOP_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e) =>
            scSchedulerOP.AppointmentCustomizing(e,
                (beiHighlightOP.EditValue ?? "").ToString(),
                bciShowStatus.Checked);

        private async void bbiBotaoAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private async void bbiAplicarAlteracoes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var ask = MessageBox.Show("Confirma aplicar alterações no ERP ?\r\n*Obs.:Essa operação não pode ser desfeita", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (ask == DialogResult.Yes)
            {
                ssManager.ShowWaitForm();
                if (await _ScheduledOrdersRotViewVM.SaveOPChangesToERP())
                    MessageBox.Show("Alterações salvas com sucesso.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                else
                    MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                scSchedulerOP.Refresh();
                ssManager.CloseWaitForm();
            }
        }

        private async void ssSchedulerOP_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            var Appointment = ((IList<Appointment>)e.Objects);

            await _ScheduledOrdersRotViewVM.SaveChangesAppointments(Appointment);
            scSchedulerOP.Refresh();
        }

        private void bbiGridSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var frm = GetForm(typeof(frmVsScheduleOrders), "frmVsScheduleOrders");
            frm.MdiParent = this.MdiParent;
            frm.Show();
            frm.BringToFront();
            this.Close();
        }

        ~frmScScheduledOrdersRot()
        {
            pdResourceBindingSource.Dispose();
            pdJobRotBindingSource.Dispose();
            _ScheduledOrdersRotViewVM.Dispose();
        }

        private async void frmScScheduledOrders_Shown(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private void scSchedulerOP_SelectionChanged(object sender, EventArgs e)
        {
            scSchedulerOP.Refresh();
            ssSchedulerOP.AppointmentDependencies.Clear();

            if (scSchedulerOP.SelectedAppointments.Any() && bciCheckDependencies.Checked)
            {
                var Ops = scSchedulerOP.SelectedAppointments.Select(r => r.CustomFields["Codordprod"].ToString()).Distinct();

                foreach (var item in Ops)
                {
                    var childsOP = ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() == item).OrderBy(r => r.Id);
                    if (childsOP.Count() > 1)
                    {
                        var FirstParent = childsOP.FirstOrDefault();
                        if (FirstParent != null)
                        {
                            foreach (var child in childsOP)
                            {
                                var dependencies = ssSchedulerOP.Appointments.Items.Where(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() == child.CustomFields["Nivelordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() != r.CustomFields["Nivelordprod"].ToString()
                                );

                                var parent = ssSchedulerOP.Appointments.Items.FirstOrDefault(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprod"].ToString() == child.CustomFields["Nivelordprod"].ToString()
                                );

                                if (parent != null && dependencies.Count() > 0)
                                {
                                    foreach (var dep in dependencies)
                                    {
                                        AppointmentDependency aptDependency = ssSchedulerOP.AppointmentDependencies.CreateAppointmentDependency(parent.Id, dep.Id);
                                        ssSchedulerOP.AppointmentDependencies.Add(aptDependency);
                                        aptDependency.Type = AppointmentDependencyType.FinishToFinish;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void beiHighlightOP_EditValueChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            Hino.Scheduler.Production.frmAppointmentDetailRot form = 
                new Hino.Scheduler.Production.frmAppointmentDetailRot(scheduler, e.Appointment, false);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }
        }

        private void bciShowStatus_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_CustomDrawAppointmentBackground(object sender, CustomDrawObjectEventArgs e) =>
            fmtSchedulerControl.CustomDraw(false, sender, e);

        private void scSchedulerOP_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)=>
            fmtSchedulerControl.PopupMenuShowing(e.Menu,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void Menu_CloseUp(object sender, EventArgs e) =>
            fmtSchedulerControl.CloseMenu(sender, e,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void ssSchedulerOP_AppointmentChanging(object sender, PersistentObjectCancelEventArgs e)
        {
            var resourceSelected = (Appointment)(e.Object);
            var resource = ssSchedulerOP.Resources.Items.Where(r => r.Id == resourceSelected.ResourceId).FirstOrDefault();

            if (resource != null &&
                resource.CustomFields["Codmaquina"] == null)
            {
                MessageBox.Show("Selecione uma máquina.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }

        private void scSchedulerOP_AllowAppointmentConflicts(object sender, AppointmentConflictEventArgs e)
        {
            try
            {
                
                fmtSchedulerControl.AppointmentConflicts(ssSchedulerOP, bciCheckDependencies.Checked, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void ttcMessages_BeforeShow(object sender, DevExpress.Utils.ToolTipControllerShowEventArgs e) =>
            fmtSchedulerControl.CustomToolTip(ttcMessages, sender, e);
    }
}