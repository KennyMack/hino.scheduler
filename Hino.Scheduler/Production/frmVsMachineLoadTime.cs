﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsMachineLoadTime : frmVsPadrao
    {

        public DateTime _DtInitial
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtInitial);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtInitial, value);
            }
        }

        public DateTime _DtEnd
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtEnd);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtEnd, value);
            }
        }

        public frmVsMachineLoadTime()
        {
            InitializeComponent();
            _DtInitial = DateTime.Now.FirstDay();
            _DtEnd = DateTime.Now.LastDay();
        }

        private async void sbCarregar_Click(object sender, EventArgs e)
        {
            await BotaoAtualizar();
        }

        private async void frmVsMachineLoad_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false; 
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            using (var _MaquinasRepository = new MaquinasRepository())
            using (var _MaquinasService = new MaquinasService(_MaquinasRepository))
            {
                gcGrid.DataSource = new System.ComponentModel.BindingList<pdMachineCellLoad>(
                    await _MaquinasService.QueryMachineCellLoad(Parameters.codEstab,
                    _DtInitial, _DtEnd));
            }
            if (_Initialize)
            {
                gvVisao.Columns["codcelula"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codmaquina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codrotina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);

                fmtGridView.OcultaColuna(
                    gvVisao.Columns["minutosprod"],
                    gvVisao.Columns["minprodcelula"],
                    gvVisao.Columns["minstrabdiamaquina"],
                    gvVisao.Columns["minstrabdiacelula"]
                );

                gvVisao.BestFitColumns();
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion
    }
}
