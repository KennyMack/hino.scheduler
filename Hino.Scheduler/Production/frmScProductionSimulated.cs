﻿using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmScProductionSimulated : frmScPadrao
    {
        /*public DateTime _DtIntial
        {
            get
            {
                return _ScheduledOrdersRotViewVM._DtIntial;
            }
            set
            {
                _ScheduledOrdersRotViewVM._DtIntial = value;
                beiDtInitial.EditValue = value;
            }
        }

        public DateTime _DtEnd
        {
            get
            {
                return _ScheduledOrdersRotViewVM._DtEnd;
            }
            set
            {
                _ScheduledOrdersRotViewVM._DtEnd = value;
                beiDtEnd.EditValue = value;
            }
        }*/


        private readonly ScheduledOrdersRotViewVM _ScheduledOrdersRotViewVM;
        public frmScProductionSimulated()
        {
            InitializeComponent();
            _ScheduledOrdersRotViewVM = new ScheduledOrdersRotViewVM();
            //_DtIntial = DateTime.Now.FirstDay().FirstHour();
            //_DtEnd = DateTime.Now.LastDay().LastHour();

            gvDemandas.DefinePadrao();
            gvDemandas.OptionsBehavior.Editable = false;

            scSchedulerOP.DefaultProperties(SchedulerViewType.Timeline);
            scSchedulerOP.AppointmentDrag += fmtSchedulerControl.AppointmentDrag;
            scSchedulerOP.AppointmentDrop += fmtSchedulerControl.AppointmentDrag;
        }

        private void MenuItemSelectOP_Click(object sender, EventArgs e)
        {
            if (scSchedulerOP.SelectedAppointments.Count > 0)
            {
                var selected = scSchedulerOP.SelectedAppointments[0];

                foreach (var item in ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() ==
                    selected.CustomFields["Codordprod"].ToString()))
                    scSchedulerOP.SelectedAppointments.Add(item);
            }
        }

        #region Botão Atualizar
        private  Task BotaoAtualizar()
        {
            //await _ScheduledOrdersRotViewVM.LoadDataDemandas();
            if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            //await _ScheduledOrdersRotViewVM.ReloadDemandas();
            if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            gcDemandas.DataSource = _ScheduledOrdersRotViewVM._DemandasDataSource;

            if (_Initialize)
            {
                gvDemandas.BestFitColumns();

                fmtGridView.OcultaColuna(
                    gvDemandas.Columns["codestab"],
                    gvDemandas.Columns["iddemanda"]);
            }

            pdResourceBindingSource.DataSource = _ScheduledOrdersRotViewVM._ResourceDataSource;
            pdJobRotBindingSource.DataSource = _ScheduledOrdersRotViewVM._JobRotDataSource;
            return null;
        }
        #endregion

        private void scSchedulerOP_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e) =>
            scSchedulerOP.AppointmentCustomizing(e,
                (beiHighlightOP.EditValue ?? "").ToString(),
                bciShowStatus.Checked);

        private async void bbiBotaoAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (MessageBox.Show("Todos as alterações não salvas serão perdidas.\r\nDeseja continuar?", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) ==
                DialogResult.Yes)
            {
                ssManager.ShowWaitForm();
                await _ScheduledOrdersRotViewVM.GeneratedJobSchedule(true, false);
                if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                    MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                await BotaoAtualizar();
                ssManager.CloseWaitForm();
            }
        }

        private async void ssSchedulerOP_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            var Appointment = ((IList<Appointment>)e.Objects);

            await _ScheduledOrdersRotViewVM.SaveChangesAppointments(Appointment);
            scSchedulerOP.Refresh();
        }

        private void bbiGridSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var frm = GetForm(typeof(frmVsScheduleOrders), "frmVsScheduleOrders");
            frm.MdiParent = this.MdiParent;
            frm.Show();
            frm.BringToFront();
            this.Close();
        }

        ~frmScProductionSimulated()
        {
            pdResourceBindingSource.Dispose();
            pdJobRotBindingSource.Dispose();
            _ScheduledOrdersRotViewVM.Dispose();
        }

        private async void frmScProductionSimulated_Shown(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
            _Initialize = false;
        }

        private void scSchedulerOP_SelectionChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void beiHighlightOP_EditValueChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            Hino.Scheduler.Production.frmAppointmentDetailRot form =
                new Hino.Scheduler.Production.frmAppointmentDetailRot(scheduler, e.Appointment, false);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }
        }

        private void bciShowStatus_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_CustomDrawAppointmentBackground(object sender, CustomDrawObjectEventArgs e) =>
            fmtSchedulerControl.CustomDraw(false, sender, e);

        private void scSchedulerOP_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e) =>
            fmtSchedulerControl.PopupMenuShowing(e.Menu,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void Menu_CloseUp(object sender, EventArgs e) =>
            fmtSchedulerControl.CloseMenu(sender, e,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void ssSchedulerOP_AppointmentChanging(object sender, PersistentObjectCancelEventArgs e)
        {
            var resourceSelected = (Appointment)(e.Object);
            var resource = ssSchedulerOP.Resources.Items.Where(r => r.Id == resourceSelected.ResourceId).FirstOrDefault();

            if (resource != null &&
                resource.CustomFields["Codmaquina"] == null)
            {

                MessageBox.Show("Selecione uma máquina.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }
        }

        private async void beiDtInitial_EditValueChanged(object sender, EventArgs e) =>
            await ReloadDemandas();

        private async void beiDtEnd_EditValueChanged(object sender, EventArgs e) =>
            await ReloadDemandas();

        #region Reload Demandas
        private  Task ReloadDemandas()
        {
            //_DtIntial = Convert.ToDateTime(beiDtInitial.EditValue);
            //_DtEnd = Convert.ToDateTime(beiDtEnd.EditValue);
            gvDemandas.ShowLoadingPanel();
            //await _ScheduledOrdersRotViewVM.ReloadDemandas();
            if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            gcDemandas.DataSource = _ScheduledOrdersRotViewVM._DemandasDataSource;

            gvDemandas.HideLoadingPanel();
            return null;
        }
        #endregion

        private void gcDemandas_DragEnter(object sender, DragEventArgs e)
        {

        }

        private  void sbAdd_Click(object sender, EventArgs e)
        {/*
            if (gvDemandas.FocusedRowHandle > -1)
            {
                await _ScheduledOrdersRotViewVM.CreateNewJob(new Model.Sales.veDemanda
                {
                    iddemanda = 0,
                    codestab = Parameters.codEstab,
                    codpedvenda = Convert.ToInt64(gvDemandas.GetFocusedRowCellValue("codpedvenda")),
                    codproduto = Convert.ToString(gvDemandas.GetFocusedRowCellValue("codproduto")),
                    //descricao = Convert.ToString(gvDemandas.GetFocusedRowCellValue("descricao")),
                    //leadtime = Convert.ToDecimal(gvDemandas.GetFocusedRowCellValue("leadtime")),
                    dtinicio = Convert.ToDateTime(gvDemandas.GetFocusedRowCellValue("dtsuginiprod")),
                    dttermino = Convert.ToDateTime(gvDemandas.GetFocusedRowCellValue("dataprograma")),
                    sldprograma = Convert.ToDecimal(gvDemandas.GetFocusedRowCellValue("sldprograma")),
                    //saldoestoque = Convert.ToDecimal(gvDemandas.GetFocusedRowCellValue("saldoestoque")),
                    //qtdneccomposta = Convert.ToDecimal(gvDemandas.GetFocusedRowCellValue("qtdneccomposta")),
                    //sldestoquefinal = Convert.ToDecimal(gvDemandas.GetFocusedRowCellValue("sldestoquefinal"))
                });

                if (_ScheduledOrdersRotViewVM.Errors.Count > 0)
                    MessageBox.Show(_ScheduledOrdersRotViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                await BotaoAtualizar();
            }*/
        }

        private void sbRemove_Click(object sender, EventArgs e)
        {

        }
    }
}
