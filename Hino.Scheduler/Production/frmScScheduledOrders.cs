﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Utils;
using Hino.Scheduler.Model;
using DevExpress.Utils;
using DevExpress.XtraScheduler.Drawing;
using System.Drawing;

namespace Hino.Scheduler.Production
{
    public partial class frmScScheduledOrders : frmScPadrao
    {
        private readonly ScheduledOrdersViewVM _ScheduledOrdersViewVM;
        public frmScScheduledOrders()
        {
            InitializeComponent();
            _ScheduledOrdersViewVM = new ScheduledOrdersViewVM();
            
            scSchedulerOP.DefaultProperties(SchedulerViewType.Gantt);
            this.scSchedulerOP.OptionsCustomization.AllowAppointmentConflicts = AppointmentConflictsMode.Custom;
            this.OnRefreshScenario += FrmScScheduledOrders_OnRefreshScenario;
        }

        private async void FrmScScheduledOrders_OnRefreshScenario(object sender, EventArgs e) =>
            await BotaoAtualizar();

        private void MenuItemSelectOP_Click(object sender, EventArgs e)
        {
            if (scSchedulerOP.SelectedAppointments.Count > 0)
            {
                var selected = scSchedulerOP.SelectedAppointments[0];

                foreach (var item in ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() ==
                    selected.CustomFields["Codordprod"].ToString()))
                    scSchedulerOP.SelectedAppointments.Add(item);
            }
        }

        private void frmScScheduledOrders_Load(object sender, EventArgs e)
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";
        }

        #region Botão Atualizar
        private async Task BotaoAtualizar()
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";
            
            _ScheduledOrdersViewVM._IdScenario = Parameters.IdScenario;
            await _ScheduledOrdersViewVM.LoadData();

            if (_ScheduledOrdersViewVM.Errors.Count > 0)
                MessageBox.Show(_ScheduledOrdersViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            pdResourceBindingSource.DataSource = _ScheduledOrdersViewVM._ResourceDataSource;
            pdJobBindingSource.DataSource = _ScheduledOrdersViewVM._JobDataSource;
        }
        #endregion

        private void scSchedulerOP_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e) =>
            scSchedulerOP.AppointmentCustomizing(e,
                (beiHighlightOP.EditValue ?? "").ToString(),
                bciShowStatus.Checked);

        private async void bbiBotaoAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private async void bbiAplicarAlteracoes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var ask = MessageBox.Show("Confirma aplicar alterações no ERP ?\r\n*Obs.:Essa operação não pode ser desfeita", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (ask == DialogResult.Yes)
            {
                ssManager.ShowWaitForm();
                if (await _ScheduledOrdersViewVM.SaveOPChangesToERP())
                    MessageBox.Show("Alterações salvas com sucesso.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                else
                    MessageBox.Show(_ScheduledOrdersViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                scSchedulerOP.Refresh();
                ssManager.CloseWaitForm();
            }
        }

        private async void ssSchedulerOP_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            var Appointment = ((IList<Appointment>)e.Objects);

            await _ScheduledOrdersViewVM.SaveChangesAppointments(Appointment);
            scSchedulerOP.Refresh();
        }

        private void bbiGridSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var frm = GetForm(typeof(frmVsScheduleOrders), "frmVsScheduleOrders");
            frm.MdiParent = this.MdiParent;
            frm.Show();
            frm.BringToFront();
            this.Close();
        }

        ~frmScScheduledOrders()
        {
            pdResourceBindingSource.Dispose();
            pdJobBindingSource.Dispose();
            _ScheduledOrdersViewVM.Dispose();
        }

        private async void frmScScheduledOrders_Shown(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
            _Initialize = false;
        }

        void CreateDependencies(Appointment pAppointmentParent, long pCodOrdProd, string pNivelOrdProd)
        {
            var item = ssSchedulerOP.Appointments.Items.FirstOrDefault(r =>
                         r.CustomFields["Codordprod"].ToString() == pCodOrdProd.ToString() &&
                         r.CustomFields["Nivelordprodpai"].ToString() == pNivelOrdProd &&
                         r.CustomFields["Nivelordprodpai"].ToString() != r.CustomFields["Nivelordprod"].ToString()
                    );
            if (item != null)
            {
                AppointmentDependency aptDependency = ssSchedulerOP.AppointmentDependencies.CreateAppointmentDependency(pAppointmentParent.Id, item.Id);
                ssSchedulerOP.AppointmentDependencies.Add(aptDependency);
                aptDependency.Type = AppointmentDependencyType.FinishToStart;
            }
        }

        private void scSchedulerOP_SelectionChanged(object sender, EventArgs e)
        {
            scSchedulerOP.Refresh();
            ssSchedulerOP.AppointmentDependencies.Clear();

            if (scSchedulerOP.SelectedAppointments.Any() && bciCheckDependencies.Checked)
            {
                var Ops = scSchedulerOP.SelectedAppointments.Select(r => r.CustomFields["Codordprod"].ToString()).Distinct();

                foreach (var item in Ops)
                {
                    var childsOP = ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() == item).OrderBy(r => r.Id);
                    if (childsOP.Count() > 1)
                    {
                        var FirstParent = childsOP.FirstOrDefault();
                        if (FirstParent != null)
                        {
                            foreach (var child in childsOP)
                            {
                                var dependencies = ssSchedulerOP.Appointments.Items.Where(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() == child.CustomFields["Nivelordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() != r.CustomFields["Nivelordprod"].ToString()
                                );

                                var parent = ssSchedulerOP.Appointments.Items.FirstOrDefault(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprod"].ToString() == child.CustomFields["Nivelordprod"].ToString()
                                );

                                if (parent != null && dependencies.Count() > 0)
                                {
                                    foreach (var dep in dependencies)
                                    {
                                        AppointmentDependency aptDependency = ssSchedulerOP.AppointmentDependencies.CreateAppointmentDependency(dep.Id, parent.Id);
                                        ssSchedulerOP.AppointmentDependencies.Add(aptDependency);
                                        aptDependency.Type = AppointmentDependencyType.FinishToStart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void beiHighlightOP_EditValueChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            Hino.Scheduler.Production.frmAppointmentDetailRot form = 
                new Hino.Scheduler.Production.frmAppointmentDetailRot(scheduler, e.Appointment, false);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }
        }

        private void bciShowStatus_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_CustomDrawAppointmentBackground(object sender, CustomDrawObjectEventArgs e) =>
            fmtSchedulerControl.CustomDraw(beiShowPriority.Checked, sender, e);

        private void scSchedulerOP_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e) =>
            fmtSchedulerControl.PopupMenuShowing(e.Menu,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void Menu_CloseUp(object sender, EventArgs e) =>
            fmtSchedulerControl.CloseMenu(sender, e,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void beiShowPriority_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_AllowAppointmentConflicts(object sender, AppointmentConflictEventArgs e)
        {
            try
            {
                fmtSchedulerControl.AppointmentConflicts(ssSchedulerOP, bciCheckDependencies.Checked, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void ttcMessages_BeforeShow(object sender, ToolTipControllerShowEventArgs e) =>
            fmtSchedulerControl.CustomToolTip(ttcMessages, sender, e);
    }
}