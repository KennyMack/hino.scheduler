﻿namespace Hino.Scheduler.Production
{
    partial class frmVsScheduleOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVsScheduleOrders));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.bbiUpdateSchedule = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(936, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 371);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(936, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 371);
            // 
            // bmControleBarra
            // 
            this.bmControleBarra.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiUpdateSchedule});
            this.bmControleBarra.MaxItemId = 95;
            // 
            // icImagens16
            // 
            this.icImagens16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImagens16.ImageStream")));
            this.icImagens16.Images.SetKeyName(0, "Novo16.png");
            this.icImagens16.Images.SetKeyName(1, "Editar 16.png");
            this.icImagens16.Images.SetKeyName(2, "Excluir_16.png");
            this.icImagens16.Images.SetKeyName(3, "Atualizar 16.png");
            this.icImagens16.Images.SetKeyName(4, "Imprimir 16.png");
            this.icImagens16.Images.SetKeyName(5, "Visualizar_Reg_16.png");
            this.icImagens16.Images.SetKeyName(6, "Configuração para Importação de Dados 16.png");
            this.icImagens16.Images.SetKeyName(7, "Gráfico 16.png");
            this.icImagens16.Images.SetKeyName(8, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(9, "Layout 16.png");
            this.icImagens16.Images.SetKeyName(10, "Filtro 16.png");
            this.icImagens16.Images.SetKeyName(11, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(12, "Definidos 16.png");
            this.icImagens16.Images.SetKeyName(13, "Primeiro16.png");
            this.icImagens16.Images.SetKeyName(14, "Anterior16.png");
            this.icImagens16.Images.SetKeyName(15, "Próximo16.png");
            this.icImagens16.Images.SetKeyName(16, "Último16.png");
            this.icImagens16.Images.SetKeyName(17, "Log 16.png");
            this.icImagens16.Images.SetKeyName(18, "Anexos 16.png");
            this.icImagens16.Images.SetKeyName(19, "Ajuda 16.png");
            this.icImagens16.Images.SetKeyName(20, "calculadora 16.png");
            this.icImagens16.Images.SetKeyName(21, "Calendário de pagamento 16.png");
            // 
            // gcGrid
            // 
            this.gcGrid.Location = new System.Drawing.Point(0, 26);
            this.gcGrid.Size = new System.Drawing.Size(936, 371);
            // 
            // gvVisao
            // 
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gvVisao.FormatRules.Add(gridFormatRule1);
            this.gvVisao.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gvVisao.OptionsBehavior.ReadOnly = true;
            this.gvVisao.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gvVisao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvVisao.OptionsPrint.AutoWidth = false;
            this.gvVisao.OptionsPrint.ExpandAllDetails = true;
            this.gvVisao.OptionsPrint.PrintDetails = true;
            this.gvVisao.OptionsPrint.PrintFilterInfo = true;
            this.gvVisao.OptionsView.BestFitMaxRowCount = 100;
            this.gvVisao.OptionsView.ColumnAutoWidth = false;
            this.gvVisao.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gvVisao.OptionsView.ShowAutoFilterRow = true;
            this.gvVisao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gvVisao.OptionsView.ShowFooter = true;
            this.gvVisao.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvVisao_RowCellStyle);
            // 
            // bsiAnexos
            // 
            this.bsiAnexos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUpdateSchedule)});
            // 
            // bbiUpdateSchedule
            // 
            this.bbiUpdateSchedule.Caption = "Atualizar Calendário";
            this.bbiUpdateSchedule.Id = 93;
            this.bbiUpdateSchedule.Name = "bbiUpdateSchedule";
            this.bbiUpdateSchedule.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUpdateSchedule_ItemClick);
            // 
            // frmVsScheduleOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 397);
            this.Name = "frmVsScheduleOrders";
            this.Text = "(PD) Calendário de O.P.s";
            this.Load += new System.EventHandler(this.frmVsScheduleOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarButtonItem bbiUpdateSchedule;
    }
}