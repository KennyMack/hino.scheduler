﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsMachineLoad : frmVsPadrao
    {
        private readonly MachineLoadVM _MachineLoadVM;
        public DateTime _DtInitial
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtInitial);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtInitial, value);
            }
        }

        public DateTime _DtEnd
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtEnd);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtEnd, value);
            }
        }

        public frmVsMachineLoad()
        {
            InitializeComponent();
            _MachineLoadVM = new MachineLoadVM();
            _DtInitial = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            _DtEnd = _DtInitial.AddDays(7);
            _MachineLoadVM._DtInitial = _DtInitial;
            _MachineLoadVM._DtEnd = _DtEnd;
            gvDetailOP.DefinePadrao();
        }

        private async void sbCarregar_Click(object sender, EventArgs e)
        {
            await BotaoAtualizar();
        }

        private async void frmVsMachineLoad_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();
            _MachineLoadVM._DtInitial = _DtInitial;
            _MachineLoadVM._DtEnd = _DtEnd;

            if (!await _MachineLoadVM.LoadMachineLoad())
            {
                MessageBox.Show(_MachineLoadVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            gcGrid.DataSource = _MachineLoadVM._MachineLoadDataSource;
            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                fmtGridView.OcultaColuna(
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["cargamaquina"],
                    gvVisao.Columns["minsdisponiveis"],
                    gvVisao.Columns["minhoraprod"]
                );

                gvVisao.Columns["dtproducao"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codmaquina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            await LoadJobsDetail();
            gvVisao.HideLoadingPanel();
        }
        #endregion

        #region Load Jobs Detail
        private async Task LoadJobsDetail()
        {
            gvDetailOP.ShowLoadingPanel();

            if (gvVisao.FocusedRowHandle > -1)
            {
                await _MachineLoadVM.LoadDetailOpMachine(
                    Convert.ToString(gvVisao.GetFocusedRowCellValue("codmaquina")),
                    Convert.ToDateTime(gvVisao.GetFocusedRowCellValue("dtproducao")));

                gcDetailOP.DataSource = _MachineLoadVM._DetailOpMachineLoadDataSource;

                if (_Initialize)
                {
                    gvDetailOP.BestFitColumns();
                    fmtGridView.OcultaColuna(
                        gvDetailOP.Columns["codestab"]
                    );
                    gvDetailOP.Columns["codordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                    gvDetailOP.Columns["nivelordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                }
            }
            gvDetailOP.HideLoadingPanel();
        }
        #endregion

        private async void gvVisao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            await LoadJobsDetail();
        }
        
        ~frmVsMachineLoad()
        {
            _MachineLoadVM.Dispose();
        }
    }
}
