﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsJobShop : frmVsPadrao
    {
        private readonly ScheduledOrdersViewVM _ScheduledOrdersViewVM;
        public DateTime _DtInitial
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtInitial);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtInitial, value);
            }
        }

        public DateTime _DtEnd
        {
            get
            {
                return base.GetValorFiltro<DateTime>(vgcFiltros, rDtEnd);
            }
            set
            {
                base.SetValorFiltro<DateTime>(vgcFiltros, rDtEnd, value);
            }
        }

        public int _TimeParts
        {
            get
            {
                return base.GetValorFiltro<int>(vgcFiltros, rTimeParts);
            }
            set
            {
                base.SetValorFiltro<int>(vgcFiltros, rTimeParts, value);
            }
        }

        public frmVsJobShop()
        {
            InitializeComponent();
            _ScheduledOrdersViewVM = new ScheduledOrdersViewVM();
            _DtInitial = DateTime.Now.FirstDay();
            _DtEnd = DateTime.Now.LastDay();
            _TimeParts = 60;
            _ScheduledOrdersViewVM._DtInitial = _DtInitial;
            _ScheduledOrdersViewVM._DtEnd = _DtEnd;
        }

        private async void sbCarregar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Todos as alterações não salvas serão perdidas.\r\nDeseja continuar?", "Confirma ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) ==
                DialogResult.Yes)
            {
                gvVisao.ShowLoadingPanel();
                await _ScheduledOrdersViewVM.GeneratedJobShopSchedule(true);
                if (_ScheduledOrdersViewVM.Errors.Count > 0)
                    MessageBox.Show(_ScheduledOrdersViewVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                await BotaoAtualizar();
                gvVisao.HideLoadingPanel();
            }
        }

        private async void frmVsMachineLoad_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();
            _ScheduledOrdersViewVM._DtInitial = _DtInitial;
            _ScheduledOrdersViewVM._DtEnd = _DtEnd;
            _ScheduledOrdersViewVM._TimeParts = _TimeParts;

            await _ScheduledOrdersViewVM.LoadDataJobShop();

            gvVisao.Columns.Clear();
            gcGrid.DataSource = null;
            gcGrid.DataSource = _ScheduledOrdersViewVM._JobShopDataTable;
            if (_Initialize)
            {
                gvVisao.BestFitColumns();
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion        
        
        ~frmVsJobShop()
        {
            _ScheduledOrdersViewVM.Dispose();
        }

        private void gvVisao_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.FieldName.IndexOf("TEMPOPART") > -1)
                {
                    if (e.RowHandle >= 0 &&
                        (e.CellValue ?? "").ToString() != "")
                    {
                        if ((e.CellValue ?? "").ToString() == "SABADO" ||
                            (e.CellValue ?? "").ToString() == "DOMINGO")
                        {
                            e.Appearance.BackColor = Color.Gainsboro;
                            e.Appearance.BackColor2 = Color.Gainsboro;
                            e.Appearance.ForeColor = Color.Gainsboro.CorDescricao();
                        }
                        else
                        {
                            var op = (e.CellValue ?? "").ToString().Split('-')[0];
                            if (!string.IsNullOrEmpty(op))
                            {
                                var cor = _ScheduledOrdersViewVM._OPColors.Where(r => r.OP == Convert.ToDecimal(op)).FirstOrDefault().Cor;
                                e.Appearance.BackColor = cor;
                                e.Appearance.BackColor2 = cor;
                                e.Appearance.ForeColor = cor.CorDescricao();
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }
    }
}
