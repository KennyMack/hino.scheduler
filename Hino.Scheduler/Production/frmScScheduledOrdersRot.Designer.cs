﻿namespace Hino.Scheduler.Production
{
    partial class frmScScheduledOrdersRot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler3 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScScheduledOrdersRot));
            this.scSchedulerOP = new DevExpress.XtraScheduler.SchedulerControl();
            this.bmDados = new DevExpress.XtraBars.BarManager(this.components);
            this.printBar1 = new DevExpress.XtraScheduler.UI.PrintBar();
            this.bsiScenario = new DevExpress.XtraBars.BarStaticItem();
            this.bbiAplicarAlteracoes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBotaoAtualizar = new DevExpress.XtraBars.BarButtonItem();
            this.navigateViewBackwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewBackwardItem();
            this.navigateViewForwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewForwardItem();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToFullWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToFullWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this.groupByNoneItem1 = new DevExpress.XtraScheduler.UI.GroupByNoneItem();
            this.groupByDateItem1 = new DevExpress.XtraScheduler.UI.GroupByDateItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.changeAppointmentStatusItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem();
            this.beiHighlightOP = new DevExpress.XtraBars.BarEditItem();
            this.riteHighlightOP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.switchTimeScalesItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesItem();
            this.changeScaleWidthItem1 = new DevExpress.XtraScheduler.UI.ChangeScaleWidthItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.switchTimeScalesCaptionItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem();
            this.bciShowStatus = new DevExpress.XtraBars.BarCheckItem();
            this.bciCheckDependencies = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.printPreviewItem1 = new DevExpress.XtraScheduler.UI.PrintPreviewItem();
            this.printItem1 = new DevExpress.XtraScheduler.UI.PrintItem();
            this.printPageSetupItem1 = new DevExpress.XtraScheduler.UI.PrintPageSetupItem();
            this.newAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewAppointmentItem();
            this.newRecurringAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem();
            this.switchCompressWeekendItem1 = new DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem();
            this.switchShowWorkTimeOnlyItem1 = new DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem();
            this.switchCellsAutoHeightItem1 = new DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem();
            this.changeSnapToCellsUIItem1 = new DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem();
            this.editAppointmentQueryItem1 = new DevExpress.XtraScheduler.UI.EditAppointmentQueryItem();
            this.editOccurrenceUICommandItem1 = new DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem();
            this.editSeriesUICommandItem1 = new DevExpress.XtraScheduler.UI.EditSeriesUICommandItem();
            this.deleteAppointmentsItem1 = new DevExpress.XtraScheduler.UI.DeleteAppointmentsItem();
            this.deleteOccurrenceItem1 = new DevExpress.XtraScheduler.UI.DeleteOccurrenceItem();
            this.deleteSeriesItem1 = new DevExpress.XtraScheduler.UI.DeleteSeriesItem();
            this.splitAppointmentItem1 = new DevExpress.XtraScheduler.UI.SplitAppointmentItem();
            this.openScheduleItem1 = new DevExpress.XtraScheduler.UI.OpenScheduleItem();
            this.saveScheduleItem1 = new DevExpress.XtraScheduler.UI.SaveScheduleItem();
            this.changeAppointmentLabelItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.changeAppointmentReminderItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem();
            this.repositoryItemDuration2 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.beiTimeParts = new DevExpress.XtraBars.BarEditItem();
            this.ribeTimeParts = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.riseHighlightOp = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.rideDtInitial = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.ricbScenario = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ssSchedulerOP = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.pdJobRotBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pdResourceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController();
            this.sccPrincipal = new DevExpress.XtraEditors.SplitContainerControl();
            this.rtResources = new DevExpress.XtraScheduler.UI.ResourcesTree();
            this.colIdsort = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.colidresource = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.coldescription = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.colTemops = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.ttcMessages = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.scSchedulerOP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteHighlightOP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribeTimeParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riseHighlightOp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideDtInitial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideDtInitial.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbScenario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssSchedulerOP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdJobRotBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdResourceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sccPrincipal)).BeginInit();
            this.sccPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtResources)).BeginInit();
            this.SuspendLayout();
            // 
            // scSchedulerOP
            // 
            this.scSchedulerOP.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Gantt;
            this.scSchedulerOP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scSchedulerOP.Location = new System.Drawing.Point(0, 0);
            this.scSchedulerOP.MenuManager = this.bmDados;
            this.scSchedulerOP.Name = "scSchedulerOP";
            this.scSchedulerOP.OptionsBehavior.ClientTimeZoneId = "Central Brazilian Standard Time";
            this.scSchedulerOP.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.scSchedulerOP.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.scSchedulerOP.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.scSchedulerOP.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.scSchedulerOP.OptionsView.EnableAnimation = false;
            this.scSchedulerOP.Size = new System.Drawing.Size(1347, 595);
            this.scSchedulerOP.Start = new System.DateTime(2019, 10, 1, 0, 0, 0, 0);
            this.scSchedulerOP.Storage = this.ssSchedulerOP;
            this.scSchedulerOP.TabIndex = 0;
            this.scSchedulerOP.Text = "schedulerControl1";
            this.scSchedulerOP.ToolTipController = this.ttcMessages;
            this.scSchedulerOP.Views.DayView.AppointmentDisplayOptions.ShowRecurrence = false;
            this.scSchedulerOP.Views.DayView.AppointmentDisplayOptions.ShowReminder = false;
            this.scSchedulerOP.Views.DayView.AppointmentDisplayOptions.ShowShadows = false;
            this.scSchedulerOP.Views.DayView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.scSchedulerOP.Views.DayView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.DayView.TimeRulers.Add(timeRuler1);
            this.scSchedulerOP.Views.FullWeekView.Enabled = true;
            this.scSchedulerOP.Views.FullWeekView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.FullWeekView.TimeRulers.Add(timeRuler2);
            this.scSchedulerOP.Views.GanttView.CellsAutoHeightOptions.Enabled = true;
            this.scSchedulerOP.Views.GanttView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.MonthView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.TimelineView.CellsAutoHeightOptions.Enabled = true;
            this.scSchedulerOP.Views.TimelineView.ResourcesPerPage = 12;
            this.scSchedulerOP.Views.TimelineView.TimelineScrollBarVisible = true;
            this.scSchedulerOP.Views.WeekView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.WorkWeekView.ResourcesPerPage = 20;
            this.scSchedulerOP.Views.WorkWeekView.TimeRulers.Add(timeRuler3);
            this.scSchedulerOP.SelectionChanged += new System.EventHandler(this.scSchedulerOP_SelectionChanged);
            this.scSchedulerOP.AppointmentViewInfoCustomizing += new DevExpress.XtraScheduler.AppointmentViewInfoCustomizingEventHandler(this.scSchedulerOP_AppointmentViewInfoCustomizing);
            this.scSchedulerOP.AllowAppointmentConflicts += new DevExpress.XtraScheduler.AppointmentConflictEventHandler(this.scSchedulerOP_AllowAppointmentConflicts);
            this.scSchedulerOP.PopupMenuShowing += new DevExpress.XtraScheduler.PopupMenuShowingEventHandler(this.scSchedulerOP_PopupMenuShowing);
            this.scSchedulerOP.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.scSchedulerOP_EditAppointmentFormShowing);
            this.scSchedulerOP.CustomDrawAppointmentBackground += new DevExpress.XtraScheduler.CustomDrawObjectEventHandler(this.scSchedulerOP_CustomDrawAppointmentBackground);
            // 
            // bmDados
            // 
            this.bmDados.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.printBar1});
            this.bmDados.DockControls.Add(this.barDockControlTop);
            this.bmDados.DockControls.Add(this.barDockControlBottom);
            this.bmDados.DockControls.Add(this.barDockControlLeft);
            this.bmDados.DockControls.Add(this.barDockControlRight);
            this.bmDados.Form = this;
            this.bmDados.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.printPreviewItem1,
            this.printItem1,
            this.printPageSetupItem1,
            this.newAppointmentItem1,
            this.newRecurringAppointmentItem1,
            this.navigateViewBackwardItem1,
            this.navigateViewForwardItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToFullWeekViewItem1,
            this.switchToMonthViewItem1,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1,
            this.groupByNoneItem1,
            this.groupByDateItem1,
            this.groupByResourceItem1,
            this.switchTimeScalesItem1,
            this.changeScaleWidthItem1,
            this.switchTimeScalesCaptionItem1,
            this.switchCompressWeekendItem1,
            this.switchShowWorkTimeOnlyItem1,
            this.switchCellsAutoHeightItem1,
            this.changeSnapToCellsUIItem1,
            this.editAppointmentQueryItem1,
            this.editOccurrenceUICommandItem1,
            this.editSeriesUICommandItem1,
            this.deleteAppointmentsItem1,
            this.deleteOccurrenceItem1,
            this.deleteSeriesItem1,
            this.splitAppointmentItem1,
            this.changeAppointmentStatusItem1,
            this.bbiBotaoAtualizar,
            this.bbiAplicarAlteracoes,
            this.beiHighlightOP,
            this.bciShowStatus,
            this.openScheduleItem1,
            this.saveScheduleItem1,
            this.changeAppointmentLabelItem1,
            this.toggleRecurrenceItem1,
            this.changeAppointmentReminderItem1,
            this.beiTimeParts,
            this.bsiScenario,
            this.bciCheckDependencies});
            this.bmDados.MaxItemId = 59;
            this.bmDados.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1,
            this.riseHighlightOp,
            this.riteHighlightOP,
            this.repositoryItemDuration2,
            this.rideDtInitial,
            this.ribeTimeParts,
            this.ricbScenario});
            // 
            // printBar1
            // 
            this.printBar1.Control = this.scSchedulerOP;
            this.printBar1.DockCol = 0;
            this.printBar1.DockRow = 0;
            this.printBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.printBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiScenario),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAplicarAlteracoes),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBotaoAtualizar),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToFullWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByNoneItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByDateItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByResourceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeAppointmentStatusItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiHighlightOP, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeScaleWidthItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesCaptionItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciCheckDependencies, true)});
            // 
            // bsiScenario
            // 
            this.bsiScenario.Caption = "Cenário";
            this.bsiScenario.Id = 56;
            this.bsiScenario.Name = "bsiScenario";
            this.bsiScenario.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bbiAplicarAlteracoes
            // 
            this.bbiAplicarAlteracoes.Caption = "Aplicar Alterações";
            this.bbiAplicarAlteracoes.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAplicarAlteracoes.Glyph")));
            this.bbiAplicarAlteracoes.Id = 41;
            this.bbiAplicarAlteracoes.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiAplicarAlteracoes.LargeGlyph")));
            this.bbiAplicarAlteracoes.Name = "bbiAplicarAlteracoes";
            this.bbiAplicarAlteracoes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAplicarAlteracoes_ItemClick);
            // 
            // bbiBotaoAtualizar
            // 
            this.bbiBotaoAtualizar.Caption = "Atualizar Dados";
            this.bbiBotaoAtualizar.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBotaoAtualizar.Glyph")));
            this.bbiBotaoAtualizar.Id = 40;
            this.bbiBotaoAtualizar.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiBotaoAtualizar.LargeGlyph")));
            this.bbiBotaoAtualizar.Name = "bbiBotaoAtualizar";
            this.bbiBotaoAtualizar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBotaoAtualizar_ItemClick);
            // 
            // navigateViewBackwardItem1
            // 
            this.navigateViewBackwardItem1.Id = 7;
            this.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1";
            // 
            // navigateViewForwardItem1
            // 
            this.navigateViewForwardItem1.Id = 8;
            this.navigateViewForwardItem1.Name = "navigateViewForwardItem1";
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 9;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 10;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 11;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 12;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 13;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 14;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            // 
            // switchToFullWeekViewItem1
            // 
            this.switchToFullWeekViewItem1.Id = 15;
            this.switchToFullWeekViewItem1.Name = "switchToFullWeekViewItem1";
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 16;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 17;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 18;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            // 
            // groupByNoneItem1
            // 
            this.groupByNoneItem1.Id = 19;
            this.groupByNoneItem1.Name = "groupByNoneItem1";
            // 
            // groupByDateItem1
            // 
            this.groupByDateItem1.Id = 20;
            this.groupByDateItem1.Name = "groupByDateItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 21;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // changeAppointmentStatusItem1
            // 
            this.changeAppointmentStatusItem1.Id = 36;
            this.changeAppointmentStatusItem1.Name = "changeAppointmentStatusItem1";
            // 
            // beiHighlightOP
            // 
            this.beiHighlightOP.Caption = "Ordem de produção:";
            this.beiHighlightOP.Edit = this.riteHighlightOP;
            this.beiHighlightOP.Id = 44;
            this.beiHighlightOP.Name = "beiHighlightOP";
            this.beiHighlightOP.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.beiHighlightOP.EditValueChanged += new System.EventHandler(this.beiHighlightOP_EditValueChanged);
            // 
            // riteHighlightOP
            // 
            this.riteHighlightOP.AutoHeight = false;
            this.riteHighlightOP.DisplayFormat.FormatString = "d";
            this.riteHighlightOP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riteHighlightOP.EditFormat.FormatString = "d";
            this.riteHighlightOP.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.riteHighlightOP.Mask.EditMask = "d";
            this.riteHighlightOP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.riteHighlightOP.Name = "riteHighlightOP";
            // 
            // switchTimeScalesItem1
            // 
            this.switchTimeScalesItem1.Id = 22;
            this.switchTimeScalesItem1.Name = "switchTimeScalesItem1";
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit1;
            this.changeScaleWidthItem1.Id = 23;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            this.changeScaleWidthItem1.UseCommandCaption = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // switchTimeScalesCaptionItem1
            // 
            this.switchTimeScalesCaptionItem1.Id = 24;
            this.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1";
            // 
            // bciShowStatus
            // 
            this.bciShowStatus.Caption = "Mostar Status";
            this.bciShowStatus.Hint = "Colore as ordens de produção conforme seus status";
            this.bciShowStatus.Id = 46;
            this.bciShowStatus.Name = "bciShowStatus";
            this.bciShowStatus.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciShowStatus_CheckedChanged);
            // 
            // bciCheckDependencies
            // 
            this.bciCheckDependencies.BindableChecked = true;
            this.bciCheckDependencies.Caption = "Validar dependencias";
            this.bciCheckDependencies.Checked = true;
            this.bciCheckDependencies.Id = 58;
            this.bciCheckDependencies.Name = "bciCheckDependencies";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1550, 28);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 623);
            this.barDockControlBottom.Size = new System.Drawing.Size(1550, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 28);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 595);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1550, 28);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 595);
            // 
            // printPreviewItem1
            // 
            this.printPreviewItem1.Id = 2;
            this.printPreviewItem1.Name = "printPreviewItem1";
            // 
            // printItem1
            // 
            this.printItem1.Id = 3;
            this.printItem1.Name = "printItem1";
            // 
            // printPageSetupItem1
            // 
            this.printPageSetupItem1.Id = 4;
            this.printPageSetupItem1.Name = "printPageSetupItem1";
            // 
            // newAppointmentItem1
            // 
            this.newAppointmentItem1.Id = 5;
            this.newAppointmentItem1.Name = "newAppointmentItem1";
            // 
            // newRecurringAppointmentItem1
            // 
            this.newRecurringAppointmentItem1.Id = 6;
            this.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1";
            // 
            // switchCompressWeekendItem1
            // 
            this.switchCompressWeekendItem1.Id = 25;
            this.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1";
            // 
            // switchShowWorkTimeOnlyItem1
            // 
            this.switchShowWorkTimeOnlyItem1.Id = 26;
            this.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1";
            // 
            // switchCellsAutoHeightItem1
            // 
            this.switchCellsAutoHeightItem1.Id = 27;
            this.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1";
            // 
            // changeSnapToCellsUIItem1
            // 
            this.changeSnapToCellsUIItem1.Id = 28;
            this.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1";
            // 
            // editAppointmentQueryItem1
            // 
            this.editAppointmentQueryItem1.Id = 29;
            this.editAppointmentQueryItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.editOccurrenceUICommandItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.editSeriesUICommandItem1)});
            this.editAppointmentQueryItem1.Name = "editAppointmentQueryItem1";
            this.editAppointmentQueryItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // editOccurrenceUICommandItem1
            // 
            this.editOccurrenceUICommandItem1.Id = 30;
            this.editOccurrenceUICommandItem1.Name = "editOccurrenceUICommandItem1";
            // 
            // editSeriesUICommandItem1
            // 
            this.editSeriesUICommandItem1.Id = 31;
            this.editSeriesUICommandItem1.Name = "editSeriesUICommandItem1";
            // 
            // deleteAppointmentsItem1
            // 
            this.deleteAppointmentsItem1.Id = 32;
            this.deleteAppointmentsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteOccurrenceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteSeriesItem1)});
            this.deleteAppointmentsItem1.Name = "deleteAppointmentsItem1";
            this.deleteAppointmentsItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // deleteOccurrenceItem1
            // 
            this.deleteOccurrenceItem1.Id = 33;
            this.deleteOccurrenceItem1.Name = "deleteOccurrenceItem1";
            // 
            // deleteSeriesItem1
            // 
            this.deleteSeriesItem1.Id = 34;
            this.deleteSeriesItem1.Name = "deleteSeriesItem1";
            // 
            // splitAppointmentItem1
            // 
            this.splitAppointmentItem1.Id = 35;
            this.splitAppointmentItem1.Name = "splitAppointmentItem1";
            // 
            // openScheduleItem1
            // 
            this.openScheduleItem1.Id = 47;
            this.openScheduleItem1.Name = "openScheduleItem1";
            // 
            // saveScheduleItem1
            // 
            this.saveScheduleItem1.Id = 48;
            this.saveScheduleItem1.Name = "saveScheduleItem1";
            // 
            // changeAppointmentLabelItem1
            // 
            this.changeAppointmentLabelItem1.Id = 49;
            this.changeAppointmentLabelItem1.Name = "changeAppointmentLabelItem1";
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 50;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // changeAppointmentReminderItem1
            // 
            this.changeAppointmentReminderItem1.Edit = this.repositoryItemDuration2;
            this.changeAppointmentReminderItem1.Id = 51;
            this.changeAppointmentReminderItem1.Name = "changeAppointmentReminderItem1";
            this.changeAppointmentReminderItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // repositoryItemDuration2
            // 
            this.repositoryItemDuration2.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration2.AutoHeight = false;
            this.repositoryItemDuration2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration2.DisabledStateText = null;
            this.repositoryItemDuration2.Name = "repositoryItemDuration2";
            this.repositoryItemDuration2.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration2.ShowEmptyItem = true;
            this.repositoryItemDuration2.ValidateOnEnterKey = true;
            // 
            // beiTimeParts
            // 
            this.beiTimeParts.Caption = "Partes de tempo:";
            this.beiTimeParts.Edit = this.ribeTimeParts;
            this.beiTimeParts.EditValue = "60";
            this.beiTimeParts.Id = 54;
            this.beiTimeParts.Name = "beiTimeParts";
            this.beiTimeParts.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // ribeTimeParts
            // 
            this.ribeTimeParts.AutoHeight = false;
            this.ribeTimeParts.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ribeTimeParts.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "120"});
            this.ribeTimeParts.Name = "ribeTimeParts";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // riseHighlightOp
            // 
            this.riseHighlightOp.AutoHeight = false;
            this.riseHighlightOp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riseHighlightOp.Name = "riseHighlightOp";
            // 
            // rideDtInitial
            // 
            this.rideDtInitial.AutoHeight = false;
            this.rideDtInitial.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rideDtInitial.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rideDtInitial.Name = "rideDtInitial";
            // 
            // ricbScenario
            // 
            this.ricbScenario.AutoHeight = false;
            this.ricbScenario.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricbScenario.Name = "ricbScenario";
            this.ricbScenario.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // ssSchedulerOP
            // 
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Corop", "corop"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Idjob", "idjob"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Codordprod", "codordprod"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Codmaquina", "codmaquina"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Nivelordprod", "nivelordprod"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Minimaltime", "minimaltime"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Nivelordprodpai", "nivelordprodpai"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Leadtime", "leadtime"));
            this.ssSchedulerOP.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Ltentrega", "ltentrega"));
            this.ssSchedulerOP.Appointments.DataSource = this.pdJobRotBindingSource;
            this.ssSchedulerOP.Appointments.Labels.Add(System.Drawing.Color.Yellow, "Pendente", "&Pendente");
            this.ssSchedulerOP.Appointments.Labels.Add(System.Drawing.Color.LightSkyBlue, "Andamento", "&Andamento");
            this.ssSchedulerOP.Appointments.Labels.Add(System.Drawing.Color.PaleGreen, "Concluido", "&Concluido");
            this.ssSchedulerOP.Appointments.Mappings.AppointmentId = "idjob";
            this.ssSchedulerOP.Appointments.Mappings.Description = "descricao";
            this.ssSchedulerOP.Appointments.Mappings.End = "dttermoper";
            this.ssSchedulerOP.Appointments.Mappings.Label = "labelop";
            this.ssSchedulerOP.Appointments.Mappings.Location = "location";
            this.ssSchedulerOP.Appointments.Mappings.ResourceId = "resourceid";
            this.ssSchedulerOP.Appointments.Mappings.Start = "dtiniciooper";
            this.ssSchedulerOP.Appointments.Mappings.Status = "statusop";
            this.ssSchedulerOP.Appointments.Mappings.Subject = "subject";
            this.ssSchedulerOP.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Sem Prioridade", "&Sem Prioridade", System.Drawing.Color.White);
            this.ssSchedulerOP.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, "Urgente", "&Urgente", System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(0))))));
            this.ssSchedulerOP.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Busy, "Andamento normal", "&Andamento normal", System.Drawing.Color.SkyBlue);
            this.ssSchedulerOP.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, "Atrasado", "A&trasado", System.Drawing.Color.Salmon);
            this.ssSchedulerOP.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.WorkingElsewhere, "Aguardando", "A&guardando", System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0))))));
            this.ssSchedulerOP.EnableReminders = false;
            this.ssSchedulerOP.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("Idsort", "idsort"));
            this.ssSchedulerOP.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("Temops", "temops"));
            this.ssSchedulerOP.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("Codmaquina", "codmaquina"));
            this.ssSchedulerOP.Resources.DataSource = this.pdResourceBindingSource;
            this.ssSchedulerOP.Resources.Mappings.Caption = "description";
            this.ssSchedulerOP.Resources.Mappings.Color = "color";
            this.ssSchedulerOP.Resources.Mappings.Id = "idresource";
            this.ssSchedulerOP.AppointmentChanging += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.ssSchedulerOP_AppointmentChanging);
            this.ssSchedulerOP.AppointmentsChanged += new DevExpress.XtraScheduler.PersistentObjectsEventHandler(this.ssSchedulerOP_AppointmentsChanged);
            // 
            // pdJobRotBindingSource
            // 
            this.pdJobRotBindingSource.DataSource = typeof(Hino.Scheduler.Model.Production.pdJobRot);
            // 
            // pdResourceBindingSource
            // 
            this.pdResourceBindingSource.DataSource = typeof(Hino.Scheduler.Model.Production.pdResource);
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.printPreviewItem1);
            this.schedulerBarController1.BarItems.Add(this.printItem1);
            this.schedulerBarController1.BarItems.Add(this.printPageSetupItem1);
            this.schedulerBarController1.BarItems.Add(this.newAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.newRecurringAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewBackwardItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewForwardItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToFullWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByNoneItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByDateItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesItem1);
            this.schedulerBarController1.BarItems.Add(this.changeScaleWidthItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesCaptionItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCompressWeekendItem1);
            this.schedulerBarController1.BarItems.Add(this.switchShowWorkTimeOnlyItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCellsAutoHeightItem1);
            this.schedulerBarController1.BarItems.Add(this.changeSnapToCellsUIItem1);
            this.schedulerBarController1.BarItems.Add(this.editAppointmentQueryItem1);
            this.schedulerBarController1.BarItems.Add(this.editOccurrenceUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.editSeriesUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteAppointmentsItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteOccurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteSeriesItem1);
            this.schedulerBarController1.BarItems.Add(this.splitAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentStatusItem1);
            this.schedulerBarController1.BarItems.Add(this.openScheduleItem1);
            this.schedulerBarController1.BarItems.Add(this.saveScheduleItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentLabelItem1);
            this.schedulerBarController1.BarItems.Add(this.toggleRecurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentReminderItem1);
            this.schedulerBarController1.Control = this.scSchedulerOP;
            // 
            // sccPrincipal
            // 
            this.sccPrincipal.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.sccPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sccPrincipal.Location = new System.Drawing.Point(0, 28);
            this.sccPrincipal.Name = "sccPrincipal";
            this.sccPrincipal.Panel1.Controls.Add(this.rtResources);
            this.sccPrincipal.Panel1.Text = "Panel1";
            this.sccPrincipal.Panel2.Controls.Add(this.scSchedulerOP);
            this.sccPrincipal.Panel2.Text = "Panel2";
            this.sccPrincipal.Size = new System.Drawing.Size(1550, 595);
            this.sccPrincipal.SplitterPosition = 197;
            this.sccPrincipal.TabIndex = 5;
            this.sccPrincipal.Text = "splitContainerControl1";
            // 
            // rtResources
            // 
            this.rtResources.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colIdsort,
            this.colidresource,
            this.coldescription,
            this.colTemops});
            this.rtResources.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtResources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtResources.Location = new System.Drawing.Point(0, 0);
            this.rtResources.MenuManager = this.bmDados;
            this.rtResources.Name = "rtResources";
            this.rtResources.OptionsBehavior.Editable = false;
            this.rtResources.SchedulerControl = this.scSchedulerOP;
            this.rtResources.Size = new System.Drawing.Size(197, 595);
            this.rtResources.TabIndex = 0;
            this.rtResources.VertScrollVisibility = DevExpress.XtraTreeList.ScrollVisibility.Always;
            // 
            // colIdsort
            // 
            this.colIdsort.FieldName = "Idsort";
            this.colIdsort.Name = "colIdsort";
            // 
            // colidresource
            // 
            this.colidresource.FieldName = "idresource";
            this.colidresource.Name = "colidresource";
            // 
            // coldescription
            // 
            this.coldescription.Caption = "Descrição";
            this.coldescription.FieldName = "description";
            this.coldescription.Name = "coldescription";
            this.coldescription.Visible = true;
            this.coldescription.VisibleIndex = 0;
            this.coldescription.Width = 122;
            // 
            // colTemops
            // 
            this.colTemops.Caption = "Tem O.Ps";
            this.colTemops.FieldName = "Temops";
            this.colTemops.Name = "colTemops";
            this.colTemops.SortOrder = System.Windows.Forms.SortOrder.Descending;
            this.colTemops.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Boolean;
            this.colTemops.Width = 53;
            // 
            // ttcMessages
            // 
            this.ttcMessages.AutoPopDelay = 50000;
            this.ttcMessages.InitialDelay = 50;
            this.ttcMessages.ReshowDelay = 10;
            this.ttcMessages.ShowShadow = false;
            this.ttcMessages.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            this.ttcMessages.BeforeShow += new DevExpress.Utils.ToolTipControllerBeforeShowEventHandler(this.ttcMessages_BeforeShow);
            // 
            // frmScScheduledOrdersRot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1550, 623);
            this.Controls.Add(this.sccPrincipal);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmScScheduledOrdersRot";
            this.Text = "(PD) Calendário ordem de produção rotinas";
            this.Load += new System.EventHandler(this.frmScScheduledOrdersRot_Load);
            this.Shown += new System.EventHandler(this.frmScScheduledOrders_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.scSchedulerOP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteHighlightOP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribeTimeParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riseHighlightOp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideDtInitial.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideDtInitial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbScenario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssSchedulerOP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdJobRotBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdResourceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sccPrincipal)).EndInit();
            this.sccPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtResources)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraScheduler.SchedulerControl scSchedulerOP;
        private DevExpress.XtraScheduler.SchedulerStorage ssSchedulerOP;
        private DevExpress.XtraBars.BarManager bmDados;
        private DevExpress.XtraScheduler.UI.PrintBar printBar1;
        private DevExpress.XtraScheduler.UI.PrintPreviewItem printPreviewItem1;
        private DevExpress.XtraScheduler.UI.PrintItem printItem1;
        private DevExpress.XtraScheduler.UI.PrintPageSetupItem printPageSetupItem1;
        private DevExpress.XtraScheduler.UI.NewAppointmentItem newAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem newRecurringAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewBackwardItem navigateViewBackwardItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewForwardItem navigateViewForwardItem1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToFullWeekViewItem switchToFullWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
        private DevExpress.XtraScheduler.UI.GroupByNoneItem groupByNoneItem1;
        private DevExpress.XtraScheduler.UI.GroupByDateItem groupByDateItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesItem switchTimeScalesItem1;
        private DevExpress.XtraScheduler.UI.ChangeScaleWidthItem changeScaleWidthItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem switchTimeScalesCaptionItem1;
        private DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem switchCompressWeekendItem1;
        private DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem switchShowWorkTimeOnlyItem1;
        private DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem switchCellsAutoHeightItem1;
        private DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem changeSnapToCellsUIItem1;
        private DevExpress.XtraScheduler.UI.EditAppointmentQueryItem editAppointmentQueryItem1;
        private DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem editOccurrenceUICommandItem1;
        private DevExpress.XtraScheduler.UI.EditSeriesUICommandItem editSeriesUICommandItem1;
        private DevExpress.XtraScheduler.UI.DeleteAppointmentsItem deleteAppointmentsItem1;
        private DevExpress.XtraScheduler.UI.DeleteOccurrenceItem deleteOccurrenceItem1;
        private DevExpress.XtraScheduler.UI.DeleteSeriesItem deleteSeriesItem1;
        private DevExpress.XtraScheduler.UI.SplitAppointmentItem splitAppointmentItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem changeAppointmentStatusItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private DevExpress.XtraBars.BarButtonItem bbiBotaoAtualizar;
        private DevExpress.XtraBars.BarButtonItem bbiAplicarAlteracoes;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit riseHighlightOp;
        private DevExpress.XtraBars.BarEditItem beiHighlightOP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riteHighlightOP;
        private DevExpress.XtraBars.BarCheckItem bciShowStatus;
        private DevExpress.XtraScheduler.UI.OpenScheduleItem openScheduleItem1;
        private DevExpress.XtraScheduler.UI.SaveScheduleItem saveScheduleItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem changeAppointmentLabelItem1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem changeAppointmentReminderItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration2;
        private DevExpress.XtraEditors.SplitContainerControl sccPrincipal;
        private DevExpress.XtraScheduler.UI.ResourcesTree rtResources;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colidresource;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn coldescription;
        private System.Windows.Forms.BindingSource pdJobRotBindingSource;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colIdsort;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colTemops;
        private System.Windows.Forms.BindingSource pdResourceBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rideDtInitial;
        private DevExpress.XtraBars.BarEditItem beiTimeParts;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ribeTimeParts;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox ricbScenario;
        private DevExpress.XtraBars.BarStaticItem bsiScenario;
        private DevExpress.XtraBars.BarCheckItem bciCheckDependencies;
        private DevExpress.Utils.ToolTipController ttcMessages;
    }
}