using System;
using System.Drawing;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.Utils.Controls;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Localization;
using DevExpress.XtraScheduler.Native;
using DevExpress.XtraScheduler.UI;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Native;
using DevExpress.Utils.Internal;
using System.Collections.Generic;
using DevExpress.XtraScheduler.Internal;
using Hino.Scheduler.Utils;
using Hino.Scheduler.Model.Production;
using System.Linq;
using Hino.Scheduler.Application.ViewModel.DetailScheduler;

namespace Hino.Scheduler.Production
{
    public partial class frmAppointmentDetailRot : DevExpress.XtraEditors.XtraForm, IDXManagerPopupMenu
    {
        #region Fields
        private readonly DetailSchedulerVM _DetailSchedulerVM;
        decimal _MinimalTime = 0;
        readonly ISchedulerStorage storage;
        readonly SchedulerControl control;
        Icon recurringIcon;
        Icon normalIcon;
        readonly AppointmentFormController controller;
        IDXMenuManager menuManager;
        #endregion

        [EditorBrowsable(EditorBrowsableState.Never)]
        public frmAppointmentDetailRot()
        {
            InitializeComponent();
        }
        public frmAppointmentDetailRot(SchedulerControl control, Appointment apt)
            : this(control, apt, false)
        {
        }
        public frmAppointmentDetailRot(SchedulerControl control, Appointment apt, bool openRecurrenceForm)
        {
            Guard.ArgumentNotNull(control, "control");
            Guard.ArgumentNotNull(control.Storage, "control.Storage");
            Guard.ArgumentNotNull(apt, "apt");
            
            this.controller = CreateController(control, apt);
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            _DetailSchedulerVM = new DetailSchedulerVM();
            _DetailSchedulerVM._IdScenario = Parameters.IdScenario;

            LoadIcons();
            this.control = control;
            this.storage = control.Storage;

            this.edtShowTimeAs.Storage = this.storage;
            this.edtLabel.Storage = storage;

            SubscribeControllerEvents(Controller);
            BindControllerToControls();
            gvRotinas.DefinePadrao();

        }
        #region Properties
        protected override FormShowMode ShowMode { get { return DevExpress.XtraEditors.FormShowMode.AfterInitialization; } }
        public IDXMenuManager MenuManager { get { return menuManager; } private set { menuManager = value; } }
        protected internal AppointmentFormController Controller { get { return controller; } }
        protected internal SchedulerControl Control { get { return control; } }
        protected internal ISchedulerStorage Storage { get { return storage; } }
        protected internal bool IsNewAppointment { get { return controller != null ? controller.IsNewAppointment : true; } }
        protected internal Icon RecurringIcon { get { return recurringIcon; } }
        protected internal Icon NormalIcon { get { return normalIcon; } }
        public bool ReadOnly
        {
            get { return Controller != null && Controller.ReadOnly; }
            set
            {
                if (Controller.ReadOnly == value)
                    return;
                Controller.ReadOnly = value;
            }
        }
        #endregion

        public async virtual void LoadFormData(Appointment appointment)
        {
            //do nothing
            _MinimalTime = (decimal)appointment.CustomFields["Minimaltime"];

            if (_MinimalTime <= 0)
                edtTempoMinimo.Text = "00:00";
            else
            {
                var hrs = Math.Floor(_MinimalTime / 60);
                var min = _MinimalTime - (Math.Floor(_MinimalTime / 60) * 60);
                edtTempoMinimo.Text = $"{hrs.ToString().PadLeft(2, '0')}:{min.ToString().PadLeft(2, '0')}";
            }

            _DetailSchedulerVM._IdScenario = Parameters.IdScenario;
            await _DetailSchedulerVM.LoadDetailRotAsync(Convert.ToInt64(appointment.CustomFields["Codordprod"]),
                appointment.CustomFields["Nivelordprod"].ToString());

            gcRotinas.DataSource = _DetailSchedulerVM._JobRotDataSource;

            gvRotinas.OptionsView.ColumnAutoWidth = true;
            gvRotinas.BestFitColumns();
            gvRotinas.Columns["operacao"].LarguraColuna(40);
            gvRotinas.Columns["codrotina"].LarguraColuna(30);
            gvRotinas.Columns["descoperacao"].LarguraColuna(120);
            gvRotinas.Columns["dtiniciooper"].LarguraColuna(100);
            gvRotinas.Columns["dttermoper"].LarguraColuna(100);
            gvRotinas.Columns["prodhoraria"].LarguraColuna(80);
            gvRotinas.Columns["codmaquina"].LarguraColuna(50);

        }
        public virtual bool SaveFormData(Appointment appointment)
        {
            return true;
        }
        public virtual bool IsAppointmentChanged(Appointment appointment)
        {
            return false;
        }
        public virtual void SetMenuManager(DevExpress.Utils.Menu.IDXMenuManager menuManager)
        {
            MenuManagerUtils.SetMenuManager(Controls, menuManager);
            this.menuManager = menuManager;
        }

        protected virtual void BindControllerToControls()
        {
            BindProperties(this.tbSubject, "Text", "Subject");
            BindProperties(this.tbLocation, "Text", "Location");
            BindProperties(this.tbDescription, "Text", "Description");
            BindProperties(this.edtShowTimeAs, "Status", "Status");
            BindProperties(this.edtStartDate, "EditValue", "DisplayStartDate");
            BindProperties(this.edtStartDate, "Enabled", "IsDateTimeEditable");
            BindProperties(this.edtStartTime, "EditValue", "DisplayStartTime");
            BindProperties(this.edtStartTime, "Visible", "IsTimeVisible");
            BindProperties(this.edtStartTime, "Enabled", "IsTimeVisible");
            BindProperties(this.edtEndDate, "EditValue", "DisplayEndDate", DataSourceUpdateMode.Never);
            BindProperties(this.edtEndDate, "Enabled", "IsDateTimeEditable", DataSourceUpdateMode.Never);
            BindProperties(this.edtEndTime, "EditValue", "DisplayEndTime", DataSourceUpdateMode.Never);
            BindProperties(this.edtEndTime, "Visible", "IsTimeVisible", DataSourceUpdateMode.Never);
            BindProperties(this.edtEndTime, "Enabled", "IsTimeVisible", DataSourceUpdateMode.Never);

            BindProperties(this.edtLabel, "Label", "Label");

        }
        protected virtual void BindControllerToIcon()
        {
            Binding binding = new Binding("Icon", Controller, "AppointmentType");
            binding.Format += AppointmentTypeToIconConverter;
            DataBindings.Add(binding);
        }
        protected virtual void ObjectToStringConverter(object o, ConvertEventArgs e)
        {
            e.Value = e.Value.ToString();
        }
        protected virtual void AppointmentTypeToIconConverter(object o, ConvertEventArgs e)
        {
            AppointmentType type = (AppointmentType)e.Value;
            if (type == AppointmentType.Pattern)
                e.Value = RecurringIcon;
            else
                e.Value = NormalIcon;
        }
        protected virtual void BindProperties(Control target, string targetProperty, string sourceProperty)
        {
            BindProperties(target, targetProperty, sourceProperty, DataSourceUpdateMode.OnPropertyChanged);
        }
        protected virtual void BindProperties(Control target, string targetProperty, string sourceProperty, DataSourceUpdateMode updateMode)
        {
            target.DataBindings.Add(targetProperty, Controller, sourceProperty, true, updateMode);
        }
        protected virtual void BindProperties(Control target, string targetProperty, string sourceProperty, ConvertEventHandler objectToStringConverter)
        {
            Binding binding = new Binding(targetProperty, Controller, sourceProperty, true);
            binding.Format += objectToStringConverter;
            target.DataBindings.Add(binding);
        }
        protected virtual void BindToBoolPropertyAndInvert(Control target, string targetProperty, string sourceProperty)
        {
            target.DataBindings.Add(new BoolInvertBinding(targetProperty, Controller, sourceProperty));
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (Controller == null)
                return;
            SubscribeControlsEvents();
            LoadFormData(Controller.EditedAppointmentCopy);
        }
        protected virtual AppointmentFormController CreateController(SchedulerControl control, Appointment apt)
        {
            return new AppointmentFormController(control, apt);
        }
        void SubscribeControllerEvents(AppointmentFormController controller)
        {
            if (controller == null)
                return;
            controller.PropertyChanged += OnControllerPropertyChanged;
        }
        void OnControllerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ReadOnly")
                UpdateReadonly();
        }
        protected virtual void UpdateReadonly()
        {
            if (Controller == null)
                return;
            IList<Control> controls = GetAllControls(this);
            foreach (Control control in controls)
            {
                BaseEdit editor = control as BaseEdit;
                if (editor == null)
                    continue;
                editor.ReadOnly = Controller.ReadOnly;
            }
        }

        List<Control> GetAllControls(Control rootControl)
        {
            List<Control> result = new List<Control>();
            foreach (Control control in rootControl.Controls)
            {
                result.Add(control);
                IList<Control> childControls = GetAllControls(control);
                result.AddRange(childControls);
            }
            return result;
        }
        protected internal virtual void LoadIcons()
        {
            Assembly asm = typeof(SchedulerControl).Assembly;
            recurringIcon = ResourceImageHelper.CreateIconFromResources(SchedulerIconNames.RecurringAppointment, asm);
            normalIcon = ResourceImageHelper.CreateIconFromResources(SchedulerIconNames.Appointment, asm);
        }
        protected internal virtual void SubscribeControlsEvents()
        {
            this.edtEndDate.Validating += new CancelEventHandler(OnEdtEndDateValidating);
            this.edtEndDate.InvalidValue += new InvalidValueExceptionEventHandler(OnEdtEndDateInvalidValue);
            this.edtEndTime.Validating += new CancelEventHandler(OnEdtEndTimeValidating);
            this.edtEndTime.InvalidValue += new InvalidValueExceptionEventHandler(OnEdtEndTimeInvalidValue);
        }
        protected internal virtual void UnsubscribeControlsEvents()
        {
            this.edtEndDate.Validating -= new CancelEventHandler(OnEdtEndDateValidating);
            this.edtEndDate.InvalidValue -= new InvalidValueExceptionEventHandler(OnEdtEndDateInvalidValue);
            this.edtEndTime.Validating -= new CancelEventHandler(OnEdtEndTimeValidating);
            this.edtEndTime.InvalidValue -= new InvalidValueExceptionEventHandler(OnEdtEndTimeInvalidValue);
        }

        private void bbiSalvar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OnOkButton();
        }
        string ErrorMessage = "";
        protected internal virtual void OnEdtEndDateValidating(object sender, CancelEventArgs e)
        {
            e.Cancel = !IsValidInterval();

            if (!e.Cancel)
                e.Cancel = !IsMinimalInterval();

            if (!e.Cancel)
                this.edtEndDate.DataBindings["EditValue"].WriteValue();
        }
        protected internal virtual void OnEdtEndDateInvalidValue(object sender, InvalidValueExceptionEventArgs e)
        {
            if (string.IsNullOrEmpty(ErrorMessage))
                ErrorMessage = SchedulerLocalizer.GetString(SchedulerStringId.Msg_InvalidEndDate);

            e.ErrorText = ErrorMessage;

            ErrorMessage = "";
        }
        protected internal virtual void OnEdtEndTimeValidating(object sender, CancelEventArgs e)
        {
            e.Cancel = !IsValidInterval();

            if (!e.Cancel)
                e.Cancel = !IsMinimalInterval();

            if (!e.Cancel)
                this.edtEndTime.DataBindings["EditValue"].WriteValue();

        }
        protected internal virtual void OnEdtEndTimeInvalidValue(object sender, InvalidValueExceptionEventArgs e)
        {
            if (string.IsNullOrEmpty(ErrorMessage))
                ErrorMessage = SchedulerLocalizer.GetString(SchedulerStringId.Msg_InvalidEndDate);

            e.ErrorText = ErrorMessage;

            ErrorMessage = "";
        }
        protected internal virtual bool IsValidInterval()
        {
            return AppointmentFormControllerBase.ValidateInterval(edtStartDate.DateTime.Date, edtStartTime.Time.TimeOfDay, edtEndDate.DateTime.Date, edtEndTime.Time.TimeOfDay);
        }
        protected internal virtual bool IsMinimalInterval()
        {
            var dtStart = new DateTime(edtStartDate.DateTime.Date.Year, edtStartDate.DateTime.Date.Month, edtStartDate.DateTime.Date.Day,
                edtStartTime.Time.Hour, edtStartTime.Time.Minute, edtStartTime.Time.Second);
            var dtEnd = new DateTime(edtEndDate.DateTime.Date.Year, edtEndDate.DateTime.Date.Month, edtEndDate.DateTime.Date.Day,
                edtEndTime.Time.Hour, edtEndTime.Time.Minute, edtEndTime.Time.Second);

            if ((dtEnd - dtStart).TotalMinutes < Convert.ToDouble(_MinimalTime) && _MinimalTime > 0)
            {
                ErrorMessage = "Tempo da O.P.� menor que o m�nimo.";
                return false;
            }

            return true;
        }
        protected internal virtual void OnOkButton()
        {
            if (!SaveFormData(Controller.EditedAppointmentCopy))
                return;
            if (!Controller.IsConflictResolved())
            {
                ShowMessageBox(SchedulerLocalizer.GetString(SchedulerStringId.Msg_Conflict), System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (Controller.IsAppointmentChanged() || Controller.IsNewAppointment || IsAppointmentChanged(Controller.EditedAppointmentCopy))
                Controller.ApplyChanges();

            this.DialogResult = DialogResult.OK;
        }
        protected internal virtual DialogResult ShowMessageBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return XtraMessageBox.Show(this, text, caption, buttons, icon);
        }

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}