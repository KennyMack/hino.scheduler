﻿namespace Hino.Scheduler.Production
{
    partial class frmVsJobShop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVsJobShop));
            this.dpMenuLateral = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.vgcFiltros = new DevExpress.XtraVerticalGrid.VGridControl();
            this.rideData = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cFiltroGrupo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rDtInitial = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rDtEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cDados = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rTimeParts = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sbCarregar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).BeginInit();
            this.dpMenuLateral.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vgcFiltros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideData.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(936, 26);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 371);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(936, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 371);
            // 
            // icImagens16
            // 
            this.icImagens16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImagens16.ImageStream")));
            this.icImagens16.Images.SetKeyName(0, "Novo16.png");
            this.icImagens16.Images.SetKeyName(1, "Editar 16.png");
            this.icImagens16.Images.SetKeyName(2, "Excluir_16.png");
            this.icImagens16.Images.SetKeyName(3, "Atualizar 16.png");
            this.icImagens16.Images.SetKeyName(4, "Imprimir 16.png");
            this.icImagens16.Images.SetKeyName(5, "Visualizar_Reg_16.png");
            this.icImagens16.Images.SetKeyName(6, "Configuração para Importação de Dados 16.png");
            this.icImagens16.Images.SetKeyName(7, "Gráfico 16.png");
            this.icImagens16.Images.SetKeyName(8, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(9, "Layout 16.png");
            this.icImagens16.Images.SetKeyName(10, "Filtro 16.png");
            this.icImagens16.Images.SetKeyName(11, "Salvar 16.png");
            this.icImagens16.Images.SetKeyName(12, "Definidos 16.png");
            this.icImagens16.Images.SetKeyName(13, "Primeiro16.png");
            this.icImagens16.Images.SetKeyName(14, "Anterior16.png");
            this.icImagens16.Images.SetKeyName(15, "Próximo16.png");
            this.icImagens16.Images.SetKeyName(16, "Último16.png");
            this.icImagens16.Images.SetKeyName(17, "Log 16.png");
            this.icImagens16.Images.SetKeyName(18, "Anexos 16.png");
            this.icImagens16.Images.SetKeyName(19, "Ajuda 16.png");
            this.icImagens16.Images.SetKeyName(20, "calculadora 16.png");
            // 
            // gcGrid
            // 
            this.gcGrid.Location = new System.Drawing.Point(200, 26);
            this.gcGrid.Size = new System.Drawing.Size(736, 371);
            // 
            // gvVisao
            // 
            this.gvVisao.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gvVisao.OptionsBehavior.ReadOnly = true;
            this.gvVisao.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gvVisao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvVisao.OptionsPrint.AutoWidth = false;
            this.gvVisao.OptionsPrint.ExpandAllDetails = true;
            this.gvVisao.OptionsPrint.PrintDetails = true;
            this.gvVisao.OptionsPrint.PrintFilterInfo = true;
            this.gvVisao.OptionsView.BestFitMaxRowCount = 100;
            this.gvVisao.OptionsView.ColumnAutoWidth = false;
            this.gvVisao.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gvVisao.OptionsView.ShowAutoFilterRow = true;
            this.gvVisao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.gvVisao.OptionsView.ShowFooter = true;
            this.gvVisao.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvVisao_RowCellStyle);
            // 
            // bbiNovo
            // 
            this.bbiNovo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiEditar
            // 
            this.bbiEditar.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiExcluir
            // 
            this.bbiExcluir.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // dmPrincipal
            // 
            this.dmPrincipal.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dpMenuLateral});
            // 
            // dpMenuLateral
            // 
            this.dpMenuLateral.Controls.Add(this.dockPanel1_Container);
            this.dpMenuLateral.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dpMenuLateral.ID = new System.Guid("f5122f6d-5e8a-4e08-9b1d-e7b93ca1f5de");
            this.dpMenuLateral.Location = new System.Drawing.Point(0, 26);
            this.dpMenuLateral.Name = "dpMenuLateral";
            this.dpMenuLateral.Options.AllowDockAsTabbedDocument = false;
            this.dpMenuLateral.Options.AllowDockBottom = false;
            this.dpMenuLateral.Options.AllowDockFill = false;
            this.dpMenuLateral.Options.AllowDockTop = false;
            this.dpMenuLateral.Options.AllowFloating = false;
            this.dpMenuLateral.Options.FloatOnDblClick = false;
            this.dpMenuLateral.Options.ShowCloseButton = false;
            this.dpMenuLateral.Options.ShowMaximizeButton = false;
            this.dpMenuLateral.OriginalSize = new System.Drawing.Size(200, 200);
            this.dpMenuLateral.Size = new System.Drawing.Size(200, 371);
            this.dpMenuLateral.Text = "Filtro";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.vgcFiltros);
            this.dockPanel1_Container.Controls.Add(this.sbCarregar);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(194, 339);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // vgcFiltros
            // 
            this.vgcFiltros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vgcFiltros.Location = new System.Drawing.Point(0, 0);
            this.vgcFiltros.Name = "vgcFiltros";
            this.vgcFiltros.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rideData});
            this.vgcFiltros.RowHeaderWidth = 90;
            this.vgcFiltros.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.cFiltroGrupo,
            this.cDados});
            this.vgcFiltros.Size = new System.Drawing.Size(194, 316);
            this.vgcFiltros.TabIndex = 0;
            // 
            // rideData
            // 
            this.rideData.AutoHeight = false;
            this.rideData.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rideData.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rideData.Name = "rideData";
            // 
            // cFiltroGrupo
            // 
            this.cFiltroGrupo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rDtInitial,
            this.rDtEnd});
            this.cFiltroGrupo.Name = "cFiltroGrupo";
            this.cFiltroGrupo.Properties.Caption = "Período";
            // 
            // rDtInitial
            // 
            this.rDtInitial.Name = "rDtInitial";
            this.rDtInitial.Properties.Caption = "Dt. Inicial";
            this.rDtInitial.Properties.Format.FormatString = "d";
            this.rDtInitial.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rDtInitial.Properties.RowEdit = this.rideData;
            // 
            // rDtEnd
            // 
            this.rDtEnd.Name = "rDtEnd";
            this.rDtEnd.Properties.Caption = "Dt. Final";
            this.rDtEnd.Properties.Format.FormatString = "d";
            this.rDtEnd.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rDtEnd.Properties.RowEdit = this.rideData;
            // 
            // cDados
            // 
            this.cDados.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rTimeParts});
            this.cDados.Height = 15;
            this.cDados.Name = "cDados";
            this.cDados.Properties.Caption = "Dados";
            // 
            // rTimeParts
            // 
            this.rTimeParts.Height = 19;
            this.rTimeParts.Name = "rTimeParts";
            this.rTimeParts.Properties.Caption = "Minutos";
            // 
            // sbCarregar
            // 
            this.sbCarregar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.sbCarregar.Location = new System.Drawing.Point(0, 316);
            this.sbCarregar.Name = "sbCarregar";
            this.sbCarregar.Size = new System.Drawing.Size(194, 23);
            this.sbCarregar.TabIndex = 1;
            this.sbCarregar.Text = "Carregar";
            this.sbCarregar.Click += new System.EventHandler(this.sbCarregar_Click);
            // 
            // frmVsJobShop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 397);
            this.Controls.Add(this.dpMenuLateral);
            this.Name = "frmVsJobShop";
            this.Text = "(PD) Gargalo de operações";
            this.Load += new System.EventHandler(this.frmVsMachineLoad_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dpMenuLateral, 0);
            this.Controls.SetChildIndex(this.gcGrid, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bmControleBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImagens16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prsImpressao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dmPrincipal)).EndInit();
            this.dpMenuLateral.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vgcFiltros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideData.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockPanel dpMenuLateral;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraVerticalGrid.VGridControl vgcFiltros;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rideData;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cFiltroGrupo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rDtInitial;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rDtEnd;
        private DevExpress.XtraEditors.SimpleButton sbCarregar;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cDados;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rTimeParts;
    }
}