﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsOrderBottleneck : frmVsPadrao
    {
        private readonly MachineLoadVM _MachineLoadVM;

        public frmVsOrderBottleneck()
        {
            InitializeComponent();
            _MachineLoadVM = new MachineLoadVM();
        }

        private async void sbCarregar_Click(object sender, EventArgs e)
        {
            await BotaoAtualizar();
        }

        private async void frmVsMachineLoad_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _MachineLoadVM._IdScenario = Parameters.IdScenario;
            await _MachineLoadVM.LoadOperationBottleneck();

            gcGrid.DataSource = _MachineLoadVM._OperationBottleneckDataSource;
            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                fmtGridView.OcultaColuna(
                    gvVisao.Columns["idjob"],
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["subject"],
                    gvVisao.Columns["location"],
                    gvVisao.Columns["codroteiro"],
                    gvVisao.Columns["codprograma"],
                    gvVisao.Columns["qtdapont"],
                    gvVisao.Columns["qtdrefugo"],
                    gvVisao.Columns["percconcluido"],
                    gvVisao.Columns["labelop"],
                    gvVisao.Columns["statusop"],
                    gvVisao.Columns["desclabelop"],
                    gvVisao.Columns["descstatusop"],
                    gvVisao.Columns["resourceid"],
                    gvVisao.Columns["corop"],
                    gvVisao.Columns["alterado"],
                    gvVisao.Columns["prodhoraria"],
                    gvVisao.Columns["descoperacao"],
                    gvVisao.Columns["jobsimulado"],
                    gvVisao.Columns["codpedvenda"],
                    gvVisao.Columns["IdLoadData"],
                    gvVisao.Columns["descricao"]
                );
                

                gvVisao.Columns["codordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Descending);
                gvVisao.Columns["nivelordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            gvVisao.HideLoadingPanel();
        }
        #endregion        
        
        ~frmVsOrderBottleneck()
        {
            _MachineLoadVM.Dispose();
        }
    }
}
