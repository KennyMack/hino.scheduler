﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Utils;

namespace Hino.Scheduler.Production
{
    public partial class frmScScheduledOrdersRotinas : frmScPadrao
    {
        private readonly RoutinesSequenceVM _RoutinesSequenceVM;
        public frmScScheduledOrdersRotinas()
        {
            InitializeComponent();
            _RoutinesSequenceVM = new RoutinesSequenceVM();

            scSchedulerOP.DefaultProperties(SchedulerViewType.Gantt);
            this.OnRefreshScenario += FrmScScheduledOrdersRotinas_OnRefreshScenario;
            scSchedulerOP.AppointmentDrag += fmtSchedulerControl.AppointmentDrag;
            scSchedulerOP.AppointmentDrop += fmtSchedulerControl.AppointmentDrag;
        }

        private async void FrmScScheduledOrdersRotinas_OnRefreshScenario(object sender, EventArgs e) =>
            await BotaoAtualizar();

        private void MenuItemSelectOP_Click(object sender, EventArgs e)
        {
            if (scSchedulerOP.SelectedAppointments.Count > 0)
            {
                var selected = scSchedulerOP.SelectedAppointments[0];

                foreach (var item in ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() ==
                    selected.CustomFields["Codordprod"].ToString()))
                    scSchedulerOP.SelectedAppointments.Add(item);
            }
        }

        private void frmScScheduledOrdersRotinas_Load(object sender, EventArgs e)
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";
        }

        private async Task BotaoAtualizar()
        {
            bsiScenario.Caption = $"Cenário: {Parameters.IdScenario}";

            _RoutinesSequenceVM._IdScenario = Parameters.IdScenario;
            await _RoutinesSequenceVM.LoadData();
            if (_RoutinesSequenceVM.Errors.Count > 0)
                MessageBox.Show(_RoutinesSequenceVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            enRotinasBindingSource.DataSource = _RoutinesSequenceVM._RoutineDataSource;
            pdJobRotBindingSource.DataSource = _RoutinesSequenceVM._JobRotDataSource;
        }

        private void scSchedulerOP_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e) =>
            scSchedulerOP.AppointmentCustomizing(e,
                (beiHighlightOP.EditValue ?? "").ToString(),
                bciShowStatus.Checked);

        private async void bbiBotaoAtualizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private async void ssSchedulerOP_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            var Appointment = ((IList<Appointment>)e.Objects);

            await _RoutinesSequenceVM.SaveChangesAppointmentsRoutines(Appointment);
            scSchedulerOP.Refresh();
        }

        ~frmScScheduledOrdersRotinas()
        {
            enRotinasBindingSource.Dispose();
            pdJobRotBindingSource.Dispose();
            _RoutinesSequenceVM.Dispose();
        }

        private async void frmScScheduledOrders_Shown(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();
            ssManager.ShowWaitForm();
            await BotaoAtualizar();
            ssManager.CloseWaitForm();
        }

        private void scSchedulerOP_SelectionChanged(object sender, EventArgs e)
        {
            scSchedulerOP.Refresh();
            ssSchedulerOP.AppointmentDependencies.Clear();

            if (scSchedulerOP.SelectedAppointments.Any() && bciCheckDependencies.Checked)
            {
                var Ops = scSchedulerOP.SelectedAppointments.Select(r => r.CustomFields["Codordprod"].ToString()).Distinct();

                foreach (var item in Ops)
                {
                    var childsOP = ssSchedulerOP.Appointments.Items.Where(r => r.CustomFields["Codordprod"].ToString() == item).OrderBy(r => r.Id);
                    if (childsOP.Count() > 1)
                    {
                        var FirstParent = childsOP.FirstOrDefault();
                        if (FirstParent != null)
                        {
                            foreach (var child in childsOP)
                            {
                                var dependencies = ssSchedulerOP.Appointments.Items.Where(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() == child.CustomFields["Nivelordprod"].ToString() &&
                                    r.CustomFields["Nivelordprodpai"].ToString() != r.CustomFields["Nivelordprod"].ToString()
                                );

                                var parent = ssSchedulerOP.Appointments.Items.FirstOrDefault(r =>
                                    r.CustomFields["Codordprod"].ToString() == child.CustomFields["Codordprod"].ToString() &&
                                    r.CustomFields["Nivelordprod"].ToString() == child.CustomFields["Nivelordprod"].ToString()
                                );

                                if (parent != null && dependencies.Count() > 0)
                                {
                                    foreach (var dep in dependencies)
                                    {
                                        AppointmentDependency aptDependency = ssSchedulerOP.AppointmentDependencies.CreateAppointmentDependency(dep.Id, parent.Id);
                                        ssSchedulerOP.AppointmentDependencies.Add(aptDependency);
                                        aptDependency.Type = AppointmentDependencyType.FinishToStart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void beiHighlightOP_EditValueChanged(object sender, EventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            Hino.Scheduler.Production.frmAppointmentDetailRot form = 
                new Hino.Scheduler.Production.frmAppointmentDetailRot(scheduler, e.Appointment, false);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }
        }

        private void bciShowStatus_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e) =>
            scSchedulerOP.Refresh();

        private void scSchedulerOP_CustomDrawAppointmentBackground(object sender, CustomDrawObjectEventArgs e) =>
            fmtSchedulerControl.CustomDraw(false, sender, e);

        private void scSchedulerOP_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)=>
            fmtSchedulerControl.PopupMenuShowing(e.Menu,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void Menu_CloseUp(object sender, EventArgs e) =>
            fmtSchedulerControl.CloseMenu(sender, e,
                MenuItemSelectOP_Click,
                Menu_CloseUp);

        private void ssSchedulerOP_AppointmentChanging(object sender, PersistentObjectCancelEventArgs e)
        {
            e.Cancel = true;
            /*var resourceSelected = (Appointment)(e.Object);
            var resource = ssSchedulerOP.Resources.Items.Where(r => r.Id == resourceSelected.ResourceId).FirstOrDefault();

            if (resource != null &&
                resource.CustomFields["Codmaquina"] == null)
            {

                MessageBox.Show("Selecione uma máquina.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                e.Cancel = true;
            }*/
        }

        private void scSchedulerOP_AllowAppointmentConflicts(object sender, AppointmentConflictEventArgs e)
        {
            e.Conflicts.Clear();

            if (e.AppointmentClone.Duration.TotalMinutes < Convert.ToDouble(e.Appointment.CustomFields["Minimaltime"]) && 
                bciCheckDependencies.Checked)
            {
                e.Conflicts.Add(e.AppointmentClone);
                MessageBox.Show("Tempo da O.P. é menor que o mínimo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

            AppointmentDependencyBaseCollection depCollectionDep =
                ssSchedulerOP.AppointmentDependencies.Items.GetDependenciesByDependentId(e.Appointment.Id);
            if (depCollectionDep.Count > 0)
            {
                if (CheckForInvalidDependenciesAsDependent(depCollectionDep, e.AppointmentClone))
                    e.Conflicts.Add(e.AppointmentClone);
            }

            AppointmentDependencyBaseCollection depCollectionPar =
                ssSchedulerOP.AppointmentDependencies.Items.GetDependenciesByParentId(e.Appointment.Id);
            if (depCollectionPar.Count > 0)
            {
                if (CheckForInvalidDependenciesAsParent(depCollectionPar, e.AppointmentClone))
                    e.Conflicts.Add(e.AppointmentClone);
            }
        }

        private bool CheckForInvalidDependenciesAsDependent(AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.ParentId).End;
                    if (apt.Start < checkTime)
                        return true;
                }
            }
            return false;
        }

        private bool CheckForInvalidDependenciesAsParent(AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.DependentId).Start;
                    if (apt.End > checkTime)
                        return true;
                }
            }
            return false;
        }

        private void ttcMessages_BeforeShow(object sender, DevExpress.Utils.ToolTipControllerShowEventArgs e) =>
            fmtSchedulerControl.CustomToolTip(ttcMessages, sender, e);
    }
}