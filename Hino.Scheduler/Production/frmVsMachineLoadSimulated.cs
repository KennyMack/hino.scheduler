﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Production
{
    public partial class frmVsMachineLoadSimulated : frmVsPadrao
    {
        private bool _LayoutDetail;
        private readonly MachineLoadVM _MachineLoadVM;

        public frmVsMachineLoadSimulated()
        {
            InitializeComponent();
            _MachineLoadVM = new MachineLoadVM();
            gvJobs.DefinePadrao();
            _LayoutDetail = true;
        }


        ~frmVsMachineLoadSimulated()
        {
            _MachineLoadVM.Dispose();
        }

        private async void sbCarregar_Click(object sender, EventArgs e)
        {
            await BotaoAtualizar();
        }

        private async void frmVsMachineLoad_Load(object sender, EventArgs e)
        {
            await TratarCarregarFormulario();
            gvVisao.MoveFirst();
            _Initialize = false;
        }

        #region Botão atualizar
        protected async override Task BotaoAtualizar()
        {
            Int32 frh = gvVisao.FocusedRowHandle;
            gvVisao.ShowLoadingPanel();

            _MachineLoadVM._IdScenario = Parameters.IdScenario;
            if (!await _MachineLoadVM.LoadMachineLoadSimulated())
            {
                MessageBox.Show(_MachineLoadVM.Errors[0], "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                return;
            }

            gcGrid.DataSource = _MachineLoadVM._MachineLoadSimulatedDataSource;

            if (_Initialize)
            {
                gvVisao.BestFitColumns();
                fmtGridView.OcultaColuna(
                    gvVisao.Columns["codestab"],
                    gvVisao.Columns["cargamaquina"],
                    gvVisao.Columns["minsdisponiveis"],
                    gvVisao.Columns["minhoraprod"]
                );
                gvVisao.Columns["dtproducao"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                gvVisao.Columns["codmaquina"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
            }

            gvVisao.FocusedRowHandle = frh;
            await LoadJobsDetail();
            gvVisao.HideLoadingPanel();
        }
        #endregion

        private void splitterControl1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }


        #region Load Jobs Detail
        private async Task LoadJobsDetail()
        {
            gvJobs.ShowLoadingPanel();

            if (gvVisao.FocusedRowHandle > -1)
            {
                await _MachineLoadVM.LoadDetailOpMachineLoadSimulated(
                    Convert.ToString(gvVisao.GetFocusedRowCellValue("codmaquina")),
                    Convert.ToDateTime(gvVisao.GetFocusedRowCellValue("dtproducao")));

                _MachineLoadVM._IdScenario = Parameters.IdScenario;
                gcJobs.DataSource = _MachineLoadVM._DetailOpMachineLoadSimulatedDataSource;

                if (_LayoutDetail)
                {
                    _LayoutDetail = false;
                    var vsIndex = 0;
                    gvJobs.Columns["codmaquina"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["codproduto"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["fsprodutodescricao"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["codordprod"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["nivelordprod"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["operacao"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["descminhora"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["dtiniciooper"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["dttermoper"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["programado"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["realizado"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["refugado"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["dtinicio"].OrdenaColuna(vsIndex++);
                    gvJobs.Columns["dttermino"].OrdenaColuna(vsIndex++);

                    gvJobs.Columns["nivelordprodpai"].OrdenaColuna(vsIndex++);

                    fmtGridView.OcultaColuna(
                        gvJobs.Columns["codestab"],
                        gvJobs.Columns["descminsacumulados"]
                    );
                    gvJobs.Columns["codordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                    gvJobs.Columns["nivelordprod"].OrdenarGrid(DevExpress.Data.ColumnSortOrder.Ascending);
                    gvJobs.BestFitColumns();
                }
            }
            gvJobs.HideLoadingPanel();
        }
        #endregion

        private async void gvVisao_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            await LoadJobsDetail();

        }
    }
}
