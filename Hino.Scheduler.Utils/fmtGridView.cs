﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using System.Data;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System.IO;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using System.Linq.Expressions;

namespace Hino.Scheduler.Utils
{
    public static class fmtGridView
    {
        #region Oculta colunas
        /// <summary>
        /// Oculta as colunas do grid
        /// </summary>
        /// <param name="Columns">Colunas</param>
        /// <param name="ShowInCustomizationForm">ShowInCustomizationForm (padrão true)</param>
        public static void OcultaColuna(params DevExpress.XtraGrid.Columns.GridColumn[] Columns)
        {
            for (int i = 0, length = Columns.Length; i < length; i++)
            {
                Columns[i].OcultaColuna();
            }            
        }
        /// <summary>
        /// Oculta coluna do grid
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="ShowInCustomizationForm">ShowInCustomizationForm (padrão true)</param>        
        public static void OcultaColuna(this DevExpress.XtraGrid.Columns.GridColumn Column)
        {
            if (Column != null)
            {
                Column.Visible = false;
            }
        }

        /// <summary>
        /// Oculta as colunas do treelist
        /// </summary>
        /// <param name="Columns">Colunas</param>
        /// <param name="ShowInCustomizationForm">ShowInCustomizationForm (padrão true)</param>
        public static void OcultaColuna(params DevExpress.XtraTreeList.Columns.TreeListColumn[] Columns)
        {
            for (int i = 0, length = Columns.Length; i < length; i++)
            {
                Columns[i].OcultaColuna();
            }
        }
        /// <summary>
        /// Oculta coluna do treelist
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="ShowInCustomizationForm">ShowInCustomizationForm (padrão true)</param>        
        public static void OcultaColuna(this DevExpress.XtraTreeList.Columns.TreeListColumn Column)
        {
            if (Column != null)
            {
                Column.Visible = false;
            }
        }

        /// <summary>
        /// Oculta a exibição da coluna no 
        /// form de colunas do gridview
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="ShowInCustomizationForm">True-> Exibe False -> Oculta</param>
        /// <returns></returns>
        public static DevExpress.XtraGrid.Columns.GridColumn MostraColuna(this DevExpress.XtraGrid.Columns.GridColumn Column, bool ShowInCustomizationForm = true)
        {
            if (Column != null)
                Column.OptionsColumn.ShowInCustomizationForm = ShowInCustomizationForm;

            return Column;
        }
        #endregion

        #region Ordena Linhas
        /// <summary>
        /// Ordena as linhas do grid
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="SortOrder">Ordenação</param>
        public static void OrdenarGrid(this DevExpress.XtraGrid.Columns.GridColumn Column, DevExpress.Data.ColumnSortOrder SortOrder = DevExpress.Data.ColumnSortOrder.Ascending)
        {
            if (Column != null)
            {
                Column.SortOrder = SortOrder;
            }
        }
        #endregion

        #region Ordena Linhas
        /// <summary>
        /// Ordena as linhas do treelist
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="SortOrder">Ordenação</param>
        public static void OrdenarGrid(this DevExpress.XtraTreeList.Columns.TreeListColumn Column, SortOrder SortOrder = SortOrder.Ascending)
        {
            if (Column != null)
            {
                Column.SortOrder = SortOrder;
            }
        }
        #endregion

        #region Define o Caption
        /// <summary>
        /// Define o Caption da Coluna
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="pCaption">Caption</param>
        public static void DefineCaption(this DevExpress.XtraGrid.Columns.GridColumn Column, string pCaption)
        {
            if (Column != null)
            {
                Column.Caption = fmtDescCampos.trataDescricao(pCaption);
            }
        }
        #endregion

        #region Define as propriedades padrões do grid Search
        /// <summary>
        /// Define as propriedades padrões do grid Search
        /// </summary>
        /// <param name="GridView">Grid de visão</param>
        public static void DefinePadraoSearch<T>(this T GridView) where T : DevExpress.XtraGrid.Views.Grid.GridView
        {
            GridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            GridView.OptionsBehavior.ReadOnly = true;
            GridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            GridView.OptionsView.ColumnAutoWidth = false;
            GridView.OptionsView.ShowGroupPanel = false;
            GridView.BestFitMaxRowCount = 10;
            GridView.PopupMenuShowing += GridView_PopupMenuShowing;
        }
        #endregion

        #region Define as propriedades padrões do grid
        /// <summary>
        /// Define as propriedades padrões do grid
        /// </summary>
        /// <param name="GridView">Grid de visão</param>
        public static void DefinePadrao<T>(this T GridView) where T : DevExpress.XtraGrid.Views.Grid.GridView
        {
            GridView.OptionsBehavior.ReadOnly = true;
            GridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            GridView.OptionsFilter.UseNewCustomFilterDialog = false;
            GridView.OptionsView.ColumnAutoWidth = false;
            GridView.OptionsView.ShowFooter = false;
            GridView.OptionsView.ShowGroupPanel = false;
            GridView.BestFitMaxRowCount = 10;
            GridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            GridView.PopupMenuShowing += GridView_PopupMenuShowing;
        }
        #endregion

        #region Define as propriedades padrões do treelist
        /// <summary>
        /// Define as propriedades padrões do treelist
        /// </summary>
        /// <param name="GridView">Grid de visão</param>
        public static void DefineTreeListPadrao<T>(this T TreeL) where T : DevExpress.XtraTreeList.TreeList
        {
            TreeL.OptionsBehavior.ReadOnly = true;
            TreeL.OptionsView.AutoWidth = false;
            TreeL.BestFitVisibleOnly = true;
        }
        #endregion

        #region Encerra a edição do grid e atualiza os dados
        /// <summary>
        /// Encerra a edição do grid e atualiza os dados
        /// </summary>
        /// <param name="GridView">Grid de visão</param>
        public static void CommitEditor<T>(this T GridView) where T : DevExpress.XtraGrid.Views.Grid.GridView
        {
            GridView.PostEditor();
            GridView.UpdateCurrentRow();
            GridView.CloseEditor();
        }
        #endregion

        #region Popup Menu
        private static void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Column)
            {
                DevExpress.XtraGrid.Menu.GridViewColumnMenu columnMenu = e.Menu as DevExpress.XtraGrid.Menu.GridViewColumnMenu;

                DevExpress.Utils.Menu.DXSubMenuItem subMenuExp = new DevExpress.Utils.Menu.DXSubMenuItem("Exportar Grid");
                subMenuExp.BeginGroup = true;

                DevExpress.Utils.Menu.DXMenuItem menuItemXLSX =
                            new DevExpress.Utils.Menu.DXMenuItem("XLSX",
                                new EventHandler(OnExportGrid), null);
                menuItemXLSX.Image = (System.Drawing.Image)Properties.Resources.excel_icone_16;
                menuItemXLSX.Tag = new object[] { e.Menu.View, "xlsx" };
                subMenuExp.Items.Add(menuItemXLSX);

                DevExpress.Utils.Menu.DXMenuItem menuItemXLS =
                            new DevExpress.Utils.Menu.DXMenuItem("XLS",
                                new EventHandler(OnExportGrid), null);
                menuItemXLS.Image = (System.Drawing.Image)Properties.Resources.excel_icone_16;
                menuItemXLS.Tag = new object[] { e.Menu.View, "xls" };
                subMenuExp.Items.Add(menuItemXLS);

                DevExpress.Utils.Menu.DXMenuItem menuItemTXT =
                            new DevExpress.Utils.Menu.DXMenuItem("TXT",
                                new EventHandler(OnExportGrid), null);
                menuItemTXT.Tag = new object[] { e.Menu.View, "txt" };
                menuItemTXT.Image = (System.Drawing.Image)Properties.Resources.notepad_icone_16;
                subMenuExp.Items.Add(menuItemTXT);
                columnMenu.Items.Add(subMenuExp);

                if (e.Menu.View.Columns.Count > 0 &&
                    e.HitInfo.Column != null)
                {
                    var caption = e.HitInfo.Column.Fixed == FixedStyle.None ? "Travar Coluna" : "Destravar Coluna";
                    if (caption == "Destravar Coluna")
                    {
                        var itemDes = new DevExpress.Utils.Menu.DXMenuItem();
                        itemDes.Caption = caption;
                        itemDes.BeginGroup = true;
                        itemDes.Click += new EventHandler(click_destravar);
                        itemDes.Tag = new object[] { e.Menu.View, e.HitInfo.Column };
                        columnMenu.Items.Add(itemDes);
                    }
                    else
                    {
                        var itemLeft = new DevExpress.Utils.Menu.DXMenuItem();
                        var itemRight = new DevExpress.Utils.Menu.DXMenuItem();
                        itemLeft.BeginGroup = true;
                        itemLeft.Caption = "Travar à Esquerda";
                        itemLeft.Click += new EventHandler(click_travaEsq);
                        itemLeft.Tag = new object[] { e.Menu.View, e.HitInfo.Column };

                        columnMenu.Items.Add(itemLeft);

                        itemRight.Caption = "Travar à Direita";
                        itemRight.Click += new EventHandler(click_travaDir);
                        itemRight.Tag = new object[] { e.Menu.View, e.HitInfo.Column };
                        columnMenu.Items.Add(itemRight);
                    }
                }


                var item = new DevExpress.Utils.Menu.DXMenuItem();
                item.Caption = "Exibir/Ocultar Rodapé";
                item.Click += new EventHandler(click_Footer);
                item.BeginGroup = true;
                item.Tag = e.Menu.View;
                columnMenu.Items.Add(item);


                if (e.Menu.View.Columns.Count > 0)
                {
                    DevExpress.Utils.Menu.DXSubMenuItem subMenu = new DevExpress.Utils.Menu.DXSubMenuItem("Foca Coluna");
                    subMenu.BeginGroup = true;

                    var cols = e.Menu.View.Columns.OrderBy(r => r.Caption);

                    foreach (DevExpress.XtraGrid.Columns.GridColumn r in cols)
                    {
                        if (item.Visible && !string.IsNullOrWhiteSpace(r.Caption))
                        {
                            DevExpress.Utils.Menu.DXMenuItem menuItem =
                                new DevExpress.Utils.Menu.DXMenuItem(r.Caption,
                                    new EventHandler(OnFocusedColumn), null);
                            menuItem.Tag = new object[] { e.Menu.View, r.FieldName };
                            subMenu.Items.Add(menuItem);
                        }
                    }
                    columnMenu.Items.Add(subMenu);
                }


            }
        }

        private static void OnExportGrid(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;
            var grid = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));

            try
            {
                var ex = obj[1].ToString();

                var arq = string.Concat(Path.GetTempPath() + "\\", Guid.NewGuid(), ".", ex.ToLower());

                if (grid.GridControl.ViewCollection.Count > 1)
                {
                    var resp = MessageBox.Show("Exportar detalhes?", "Exportar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (resp == System.Windows.Forms.DialogResult.Yes)
                        grid.OptionsPrint.PrintDetails = true;
                    else
                        grid.OptionsPrint.PrintDetails = false;
                }

                switch (arq.Substring(arq.LastIndexOf(".")))
                {
                    case ".txt":
                        grid.ExportToText(arq);
                        break;
                    case ".xls":
                        {
                            XlsExportOptionsEx xlsxOptionsExp = new XlsExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            grid.ExportToXls(arq, xlsxOptionsExp);
                        }
                        break;
                    case ".xlsx":
                        {
                            XlsxExportOptionsEx xlsxOptionsExp = new XlsxExportOptionsEx();
                            xlsxOptionsExp.ExportType = ExportType.WYSIWYG;
                            grid.ExportToXlsx(arq, xlsxOptionsExp);
                        }
                        break;
                }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = arq;
                process.StartInfo.Verb = "Open";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                process.Start();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possível gerar os dados.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

        }

        private static void click_travaDir(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;
            var gv = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));
            var col = (GridColumn)obj[1];

            if (gv != null)
            {
                if (gv.Columns.Count > 0 && col != null)
                    col.Fixed = FixedStyle.Right;
            }
        }

        private static void click_travaEsq(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;
            var gv = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));
            var col = (GridColumn)obj[1];

            if (gv != null)
            {
                if (gv.Columns.Count > 0 && col != null)
                    col.Fixed = FixedStyle.Left;
            }
        }

        private static void click_destravar(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;
            var gv = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));
            var col = (GridColumn)obj[1];

            if (gv != null)
            {
                if (gv.Columns.Count > 0 && col != null)
                    col.Fixed = FixedStyle.None;
            }
        }

        private static void OnFocusedColumn(object sender, EventArgs e)
        {
            var obj = (object[])((DevExpress.Utils.Menu.DXMenuItem)sender).Tag;

            var gv = ((DevExpress.XtraGrid.Views.Grid.GridView)(obj[0]));
            var col = obj[1].ToString(); 

            if (gv != null)
            {
                if (gv.Columns.Count > 0 && col != null)
                    gv.FocusedColumn = gv.Columns[col];
            }

        }

        private static void click_Footer(object sender, EventArgs e)
        {
            var gv = ((DevExpress.XtraGrid.Views.Grid.GridView)
                (((DevExpress.Utils.Menu.DXMenuItem)sender).Tag));

            if (gv != null)
                gv.OptionsView.ShowFooter = !gv.OptionsView.ShowFooter;
        }
        #endregion

        #region Campo observação
        /// <summary>
        /// Campo de observação
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Observacao(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit rimObservacao =
                    new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
                rimObservacao.AutoHeight = true;
                rimObservacao.Name = "rimObservacao";

                coluna.OptionsColumn.AllowEdit = true;
                coluna.OptionsColumn.ReadOnly = false;
                coluna.ColumnEdit = rimObservacao;
                coluna.OptionsFilter.AllowFilter = false;
                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion 
       
        #region Campo observação
        /// <summary>
        /// Cria combobox com imagem no grid
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void ComboImagem(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit rimObservacao =
                    new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
                rimObservacao.AutoHeight = true;
                rimObservacao.Name = "rimObservacao";

                coluna.OptionsColumn.AllowEdit = true;
                coluna.OptionsColumn.ReadOnly = false;
                coluna.ColumnEdit = rimObservacao;
                coluna.OptionsFilter.AllowFilter = false;
                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion
        
        #region Campo Percentual
        /// <summary>
        /// Campo de Percentual
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Percentual(DevExpress.XtraGrid.Columns.GridColumn coluna, bool pMostraValor, int pMinimo = 0, int pMaximo = 100)
        {
            if (coluna != null)
            {
                RepositoryItemProgressBar rimPercentual = new RepositoryItemProgressBar();
                rimPercentual.Minimum = pMinimo;
                rimPercentual.Maximum = pMaximo;
                rimPercentual.ShowTitle = pMostraValor;

                rimPercentual.AutoHeight = true;
                rimPercentual.Name = "rimPercentual";

                coluna.OptionsColumn.AllowEdit = false;
                coluna.OptionsColumn.ReadOnly = true;
                coluna.ColumnEdit = rimPercentual;
                coluna.OptionsFilter.AllowFilter = false;
            }
        }
        #endregion        

        #region Campo Numérico
        /// <summary>
        /// Campo Numérico
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Numerico(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit rimCalcEdit =
                    new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
                rimCalcEdit.AutoHeight = true;
                rimCalcEdit.Name = "rimCalcEdit";

                coluna.ColumnEdit = rimCalcEdit;
                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion  
 
        #region Campo Numérico
        /// <summary>
        /// Campo Numérico
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Numero(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit rimCalcEdit =
                    new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
                rimCalcEdit.AutoHeight = true;
                rimCalcEdit.Name = "rimCalcEdit";
                rimCalcEdit.EditFormat.FormatString = "n0";
                rimCalcEdit.EditMask = "n0";
                rimCalcEdit.MaxLength = 3;
                coluna.ColumnEdit = rimCalcEdit;
                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion   
    
        #region Checked Box Exibir
        public static void CheckBoxEdicao(DevExpress.XtraGrid.Columns.GridColumn coluna, String caption = " ")
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ckFlag = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                coluna.OptionsColumn.AllowEdit = true;
                coluna.OptionsColumn.ReadOnly = false;
                coluna.ColumnEdit = ckFlag;
                coluna.Caption = caption;
                coluna.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                coluna.Visible = true;
                ckFlag.ValueChecked =  "1";
                ckFlag.ValueUnchecked = "0";
                ckFlag.AllowGrayed = false;
            }
        }
        #endregion

        #region Checked Box Exibir
        public static void CheckBoxExibir(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ckFlag = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                ckFlag.ValueChecked = "1";
                ckFlag.ValueUnchecked = "0";
                ckFlag.AllowGrayed = false;
                coluna.ColumnEdit = ckFlag;
            }
        }
        #endregion

        #region Campo Data
        /// <summary>
        /// Habilita o calendário para edição no grid
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Data(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rimDateEdit =
                    new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
                rimDateEdit.AutoHeight = true;
                rimDateEdit.Name = "rimDateEdit";
                rimDateEdit.AllowMouseWheel = false;
                rimDateEdit.Mask.EditMask = "d";
                rimDateEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;

                coluna.ColumnEdit = rimDateEdit;
                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion

        #region Trata Grid
        /// <summary>
        /// Preenche o caption das colunas do grid
        /// </summary>
        /// <param name="Grid">GridView</param>
        /// <param name="Classe">Classe</param>
        public static void TrataExibirGrid(DevExpress.XtraGrid.Views.Grid.GridView Grid, object Classe)
        {
            string caption = "";
            if ((Grid != null) && (Classe != null))
            {
                for (int i = 0, length = Grid.Columns.Count; i < length; i++)
                {
                    if (Grid.Columns[i] != null)
                    {
                        Grid.Columns[i].Visible = true;
                        if ((Grid.Columns[i].Caption == "") ||
                            (Grid.Columns[i].Caption == Grid.Columns[i].FieldName))
                        {
                            caption = (String)Classe.GetType().GetMethod("GetDescCampo").Invoke(Classe, new object[] { Grid.Columns[i].FieldName });

                            Grid.Columns[i].Caption = fmtDescCampos.trataDescricao(caption);
                        }
                        if (Grid.Columns[i].Caption == "")
                                Grid.Columns[i].Visible = false;

                    }
                }
            }
        }

        /// <summary>
        /// Preenche o caption das colunas do grid
        /// </summary>
        /// <param name="Grid">GridView</param>
        /// <param name="Classe">Classes</param>
        public static void TrataExibirGrid(DevExpress.XtraGrid.Views.Grid.GridView Grid, params object[] Classe)
        {
            for (int i = 0, length = Classe.Length; i < length; i++)
            {
                if (Classe[i] != null)
                    fmtGridView.TrataExibirGrid(Grid, Classe[i]);
            }
        }
        #endregion
        
        #region Trata Treelist
        /// <summary>
        /// Preenche o caption das colunas do treelist
        /// </summary>
        /// <param name="treeL">Treelist</param>
        /// <param name="Classe">Classe</param>
        public static void TrataExibirTreeList(DevExpress.XtraTreeList.TreeList treeL, object Classe)
        {
            string caption = "";
            if ((treeL != null) && (Classe != null))
            {
                for (int i = 0, length = treeL.Columns.Count; i < length; i++)
                {
                    if (treeL.Columns[i] != null)
                    {
                        treeL.Columns[i].Visible = true;
                        if ((treeL.Columns[i].Caption == "") ||
                            (treeL.Columns[i].Caption == treeL.Columns[i].FieldName))
                        {
                            caption = (String)Classe.GetType().GetMethod("GetDescCampo").Invoke(Classe, new object[] { treeL.Columns[i].FieldName });

                            treeL.Columns[i].Caption = fmtDescCampos.trataDescricao(caption);
                        }
                        if (treeL.Columns[i].Caption == "")
                            treeL.Columns[i].Visible = false;

                    }
                }
            }
        }

        /// <summary>
        /// Preenche o caption das colunas do treelist
        /// </summary>
        /// <param name="Grid">Treelist</param>
        /// <param name="Classe">Classes</param>
        public static void TrataExibirTreeList(DevExpress.XtraTreeList.TreeList treeL, params object[] Classe)
        {
            for (int i = 0, length = Classe.Length; i < length; i++)
            {
                if (Classe[i] != null)
                    fmtGridView.TrataExibirTreeList(treeL, Classe[i]);
            }
        }
        #endregion

        #region Ordena colunas
        /// <summary>
        /// Altera a ordem de visualização das colunas do grid
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="VisibleIndex">Index da coluna</param>
        /// <returns></returns>
        public static DevExpress.XtraGrid.Columns.GridColumn OrdenaColuna(this DevExpress.XtraGrid.Columns.GridColumn Column, int VisibleIndex)
        {
            if (Column != null)
                Column.VisibleIndex = VisibleIndex;

            return Column; 
        }
        #endregion

        #region Ordena colunas
        /// <summary>
        /// Altera a ordem de visualização das colunas do treelist
        /// </summary>
        /// <param name="Column">Coluna</param>
        /// <param name="VisibleIndex">Index da coluna</param>
        /// <returns></returns>
        public static DevExpress.XtraTreeList.Columns.TreeListColumn OrdenaColuna(this DevExpress.XtraTreeList.Columns.TreeListColumn Column, int VisibleIndex)
        {
            if (Column != null)
                Column.VisibleIndex = VisibleIndex;

            return Column;
        }
        #endregion

        #region Coluna Botão
        /// <summary>
        /// Cria um botão no Grid
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pClick">Evento click da coluna</param>
        /// <param name="pGlyph">Estilo do botão</param>
        public static void Botao(DevExpress.XtraGrid.Columns.GridColumn pColuna, EventHandler pClick, DevExpress.XtraEditors.Controls.ButtonPredefines pGlyph = DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
        {
            if (pColuna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ritem = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
                ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                ritem.Buttons[0].Kind = pGlyph;
                ritem.Click += pClick;
                pColuna.ColumnEdit = ritem;
            }
        }
        #endregion

        #region Coluna Botão com imagem
        /// <summary>
        /// Cria um botão no grid
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pImage">Imagem do botão</param>
        /// <param name="pClick">Evento click da coluna</param>
        public static void Botao(DevExpress.XtraGrid.Columns.GridColumn pColuna, System.Drawing.Image pImage, EventHandler pClick)
        {
            if (pColuna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ritem = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
                ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                ritem.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
                ritem.Buttons[0].Image = pImage;
                ritem.Click += pClick;
                pColuna.ColumnEdit = ritem;
            }
        }
        #endregion

        #region Coluna Fixa
        /// <summary>
        /// Fixa a coluna ao rolar o grid
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pPosicao">Posicao que a coluna vai ficar</param>
        public static DevExpress.XtraGrid.Columns.GridColumn FixaColuna(this DevExpress.XtraGrid.Columns.GridColumn pColuna, DevExpress.XtraGrid.Columns.FixedStyle pPosicao)
        {
            if (pColuna != null)
            {
                pColuna.Fixed = pPosicao;
            }

            return pColuna;
        }
        #endregion

        #region Coluna Fixa
        /// <summary>
        /// Define se oculta a coluna
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pVisivel">Visivel</param>
        public static DevExpress.XtraGrid.Columns.GridColumn Visivel(this DevExpress.XtraGrid.Columns.GridColumn pColuna, bool pVisivel)
        {
            if (pColuna != null)
            {
                pColuna.Visible = pVisivel;
            }

            return pColuna;
        }
        #endregion

        #region Largura
        /// <summary>
        /// Define a largura da coluna
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pLargura">Largura</param>
        public static DevExpress.XtraGrid.Columns.GridColumn LarguraColuna(this DevExpress.XtraGrid.Columns.GridColumn pColuna, int pLargura)
        {
            if (pColuna != null)
            {
                pColuna.Width = pLargura;
            }

            return pColuna;
        }
        #endregion

        #region Campo de cor
        /// <summary>
        /// Campo de cor
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Cor(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit rimObservacao =
                    new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
                rimObservacao.AutoHeight = true;
                rimObservacao.Name = "rimCor";
                rimObservacao.ColorAlignment = DevExpress.Utils.HorzAlignment.Center;

                coluna.OptionsColumn.AllowEdit = true;
                coluna.OptionsColumn.ReadOnly = false;
                coluna.ColumnEdit = rimObservacao;
                coluna.OptionsFilter.AllowFilter = false;

                coluna.UnboundType = DevExpress.Data.UnboundColumnType.String;
            }
        }
        #endregion        

        #region Colore o Grid
        public static void ColoreLinha(object pSender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e, string pColunaNome, string pValor, Color pCorLinha)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = pSender as DevExpress.XtraGrid.Views.Grid.GridView;
            if (e.RowHandle >= 0)
            {
                try
                {
                    String vlrCelula = View.GetRowCellDisplayText(e.RowHandle, View.Columns[pColunaNome]).ToString().Trim();

                    if (vlrCelula == pValor)
                    {
                        e.Appearance.BackColor = pCorLinha;
                        e.Appearance.BackColor2 = pCorLinha;
                        e.Appearance.ForeColor = pCorLinha.CorDescricao();
                    }
                }
                catch 
                { 

                }

            }
        }
        #endregion

        #region Colore o Grid
        public static void ColoreLinha(object pSender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e, bool pValor, Color pCorLinha)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = pSender as DevExpress.XtraGrid.Views.Grid.GridView;
            if (e.RowHandle >= 0)
            {
                try
                {
                    if (pValor)
                    {
                        e.Appearance.BackColor = pCorLinha;
                        e.Appearance.BackColor2 = pCorLinha;
                        e.Appearance.ForeColor = pCorLinha.CorDescricao();
                    }
                }
                catch
                {

                }

            }
        }
        #endregion

        #region Colore coluna do grid
        public static void ColoreColuna(DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e, string pColunaNome, string pValor, Color pCorColuna)
        {
            if (e.Column.FieldName == pColunaNome)
            {
                if (e.RowHandle >= 0)
                {

                    if ((e.CellValue ?? "").ToString() == pValor)
                    {
                        e.Appearance.BackColor = pCorColuna;
                        e.Appearance.BackColor2 = pCorColuna;
                        e.Appearance.ForeColor = pCorColuna.CorDescricao();
                    }

                }
            }
        }
        #endregion

        #region Colore coluna do grid
        public static void ColoreColuna(DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e, string pColunaNome, bool pValor, Color pCorColuna)
        {
            if (e.Column.FieldName == pColunaNome)
            {
                if (e.RowHandle >= 0)
                {

                    if (pValor)
                    {
                        e.Appearance.BackColor = pCorColuna;
                        e.Appearance.BackColor2 = pCorColuna;
                        e.Appearance.ForeColor = pCorColuna.CorDescricao();
                    }
                }
            }
        }
        #endregion

        #region Imagem na coluna do grid
        /// <summary>
        /// Imagem na coluna do grid
        /// </summary>
        /// <param name="e">Event Arg</param>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pImage">Imagem da coluna</param>    
        public static void ImagemField(DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e,
                                       DevExpress.XtraGrid.Columns.GridColumn pColuna,
                                       System.Drawing.Image pImage)
        {
            if (pColuna != null)
            {
                if (pColuna.FieldName == e.Column.FieldName)
                {
                    e.Graphics.DrawImage(pImage, new Rectangle((int)Math.Truncate(e.Bounds.X + (decimal)(e.Column.Width / 2 - 8)), e.Bounds.Y, 16, 16));
                    e.Handled = true;
                }
            }
        }
        #endregion

        #region Imagem na coluna do grid
        /// <summary>
        /// Imagem na coluna do grid
        /// </summary>
        /// <param name="e">Event Arg</param>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pCondicao">Condição para exibir a imagem</param>
        /// <param name="pImage">Imagem da coluna</param>    
        public static void ImagemField(DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e,
                                       DevExpress.XtraGrid.Columns.GridColumn pColuna,
                                       bool pCondicao,
                                       System.Drawing.Image pImage)
        {
            if ((pColuna != null) && (pColuna.FieldName == e.Column.FieldName) && (pCondicao))
            {
                e.Graphics.DrawImage(pImage, new Rectangle((int)Math.Truncate(e.Bounds.X + (decimal)(e.Column.Width / 2 - 8)), e.Bounds.Y, 16, 16));
                e.Handled = true;
            }

        }
        #endregion

        #region Imagem na coluna do grid
        /// <summary>
        /// Imagem na coluna do grid
        /// </summary>
        /// <param name="e">Event Arg</param>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pValor">Valor condicional para exibir a imagem</param>
        /// <param name="pImage">Imagem da coluna</param>    
        public static void ImagemField(DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e,
                                       DevExpress.XtraGrid.Columns.GridColumn pColuna,
                                       string pValor,
                                       System.Drawing.Image pImage)
        {
            if ((pColuna != null) && (pColuna.FieldName == e.Column.FieldName) && (e.CellValue  != null)&& (e.CellValue.ToString() == pValor))
            {
                e.Graphics.DrawImage(pImage, new Rectangle((int)Math.Truncate(e.Bounds.X + (decimal)(e.Column.Width / 2 - 8)), e.Bounds.Y, 16, 16));
                e.Handled = true;
            }

        }
        #endregion

        #region Cria um Hyperlink
        /// <summary>
        /// Cria o HyperLink na Coluna
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pClick">Evento Click da Coluna</param>
        public static void HyperLink(DevExpress.XtraGrid.Columns.GridColumn pColuna, EventHandler pClick)
        {
            // Verificando se a coluna existe e se o tipo do form foi definido
            if (pColuna != null)
            {                
                // Instanciando a coluna
                var colLink = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();

                // Definindo como auto Height
                colLink.AutoHeight = true;

                // Definindo o nome da coluna
                colLink.Name = string.Format("colLink{0}", pColuna.FieldName);

                // Definindo o evento click da coluna
                colLink.Click += pClick;

                // Passando a coluna para o grid
                pColuna.ColumnEdit = colLink;

            }
        }
        #endregion

        #region CheckBox 1 click
        public static void CheckBox1Click<T>(this T pGrid) where T : DevExpress.XtraGrid.Views.Grid.GridView
        {
            if (pGrid != null)
            {
                pGrid.MouseDown += gv_MouseDown;

                /*GridHitInfo hitInfo = pGrid.CalcHitInfo(e.Location);
                if (hitInfo.InRowCell)
                {
                    if (hitInfo.Column.RealColumnEdit is DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit)
                    {
                        pGrid.FocusedColumn = hitInfo.Column;
                        pGrid.FocusedRowHandle = hitInfo.RowHandle;
                        pGrid.ShowEditor();
                        CheckEdit edit = pGrid.ActiveEditor as CheckEdit;
                        if (edit == null) return;
                        edit.Toggle();
                        DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                    }
                }*/
            }
        }

        private static void gv_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = sender as DevExpress.XtraGrid.Views.Grid.GridView;

            if (View != null)
            {
                GridHitInfo hitInfo = View.CalcHitInfo(e.Location);
                if (hitInfo.InRowCell)
                {
                    if (hitInfo.Column.RealColumnEdit is DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit)
                    {
                        View.FocusedColumn = hitInfo.Column;
                        View.FocusedRowHandle = hitInfo.RowHandle;
                        View.ShowEditor();
                        CheckEdit edit = View.ActiveEditor as CheckEdit;
                        if (edit == null) return;
                        edit.Toggle();
                        DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                    }
                }
            }
        }
        #endregion

        #region Get Visible Rows
        /// <summary>
        /// Retorna todas as linhas visiveis no grid
        /// </summary>
        /// <param name="grid">Grid</param>
        public static DataRow[] GetVisibleRows(this DevExpress.XtraGrid.Views.Grid.GridView grid)
        {
            var DtTable = new DataTable();

            grid.Columns.ToList()
            .ForEach(col =>
            {
                DtTable.Columns.Add(col.FieldName, col.ColumnType);
            });

            for (int i = 0, length = grid.RowCount; i < length; i++)
            {
                DtTable.ImportRow(grid.GetDataRow(grid.GetVisibleRowHandle(i)));
            }

            return DtTable.Select();
        }
        #endregion

        #region Remover todos os Summary do grid
        /// <summary>
        /// Remover todos os Summary do grid
        /// </summary>
        /// <param name="pGrid">Grid</param>
        public static void RemoverAllGridSummary(this GridView pGrid)
        {
            if (pGrid != null)
            {
                foreach (GridColumn col in pGrid.Columns)
                    col.Summary.Clear();
            }

        }
        #endregion

        #region Remover todos os Summary da coluna
        /// <summary>
        /// Remover todos os summary da coluna
        /// </summary>
        /// <param name="pColuna">Coluna que será removido o summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn RemoverAllSummary(this GridColumn pColuna)
        {
            if (pColuna != null)
            {
                pColuna.Summary.Clear();
            }

            return pColuna;
        }
        #endregion

        #region Remover Summary especifico da coluna
        /// <summary>
        /// Remover summary especificado da coluna
        /// </summary>
        /// <param name="pColuna">Coluna que será removido o summary</param>
        /// <param name="pTipo">Tipo que será removido</param>
        /// <returns>GridColumn</returns>
        public static GridColumn RemoverSummary(this GridColumn pColuna, SummaryItemType pTipo)
        {
            if (pColuna != null)
            {
                var item = pColuna.Summary.Where(r => r.SummaryType == pTipo).FirstOrDefault();
                if (item != null)
                    pColuna.Summary.Remove(item);
            }

            return pColuna;
        }
        #endregion

        #region Cria um summary com o menor do grid
        /// <summary>
        /// Cria um summary com o menor do grid
        /// </summary>
        /// <param name="pColuna">Coluna que será adicionado o summary</param>
        /// <param name="pFormat">Formatação do summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn MenorGrid(this GridColumn pColuna, string pFormat = "")
        {
            if (pColuna != null)
            {
                pColuna.AddSummary(SummaryItemType.Min, pFormat);
            }

            return pColuna;
        }
        #endregion

        #region Cria um summary com o menor do grid
        /// <summary>
        /// Cria um summary com o maior do grid
        /// </summary>
        /// <param name="pColuna">Coluna que será adicionado o summary</param>
        /// <param name="pFormat">Formatação do summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn MaiorGrid(this GridColumn pColuna, string pFormat = "")
        {
            if (pColuna != null)
            {
                pColuna.AddSummary(SummaryItemType.Max, pFormat);
            }

            return pColuna;
        }
        #endregion

        #region Cria um summary com a contagem de registros do grid
        /// <summary>
        /// Cria um summary com o a contagem de registros do grid
        /// </summary>
        /// <param name="pColuna">Coluna que será adicionado o summary</param>
        /// <param name="pFormat">Formatação do summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn ContarGrid(this GridColumn pColuna, string pFormat = "")
        {
            if (pColuna != null)
            {
                pColuna.AddSummary(SummaryItemType.Count, pFormat);
            }

            return pColuna;
        }
        #endregion

        #region Cria um summary com a média dos valores registros do grid
        /// <summary>
        /// Cria um summary com o a média dos valores registros do grid
        /// </summary>
        /// <param name="pColuna">Coluna que será adicionado o summary</param>
        /// <param name="pFormat">Formatação do summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn MediaGrid(this GridColumn pColuna, string pFormat = "")
        {
            if (pColuna != null)
            {
                pColuna.AddSummary(SummaryItemType.Average, pFormat);
            }

            return pColuna;
        }
        #endregion

        #region Cria um summary com o a soma dos valores registros do grid
        /// <summary>
        /// Cria um summary com o a soma dos valores registros do grid
        /// </summary>
        /// <param name="pColuna">Coluna que será adicionado o summary</param>
        /// <param name="pFormat">Formatação do summary</param>
        /// <returns>GridColumn</returns>
        public static GridColumn SomarGrid(this GridColumn pColuna, string pFormat = "")
        {
            if (pColuna != null)
            {
                pColuna.AddSummary(SummaryItemType.Sum, pFormat);
            }

            return pColuna;
        }
        #endregion

        #region Adiciona o summary
        private static GridColumn AddSummary(this GridColumn pColuna, SummaryItemType pTipo, string pFormat = "")
        {
            if (pColuna != null)
            {
                try
                {
                    ((GridView)pColuna.View).OptionsView.ShowFooter = true;
                }
                catch
                {
                }

                var Format = pFormat;
                switch (pTipo)
                {
                    case SummaryItemType.Sum:
                        Format = "{0:n2}";
                        break;
                    case SummaryItemType.Min:
                        Format = "{0:n4}";
                        break;
                    case SummaryItemType.Max:
                        Format = "{0:n4}";
                        break;
                    case SummaryItemType.Count:
                        Format = "{0:n0}";
                        break;
                    case SummaryItemType.Average:
                        Format = "{0:n2}";
                        break;
                    case SummaryItemType.Custom:
                        Format = "";
                        break;
                    case SummaryItemType.None:
                        Format = "";
                        break;
                    default:
                        Format = "";
                        break;
                }
                GridColumnSummaryItem ItemSummary = new GridColumnSummaryItem();
                ItemSummary.DisplayFormat = Format;
                ItemSummary.SummaryType = pTipo;

                pColuna.Summary.Add(ItemSummary);
            }

            return pColuna;
        }
        #endregion
        
        #region Regra para colorir a coluna usando condicional 
        /// <summary>
        /// Cria regra para colorir coluna usando uma expressão como condição
        /// </summary>
        /// <param name="pColuna">Coluna</param>
        /// <param name="pCorLinha">Cor para a regra</param>
        /// <param name="pExpressao">Expressão</param>
        public static void ColoreColunaCondicional(
            this GridColumn pColuna, 
            Color pCorLinha,
            string pExpressao,
            bool LinhaToda = true
            )
        {
            var rand = new Random();
            var cod = rand.Next(0, 100).ToString().PadLeft(3, '0');
            
            var formatConditionRuleExpression = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            var gridFormatRule = new DevExpress.XtraGrid.GridFormatRule();
            gridFormatRule.ApplyToRow = LinhaToda;
            gridFormatRule.Column = pColuna;
            gridFormatRule.Name = string.Format("rule{0}{1}", cod, Guid.NewGuid());
            formatConditionRuleExpression.Appearance.BackColor = pCorLinha;
            formatConditionRuleExpression.Appearance.BackColor2 = pCorLinha;
            formatConditionRuleExpression.Appearance.ForeColor = pCorLinha.CorDescricao();
            formatConditionRuleExpression.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression.Expression = pExpressao;
            gridFormatRule.Rule = formatConditionRuleExpression;

            ((GridView)pColuna.View).FormatRules.Add(gridFormatRule);
        }
        #endregion

    }
}