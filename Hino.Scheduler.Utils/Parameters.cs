﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public static class Parameters
    {
        public static string UserLogged { get; set; }

        /// <summary>
        /// String de conexão do banco de dados
        /// </summary>
        public static String connStr { get; set; }

        /// <summary>
        /// Alias do banco de dados
        /// </summary>
        public static String alias { get; set; }

        /// <summary>
        /// Usuário do banco de dados
        /// </summary>
        public static String dbUser { get; set; }

        /// <summary>
        /// Senha do banco de dados
        /// </summary>
        public static String dbPass { get; set; }

        /// <summary>
        /// Host do banco de dados
        /// </summary>
        public static String dbHost { get; set; }

        public static int codEstab { get { return 1; } }

        public static long IdScenario { get; set; }
    }
}
