﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public static class DateUtil
    {
        #region Numero da semana
        public static int MonthWeekNumber(this DateTime date)
        {
            var numSemana = 1;
            var numDias = DateTime.DaysInMonth(date.Year, date.Month);
            var diaSemana = (int)date.DayOfWeek;

            for (int i = 0; i < numDias; i++)
            {
                if (i == date.Day)
                {
                    break;
                }

                diaSemana++;

                if (diaSemana > 7)
                {
                    numSemana++;
                    diaSemana = 1;
                }
            }

            return numSemana;
        }
        #endregion

        #region Primeiro dia do mes
        /// <summary>
        /// Retorna o primeiro dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1, date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo dia do mes
        /// <summary>
        /// Retorna o ultimo dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month,
                        DateTime.DaysInMonth(date.Year, date.Month),
                       date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo hora do dia
        /// <summary>
        /// Retorna a ultima hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                       23, 59, 59, 59);
        }
        #endregion

        #region Primeira hora do dia
        /// <summary>
        /// Retorna a primeira hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                        0, 0, 0, 0);
        }
        #endregion
        
        #region Convert date to minute
        public static int ToMinutes(this DateTime pDate)
        {
            try
            {
                return pDate.Hour * 60 + pDate.Minute;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        #endregion

        #region Convert int minutes to Hour minute
        public static string ToHourMinute(this int pMinutes)
        {
            try
            {
                var hours = Math.Floor(Convert.ToDecimal(pMinutes / 60));
                var minutes = pMinutes - (Math.Floor(Convert.ToDecimal(pMinutes / 60)) * 60);

                return $"{hours.ToString().PadLeft(2, '0')}:{minutes.ToString().PadLeft(2, '0')}";
            }
            catch (Exception)
            {
                return "00:00";
            }
        }

        public static string ToHourMinute(this short pMinutes) =>
            Convert.ToInt32(pMinutes).ToHourMinute();
        #endregion
        
        #region Convert number to WeekDay
        public static string ToWeekDayDesc(this int pDay)
        {
            try
            {
                switch (pDay)
                {
                    case 1:
                        return "Domingo";
                    case 2:
                        return "Segunda-Feira";
                    case 3:
                        return "Terça-Feira";
                    case 4:
                        return "Quarta-Feira";
                    case 5:
                        return "Quinta-Feira";
                    case 6:
                        return "Sexta-Feira";
                    case 7:
                        return "Sábado";
                    default:
                        return "";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string ToWeekDayDesc(this short pDay) =>
            Convert.ToInt32(pDay).ToWeekDayDesc();
        #endregion

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
    }
}
