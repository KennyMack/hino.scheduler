﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public static class betweenEx
    {
        #region Verifica se os valores estão dentro da faixa incluindo o menor e o maior
        /// <summary>
        /// Verifica se os valores estão dentro da faixa
        /// retorna true se o valor for maior ou igual ao valor minimo e for menor ou igual ao valor máximo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valor">Valor para verificar</param>
        /// <param name="minimo">valor minimo(I)</param>
        /// <param name="maximo">valor máximo(I)</param>
        /// <returns>bool</returns>
        public static bool IsBetweenII<T>(this T valor, T minimo, T maximo) where T : IComparable
        {
            return (minimo.CompareTo(valor) <= 0) && (valor.CompareTo(maximo) <= 0);
        }
        #endregion

        #region Verifica se os valores estão dentro da faixa excluindo o menor e incluindo o maior
        /// <summary>
        /// Verifica se os valores estão dentro da faixa
        /// retorna true se o valor for maior que o minimo e menor ou igual ao valor maximo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valor">Valor para verificar</param>
        /// <param name="minimo">valor minimo(E)</param>
        /// <param name="maximo">valor máximo(I)</param>
        /// <returns>bool</returns>
        public static bool IsBetweenEI<T>(this T valor, T minimo, T maximo) where T : IComparable
        {
            return (minimo.CompareTo(valor) < 0) && (valor.CompareTo(maximo) <= 0);
        }
        #endregion

        #region Verifica se os valores estão dentro da faixa incluindo o menor e excluindo o maior
        /// <summary>
        /// Verifica se os valores estão dentro da faixa
        /// retorna true se o valor for maior ou igual ao valor minimo e menor que o valor maximo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valor">Valor para verificar</param>
        /// <param name="minimo">valor minimo(I)</param>
        /// <param name="maximo">valor máximo(E)</param>
        /// <returns>bool</returns>
        public static bool IsBetweenIE<T>(this T valor, T minimo, T maximo) where T : IComparable
        {
            return (minimo.CompareTo(valor) <= 0) && (valor.CompareTo(maximo) < 0);
        }
        #endregion

        #region Verifica se os valores estão dentro da faixa excluindo o maior e menor
        /// <summary>
        /// Verifica se os valores estão dentro da faixa
        /// retorna true se o valor for maior que o valor minimo e menor que o valor maximo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valor">Valor para verificar</param>
        /// <param name="minimo">valor minimo(E)</param>
        /// <param name="maximo">valor máximo(E)</param>
        /// <returns>bool</returns>
        public static bool IsBetweenEE<T>(this T valor, T minimo, T maximo) where T : IComparable
        {
            return (minimo.CompareTo(valor) < 0) && (valor.CompareTo(maximo) < 0);
        }
        #endregion

    }
}
