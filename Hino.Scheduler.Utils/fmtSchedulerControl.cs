﻿using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public static class fmtSchedulerControl
    {
        public static ObservableCollection<string> ErrorMessages { get; set; } = new ObservableCollection<string>();

        public static void DefaultProperties(this SchedulerControl pSchedule, SchedulerViewType pActiveView)
        {
            if (pSchedule != null)
            {
                if (pSchedule.Storage != null)
                {
                    
                    pSchedule.Storage.TimeZoneId = "Central Brazilian Standard Time";
                }

                pSchedule.OptionsView.ToolTipVisibility = ToolTipVisibility.Always;
                pSchedule.OptionsBehavior.ClientTimeZoneId = "Central Brazilian Standard Time";
                pSchedule.ActiveViewType = pActiveView;
                pSchedule.GroupType = SchedulerGroupType.Resource;
               // pSchedule.TimelineView.ShowResourceHeaders = false;
                pSchedule.TimelineView.CellsAutoHeightOptions.Enabled = true;
                pSchedule.TimelineView.NavigationButtonVisibility = NavigationButtonVisibility.Never;
                
                //pSchedule.GanttView.ShowResourceHeaders = false;
                pSchedule.GanttView.CellsAutoHeightOptions.Enabled = true;
                pSchedule.GanttView.NavigationButtonVisibility = NavigationButtonVisibility.Never;
                pSchedule.Start = DateTime.Now.AddDays(-2);

                /*pSchedule.WeekView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.WeekView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;

                pSchedule.DayView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.DayView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;

                pSchedule.WeekView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.WeekView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;

                pSchedule.MonthView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.MonthView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;

                pSchedule.TimelineView.AppointmentDisplayOptions.TimeDisplayType = AppointmentTimeDisplayType.Text;
                pSchedule.TimelineView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.TimelineView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;

                pSchedule.GanttView.AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                pSchedule.GanttView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;*/

                for (int i = 0, length = pSchedule.Views.Count; i < length; i++)
                {
                    pSchedule.Views[i].AppointmentDisplayOptions.TimeDisplayType = AppointmentTimeDisplayType.Text;
                    pSchedule.Views[i].AppointmentDisplayOptions.StartTimeVisibility = AppointmentTimeVisibility.Never;
                    pSchedule.Views[i].AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;
                }

                /*pSchedule.SelectionChanged += (sender, obj) =>
                {
                    var HasSelect = (pSchedule.SelectedAppointments.Count > 0);
                    for (int i = 0, length = pSchedule.Views.Count; i < length; i++)
                    {
                        pSchedule.Views[i].AppointmentDisplayOptions.TimeDisplayType = AppointmentTimeDisplayType.Text;
                        pSchedule.Views[i].AppointmentDisplayOptions.StartTimeVisibility = HasSelect ? AppointmentTimeVisibility.Always : AppointmentTimeVisibility.Never;
                        pSchedule.Views[i].AppointmentDisplayOptions.EndTimeVisibility = HasSelect ? AppointmentTimeVisibility.Always : AppointmentTimeVisibility.Never;
                    }
                };*/
            }
        }

        public static void AppointmentDrag(object sender, AppointmentDragEventArgs e)
        {
            if (((SchedulerControl)sender).SelectedAppointments.Count > 1)
            {
                e.NewAppointmentResourceIds = new ResourceIdCollection();
                e.NewAppointmentResourceIds.AddRange(e.SourceAppointment.ResourceIds);
            }
        }

        public static void AdjustRowHeight(this SchedulerControl pSchedule)
        {
            int normalHeaders = 0;
            int minHeadersHeight = 0;
            if (pSchedule.ActiveViewType == SchedulerViewType.Timeline && pSchedule.TimelineView.ViewInfo != null)
            {
                foreach (SchedulerHeader header in pSchedule.TimelineView.ViewInfo.ResourceHeaders)
                {
                    if (header.Bounds.Height < pSchedule.TimelineView.CellsAutoHeightOptions.MinHeight)
                    {

                        minHeadersHeight += header.Bounds.Height;
                    }
                    else
                        normalHeaders++;
                }
                pSchedule.TimelineView.ResourcesPerPage = normalHeaders + minHeadersHeight /
                    pSchedule.TimelineView.CellsAutoHeightOptions.MinHeight;
                pSchedule.TimelineView.CellsAutoHeightOptions.MinHeight = 12;
            }
        }

        public static void AppointmentCustomizing(this SchedulerControl pSchedule,
            AppointmentViewInfoCustomizingEventArgs e, string pHighlightOP, bool pShowStatus)
        {
            try
            {
                if (!pShowStatus)
                {
                    var codOrdProdAct = e.ViewInfo.Appointment.CustomFields["Codordprod"].ToString();
                    long OrdDig = 0;
                    long? codOrdProd = null;

                    if (long.TryParse((pHighlightOP ?? "").ToString(), out OrdDig) && OrdDig > 0)
                        codOrdProd = OrdDig;

                    var Selected = pSchedule.SelectedAppointments.ToArray();
                    Color cor = Color.White;
                    if (Selected.Length <= 0 && codOrdProd == null)
                        cor = ColorTranslator.FromHtml(e.ViewInfo.Appointment.CustomFields["Corop"].ToString());
                    else if (Selected.Length <= 0 && codOrdProd != null)
                    {
                        if (codOrdProd.ToString() == codOrdProdAct)
                            cor = ColorTranslator.FromHtml(e.ViewInfo.Appointment.CustomFields["Corop"].ToString());
                    }
                    else
                    {
                        if (Selected.Where(r => r.CustomFields["Codordprod"].ToString() == codOrdProdAct).Count() > 0)
                            cor = ColorTranslator.FromHtml(e.ViewInfo.Appointment.CustomFields["Corop"].ToString());
                    }


                    e.ViewInfo.Appearance.BackColor = cor;
                    e.ViewInfo.Appearance.BackColor2 = cor;
                    e.ViewInfo.Appearance.ForeColor = cor.CorDescricao();
                }

                //e.ViewInfo.ToolTipText = e.ViewInfo.DisplayText;// "sdsad";
            }
            catch (Exception)
            {
            }
        }

        public static void CloseMenu(object sender, EventArgs e,
            EventHandler pMenuItemSelectOP_Click,
            EventHandler pMenu_CloseUp)
        {
            SchedulerPopupMenu menu = sender as SchedulerPopupMenu;
            foreach (DXMenuItem item in menu.Items)
                item.Click -= pMenuItemSelectOP_Click;
            menu.CloseUp -= pMenu_CloseUp;
        }

        public static void PopupMenuShowing(this SchedulerPopupMenu pMenu,
            EventHandler pMenuItemSelectOP_Click,
            EventHandler pMenu_CloseUp)
        {
            if (pMenu != null)
            {

                if (pMenu.Id == SchedulerMenuItemId.AppointmentMenu)
                    pMenu.RemoveMenuItem(SchedulerMenuItemId.AppointmentDependencyCreation);

                try
                {
                    if (pMenu.Id == SchedulerMenuItemId.DefaultMenu)
                    {
                        (pMenu.Items[0] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[1] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[2] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[3] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[8] as SchedulerPopupMenu).Visible = false;
                    }

                    if (pMenu.Id == SchedulerMenuItemId.AppointmentMenu)
                    {
                        (pMenu.Items[1] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[2] as SchedulerMenuItem).Visible = false;
                        (pMenu.Items[3] as SchedulerPopupMenu).Caption = "Prioridade";
                        (pMenu.Items[4] as SchedulerPopupMenu).Visible = false;
                        (pMenu.Items[5] as SchedulerMenuItem).Visible = false;

                        SchedulerMenuItem newItem = new SchedulerMenuItem("Selecionar O.P.");
                        newItem.Click += pMenuItemSelectOP_Click;
                        pMenu.Items.Add(newItem);
                        pMenu.CloseUp += pMenu_CloseUp;
                    }
                }
                catch (Exception)
                {

                }
            }

        }

        public static void CustomDraw(bool pShowStatus, object sender, CustomDrawObjectEventArgs e)
        {
            if (pShowStatus)
            {
                AppointmentViewInfo viewInfo = e.ObjectInfo as AppointmentViewInfo;
                e.DrawDefault();
                Rectangle bounds = CalculateEntireAppointmentBounds(viewInfo);

                DrawBackGroundCore(e.Cache,
                    (int)(viewInfo.Appointment.StatusKey),
                    (int)(viewInfo.Appointment.LabelKey), bounds);
                e.Handled = true;
            }
        }
        static Rectangle CalculateEntireAppointmentBounds(AppointmentViewInfo viewInfo)
        {
            Rectangle bounds = viewInfo.InnerBounds;
            return Rectangle.FromLTRB(bounds.Left - 2, bounds.Y - 2, bounds.Right - 2, bounds.Bottom - 2);
        }
        static void DrawBackGroundCore(DevExpress.Utils.Drawing.GraphicsCache cache, int pStatus, int pLabel, Rectangle bounds)
        {
            var colorStatus = Color.White;
            var colorLabel = Color.Yellow;
            switch (pStatus)
            {
                case 0:
                    colorStatus = Color.White;
                    break;
                case 1:
                    colorStatus = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(165)))), ((int)(((byte)(0)))));
                    break;
                case 2:
                    colorStatus = Color.SkyBlue;
                    break;
                case 3:
                    colorStatus = Color.Salmon;
                    break;
                case 4:
                    colorStatus = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
                    break;
            }

            switch (pLabel)
            {
                case 0:
                    colorLabel = Color.Yellow;
                    break;
                case 1:
                    colorLabel = Color.LightSkyBlue;
                    break;
                case 2:
                    colorLabel = Color.PaleGreen;
                    break;
                case 3:
                    colorLabel = Color.Yellow;
                    break;
            }

            Brush brushStatus = new SolidBrush(colorStatus);
            cache.FillRectangle(brushStatus, new Rectangle(bounds.X, bounds.Y, bounds.Width + 2, 4));
            // cache.DrawString("●", new Font("Arial Black", 16), brushStatus,
            //     new Rectangle(bounds.X, bounds.Y - 8, 20, 25), StringFormat.GenericDefault);
            Brush brushLabel = new SolidBrush(colorLabel);
            cache.FillRectangle(brushLabel, new Rectangle(bounds.X, bounds.Y + 16, bounds.Width + 2, 4));

            // cache.DrawString("●", new Font("Arial Black", 16), brushLabel,
            //     new Rectangle(bounds.X + 10, bounds.Y - 8, 20, 25), StringFormat.GenericDefault);
        }
        public static void CustomToolTip(ToolTipController ttcMessage, object sender, ToolTipControllerShowEventArgs e)
        {
            ToolTipController controller = sender as ToolTipController;
            AppointmentViewInfo aptViewInfo = controller.ActiveObject as AppointmentViewInfo;
            if (aptViewInfo == null) return;

            if (ttcMessage.ToolTipType == ToolTipType.Standard && aptViewInfo.Appointment != null)
            {
                e.IconType = ToolTipIconType.Information;
                e.ToolTip = aptViewInfo.Description;
            }

            if (ttcMessage.ToolTipType == ToolTipType.SuperTip && aptViewInfo.Appointment != null)
            {
                SuperToolTip SuperTip = new SuperToolTip();
                SuperToolTipSetupArgs args = new SuperToolTipSetupArgs();
                args.Title.Text = aptViewInfo.Appointment.Subject;

                var TotalMinutes = (aptViewInfo.Appointment.End - aptViewInfo.Appointment.Start).TotalMinutes;
                var hrs = Math.Floor(TotalMinutes / 60);
                var min = TotalMinutes - (Math.Floor(TotalMinutes / 60) * 60);
                var HHMM = $"{hrs.ToString().PadLeft(2, '0')}:{min.ToString().PadLeft(2, '0')}";

                var label = Convert.ToInt32(aptViewInfo.Appointment.LabelKey);
                Image resImage = null;

                switch (label)
                {
                    case 0:
                        resImage = ((System.Drawing.Image)(Hino.Scheduler.Utils.Properties.Resources.Bola_amarela));
                        break;
                    case 1:
                        resImage = ((System.Drawing.Image)(Hino.Scheduler.Utils.Properties.Resources.Bola_azul));
                        break;
                    case 2:
                        resImage = ((System.Drawing.Image)(Hino.Scheduler.Utils.Properties.Resources.Bola_verde));
                        break;
                    case 3:
                        resImage = ((System.Drawing.Image)(Hino.Scheduler.Utils.Properties.Resources.Bola_amarela));
                        break;
                }
                args.Contents.Text = $"{aptViewInfo.Appointment.Subject} ({aptViewInfo.Appointment.Location})\r\n{aptViewInfo.Appointment.Start.ToString("dd/MM/yyyy HH:mm:ss")} ~ {aptViewInfo.Appointment.End.ToString("dd/MM/yyyy HH:mm:ss")} - ({HHMM})";
                args.Contents.Image = resImage;
                args.ShowFooterSeparator = true;

                var colorStatus = Color.White;
                var status = Convert.ToInt32(aptViewInfo.Appointment.StatusKey);
                var TextStatus = "Sem Prioridade";
                Image resImageStatus = Properties.Resources.Sem_prioridade;
                switch (status)
                {
                    case 0:
                        resImageStatus = Properties.Resources.Sem_prioridade;
                        TextStatus = "Sem Prioridade";
                        break;
                    case 1:
                        resImageStatus = Properties.Resources.Urgente;
                        TextStatus = "Urgente";
                        break;
                    case 2:
                        resImageStatus = Properties.Resources.AndamentoNormal;
                        TextStatus = "Andamento normal";
                        break;
                    case 3:
                        resImageStatus = Properties.Resources.Atrasada;
                        TextStatus = "Atrasado";
                        break;
                    case 4:
                        resImageStatus = Properties.Resources.Aguardando;
                        TextStatus = "Aguardando";
                        break;
                }
                args.Footer.Text = TextStatus;
                args.Footer.Image = (Image)new Bitmap(resImageStatus, new Size(16, 16));

                SuperTip.Setup(args);
                e.SuperTip = SuperTip;
            }
        }
        public static void AppointmentConflicts(SchedulerStorage storage, bool checkDependencies, AppointmentConflictEventArgs e)
        {
            e.Conflicts.Clear();
            ErrorMessages.Clear();

            if (checkDependencies)
            {
                if (e.AppointmentClone.Duration.TotalMinutes < Convert.ToDouble(e.Appointment.CustomFields["Minimaltime"]))
                {
                    e.Conflicts.Add(e.AppointmentClone);
                    ErrorMessages.Add("Tempo da O.P. é menor que o mínimo.");
                    //MessageBox.Show("Tempo da O.P. é menor que o mínimo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }

                AppointmentDependencyBaseCollection depCollectionDep =
                    storage.AppointmentDependencies.Items.GetDependenciesByDependentId(e.Appointment.Id);
                if (depCollectionDep.Count > 0)
                {
                    if (CheckForInvalidDependenciesAsDependent(storage, depCollectionDep, e.AppointmentClone))
                        e.Conflicts.Add(e.AppointmentClone);
                }

                AppointmentDependencyBaseCollection depCollectionPar =
                    storage.AppointmentDependencies.Items.GetDependenciesByParentId(e.Appointment.Id);
                if (depCollectionPar.Count > 0)
                {
                    if (CheckForInvalidDependenciesAsParent(storage, depCollectionPar, e.AppointmentClone))
                        e.Conflicts.Add(e.AppointmentClone);
                }
            }
        }
        static bool CheckForInvalidDependenciesAsDependent(SchedulerStorage ssSchedulerOP, AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.ParentId).End;
                    if (apt.Start < checkTime)
                        return true;
                }
            }
            return false;
        }
        static bool CheckForInvalidDependenciesAsParent(SchedulerStorage ssSchedulerOP, AppointmentDependencyBaseCollection depCollection, Appointment apt)
        {
            foreach (AppointmentDependency dep in depCollection)
            {
                if (dep.Type == AppointmentDependencyType.FinishToStart)
                {
                    DateTime checkTime = ssSchedulerOP.Appointments.Items.GetAppointmentById(dep.DependentId).Start;
                    if (apt.End > checkTime)
                        return true;
                }
            }
            return false;
        }
    }
}
