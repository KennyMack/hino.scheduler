﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public static class exReflection
    {
        public static IEnumerable<T> Select<T>(this IDataReader reader,
                                          Func<IDataReader, T> projection)
        {
            while (reader.Read())
            {
                yield return projection(reader);
            }
        }

        public static object TryGetValue(this IDataReader reader, string pProperty, object pDefault = null)
        {

            try
            {
                return reader[pProperty];
            }
            catch (Exception)
            {

            }
            return pDefault;
        }

        /// <summary>
        ///     Copia o valor das propriedades comuns entre dois objetos
        /// </summary>
        /// <typeparam name="TOrigem"></typeparam>
        /// <typeparam name="TDestino"></typeparam>
        /// <param name="objetoOrigem"></param>
        /// <param name="objetoDestino"></param>
        public static void CopiarPropriedades<TDestino, TOrigem>(this TDestino objDest, TOrigem objOrig) where TDestino : class where TOrigem : class
        {
            var isNullable = false;
            Type PropType;
            foreach (var attr in objOrig.GetType().GetProperties().Where(p => p.CanRead))
            {
                var propertyInfo = objDest.GetType().GetProperty(attr.Name, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                try
                {
                    if (propertyInfo != null && propertyInfo.CanWrite)
                    {
                        PropType = propertyInfo.PropertyType;
                        isNullable = false;
                        if (propertyInfo.PropertyType.Name.IndexOf("Nullable`", StringComparison.Ordinal) > -1)
                        {
                            PropType = Nullable.GetUnderlyingType(PropType);
                            isNullable = true;
                        }

                        var attrVal = attr.GetValue(objOrig, null);

                        if (isNullable && attrVal == null)
                        {
                            propertyInfo.SetValue(objDest, null, null);
                        }
                        else
                        {
                            switch (PropType.Name)
                            {
                                case "Int16":
                                    propertyInfo.SetValue(objDest, Convert.ToInt16(attrVal), null);
                                    break;
                                case "Int32":
                                    propertyInfo.SetValue(objDest, Convert.ToInt32(attrVal), null);
                                    break;
                                case "Int64":
                                    propertyInfo.SetValue(objDest, Convert.ToInt64(attrVal), null);
                                    break;
                                case "SByte":
                                    propertyInfo.SetValue(objDest, Convert.ToSByte(attrVal), null);
                                    break;
                                case "DateTime":
                                    propertyInfo.SetValue(objDest, Convert.ToDateTime(attrVal, new CultureInfo("pt-BR")), null);
                                    break;
                                case "Boolean":
                                    if (attrVal == null)
                                    {
                                        propertyInfo.SetValue(objDest, false, null);
                                        break;
                                    }

                                    int val;
                                    if (int.TryParse(attrVal.ToString(), out val))
                                    {
                                        propertyInfo.SetValue(objDest, val > 0, null);
                                        break;
                                    }
                                    bool valb;
                                    if (bool.TryParse(attrVal.ToString(), out valb))
                                    {
                                        propertyInfo.SetValue(objDest, valb, null);
                                        break;
                                    }

                                    propertyInfo.SetValue(objDest, false, null);
                                    break;
                                default:
                                    propertyInfo.SetValue(objDest, attrVal, null);
                                    break;
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    throw new Exception("Não foi possível converter a property {attr.Name} para {propertyInfo.Name} valor '{attr.GetValue(objOrig, null)}' motivo: " + e.Message);
                }
            }
        }

        /// <summary>
        ///     Obtém informações de uma propriedade de um objeto.
        ///     <example>var propinfo = Funcoes.ObterPropriedadeInfo(_cfgServico, c => c.DiretorioSalvarXml);</example>
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyLambda"></param>
        /// <returns>Retorna um objeto do tipo PropertyInfo com as informações da propriedade, como nome, tipo, etc</returns>
        public static PropertyInfo ObterPropriedadeInfo<TSource, TProperty>(this TSource source, Expression<Func<TSource, TProperty>> propertyLambda)
        {
            var type = typeof(TSource);

            var member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format("A expressão '{0}' se refere a um método, não a uma propriedade!", propertyLambda));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format("A expressão '{0}' se refere a um campo, não a uma propriedade!", propertyLambda));

            if (propInfo.ReflectedType != null && (type != propInfo.ReflectedType && !type.IsSubclassOf(propInfo.ReflectedType)))
                throw new ArgumentException(string.Format("A expressão '{0}' refere-se a uma propriedade, mas não é do tipo {1}!", propertyLambda, type));

            return propInfo;
        }

        /// <summary>
        ///     Obtém as propriedades de um determinado objeto
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns>Retorna um objeto Dictionary contendo o nome da propriedade e seu valor</returns>
        public static Dictionary<string, object> LerPropriedades<T>(this T objeto) where T : class
        {
            //A função pode ser melhorada para trazer recursivamente as propriedades dos objetos filhos
            var dicionario = new Dictionary<string, object>();

            foreach (var attributo in objeto.GetType().GetProperties())
            {
                var value = attributo.GetValue(objeto, null);
                dicionario.Add(attributo.Name, value);
            }

            return dicionario;
        }

        /// <summary>
        ///     Obtém uma lista contendo os nomes das propriedades cujo valor não foi definido ou está vazio, de um determinado objeto
        ///     passado como parâmetro
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objeto"></param>
        /// <returns>Retorna uma lista de strings</returns>
        public static List<string> ObterPropriedadesEmBranco<T>(this T objeto)
        {
            return
                (from attributo in objeto.GetType().GetProperties()
                 let value = attributo.GetValue(objeto, null)
                 where value == null || string.IsNullOrEmpty(value.ToString())
                 select attributo.Name).ToList();
        }

        public static string GetDescription(this Enum enumerationValue)
        {
            Type type = enumerationValue.GetType();
            MemberInfo member = type.GetMembers().Where(w => w.Name == Enum.GetName(type, enumerationValue)).FirstOrDefault();
            DescriptionAttribute attribute;

            if (member != null)
            {
                attribute = member.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
                return attribute.Description;
            }
            else
                return enumerationValue.ToString();
        }
    }
}