﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Utils
{
    public class fmtMascara
    {
        #region Formata Data 'DD/MM/YYYY'
        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void Data(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "d";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "d";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "d";
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.False;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void Data(DevExpress.XtraEditors.TextEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "d";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "d";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "d";
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Data(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
                rite.Mask.EditMask = "d";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "d";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "d";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Data(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "d";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "d";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "d";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion

        #region Formata Data 'DD/MM/YY'
        /// <summary>
        /// Formata Data 'DD/MM/YY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void DataCurta(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "dd/MM/yy";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "dd/MM/yy";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "dd/MM/yy";
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataCurta(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "dd/MM/yy";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "dd/MM/yy";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "dd/MM/yy";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata Data 'DD/MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataCurta(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "dd/MM/yy";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "dd/MM/yy";
                rite.AllowMouseWheel = true;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "dd/MM/yy";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion        

        #region Formata data e hora 'DD/MM/YYYY HH:MM'
        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void DataHora(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "g";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "g";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "g";
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataHora(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "g";
                rite.AllowMouseWheel = true;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "g";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataHora(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "g";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "g";
                rite.AllowMouseWheel = true;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "g";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion        

        #region Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void DataHoraSeg(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "G";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "G";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "G";
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataHoraSeg(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "G";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "G";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "G";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata data e hora 'DD/MM/YYYY HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void DataHoraSeg(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "G";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "G";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "G";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion

        #region Formata data 'MM/YYYY'
        /// <summary>
        /// Formata data 'MM/YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void MesAno(DevExpress.XtraEditors.Repository.RepositoryItemDateEdit de)
        {
            de.DisplayFormat.FormatString = "MM/yyyy";
            de.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.EditFormat.FormatString = "MM/yyyy";
            de.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Mask.EditMask = "MM/yyyy";
            de.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearView;
            de.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data 'MM/YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void MesAno(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "MM/yyyy";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "MM/yyyy";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "MM/yyyy";
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearView;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data 'MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void MesAno(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
                rite.Mask.EditMask = "MM/yyyy";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "MM/yyyy";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata data 'MM/YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void MesAno(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "MM/yyyy";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "MM/yyyy";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "MM/yyyy";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion

        #region Formata data 'YYYY'
        /// <summary>
        /// Formata data 'YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void Ano(DevExpress.XtraEditors.Repository.RepositoryItemDateEdit de)
        {
            de.DisplayFormat.FormatString = "yyyy";
            de.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.EditFormat.FormatString = "yyyy";
            de.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Mask.EditMask = "yyyy";
            de.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearsGroupView;
            de.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data 'YYYY'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void Ano(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "yyyy";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "yyyy";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "yyyy";
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearsGroupView;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata data 'YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Ano(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
                rite.Mask.EditMask = "yyyy";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "yyyy";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata data 'YYYY'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Ano(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "yyyy";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "yyyy";

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "yyyy";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion


        #region Formata quantidade
        /// <summary>
        /// Formata quantidade com o numero de casas decimais definidos no sistema
        /// </summary>
        /// <param name="te">Campo</param>
        public static void Quantidade(DevExpress.XtraEditors.TextEdit te)
        {
            te.Properties.DisplayFormat.FormatString = "n6";
            te.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.EditFormat.FormatString = "n6";
            te.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.Mask.EditMask = "n6";
            te.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            te.Properties.Mask.SaveLiteral = false;
            te.Properties.Mask.UseMaskAsDisplayFormat = true;
            te.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata quantidade com o numero de casas decimais definidos no sistema
        /// </summary>
        /// <param name="rib">Campo</param>
        public static void Quantidade(RepositoryItemButtonEdit rib)
        {
            rib.DisplayFormat.FormatString = "n6";
            rib.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            rib.EditFormat.FormatString = "n6";
            rib.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            rib.Mask.EditMask = "n6";
            rib.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            rib.KeyPress += rib_KeyPress;
            rib.Mask.SaveLiteral = false;
            rib.Mask.UseMaskAsDisplayFormat = true;
            rib.AllowMouseWheel = false;
        }

        static void rib_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        /// <summary>
        /// Formata quantidade com o numero de casas decimais definidos no sistema
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Quantidade(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                rite.Mask.EditMask = "n6";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                coluna.DisplayFormat.FormatString = "n6";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata quantidade com o numero de casas decimais definidos no sistema
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Quantidade(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "n6";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "n6";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "n6";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion


        #region Formata hora 'HH:MM:SS'
        /// <summary>
        /// Formata hora 'HH:MM:SS'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void HoraMinSeg(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "HH:mm:ss";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "HH:mm:ss";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "HH:mm:ss";
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.AllowMouseWheel = false;
        }


        /// <summary>
        /// Formata hora 'HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void HoraMinSeg(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "t";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "HH:mm:ss";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata hora 'HH:MM:SS'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void HoraMinSeg(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "HH:mm:ss";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "HH:mm:ss";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "HH:mm:ss";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion        

        #region Formata hora 'HH:MM'
        /// <summary>
        /// Formata hora 'HH:MM'
        /// </summary>
        /// <param name="de">Campo</param>
        public static void Hora(DevExpress.XtraEditors.DateEdit de)
        {
            de.Properties.DisplayFormat.FormatString = "t";
            de.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.EditFormat.FormatString = "t";
            de.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            de.Properties.Mask.EditMask = "t";
            de.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            de.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            de.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata hora 'HH:MM'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Hora(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "t";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.DisplayFormat.FormatString = "t";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata hora 'HH:MM'
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Hora(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "t";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "t";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "t";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion


        #region Formata campos inteiro sem pontos
        /// <summary>
        /// Formata Campos numéricos inteiro sem ponto
        /// </summary>
        /// <param name="te">Campo</param>
        public static void InteiroSemPontos(DevExpress.XtraEditors.TextEdit te)
        {
            te.Properties.DisplayFormat.FormatString = "f0";
            te.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.EditFormat.FormatString = "f0";
            te.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.Mask.EditMask = "f0";
            te.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            te.Properties.Mask.SaveLiteral = false;
            te.Properties.Mask.UseMaskAsDisplayFormat = true;
            te.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata Campos numéricos inteiro sem ponto
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void InteiroSemPontos(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                rite.Mask.EditMask = "f0";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                coluna.DisplayFormat.FormatString = "f0";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata Campos numéricos inteiro sem ponto
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void InteiroSemPontos(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = "f0";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = "f0";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = "f0";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion        

        #region Formata campos numericos
        /// <summary>
        /// Formata Campos numéricos
        /// </summary>
        /// <param name="te">Campo</param>
        /// <param name="numCasaDec">Numero de casas decimais</param>
        public static void Numero(DevExpress.XtraEditors.TextEdit te, SByte numCasaDec)
        {
            te.Properties.DisplayFormat.FormatString = "n" + numCasaDec.ToString();
            te.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.EditFormat.FormatString = "n" + numCasaDec.ToString();
            te.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.Mask.EditMask = "n" + numCasaDec.ToString();
            te.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            te.Properties.Mask.SaveLiteral = false;
            te.Properties.Mask.UseMaskAsDisplayFormat = true;
            te.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata Campos numéricos
        /// </summary>
        /// <param name="coluna">Coluna</param>
        /// <param name="numCasaDec">Numero de casas decimais</param>
        public static void Numero(DevExpress.XtraGrid.Columns.GridColumn coluna, SByte numCasaDec)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                rite.Mask.EditMask = string.Concat("n", numCasaDec);
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                coluna.DisplayFormat.FormatString = string.Concat("n", numCasaDec);
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata Campos numéricos
        /// </summary>
        /// <param name="coluna">Coluna</param>
        /// <param name="numCasaDec">Numero de casas decimais</param>
        public static void Numero(DevExpress.XtraTreeList.Columns.TreeListColumn coluna, SByte numCasaDec)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                rite.Mask.EditMask = string.Concat("n", numCasaDec);
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                rite.DisplayFormat.FormatString = string.Concat("n", numCasaDec);
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
                coluna.Format.FormatString = string.Concat("n", numCasaDec);
                coluna.ColumnEdit = rite;
            }
        }
        #endregion

        #region Formata Percentual
        /// <summary>
        /// Formata as casas decimais do campo percentual
        /// </summary>
        /// <param name="te">Campo</param>
        public static void Percentual(DevExpress.XtraEditors.TextEdit te)
        {
            te.Properties.DisplayFormat.FormatString = "n5";
            te.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.EditFormat.FormatString = "n5";
            te.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            te.Properties.Mask.EditMask = "n5";
            te.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            te.Properties.Mask.SaveLiteral = false;
            te.Properties.Mask.UseMaskAsDisplayFormat = true;
            te.Properties.AllowMouseWheel = false;
        }

        /// <summary>
        /// Formata as casas decimais do campo percentual
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Percentual(DevExpress.XtraGrid.Columns.GridColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                rite.Mask.EditMask = "n5";
                rite.AllowMouseWheel = false;

                coluna.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                coluna.DisplayFormat.FormatString = "n5";
                coluna.ColumnEdit = rite;
            }
        }

        /// <summary>
        /// Formata as casas decimais do campo percentual
        /// </summary>
        /// <param name="coluna">Coluna</param>
        public static void Percentual(DevExpress.XtraTreeList.Columns.TreeListColumn coluna)
        {
            if (coluna != null)
            {
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rite = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                rite.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                rite.Mask.EditMask = "n5";
                rite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                rite.DisplayFormat.FormatString = "n5";
                rite.AllowMouseWheel = false;

                coluna.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
                coluna.Format.FormatString = "n5";
                coluna.ColumnEdit = rite;
            }
        }
        #endregion        
    }
}
