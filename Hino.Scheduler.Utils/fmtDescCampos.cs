﻿using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Hino.Scheduler.Utils
{
    public static class fmtDescCampos
    {
        #region Limpa Caption
        /// <summary>
        /// Limpa o texto removendo todos os caracteres entre 
        /// os caracteres < e >
        /// </summary>
        /// <param name="pText">Texto</param>
        /// <returns>string</returns>
        public static string limpaText(this string pText)
        {
            return Regex.Replace(pText, @"<[\s]*(.*?)>", string.Empty);
        }
        #endregion

        #region Trata descrição
        /// <summary>
        /// Trata a descrição dos campos antes de exibir na tela
        /// </summary>
        /// <param name="pText">Texto</param>
        /// <returns>String</returns>
        public static string trataDescricao(this string pText)
        {
            return limpaText(pText);
        }
        #endregion

        #region Define a primeira letra de cada palavra como maiúscula
        /// <summary>
        /// Define a primeira letra de cada palavra como maiúscula
        /// </summary>
        /// <param name="pText"></param>
        /// <returns></returns>
        public static string titularizar(this string pText)
        {
            return Regex.Replace(pText.ToLower(), @"\b[a-zA-Z]", m => m.Value.ToUpper());
        }
        #endregion

        #region Trata todos os labels
        public static void TrataLabels(Control.ControlCollection pControle)
        {
            foreach (Control item in pControle)
            {
                if (item is DevExpress.XtraEditors.LabelControl)
                {
                    var cap = (item as DevExpress.XtraEditors.LabelControl).Text;
                    (item as DevExpress.XtraEditors.LabelControl).Text = cap.trataDescricao();
                }
                else if (item is DevExpress.XtraEditors.GroupControl)
                {
                    TrataLabels(item.Controls);
                }
                else if (item is DevExpress.XtraTab.XtraTabControl)
                {
                    foreach (DevExpress.XtraTab.XtraTabPage page in (item as DevExpress.XtraTab.XtraTabControl).TabPages)
                    {
                        TrataLabels(page.Controls);
                    }
                }
                else if (item is DevExpress.XtraEditors.XtraPanel)
                {
                    TrataLabels(item.Controls);
                }

            }
        }
        #endregion

    }
}
