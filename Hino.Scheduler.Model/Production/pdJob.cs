﻿using Hino.Scheduler.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Production
{
    [Table("PDOPJOBS")]
    public class pdJob: BaseEntity, IEquatable<pdJob>
    {
        [Key]
        [Column("IDJOB", TypeName = "number")]
        [DisplayName("Id job")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long idjob { get; set; }

        [Column("CODESTAB", TypeName = "number")]
        [DisplayName("Cód. Estab")]
        public int codestab { get; set; }

        [Column("CODORDPROD", TypeName = "number")]
        [DisplayName("Cód. ord. prod.")]
        public long codordprod { get; set; }

        [Column("CODESTRUTURA", TypeName = "number")]
        [DisplayName("Cód. estrutura")]
        public long codestrutura { get; set; }

        [Column("NIVELORDPROD")]
        [DisplayName("Nivel ord. prod.")]
        public string nivelordprod { get; set; }

        [Column("NIVELORDPRODPAI")]
        [DisplayName("Nivel ord. prod. pai")]
        public string nivelordprodpai { get; set; }

        [Column("CODPRODUTO")]
        [DisplayName("Cód. produto")]
        public string codproduto { get; set; }

        [Column("FSPRODUTODESCRICAO")]
        [DisplayName("Desc. produto")]
        public string fsprodutodescricao { get; set; }

        [NotMapped]
        [DisplayName("Assunto O.P.")]
        public string subject
        {
            get
            {
                return $"{codordprod} - {nivelordprod}";
            }
        }


        [Column("CODROTEIRO", TypeName = "number")]
        [DisplayName("Cód. roteiro")]
        public int codroteiro { get; set; }

        [Column("CODPROGRAMA", TypeName = "number")]
        [DisplayName("Cód. programa")]
        public decimal codprograma { get; set; }

        [Column("DATAINIOP")]
        [DisplayName("Data prev. início")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime datainiop { get; set; }

        [Column("DATAFIMOP")]
        [DisplayName("Data prév. término")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime datafimop { get; set; }

        [Column("QTDPROG", TypeName = "number")]
        [DisplayName("Programado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdprog { get; set; }

        [Column("QTDAPONT", TypeName = "number")]
        [DisplayName("Realizado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdapont { get; set; }

        [Column("QTDREFUGO", TypeName = "number")]
        [DisplayName("Refugado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdrefugo { get; set; }

        [Column("PERCCONCLUIDO", TypeName = "number")]
        [DisplayName("Perc. concluído")]
        public decimal percconcluido { get; set; }

        [Column("LABELOP", TypeName = "number")]
        [DisplayName("Status O.P.")]
        public int labelop { get; set; }

        [Column("STATUSOP", TypeName = "number")]
        [DisplayName("Prioridade")]
        public int statusop { get; set; }

        [NotMapped]
        [DisplayName("Status O.P.")]
        public StatusOP desclabelop
        {
            get
            {
                switch (labelop)
                {
                    case 0:
                        return StatusOP.Pendente;
                    case 1:
                        return StatusOP.Andamento;
                    case 2:
                        return StatusOP.Concluido;
                    case 3:
                        return StatusOP.Cancelado;
                    default:
                        return StatusOP.Pendente;
                }
            }
        }

        [NotMapped]
        [Display(Order = -1, Description = "Tempo Minimo em minutos")]
        public decimal minimaltime { get; set; }

        [NotMapped]
        [DisplayName("Prioridade")]
        public PriorityOP descstatusop
        {
            get
            {
                switch (statusop)
                {
                    case 0:
                        return PriorityOP.SemPrioridade;
                    case 1:
                        return PriorityOP.Urgente;
                    case 2:
                        return PriorityOP.Andamento;
                    case 3:
                        return PriorityOP.Atrasado;
                    case 4:
                        return PriorityOP.Aguardando;
                    default:
                        return PriorityOP.SemPrioridade;
                }
            }
        }

        [Column("RESOURCEID", TypeName = "number")]
        [DisplayName("Cód. recurso")]
        public long resourceid { get; set; }

        [Column("CODMAQUINA")]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("OPERACAO", TypeName = "number")]
        [DisplayName("Operação")]
        public int? operacao { get; set; }

        [Column("DESCRICAO")]
        [DisplayName("Descrição")]
        public string descricao { get; set; }

        [Column("COROP")]
        [DisplayName("Cor O.P.")]
        public string corop { get; set; }

        [Column("ALTERADO")]
        [DisplayName("Alterado")]
        public int alterado { get; set; }

        [Column("IDLOADDATA")]
        [DisplayName("Id Load")]
        public long IdLoadData { get; set; }

        [Column("CODPEDVENDA")]
        [DisplayName("Cód. Ped. Venda")]
        public long codpedvenda { get; set; }

        public bool Equals(pdJob other)
        {
            if (Object.ReferenceEquals(other, null)) return false;
            
            if (Object.ReferenceEquals(this, other)) return true;

            if (this.idjob > 0)
                return this.idjob == other.idjob;

            return this.codordprod == other.codordprod &&
                this.nivelordprod == other.nivelordprod &&
                this.codproduto == other.codproduto;

        }

        public override int GetHashCode()
        {

            //Get hash code for the Name field if it is not null. 
            int hashProductName = idjob.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = idjob.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }
    }
}
