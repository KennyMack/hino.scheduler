﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Production
{
    public class pdOrdemProdDet : BaseEntity
    {
        [Column("CODORDPROD", Order = 0)]
        [DisplayName("Cod. ord. prod.")]
        public decimal codordprod { get; set; }

        [Column("NIVELORDPROD", Order = 1)]
        [DisplayName("Nivel ord. prod.")]
        public string nivelordprod { get; set; }

        [Column("NIVELORDPRODPAI", Order = 2)]
        [DisplayName("Nivel ord. prod. pai")]
        public string nivelordprodpai { get; set; }

        [Column("CODESTAB", Order = 3)]
        [DisplayName("Cod. estab.")]
        public int codestab { get; set; }

        [Column("CODPRODUTO", Order = 4)]
        [DisplayName("Cód. produto")]
        public string codproduto { get; set; }

        [Column("FSPRODUTODESCRICAO", Order = 4)]
        [DisplayName("Desc. produto")]
        public string fsprodutodescricao { get; set; }

        [Column("DTINICIO", Order = 5)]
        [DisplayName("Dt início")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime dtinicio { get; set; }

        [Column("DTTERMINO", Order = 6)]
        [DisplayName("Dt término")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime dttermino { get; set; }

        [Column("DTINICIOOPER", Order = 12)]
        [DisplayName("Dt início oper.")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime dtiniciooper { get; set; }

        [Column("DTTERMOPER", Order = 13)]
        [DisplayName("Dt término oper")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime dttermoper { get; set; }

        [Column("PROGRAMADO", Order = 7)]
        [DisplayName("Programado")]
        public decimal programado { get; set; }

        [Column("REALIZADO", Order = 8)]
        [DisplayName("Realizado")]
        public decimal realizado { get; set; }

        [Column("REFUGADO", Order = 9)]
        [DisplayName("Refugado")]
        public decimal refugado { get; set; }

        [Column("CODMAQUINA", Order = 10)]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("OPERACAO", Order = 11)]
        [DisplayName("Operação")]
        public Int16 operacao { get; set; }

        [Column("DESCMINHORA", Order = 14)]
        [DisplayName("Horas necessárias")]
        public string descminhora { get; set; }

        [Column("DESCMINSACUMULADOS", Order = 15)]
        [DisplayName("Horas necessárias acumuladas")]
        public string descminsacumulados { get; set; }
    }
}
