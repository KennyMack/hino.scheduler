﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Production
{
    public class ColorOP
    {
        public Color Cor { get; set; }
        public int OP { get; set; }
    }
}
