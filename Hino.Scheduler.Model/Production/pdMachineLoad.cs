﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Production
{
    public class pdMachineLoad : BaseEntity
    {
        [Column("CODMAQUINA")]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("ENMAQUINASDESCRICAO")]
        [DisplayName("Descrição")]
        public string enmaquinasdescricao { get; set; }

        [Column("CODESTAB")]
        [DisplayName("Cód. estab.")]
        public int codestab { get; set; }

        [Column("DTPRODUCAO")]
        [DisplayName("Data produção")]
        public DateTime dtproducao { get; set; }

        [Column("MINSDISPONIVEIS")]
        [DisplayName("Horas disponíveis")]
        public decimal minsdisponiveis { get; set; }

        [Column("DESCMINSDISPONIVEIS")]
        [DisplayName("Horas disponíveis")]
        public string descminsdisponiveis { get; set; }

        [Column("MINHORAPROD")]
        [DisplayName("Horas programadas")]
        public decimal minhoraprod { get; set; }

        [Column("DESCMINHORA")]
        [DisplayName("Horas programadas")]
        public string descminhora { get; set; }

        [Column("CARGAMAQUINA")]
        [DisplayName("Carga máquina")]
        public decimal cargamaquina { get; set; }

        [NotMapped]
        [DisplayName("(%) Carga dia")]
        [DisplayFormat(DataFormatString = "{0:n0}%")]
        public decimal cargadia
        {
            get
            {
                return cargamaquina * 100;
            }
        }

        [Column("SEMTEMPOSEMANA")]
        [DisplayName("Máq. sem tempo")]
        public string semtemposemana { get; set; }
    }
}
