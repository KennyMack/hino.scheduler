﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Production
{
    [Table("PDOPCOLORS")]
    public class pdOPColors: BaseEntity
    {
        [Key]
        [Column("CODCOR")]
        [DisplayName("CODCOR")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long? codcor { get; set; }

        [Column("CODESTAB")]
        [DisplayName("CODESTAB")]
        public int? codestab { get; set; }
        
        [Column("CODORDPROD")]
        [DisplayName("CODORDPROD")]
        public long? codordprod { get; set; }

        [Column("COROP")]
        [DisplayName("COROP")]
        public string corop { get; set; }
    }
}
