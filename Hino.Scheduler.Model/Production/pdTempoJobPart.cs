﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Production
{
    [Table("PDTEMPOJOBPART")]
    public class pdTempoJobPart : BaseEntity
    {
        [Key]
        [Column("IDJOBPART")]
        [DisplayName("Id job")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long idjobpart { get; set; }

        [Column("CODESTAB")]
        [DisplayName("Codestab")]
        public int codestab { get; set; }

        [Column("CODMAQUINA")]
        [DisplayName("Codmaquina")]
        public string codmaquina { get; set; }

        [Column("CODORDPROD")]
        [DisplayName("Codordprod")]
        public long? codordprod { get; set; }

        [Column("NIVELORDPROD")]
        [DisplayName("Nivelordprod")]
        public string nivelordprod { get; set; }

        [Column("NIVELORDPRODPAI")]
        [DisplayName("Nivelordprodpai")]
        public string nivelordprodpai { get; set; }

        [Column("TEMPOPART")]
        [DisplayName("Tempopart")]
        public decimal tempopart { get; set; }

        [Column("TEMPOALOC")]
        [DisplayName("Tempoaloc")]
        public string tempoaloc { get; set; }

        [Column("OPERACAO")]
        [DisplayName("Operacao")]
        public decimal? operacao { get; set; }

        [Column("DTINICIAL")]
        [DisplayName("Dtinicial")]
        public DateTime dtinicial { get; set; }

        [Column("PRECISAO")]
        [DisplayName("Precisao")]
        public decimal precisao { get; set; }

        [Column("DTFINAL")]
        [DisplayName("Dtfinal")]
        public DateTime dtfinal { get; set; }

    }
}
