﻿using Hino.Scheduler.Model.Engenharia;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Production
{
    [Table("PDOPRESOURCES")]
    public class pdResource : BaseEntity
    {
        public pdResource()
        {
        }

        [Key]
        [Column("IDRESOURCE")]
        [DisplayName("Cód. Recurso")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long idresource { get; set; }

        [Column("IDSORT")]
        [DisplayName("Ordenação")]
        public int idsort { get; set; }

        [Column("PARENTID")]
        [DisplayName("Recurso Pai")]
        public long? parentid { get; set; }

        [Column("DESCRIPTION")]
        [DisplayName("Descrição")]
        public string description { get; set; }

        [Column("COLOR")]
        [DisplayName("Cor")]
        public string color { get; set; }

        [Column("CODMAQUINA")]
        [DisplayName("Cód. Máquina")]
        public string codmaquina { get; set; }

        [Column("TEMOPS")]
        [DisplayName("Tem O.P.s")]
        public int temops { get; set; }

        [Column("TYPERESOURCE")]
        [DisplayName("Tipo de recurso")]
        public int typeresource { get; set; }
        
        [Column("IDLOADDATA")]
        [DisplayName("Id Load")]
        public long IdLoadData { get; set; }
    }
}
