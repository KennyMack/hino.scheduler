﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Production
{
    public class pdMachineCellLoad : BaseEntity
    {
        [Column("CODMAQUINA")]
        [DisplayName("Cód máquina")]
        public string codmaquina { get; set; }

        [Column("ENMAQUINASDESCRICAO")]
        [DisplayName("Desc. máquina")]
        public string enmaquinasdescricao { get; set; }

        [Column("CODCELULA")]
        [DisplayName("Cód. célula")]
        public short? codcelula { get; set; }

        [Column("ENCELULASDESCRICAO")]
        [DisplayName("Desc. célula")]
        public string encelulasdescricao { get; set; }

        [Column("CODROTINA")]
        [DisplayName("Cod. rotina")]
        public short? codrotina { get; set; }

        [Column("ENROTINASDESCRICAO")]
        [DisplayName("Desc. rotina")]
        public string enrotinasdescricao { get; set; }

        [Column("DIASPERIODO")]
        [DisplayName("Dias período")]
        public decimal diasperiodo { get; set; }

        [Column("MINUTOSPROD")]
        [DisplayName("Minutos em produção x máquina")]
        public decimal minutosprod { get; set; }

        [Column("HORASPRODMAQ")]
        [DisplayName("Horas em produção x máquina")]
        public string horasprodmaq { get; set; }

        [Column("MINPRODCELULA")]
        [DisplayName("Minutos em produção x célula")]
        public decimal minprodcelula { get; set; }

        [Column("HORASPRODCELULA")]
        [DisplayName("Horas em produção x célula")]
        public string horasprodcelula { get; set; }

        [Column("MINSTRABDIAMAQUINA")]
        [DisplayName("Minutos trab. x dia x máquina")]
        public decimal minstrabdiamaquina { get; set; }
        
        [Column("HORASTRABDIAMAQUINA")]
        [DisplayName("Horas trab. x dia x máquina")]
        public string horastrabdiamaquina { get; set; }

        [Column("MINSTRABDIACELULA")]
        [DisplayName("Minutos trab. x dia x célula")]
        public decimal minstrabdiacelula { get; set; }

        [Column("HORASTRABDIACELULA")]
        [DisplayName("Horas trab. x dia x célula")]
        public string horastrabdiacelula { get; set; }
    }
}
