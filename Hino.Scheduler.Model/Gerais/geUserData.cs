﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Gerais
{
    [Table("GEUSERDATA")]
    public class geUserData : BaseEntity
    {
        [Key]
        [Column("CODUSUARIO")]
        [DisplayName("Cod Usuário")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CodUsuario { get; set; }

        [Column("IDLOADDATA")]
        [DisplayName("Último cenário aberto")]
        public long? IdLoadData { get; set; }
    }
}
