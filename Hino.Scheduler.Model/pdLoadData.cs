﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model
{
    [Table("PDLOADDATA")]
    public class pdLoadData : BaseEntity
    {
        [Key]
        [Column("IDLOADDATA")]
        [DisplayName("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdLoadData { get; set; }

        [Column("DATELOADED")]
        [DisplayName("Data carga")]
        public DateTime DateLoaded { get; set; }

        [Column("CODUSUARIO")]
        [DisplayName("Cód. Usuário")]
        public string CodUsuario { get; set; }

        [Column("INITIALDATE")]
        [DisplayName("Data início")]
        public DateTime InitialDate { get; set; }

        [Column("ENDDATE")]
        [DisplayName("Data término")]
        public DateTime EndDate { get; set; }
    }
}
