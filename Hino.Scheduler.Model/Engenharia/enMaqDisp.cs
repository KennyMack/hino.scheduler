﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Engenharia
{
    public class enMaqDisp
    {
        [Column("CODESTAB", Order = 0)]
        [DisplayName("Cód. estab.")]
        public int codestab { get; set; }
        
        [Column("CODMAQUINA", Order = 1)]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("DOMINGO", Order = 2)]
        [DisplayName("Domingo")]
        public decimal domingo { get; set; }

        [Column("SEGUNDA", Order = 3)]
        [DisplayName("Segunda")]
        public decimal segunda { get; set; }

        [Column("TERCA", Order = 4)]
        [DisplayName("Terca")]
        public decimal terca { get; set; }

        [Column("QUARTA", Order = 5)]
        [DisplayName("Quarta")]
        public decimal quarta { get; set; }

        [Column("QUINTA", Order = 6)]
        [DisplayName("Quinta")]
        public decimal quinta { get; set; }

        [Column("SEXTA", Order = 7)]
        [DisplayName("Sexta")]
        public decimal sexta { get; set; }

        [Column("SABADO", Order = 8)]
        [DisplayName("Sábado")]
        public decimal sabado { get; set; }

    }
}
