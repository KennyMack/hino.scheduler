﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Engenharia
{
    [Table("ENMAQUINAS")]
    public class enMaquinas : BaseEntity
    {
        [Key]
        [Column("CODESTAB", Order = 0)]
        [DisplayName("Cód. estab.")]
        public int codestab { get; set; }

        [Key]
        [Column("CODMAQUINA", Order = 1)]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("CODCELULA")]
        [DisplayName("Cód. célula")]
        [Display(Order = -1)]
        public int codcelula { get; set; }

        [Column("DESCRICAO")]
        [DisplayName("Descrição")]
        public string descricao { get; set; }

        [Column("STATUS")]
        [DisplayName("Status")]
        public bool status { get; set; }

        [Column("QUANTIDADE")]
        [DisplayName("Quantidade")]
        public int quantidade { get; set; }

        [Column("CODPROCESSO")]
        [DisplayName("Cód. processo")]
        [Display(Order = -1)]
        public int? codprocesso { get; set; }

        [Column("CODINPUT")]
        [DisplayName("Cód. input")]
        [Display(Order = -1)]
        public int? codinput { get; set; }

        [Column("RENDIMENTO")]
        [DisplayName("Rendimento")]
        public int rendimento { get; set; }

        [Column("DESCCALCAPTO")]
        [DisplayName("Desconsiderar no calculo do apontamento?")]
        [Display(Order = -1)]
        public int desccalcapto { get; set; }

        [Column("CODIMOB")]
        [DisplayName("Cód. imob.")]
        [Display(Order = -1)]
        public int? codimob { get; set; }

        [Column("INCORPORACAO")]
        [DisplayName("Incorporação")]
        [Display(Order = -1)]
        public string incorporacao { get; set; }

        [Column("OBSERVACAO")]
        [DisplayName("Observação")]
        public string observacao { get; set; }
    }
}
