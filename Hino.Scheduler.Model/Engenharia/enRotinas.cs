﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Engenharia
{
    [Table("ENROTINAS")]
    public class enRotinas: BaseEntity
    {
        [Key]
        [Column("CODROTINA")]
        [DisplayName("Cód rotina")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codrotina { get; set; }

        [Column("DESCRICAO")]
        [DisplayName("Descrição")]
        public string descricao { get; set; }

        [Column("ROTINATERCEIRO")]
        [DisplayName("Rotina de terceiro")]
        public short rotinaterceiro { get; set; }

        [Column("ROTINAFERRAMENTAL")]
        [DisplayName("Rotina ferramental")]
        public short rotinaferramental { get; set; }

        [Column("APONTAACABADO")]
        [DisplayName("Aponta acabado")]
        public short apontaacabado { get; set; }

        [Column("APONTAPROCESSO")]
        [DisplayName("Aponta processo")]
        public short apontaprocesso { get; set; }

        [Column("TEMPO")]
        [DisplayName("Tempo")]
        public string tempo { get; set; }

        [Column("NUMOPERADORES")]
        [DisplayName("Num. operadores")]
        public string numoperadores { get; set; }

        [NotMapped]
        [DisplayName("Tem O.Ps")]
        public int temops { get; set; }
    }
}
