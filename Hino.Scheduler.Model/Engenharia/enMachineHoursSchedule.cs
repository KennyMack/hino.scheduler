﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Engenharia
{
    public class enMachineHoursSchedule: BaseEntity
    {
        public string Machine { get; set; }

        [Display(Order = -1, Description = "Domingo")]
        public DateTime? Domingo { get; set; }
        [Display(Order = -1, Description = "Prog")]
        public short WorkHourDom { get; set; }
        [Display(Order = -1, Description = "Par")]
        public short StopHourDom { get; set; }
        [Display(Order = -1, Description = "Prog")]
        public string DescWorkHourDom { get; set; }
        [Display(Order = -1, Description = "Par")]
        public string DescStopHourDom { get; set; }


        public DateTime? Segunda { get; set; }
        public short WorkHourSeg { get; set; }
        public short StopHourSeg { get; set; }
        public string DescWorkHourSeg { get; set; }
        public string DescStopHourSeg { get; set; }


        public DateTime? Terca { get; set; }
        public short WorkHourTer { get; set; }
        public short StopHourTer { get; set; }
        public string DescWorkHourTer { get; set; }
        public string DescStopHourTer { get; set; }

        public DateTime? Quarta { get; set; }
        public short WorkHourQua { get; set; }
        public short StopHourQua { get; set; }
        public string DescWorkHourQua { get; set; }
        public string DescStopHourQua { get; set; }

        public DateTime? Quinta { get; set; }
        public short WorkHourQui { get; set; }
        public short StopHourQui { get; set; }
        public string DescWorkHourQui { get; set; }
        public string DescStopHourQui { get; set; }

        public DateTime? Sexta { get; set; }
        public short WorkHourSex { get; set; }
        public short StopHourSex { get; set; }
        public string DescWorkHourSex { get; set; }
        public string DescStopHourSex { get; set; }

        public DateTime? Sabado { get; set; }
        public short WorkHourSab { get; set; }
        public short StopHourSab { get; set; }
        public string DescWorkHourSab { get; set; }
        public string DescStopHourSab { get; set; }

        public int NumSemana { get; set; }
        public string MesAno { get; set; }

        public short WorkHours { get; set; }

        public short StopHours { get; set; }
        
    }
}
