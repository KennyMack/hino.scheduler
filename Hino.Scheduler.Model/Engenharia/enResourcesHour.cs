﻿using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Engenharia
{
    [Table("ENRESOURCESHOUR")]
    public class enResourcesHour : BaseEntity
    {
        [Key]
        [Display(Order = -1)]
        [Column("IDRESHOUR")]
        [DisplayName("Cód. hora recurso")]
        public int codestab { get; set; }

        [Column("IDLOADDATA")]
        [Display(Order = -1)]
        [DisplayName("Id Load")]
        public long IdLoadData { get; set; }

        [Column("WEEKDAY")]
        [Display(Order = -1)]
        [DisplayName("Dia da semana")]
        public short WeekDay { get; set; }

        [Column("WORKHOURS")]
        [Display(Order = -1)]
        [DisplayName("Horas programadas")]
        public short WorkHours { get; set; }

        [Column("STOPHOURS")]
        [Display(Order = -1)]
        [DisplayName("Horas paradas")]
        public short StopHours { get; set; }

        [Column("CODMAQUINA")]
        [Display(Order = -1)]
        [DisplayName("Cod. Máquina")]
        public string CodMaquina { get; set; }

        [Column("ENMAQUINASDESCRICAO")]
        [Display(Order = 1)]
        [DisplayName("Máquina")]
        public string DescMaquina { get; set; }

        [NotMapped]
        [Display(Order = 1)]
        [DisplayName("Dia da semana")]
        public string DescWeekDay
        {
            get
            {
                return WeekDay.ToWeekDayDesc();
            }
        }

        [NotMapped]
        [Display(Order = 2)]
        [DisplayName("Horas programadas")]
        public string DescWorkHours
        {
            get
            {
                return WorkHours.ToHourMinute();
            }
        }

        [NotMapped]
        [Display(Order = 3)]
        [DisplayName("Horas paradas")]
        public string DescStopHours
        {
            get
            {
                return StopHours.ToHourMinute();
            }
        }

        [NotMapped]
        [Display(Order = 4)]
        [DisplayName("Horas disponíveis")]
        public string DispHours
        {
            get
            {
                return (WorkHours - StopHours).ToHourMinute();
            }
        }
    }
}
