﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Engenharia
{
    public class enMaqTurno : BaseEntity
    {
        [Column("CODMAQUINA", Order = 0)]
        [DisplayName("Cód. máquina")]
        public string codmaquina { get; set; }

        [Column("CODESTAB", Order = 1)]
        [DisplayName("Cód. estab.")]
        public int codestab { get; set; }

        [Column("TURNO", Order = 2)]
        [DisplayName("Turno")]
        public string turno { get; set; }

        [Column("DIASEMANA", Order = 3)]
        [DisplayName("Dia Semana")]
        public short diasemana { get; set; }
        
        [NotMapped]
        [DisplayName("Dia Semana")]
        public DayOfWeek? DayWeekEnum
        {
            get
            {
                switch (diasemana)
                {
                    case 1:
                        return DayOfWeek.Sunday;
                    case 2:
                        return DayOfWeek.Monday;
                    case 3:
                        return DayOfWeek.Tuesday;
                    case 4:
                        return DayOfWeek.Wednesday;
                    case 5:
                        return DayOfWeek.Thursday;
                    case 6:
                        return DayOfWeek.Friday;
                    case 7:
                        return DayOfWeek.Saturday;
                    default:
                        return null;
                }
            }
        }

        [Column("HORAINICIO", Order = 4)]
        [DisplayName("Hora início")]
        public short horainicio { get; set; }

        [Column("HORATERMINO", Order = 5)]
        [DisplayName("Hora término")]
        public short horatermino { get; set; }

        [Column("HRPARADAINI", Order = 6)]
        [DisplayName("Hr. parada ini.")]
        public short hrparadaini { get; set; }

        [Column("HRPARADATERM", Order = 7)]
        [DisplayName("Hr. para term.")]
        public short hrparadaterm { get; set; }
        

    }
}
