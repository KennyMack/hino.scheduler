﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Engenharia
{
    public class enMachineWorkDays
    {
        public string codmaquina { get; set; }
        public DateTime dia { get; set; }
        public double minutes { get; set; }
        public DayOfWeek? DayWeekEnum
        {
            get
            {
                return dia.DayOfWeek;
            }
        }
    }
}
