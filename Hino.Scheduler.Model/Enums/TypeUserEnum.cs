﻿using System.ComponentModel;

namespace Hino.Scheduler.Model.Enums
{
    public enum TypeUserEnum : byte
    {
        [Description("COMUM")]
        Comum = 0,
        [Description("ADMINISTRADOR")]
        Administrador = 1,
        [Description("HINO")]
        Hino = 0
    }
}
