﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Enums
{
    public enum StatusOP
    {
        [Description("PENDENTE")]
        Pendente = 0,
        [Description("ANDAMENTO")]
        Andamento = 1,
        [Description("CONCLUIDO")]
        Concluido = 2,
        [Description("CANCELADO")]
        Cancelado = 3
    }

    public enum PriorityOP
    {
        [Description("SEM PRIORIDADE")]
        SemPrioridade = 0,
        [Description("URGENTE")]
        Urgente = 1,
        [Description("ANDAMENTO")]
        Andamento = 2,
        [Description("ATRASADO")]
        Atrasado = 3,
        [Description("AGUARDANDO")]
        Aguardando = 4
    }
}
