﻿using System.ComponentModel;

namespace Hino.Scheduler.Model.Enums
{
    public enum StatusEnum : byte
    {
        [Description("ATIVO")]
        Ativo = 1,
        [Description("INATIVO")]
        Inativo = 0
    }
}
