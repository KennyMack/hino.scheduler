﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Scheduler.Model.Enums;

namespace Hino.Scheduler.Model.Auth
{
    [Table("GEUSUARIOS")]
    public class Users: BaseEntity
    {
        [Key]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Código do usuário é de preenchimento obrigatório")]
        [StringLength(25, ErrorMessage = "O usuário deve ter no máximo 25 caracteres")]
        [DisplayName("Cód. Usuario")]
        [Column("CODUSUARIO")]
        public String codUsuario { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O nome do usuário é de preenchimento obrigatório")]
        [StringLength(60, ErrorMessage = "O nome do usuário deve ter no máximo 60 caracteres")]
        [DisplayName("Nome")]
        [Column("NOME")]
        public String nome { get; set; }

        [Required]
        [DisplayName("Situação")]
        [Column("STATUS")]
        public StatusEnum status { get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName("Senha")]
        [Column("SENHA")]
        public String senha { get; set; }

        [Required]
        [DisplayName("Tipo")]
        [Column("TIPUSUARIO")]
        public TypeUserEnum tipo { get; set; }

    }
}
