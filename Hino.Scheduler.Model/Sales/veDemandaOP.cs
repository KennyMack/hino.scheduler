﻿using Hino.Scheduler.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Sales
{
    public class veDemandaOP
    {
        [Column("CODORDPROD", TypeName = "number")]
        [DisplayName("Cód. ord. prod.")]
        public long codordprod { get; set; }

        [Column("NIVELORDPROD")]
        [DisplayName("Nivel ord. prod.")]
        public string nivelordprod { get; set; }

        [Column("NIVELORDPRODPAI")]
        [DisplayName("Nivel ord. prod. pai")]
        public string nivelordprodpai { get; set; }

        [Column("CODPRODUTO")]
        [DisplayName("Cód. produto")]
        public string codproduto { get; set; }

        [Column("FSPRODUTODESCRICAO")]
        [DisplayName("Desc. produto")]
        public string fsprodutodescricao { get; set; }

        [Column("DATAINIOP")]
        [DisplayName("Data prev. início")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime datainiop { get; set; }

        [Column("DATAFIMOP")]
        [DisplayName("Data prév. término")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy HH:mm")]
        public DateTime datafimop { get; set; }

        [Column("QTDPROG", TypeName = "number")]
        [DisplayName("Programado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdprog { get; set; }

        [Column("QTDAPONT", TypeName = "number")]
        [DisplayName("Realizado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdapont { get; set; }

        [Column("QTDREFUGO", TypeName = "number")]
        [DisplayName("Refugado")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal qtdrefugo { get; set; }

        [Column("LABELOP", TypeName = "number")]
        [DisplayName("Status O.P.")]
        public int labelop { get; set; }

        [NotMapped]
        [DisplayName("Status O.P.")]
        public StatusOP desclabelop
        {
            get
            {
                switch (labelop)
                {
                    case 0:
                        return StatusOP.Pendente;
                    case 1:
                        return StatusOP.Andamento;
                    case 2:
                        return StatusOP.Concluido;
                    case 3:
                        return StatusOP.Cancelado;
                    default:
                        return StatusOP.Pendente;
                }
            }
        }

        [Column("STATUSOP", TypeName = "number")]
        [DisplayName("Prioridade")]
        public int statusop { get; set; }

        [NotMapped]
        [DisplayName("Prioridade")]
        public PriorityOP descstatusop
        {
            get
            {
                switch (statusop)
                {
                    case 0:
                        return PriorityOP.SemPrioridade;
                    case 1:
                        return PriorityOP.Urgente;
                    case 2:
                        return PriorityOP.Andamento;
                    case 3:
                        return PriorityOP.Atrasado;
                    case 4:
                        return PriorityOP.Aguardando;
                    default:
                        return PriorityOP.SemPrioridade;
                }
            }
        }

        [Column("IDLOADDATA")]
        [DisplayName("Id Load")]
        public long IdLoadData { get; set; }

        [Column("IDDEMANDA")]
        [DisplayName("Id Demanda")]
        public long iddemanda { get; set; }

    }
}
