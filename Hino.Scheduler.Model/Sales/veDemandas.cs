﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Sales
{
    [Table("VEDEMANDA")]
    public class veDemanda : BaseEntity
    {
        [Key]
        [Column("IDDEMANDA")]
        [DisplayName("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal iddemanda { get; set; }

        [Column("CLIENT")]
        [DisplayName("Cliente")]
        public string client { get; set; }
        
        [Column("CODESTAB")]
        [DisplayName("Cod. estab.")]
        public int codestab { get; set; }

        [Column("CODPEDVENDA")]
        [DisplayName("Cód. ped. venda")]
        public long codpedvenda { get; set; }

        [Column("CODPRODUTO")]
        [DisplayName("Cód. produto")]
        public string codproduto { get; set; }

        [Column("FSPRODUTODESCRICAO")]
        [DisplayName("Desc. produto")]
        public string fsprodutodescricao { get; set; }

        [Column("DATAPROGVENDA")]
        [DisplayName("Data programada do pedido")]
        public DateTime dataprogvenda { get; set; }

        [Column("DTINICIO")]
        [DisplayName("Dt. prev. ini. prod.")]
        public DateTime dtinicio { get; set; }

        [Column("DTTERMINO")]
        [DisplayName("Dt. prev. term. prod.")]
        public DateTime dttermino { get; set; }

        [Column("SALDO")]
        [DisplayName("Qtd. Produção")]
        public decimal sldprograma { get; set; }

        [Column("QTDPROGRAMADA")]
        [DisplayName("Qtd. programada")]
        public decimal qtdprogramada { get; set; }
        
        [Column("ATENDIDA")]
        [DisplayName("Atendida")]
        public bool atendida { get; set; }
        
        [Column("TEMOPS")]
        [DisplayName("Tem O.Ps")]
        public bool temops { get; set; }

        [NotMapped]
        [DisplayName("Diferença")]
        public decimal diferenca
        {
            get
            {
                return qtdprogramada - sldprograma;
            }
        }

        [NotMapped]
        [DisplayName("Status Demanda")]
        public string StatusDemanda
        {
            get
            {
                if (!temops && !atendida)
                    return "SEM PRODUÇÃO";
                else if ((temops | atendida) && DiasDifProd > 0)
                    return "ATRASADA";
                else if ((temops | atendida) && DiasDifProd < 0)
                    return "ADIANTADA";
                
                return "NA DATA";
            }
        }

        [NotMapped]
        [DisplayName("Dias dif. prod.")]
        public int DiasDifProd
        {
            get
            {
                if (atendida | temops)
                    return (dttermino - dataprogvenda).Days;

                return 0;
            }
        }

        [NotMapped]
        [DisplayName("Saldo Programa")]
        public string AtendeSaldoProg
        {
            get
            {
                if (qtdprogramada - sldprograma  == 0)
                    return "ATENDE TOTAL";
                else if (sldprograma > 0 && 
                         sldprograma < qtdprogramada)
                    return "ATENDE PARCIAL";
                else
                    return "NÃO ATENDE";
            }
        }

        [Column("IDJOBSIMU")]
        [DisplayName("Id. Job. Simulado")]
        public long? IDJOBSIMU { get; set; }

        [Column("IDLOADDATA")]
        [DisplayName("Id Load")]
        public long IdLoadData { get; set; }
    }
}
