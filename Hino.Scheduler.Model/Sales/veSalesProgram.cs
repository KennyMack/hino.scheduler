﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Model.Sales
{
    public class veSalesProgram : BaseEntity
    {
        [Column("CODEMPRESA", Order = 0)]
        [DisplayName("Cód. empresa")]
        public int codempresa { get; set; }

        [Column("NOMEFANTASIA", Order = 1)]
        [DisplayName("Nome fantasia")]
        public string nomefantasia { get; set; }

        [Column("RAZAOSOCIAL", Order = 2)]
        [DisplayName("Razão social")]
        public string razaosocial { get; set; }

        [Column("CODPRODUTO", Order = 3)]
        [DisplayName("Cód. produto")]
        public string codproduto { get; set; }

        [Column("DESCRICAO", Order = 4)]
        [DisplayName("Descrição")]
        public string descricao { get; set; }

        [Column("QTDTOTALITEM", Order = 5)]
        [DisplayName("Qtd total pedido")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal qtdtotalitem { get; set; }

        [Column("DATAPROGRAMA", Order = 6)]
        [DisplayName("Data programa")]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime dataprograma { get; set; }

        [Column("QTDEPROGRAMA", Order = 7)]
        [DisplayName("Qtd. programa")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal qtdeprograma { get; set; }

        [Column("QTDEREALIZADA", Order = 8)]
        [DisplayName("Qtd. realizada")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal qtderealizada { get; set; }

        [Column("QTDEFATURADA", Order = 9)]
        [DisplayName("Qtd. faturada")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal qtdefaturada { get; set; }

        [Column("QTDPRODUCAO", Order = 10)]
        [DisplayName("Qtd. em produção")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal qtdproducao { get; set; }

        [Column("SALDOESTOQUEPADRAO", Order = 11)]
        [DisplayName("Estoque padrão")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal saldoestoquepadrao { get; set; }

        [Column("SALDOESTOQUERESERVA", Order = 12)]
        [DisplayName("Estoque reserva")]
        [DisplayFormat(DataFormatString = "{0:n4}")]
        public decimal saldoestoquereserva { get; set; }

        

    }
}
