﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Scheduler.Model.Sales
{
    [Table("VEDEMANDADET")]
    public class veDemandaDet : BaseEntity
    {
        [Key]
        [Column("IDJOBDEMDET")]
        [DisplayName("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long idjobdemdet { get; set; }

        [Column("IDDEMANDA")]
        [DisplayName("Id. Demanda")]
        public long iddemanda { get; set; }

        [Column("IDJOB")]
        [DisplayName("Id. Job.")]
        public long idjob { get; set; }

        [Column("IDLOADDATA")]
        [DisplayName("Id. cenário")]
        public long IdLoadData { get; set; }

        [Column("QUANTIDADE")]
        [DisplayName("Quantidade")]
        public string quantidade { get; set; }
    }
}
