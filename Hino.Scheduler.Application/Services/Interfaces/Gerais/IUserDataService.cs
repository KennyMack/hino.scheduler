﻿using Hino.Scheduler.Model.Gerais;
using System;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Gerais
{
    public interface IUserDataService : IBaseService<geUserData>
    {
        Task<geUserData> GetByCodUsuarioAsync(string pCodUsuario);
    }
}
