﻿using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Sales
{
    public interface IDemandaService : IBaseService<veDemanda>
    {
        Task<IEnumerable<veDemanda>> GetByEstabIdAsync(int pCodEstab);
        Task CreateJobFromDemanda(int pCodEstab, long pIdDemanda);
        Task AtualizaOPDemandaAsync(int pCodEstab, long pIdLoadData);
        Task<List<veSalesProgram>> GetSalesProgramListAsync(int pCodEstab);
    }
}
