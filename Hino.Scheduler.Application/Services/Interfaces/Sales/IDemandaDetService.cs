﻿using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Sales
{
    public interface IDemandaDetService : IBaseService<veDemandaDet>
    {
    }
}
