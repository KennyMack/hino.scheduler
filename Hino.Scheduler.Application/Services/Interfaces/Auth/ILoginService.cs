﻿using Hino.Scheduler.Model.Auth;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Auth
{
    public interface ILoginService : IBaseService<Users>
    {
        Task<Users> ValidateUserPass(string user, string password);
    }
}
