﻿using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Engenharia
{
    public interface IMaquinasService : IBaseService<enMaquinas>
    {
        Task<List<pdMachineCellLoad>> QueryMachineCellLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd);
        Task<List<pdMachineLoad>> QueryMachineLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario);
        Task<List<pdMachineLoad>> QueryMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodUsuario);
        Task<bool> ExecLoadSimulCargaMaquinaAsync(int pCodEstab, long pIdLoadData, string pCodUsuario);
        Task<bool> ExecLoadCargaMaquinaAsync(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario);
    }
}
