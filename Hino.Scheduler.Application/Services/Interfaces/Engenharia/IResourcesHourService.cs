﻿using Hino.Scheduler.Model.Engenharia;
using System;

namespace Hino.Scheduler.Application.Services.Interfaces.Engenharia
{
    public interface IResourcesHourService : IBaseService<enResourcesHour>
    {
    }
}
