﻿using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface ILoadDataService : IBaseService<pdLoadData>
    {
        Task<long> CreateScenario(int pCodEstab, string pCodUsuario, DateTime pInitalDate, DateTime pEndDate);
        Task DeleteScenario(int pCodEstab, long pIdDataLoad);
    }
}
