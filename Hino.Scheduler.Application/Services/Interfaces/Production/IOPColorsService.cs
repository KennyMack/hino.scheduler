﻿using Hino.Scheduler.Model.Production;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface IOPColorsService : IBaseService<pdOPColors>
    {
        Task ClearColors();
        Task AttachColorToOP(int pCodEstab);
        Task AttachColorToOPRot(int pCodEstab);
    }
}
