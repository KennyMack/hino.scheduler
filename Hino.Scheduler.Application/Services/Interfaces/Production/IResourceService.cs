﻿using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface IResourceService : IBaseService<pdResource>
    {
        Task<List<enMaqTurno>> GetMachinesTurnoAsync(int pCodEstab, long pIdLoadData);
        Task<List<enMaqDisp>> GetMachineDispAsync(int pCodEstab, long pIdLoadData);
        Task<IEnumerable<pdResource>> GetResourcesByType(int pType, long pIdLoadData);
        Task<List<enMaquinas>> GetMachinesListAsync(int pCodEstab);
        Task GeneratedResources(int pCodEstab, bool pClearJobs);
        Task AttachResourceToOPRot(int pCodEstab, long pIdLoadData);
    }
}
