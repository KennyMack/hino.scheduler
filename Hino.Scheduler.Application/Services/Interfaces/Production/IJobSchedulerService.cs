﻿using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface IJobSchedulerService : IBaseService<pdJob>
    {
        Task<List<pdOrdemProdDet>> DetailMachineLoad(int pCodEstab, string pCodMaquina, DateTime pDtProducao, string pCodUsuario);
        Task<List<pdOrdemProdDet>> DetailMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodMaquina, DateTime pDtProducao, string pCodUsuario);
        Task GeneratedJobSchedule(int pCodEstab, bool pClearJobs);
        Task<IEnumerable<pdJob>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData);
        Task<IEnumerable<pdJob>> GetByEstabIdAsync(int pCodEstab);
        Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData);
    }
}
