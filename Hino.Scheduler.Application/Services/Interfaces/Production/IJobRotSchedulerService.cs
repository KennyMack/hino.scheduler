﻿using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface IJobRotSchedulerService : IBaseService<pdJobRot>
    {
        Task<IEnumerable<veDemanda>> QueryDemandas(int pCodEstab, DateTime pDtInitial, DateTime pDtFinal);
        Task GeneratedJobRoutines(int pCodEstab, bool pClearJobs);
        Task GeneratedJobRotSchedule(int pCodEstab, DateTime pDtInitial, int pTimeParts, bool pClearJobs, bool pSimulate);
        Task<IEnumerable<pdJobRot>> QueryScheduledJobsRot(int pCodEstab, DateTime pDtInitial);
        Task<IEnumerable<pdJobRot>> QueryJobsRotDetailAsync(int pCodEstab, long pIdLoadData, long pCodOrdProd, string pNivelOrdProd);
        Task<IEnumerable<pdJobRot>> GetByEstabIdAsync(int pCodEstab);
        Task<IEnumerable<pdJobRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData);
        Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData);
        Task<List<pdJobRot>> QueryOperationBottleneck(int pCodEstab, long pIdScenario);
        Task GenerateJobShop(int pCodEstab, DateTime pDtInitial, int pTimeParts);
    }
}
