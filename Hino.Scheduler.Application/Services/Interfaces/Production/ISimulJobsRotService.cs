﻿using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface ISimulJobsRotService : IBaseService<pdSimulJobsRot>
    {
        Task GeneratedJobSchedule(int pCodEstab, long pIdLoadData, DateTime pDateIni, int pPartesTempo);
        Task<IEnumerable<pdSimulJobsRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData);
        Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData);
    }
}
