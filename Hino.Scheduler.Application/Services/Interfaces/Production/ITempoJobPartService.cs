﻿using Hino.Scheduler.Model.Production;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Interfaces.Production
{
    public interface ITempoJobPartService : IBaseService<pdTempoJobPart>
    {
    }
}
