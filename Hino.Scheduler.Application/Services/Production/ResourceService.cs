﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hino.Scheduler.Model.Engenharia;

namespace Hino.Scheduler.Application.Services.Production
{
    public class ResourceService : BaseService<pdResource>, IResourceService
    {
        private readonly IResourceRepository _IResourceRepository;

        public ResourceService(IResourceRepository repo) : base(repo)
        {
            _IResourceRepository = repo;
        }

        public async Task GeneratedResources(int pCodEstab, bool pClearJobs) =>
            await _IResourceRepository.GeneratedResources(pCodEstab, pClearJobs);

        public async Task<IEnumerable<pdResource>> GetResourcesByType(int pType, long pIdLoadData) =>
            await _IResourceRepository.QueryAsync(r => r.parentid != null && r.typeresource == pType && r.IdLoadData == pIdLoadData);

        public async Task AttachResourceToOPRot(int pCodEstab, long pIdLoadData)
        {
            try
            {
                await _IResourceRepository.AttachResourceToOPRot(pCodEstab, pIdLoadData);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
            
        }

        public async Task<List<enMaqTurno>> GetMachinesTurnoAsync(int pCodEstab, long pIdLoadData) =>
            await _IResourceRepository.GetMachinesTurnoAsync(pCodEstab, pIdLoadData);

        public async Task<List<enMaqDisp>> GetMachineDispAsync(int pCodEstab, long pIdLoadData) =>
            await _IResourceRepository.GetMachineDispAsync(pCodEstab, pIdLoadData);

        public async Task<List<enMaquinas>> GetMachinesListAsync(int pCodEstab) =>
            await _IResourceRepository.GetMachinesListAsync(pCodEstab);

    }
}
