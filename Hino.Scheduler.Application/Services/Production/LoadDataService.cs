﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hino.Scheduler.Model;

namespace Hino.Scheduler.Application.Services.Production
{
    public class LoadDataService : BaseService<pdLoadData>, ILoadDataService
    {
        private readonly ILoadDataRepository _ILoadDataRepository;

        public LoadDataService(ILoadDataRepository repo) : base(repo)
        {
            _ILoadDataRepository = repo;
        }

        public async Task<long> CreateScenario(int pCodEstab, string pCodUsuario, DateTime pInitalDate, DateTime pEndDate)
        {
            try
            {
                return await _ILoadDataRepository.CreateScenario(pCodEstab, pCodUsuario, pInitalDate, pEndDate);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
                return 0;
            }
        }

        public async Task DeleteScenario(int pCodEstab, long pIdDataLoad)
        {
            try
            {
                await _ILoadDataRepository.DeleteScenario(pCodEstab, pIdDataLoad);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }
    }
}
