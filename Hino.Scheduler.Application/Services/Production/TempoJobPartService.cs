﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;

namespace Hino.Scheduler.Application.Services.Production
{
    public class TempoJobPartService : BaseService<pdTempoJobPart>, ITempoJobPartService
    {
        private readonly ITempoJobPartRepository _ITempoJobPartRepository;

        public TempoJobPartService(ITempoJobPartRepository repo) : base(repo)
        {
            _ITempoJobPartRepository = repo;
        }        
    }
}
