﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;

namespace Hino.Scheduler.Application.Services.Production
{
    public class JobRotSchedulerService : BaseService<pdJobRot>, IJobRotSchedulerService
    {
        private readonly IJobRotSchedulerRepository _IJobRotSchedulerRepository;

        public JobRotSchedulerService(IJobRotSchedulerRepository repo) : base(repo)
        {
            _IJobRotSchedulerRepository = repo;
        }

        public async Task GeneratedJobRotSchedule(int pCodEstab, DateTime pDtInitial, int pTimeParts, bool pClearJobs, bool pSimulate)
        {
            try
            {
                await _IJobRotSchedulerRepository.GeneratedJobRotSchedule(pCodEstab, pDtInitial, pTimeParts, pClearJobs, pSimulate);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }

        public async Task GeneratedJobRoutines(int pCodEstab, bool pClearJobs)
        {
            try
            {
                await _IJobRotSchedulerRepository.GeneratedJobRoutines(pCodEstab, pClearJobs);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }

        public async Task<IEnumerable<pdJobRot>> GetByEstabIdAsync(int pCodEstab) =>
            await QueryAsync(r => r.codestab == pCodEstab);

        public async Task<IEnumerable<pdJobRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData) =>
            await QueryAsync(r => r.codestab == pCodEstab && r.IdLoadData == pIdLoadData);

        public async Task<IEnumerable<pdJobRot>> QueryScheduledJobsRot(int pCodEstab, DateTime pDtInitial)
        {
            return await _IJobRotSchedulerRepository.QueryScheduledJobsRot(pCodEstab, pDtInitial);
        }

        public async Task<IEnumerable<pdJobRot>> QueryJobsRotDetailAsync(int pCodEstab, long pIdLoadData, long pCodOrdProd, string pNivelOrdProd) =>
            await _IJobRotSchedulerRepository.QueryJobsRotDetailAsync(pCodEstab, pIdLoadData, pCodOrdProd, pNivelOrdProd);

        public async Task<IEnumerable<veDemanda>> QueryDemandas(int pCodEstab, DateTime pDtInitial, DateTime pDtFinal) =>
            await _IJobRotSchedulerRepository.QueryDemandas(pCodEstab, pDtInitial, pDtFinal);

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            try
            {
                await _IJobRotSchedulerRepository.SaveOPChangesToERP(pCodEstab, pIdLoadData);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }

        public async Task<List<pdJobRot>> QueryOperationBottleneck(int pCodEstab, long pIdScenario) =>
            await _IJobRotSchedulerRepository.QueryOperationBottleneck(pCodEstab, pIdScenario);

        public async Task GenerateJobShop(int pCodEstab, DateTime pDtInitial, int pTimeParts)
        {
            try
            {
                await _IJobRotSchedulerRepository.GenerateJobShop(pCodEstab, pDtInitial, pTimeParts);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }
    }
}
