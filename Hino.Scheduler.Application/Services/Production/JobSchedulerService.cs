﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;

namespace Hino.Scheduler.Application.Services.Production
{
    public class JobSchedulerService : BaseService<pdJob>, IJobSchedulerService
    {
        private readonly IJobSchedulerRepository _IJobSchedulerRepository;

        public JobSchedulerService(IJobSchedulerRepository repo) : base(repo)
        {
            _IJobSchedulerRepository = repo;
        }

        public async Task<List<pdOrdemProdDet>> DetailMachineLoad(int pCodEstab, string pCodMaquina, DateTime pDtProducao, string pCodUsuario) =>
            await _IJobSchedulerRepository.DetailMachineLoad(pCodEstab, pCodMaquina, pDtProducao, pCodUsuario);

        public async Task<List<pdOrdemProdDet>> DetailMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodMaquina, DateTime pDtProducao, string pCodUsuario) =>
            await _IJobSchedulerRepository.DetailMachineLoadSimulated(pCodEstab, pIdLoadData, pCodMaquina, pDtProducao, pCodUsuario);

        [Obsolete("Criado o sistema de fazer cenários", true)]
        public async Task GeneratedJobSchedule(int pCodEstab, bool pClearJobs)
        {
            try
            {
                await _IJobSchedulerRepository.GeneratedJobSchedule(pCodEstab, pClearJobs);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }

        public async Task<IEnumerable<pdJob>> GetByEstabIdAsync(int pCodEstab) =>
            await QueryAsync(r => r.codestab == pCodEstab);

        public async Task<IEnumerable<pdJob>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData) =>
            await QueryAsync(r => r.codestab == pCodEstab && r.IdLoadData == pIdLoadData);

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            try
            {
                await _IJobSchedulerRepository.SaveOPChangesToERP(pCodEstab, pIdLoadData);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }
    }
}
