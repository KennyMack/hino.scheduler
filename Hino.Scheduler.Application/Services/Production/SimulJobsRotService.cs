﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Production
{
    public class SimulJobsRotService : BaseService<pdSimulJobsRot>, ISimulJobsRotService
    {
        private readonly ISimulJobsRotRepository _ISimulJobsRotRepository;

        public SimulJobsRotService(ISimulJobsRotRepository repo) : base(repo)
        {
            _ISimulJobsRotRepository = repo;
        }

        public async Task<IEnumerable<pdSimulJobsRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData) =>
            await _ISimulJobsRotRepository.GetByEstabIdAndIdLoadAsync(pCodEstab, pIdLoadData);

        public async Task GeneratedJobSchedule(int pCodEstab, long pIdLoadData, DateTime pDateIni, int pPartesTempo) =>
            await _ISimulJobsRotRepository.GeneratedJobSchedule(pCodEstab, pIdLoadData, pDateIni, pPartesTempo);

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            try
            {
                await _ISimulJobsRotRepository.SaveOPChangesToERP(pCodEstab, pIdLoadData);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }
    }
}
