﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Production
{
    public class OPColorsService : BaseService<pdOPColors>, IOPColorsService
    {
        private readonly IOPColorsRepository _IOPColorsRepository;

        public OPColorsService(IOPColorsRepository repo) : base(repo)
        {
            _IOPColorsRepository = repo;
        }

        public async Task ClearColors() =>
            await _IOPColorsRepository.ClearColors();

        public async Task AttachColorToOP(int pCodEstab) =>
            await _IOPColorsRepository.AttachColorToOP(pCodEstab);

        public async Task AttachColorToOPRot(int pCodEstab)
        {
            try
            {
                await _IOPColorsRepository.AttachColorToOPRot(pCodEstab);
            }
            catch (System.Exception e)
            {
                Errors.Add(e.Message);
            }
            
        }
    }
}
