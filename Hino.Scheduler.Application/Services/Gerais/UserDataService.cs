﻿using Hino.Scheduler.Application.Services.Interfaces.Gerais;
using Hino.Scheduler.Data.Repositories.Interfaces.Gerais;
using Hino.Scheduler.Model.Gerais;
using System;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services.Gerais
{
    public class UserDataService : BaseService<geUserData>, IUserDataService
    {
        private readonly IUserDataRepository _IUserDataRepository;

        public UserDataService(IUserDataRepository repo) : base(repo)
        {
            _IUserDataRepository = repo;
        }

        public async Task<geUserData> GetByCodUsuarioAsync(string pCodUsuario) =>
            await _IUserDataRepository.GetByCodUsuarioAsync(pCodUsuario);
    }
}
