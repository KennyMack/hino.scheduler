﻿using System;
using System.Threading.Tasks;
using Hino.Scheduler.Application.Services.Interfaces.Auth;
using Hino.Scheduler.Data.Repositories;
using Hino.Scheduler.Model.Auth;
using Hino.Scheduler.Utils;
using Hino.Scheduler.Data.Repositories.Interfaces.Auth;

namespace Hino.Scheduler.Application.Services.Auth
{
    public class LoginService : BaseService<Users>, ILoginService
    {
        private readonly IUsersRepository _IUsersRepository;

        public LoginService(IUsersRepository repo) : base(repo)
        {
            _IUsersRepository = repo;
        }

        public async Task<Users> ValidateUserPass(string userName, string password)
        {
            var user = await _IUsersRepository.GetByIdAsync(new Users
            {
                codUsuario = userName
            });

            if (user == null)
            {
                Errors.Add("Usuário não encontrado");
                return null;
            }

            Criptografia cripto = new Criptografia();

            if (user.senha != cripto.Encrypt(password))
            {
                Errors.Add("Usuário ou senha inválida.");
                return null;
            }

            return user;
        }
    }
}
