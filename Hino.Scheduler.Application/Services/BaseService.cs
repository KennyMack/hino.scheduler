﻿using Hino.Scheduler.Application.Services.Interfaces;
using Hino.Scheduler.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.Services
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : class
    {
        public List<string> Errors { get; set; }
        private readonly IBaseRepository<T> BaseRepo;

        public BaseService(IBaseRepository<T> repo)
        {
            BaseRepo = repo;
            Errors = new List<string>();
        }

        public T Add(T model)
        {
            try
            {
                BaseRepo.Add(model);
            }
            catch (Exception e)
            {
                Errors.Add(string.Join("", new string[] { e.Message,
                                          e?.InnerException?.InnerException?.Message  }));
            }
            return model;
        }

        public T Update(T model)
        {
            try
            {
                BaseRepo.Update(model);
            }
            catch (Exception e)
            {
                Errors.Add(string.Join("", new string[] { e.Message,
                                          e?.InnerException?.InnerException?.Message  }));
            }
            return model;
        }

        public T Remove(T model)
        {
            try
            {
                BaseRepo.Remove(model);
            }
            catch (Exception e)
            {
                Errors.Add(string.Join("", new string[] { e.Message,
                                          e?.InnerException?.InnerException?.Message  }));
            }
            return model;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.GetAllAsync(includeProperties);

        public virtual async Task<T> GetByIdAsync(T model, params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.GetByIdAsync(model, includeProperties);

        public virtual async Task<IEnumerable<T>> QueryAsync(System.Linq.Expressions.Expression<Func<T, bool>> predicate,
            params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.QueryAsync(predicate, includeProperties);

        public virtual async Task<long> NextSequenceAsync() =>
            await BaseRepo.NextSequenceAsync();

        public virtual void RollBackChanges()
        {
            try
            {
                 BaseRepo.RollBackChanges();
            }
            catch (Exception e)
            {
                Errors.Add(string.Join("", new string[] { e.Message,
                                          e?.InnerException?.InnerException?.Message  }));
            }
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            try
            {
                return await BaseRepo.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Errors.Add(string.Join("", new string[] { e.Message,
                                          e?.InnerException?.InnerException?.Message  }));
            }
            return -1;
        }

        public void Dispose()
        {
            BaseRepo.Dispose();
        }
    }
}
