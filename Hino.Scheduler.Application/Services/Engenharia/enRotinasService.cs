﻿using Hino.Scheduler.Application.Services.Interfaces.Engenharia;
using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using System;

namespace Hino.Scheduler.Application.Services.Engenharia
{
    public class enRotinasService : BaseService<enRotinas>, IenRotinasService
    {
        private readonly IenRotinasRepository _IenRotinasRepository;

        public enRotinasService(IenRotinasRepository repo) : base(repo)
        {
            _IenRotinasRepository = repo;
        }
    }
}
