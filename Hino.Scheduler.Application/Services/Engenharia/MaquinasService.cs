﻿using System;
using System.Threading.Tasks;
using Hino.Scheduler.Application.Services.Interfaces.Engenharia;
using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System.Collections.Generic;

namespace Hino.Scheduler.Application.Services.Engenharia
{
    public class MaquinasService : BaseService<enMaquinas>, IMaquinasService
    {
        private readonly IMaquinasRepository _IMaquinasRepository;

        public MaquinasService(IMaquinasRepository repo) : base(repo)
        {
            _IMaquinasRepository = repo;
        }

        public async Task<List<pdMachineCellLoad>> QueryMachineCellLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd) =>
            await _IMaquinasRepository.QueryMachineCellLoad(pCodEstab, pDtInitial, pDtEnd);

        public async Task<List<pdMachineLoad>> QueryMachineLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario) =>
            await _IMaquinasRepository.QueryMachineLoad(pCodEstab, pDtInitial, pDtEnd, pCodUsuario);

        public async Task<List<pdMachineLoad>> QueryMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodUsuario) =>
            await _IMaquinasRepository.QueryMachineLoadSimulated(pCodEstab, pIdLoadData, pCodUsuario);

        public async Task<bool> ExecLoadCargaMaquinaAsync(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario)
        {
            try
            {
                await _IMaquinasRepository.ExecLoadCargaMaquinaAsync(pCodEstab, pDtInitial, pDtEnd, pCodUsuario);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> ExecLoadSimulCargaMaquinaAsync(int pCodEstab, long pIdLoadData, string pCodUsuario)
        {
            try
            {
                await _IMaquinasRepository.ExecLoadSimulCargaMaquinaAsync(pCodEstab, pIdLoadData, pCodUsuario);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
                return false;
            }
            return true;
        }

    }
}
