﻿using Hino.Scheduler.Application.Services.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using System;

namespace Hino.Scheduler.Application.Services.Engenharia
{
    public class ResourcesHourService : BaseService<enResourcesHour>, IResourcesHourService
    {
        private readonly IResourcesHourRepository _IResourcesHourRepository;

        public ResourcesHourService(IResourcesHourRepository repo) : base(repo)
        {
            _IResourcesHourRepository = repo;
        }
    }
}
