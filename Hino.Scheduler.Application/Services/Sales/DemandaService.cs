﻿using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Application.Services.Interfaces.Sales;
using Hino.Scheduler.Data.Repositories.Interfaces.Sales;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Scheduler.Application.Services.Sales
{
    public class DemandaService : BaseService<veDemanda>, IDemandaService
    {
        private readonly IDemandaRepository _IDemandaRepository;

        public DemandaService(IDemandaRepository repo) : base(repo)
        {
            _IDemandaRepository = repo;
        }

        public async Task<IEnumerable<veDemanda>> GetByEstabIdAsync(int pCodEstab) =>
            await QueryAsync(r => r.codestab == pCodEstab);

        public async Task CreateJobFromDemanda(int pCodEstab, long pIdDemanda) =>
            await _IDemandaRepository.CreateJobFromDemanda(pCodEstab, pIdDemanda);

        public async Task<List<veSalesProgram>> GetSalesProgramListAsync(int pCodEstab) =>
            await _IDemandaRepository.GetSalesProgramListAsync(pCodEstab);

        public async Task AtualizaOPDemandaAsync(int pCodEstab, long pIdLoadData)
        {
            Errors.Clear();

            try
            {
                await _IDemandaRepository.AtualizaOPDemandaAsync(pCodEstab, pIdLoadData);
            }
            catch (Exception e)
            {
                Errors.Add(e.Message);
            }
        }
    }
}
