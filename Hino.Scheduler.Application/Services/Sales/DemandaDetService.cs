﻿using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Application.Services.Interfaces.Sales;
using Hino.Scheduler.Data.Repositories.Interfaces.Sales;

namespace Hino.Scheduler.Application.Services.Sales
{
    public class DemandaDetService : BaseService<veDemandaDet>, IDemandaDetService
    {
        private readonly IDemandaDetRepository _IDemandaDetRepository;

        public DemandaDetService(IDemandaDetRepository repo) : base(repo)
        {
            _IDemandaDetRepository = repo;
        }
    }
}
