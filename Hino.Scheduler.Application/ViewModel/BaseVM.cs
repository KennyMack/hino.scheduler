﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hino.Scheduler.Application.ViewModel
{
    public class BaseVM
    {
        public List<string> Errors;
        public BaseVM()
        {
            Errors = new List<string>();

        }

        public virtual Task Initialize()
        {
            throw new NotImplementedException();
        }

        public virtual Task LoadData()
        {
            throw new NotImplementedException();
        }

        protected void BaseDispose()
        {
        }
    }
}
