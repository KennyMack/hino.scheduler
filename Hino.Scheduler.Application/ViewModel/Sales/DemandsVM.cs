﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.Services.Sales;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Sales
{
    public class DemandsVM: BaseVM, IDisposable
    {
        public BindingList<veDemanda> _DemandDataSource;
        public BindingList<veSalesProgram> _SalesProgramDataSource;
        public long _IdScenario;

        public DemandsVM()
        {
            _IdScenario = 0;
            _DemandDataSource = new BindingList<veDemanda>();
            _SalesProgramDataSource = new BindingList<veSalesProgram>();
        }

        public async Task LoadSalesProgram()
        {
            using (var _veDemandaRepository = new DemandaRepository())
            using (var _veDemandaService = new DemandaService(_veDemandaRepository))
            {
                _SalesProgramDataSource = new BindingList<veSalesProgram>(
                    await _veDemandaRepository.GetSalesProgramListAsync(Parameters.codEstab));
            }
        }

        public async Task LoadDataSource()
        {
            using (var _veDemandaRepository = new DemandaRepository())
            using (var _veDemandaService = new DemandaService(_veDemandaRepository))
            {
                _DemandDataSource = new BindingList<veDemanda>(
                    (await _veDemandaRepository.QueryAsync(r => r.IdLoadData == _IdScenario)).ToList());
            }
        }

        public void Dispose()
        {
            _DemandDataSource.Clear();

            _DemandDataSource = null;

            _SalesProgramDataSource.Clear();

            _SalesProgramDataSource = null;

            base.BaseDispose();
        }
    }
}
