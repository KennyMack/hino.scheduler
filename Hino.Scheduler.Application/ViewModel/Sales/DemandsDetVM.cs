﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.Services.Sales;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Sales
{
    public class DemandsDetVM: BaseVM, IDisposable
    {
        public BindingList<veDemandaDet> _DemandaDetDataSource;
        public BindingList<veDemandaOP> _DemandaDetOPDataSource;
        public long _IdScenario;

        public DemandsDetVM()
        {
            _IdScenario = 0;
            _DemandaDetDataSource = new BindingList<veDemandaDet>();
            _DemandaDetOPDataSource = new BindingList<veDemandaOP>();
        }

        public async Task LoadDataSource()
        {
            using (var _DemandaDetRepository = new DemandaDetRepository())
            using (var _DemandaDetService = new DemandaDetService(_DemandaDetRepository))
                _DemandaDetDataSource = new BindingList<veDemandaDet>(
                    (await _DemandaDetRepository.QueryAsync(r => r.IdLoadData == _IdScenario)).ToList());
        }

        public async Task LoadDemandaDetOP()
        {
            using (var _DemandaDetRepository = new DemandaDetRepository())
            using (var _DemandaDetService = new DemandaDetService(_DemandaDetRepository))
                _DemandaDetOPDataSource = new BindingList<veDemandaOP>(
                    (await _DemandaDetRepository.QueryDemandaOP(Parameters.codEstab, _IdScenario)).ToList());
        }

        public void Dispose()
        {
            _DemandaDetDataSource.Clear();
            _DemandaDetOPDataSource.Clear();

            _DemandaDetDataSource = null;
            _DemandaDetOPDataSource = null;

            base.BaseDispose();
        }
    }
}

