﻿using Hino.Scheduler.Application.Services.Gerais;
using Hino.Scheduler.Data.Repositories.Gerais;
using Hino.Scheduler.Model.Gerais;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Gerais
{
    public class UserDataVM : BaseVM, IDisposable
    {
        public geUserData _geUserData;

        public UserDataVM()
        {
            _geUserData = new geUserData();
        }

        public async override Task LoadData()
        {
            using (var _UserDataRepository = new UserDataRepository())
            using (var _UserDataService = new UserDataService(_UserDataRepository))
            {
                _geUserData = await _UserDataService.GetByCodUsuarioAsync(Parameters.UserLogged);
                if (_geUserData == null)
                {
                    _geUserData = new geUserData
                    {
                        CodUsuario = Parameters.UserLogged
                    };
                    _UserDataService.Add(_geUserData);
                    await _UserDataService.SaveChangesAsync();
                }
            }
        }

        public async Task UpdateUserData()
        {
            using (var _UserDataRepository = new UserDataRepository())
            using (var _UserDataService = new UserDataService(_UserDataRepository))
            {
                try
                {
                    _geUserData.IdLoadData = Parameters.IdScenario;
                    _UserDataService.Update(_geUserData);
                    await _UserDataService.SaveChangesAsync();
                }
                catch (Exception)
                {
                }
            }
        }

        public void Dispose()
        {
            _geUserData = null;

            base.BaseDispose();
        }
    }
}

