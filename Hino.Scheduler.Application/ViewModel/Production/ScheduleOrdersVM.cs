﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class ScheduleOrdersVM : BaseVM, IDisposable
    {
        private readonly IJobSchedulerRepository _JobSchedulerRepository;
        public readonly IJobSchedulerService _JobSchedulerService;
        public BindingList<pdJob> _DataSource;

        public ScheduleOrdersVM()
        {
            _DataSource = new BindingList<pdJob>();
            _JobSchedulerRepository = new JobSchedulerRepository();
            _JobSchedulerService = new JobSchedulerService(_JobSchedulerRepository);
        }

        private async Task GeneratedJobSchedule(bool pClearData)
        {
            await _JobSchedulerService.GeneratedJobSchedule(Parameters.codEstab, false);
            Errors = _JobSchedulerService.Errors;
        }

        private async Task LoadDataSource()
        {
            _DataSource = null;
            _DataSource = new BindingList<pdJob>(
                 (await _JobSchedulerService.GetByEstabIdAsync(Parameters.codEstab)).ToList());
        }

        public async override Task LoadData()
        {
            Errors.Clear();
            await GeneratedJobSchedule(false);
            if (Errors.Count <= 0)
                await LoadDataSource();
        }

        public async Task UpdateScheduledOrders()
        {
            Errors.Clear();
            await GeneratedJobSchedule(true);
            if (Errors.Count <= 0)
                await LoadDataSource();

        }

        public void Dispose()
        {
            _DataSource.Clear();
            _DataSource = null;
            _JobSchedulerService.Dispose();
            _JobSchedulerRepository.Dispose();

            base.BaseDispose();
        }
    }
}
