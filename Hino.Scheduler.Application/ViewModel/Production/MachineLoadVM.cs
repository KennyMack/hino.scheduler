﻿
using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class MachineLoadVM : BaseVM, IDisposable
    {
        public long _IdScenario { get; set; }
        public DateTime _DtInitial { get; set; }
        public DateTime _DtEnd { get; set; }

        public BindingList<pdMachineLoad> _MachineLoadDataSource;
        public BindingList<pdMachineLoad> _MachineLoadSimulatedDataSource;
        public BindingList<pdOrdemProdDet> _DetailOpMachineLoadSimulatedDataSource;
        public BindingList<pdOrdemProdDet> _DetailOpMachineLoadDataSource;
        public BindingList<pdJobRot> _OperationBottleneckDataSource;

        public MachineLoadVM()
        {
            _IdScenario = 0;
            _MachineLoadDataSource = new BindingList<pdMachineLoad>();
            _MachineLoadSimulatedDataSource = new BindingList<pdMachineLoad>();
            _DetailOpMachineLoadSimulatedDataSource = new BindingList<pdOrdemProdDet>();
            _DetailOpMachineLoadDataSource = new BindingList<pdOrdemProdDet>();
        }

        public async Task<bool> LoadMachineLoad()
        {
            using (var _MaquinasRepository = new MaquinasRepository())
            using (var _MaquinasService = new MaquinasService(_MaquinasRepository))
            {
                Errors.Clear();
                _MaquinasService.Errors.Clear();

                if (await _MaquinasService.ExecLoadCargaMaquinaAsync(Parameters.codEstab, _DtInitial, _DtEnd, Parameters.UserLogged))
                {
                    _MachineLoadDataSource = new BindingList<pdMachineLoad>(
                    await _MaquinasService.QueryMachineLoad(Parameters.codEstab,
                        _DtInitial, _DtEnd, Parameters.UserLogged));
                }
                if (_MaquinasService.Errors.Any())
                {
                    Errors = _MaquinasService.Errors;
                    return false;
                }
            }
            return true;
        }

        public async Task<bool> LoadMachineLoadSimulated()
        {
            using (var _MaquinasRepository = new MaquinasRepository())
            using (var _MaquinasService = new MaquinasService(_MaquinasRepository))
            {
                Errors.Clear();
                _MaquinasService.Errors.Clear();

                if (await _MaquinasService.ExecLoadSimulCargaMaquinaAsync(Parameters.codEstab, _IdScenario, Parameters.UserLogged))
                {
                    _MachineLoadSimulatedDataSource = new BindingList<pdMachineLoad>(
                        await _MaquinasService.QueryMachineLoadSimulated(Parameters.codEstab, _IdScenario, Parameters.UserLogged));
                }
                if (_MaquinasService.Errors.Any())
                {
                    Errors = _MaquinasService.Errors;
                    return false;
                }
            }
            return true;
        }

        public async Task LoadDetailOpMachine(string pCodMaquina, DateTime pDtProducao)
        {
            using (var _MaquinasRepository = new JobSchedulerRepository())
            using (var _MaquinasService = new JobSchedulerService(_MaquinasRepository))
            {
                _DetailOpMachineLoadDataSource = new BindingList<pdOrdemProdDet>(
                    await _MaquinasService.DetailMachineLoad(Parameters.codEstab, pCodMaquina, pDtProducao, Parameters.UserLogged));
            }
        }

        public async Task LoadDetailOpMachineLoadSimulated(string pCodMaquina, DateTime pDtProducao)
        {
            using (var _MaquinasRepository = new JobSchedulerRepository())
            using (var _MaquinasService = new JobSchedulerService(_MaquinasRepository))
            {
                _DetailOpMachineLoadSimulatedDataSource = new BindingList<pdOrdemProdDet>(
                    await _MaquinasService.DetailMachineLoadSimulated(Parameters.codEstab, _IdScenario, pCodMaquina, pDtProducao, Parameters.UserLogged));
            }
        }

        public async Task LoadOperationBottleneck()
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                _OperationBottleneckDataSource = new BindingList<pdJobRot>(
                    await _JobRotSchedulerService.QueryOperationBottleneck(Parameters.codEstab, _IdScenario));
            }
        }

        public void Dispose()
        {
            _MachineLoadDataSource.Clear();
            _MachineLoadDataSource = null;

            _MachineLoadSimulatedDataSource.Clear();
            _MachineLoadSimulatedDataSource = null;

            _DetailOpMachineLoadSimulatedDataSource.Clear();
            _DetailOpMachineLoadSimulatedDataSource = null;

            _DetailOpMachineLoadDataSource.Clear();
            _DetailOpMachineLoadDataSource = null;

            _OperationBottleneckDataSource.Clear();
            _OperationBottleneckDataSource = null;

            base.BaseDispose();
        }
    }
}
