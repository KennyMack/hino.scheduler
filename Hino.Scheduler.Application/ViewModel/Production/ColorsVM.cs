﻿using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class ColorsVM : BaseVM, IDisposable
    {
        public bool _HasToReload { get; private set; }

        public async Task GenerateOPColors(
            IList<pdJob> pJobs)
        {
            _HasToReload = false;
            if (pJobs.Where(r => string.IsNullOrEmpty(r.corop)).Count() > 0)
            {
                _HasToReload = true;
                var jobsList = pJobs.GroupBy(r => r.codordprod)
                    .Select(r => r.Key).ToList();

                BaseColors.GenerateRandomHSV(jobsList.Count);
                var JobColors = BaseColors.JobColors;

                using (var _OPColorsRepositoy = new OPColorsRepository())
                using (var _OPColorsService = new OPColorsService(_OPColorsRepositoy))
                {
                    await _OPColorsService.ClearColors();

                    for (int i = 0, length = JobColors.Length; i < length; i++)
                    {
                        var CorId = await _OPColorsService.NextSequenceAsync();
                        _OPColorsService.Add(new pdOPColors
                        {
                            codestab = Parameters.codEstab,
                            codordprod = jobsList[i],
                            codcor = Convert.ToInt32(CorId),
                            corop = ColorTranslator.ToHtml(JobColors[i])
                        });
                    }

                    await _OPColorsService.SaveChangesAsync();
                    Errors = _OPColorsService.Errors;

                    await _OPColorsService.AttachColorToOP(Parameters.codEstab);
                }
            }
        }

        public async Task GenerateOPRotColors(
            IList<pdJobRot> pJobsRot)
        {
            _HasToReload = false;
            if (pJobsRot.Where(r => string.IsNullOrEmpty(r.corop)).Count() > 0)
            {

                _HasToReload = true;
                var jobsList = pJobsRot.GroupBy(r => r.codordprod)
                    .Select(r => r.Key).ToList();

                BaseColors.GenerateRandomHSV(jobsList.Count);
                var JobColors = BaseColors.JobColors;

                using (var _OPColorsRepositoy = new OPColorsRepository())
                using (var _OPColorsService = new OPColorsService(_OPColorsRepositoy))
                {  
                    await _OPColorsService.ClearColors();

                    for (int i = 0, length = JobColors.Length; i < length; i++)
                    {
                        var CorId = await _OPColorsService.NextSequenceAsync();
                        _OPColorsService.Add(new pdOPColors
                        {
                            codestab = Parameters.codEstab,
                            codordprod = Convert.ToInt32(jobsList[i]),
                            codcor = Convert.ToInt32(CorId),
                            corop = ColorTranslator.ToHtml(JobColors[i])
                        });
                    }

                    await _OPColorsService.SaveChangesAsync();
                    Errors = _OPColorsService.Errors;

                    await _OPColorsService.AttachColorToOPRot(Parameters.codEstab);
                }
            }
        }

        public async Task GenerateOPRotColors(
            IList<pdSimulJobsRot> pJobsRot)
        {
            _HasToReload = false;
            if (pJobsRot.Where(r => string.IsNullOrEmpty(r.corop)).Count() > 0)
            {
                _HasToReload = true;
                var jobsList = pJobsRot.GroupBy(r => r.codordprod)
                    .Select(r => r.Key).ToList();

                BaseColors.GenerateRandomHSV(jobsList.Count);
                var JobColors = BaseColors.JobColors;

                using (var _OPColorsRepositoy = new OPColorsRepository())
                using (var _OPColorsService = new OPColorsService(_OPColorsRepositoy))
                {
                    await _OPColorsService.ClearColors();

                    for (int i = 0, length = JobColors.Length; i < length; i++)
                    {
                        var CorId = await _OPColorsService.NextSequenceAsync();
                        _OPColorsService.Add(new pdOPColors
                        {
                            codestab = Parameters.codEstab,
                            codordprod = Convert.ToInt32(jobsList[i]),
                            codcor = Convert.ToInt32(CorId),
                            corop = ColorTranslator.ToHtml(JobColors[i])
                        });
                    }

                    await _OPColorsService.SaveChangesAsync();
                    Errors = _OPColorsService.Errors;

                    await _OPColorsService.AttachColorToOPRot(Parameters.codEstab);
                }
            }
        }

        public void Dispose()
        {
            base.BaseDispose();
        }
    }
}
