﻿using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.Services.Sales;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class ScheduledOrdersRotViewVM : BaseVM, IDisposable
    {
        public List<pdLoadData> _Scenarios;
        public long _IdScenario { get; set; }
        public BindingList<pdResource> _ResourceDataSource;
        public BindingList<pdJobRot> _JobRotDataSource;
        public BindingList<veDemanda> _DemandasDataSource;

        public ScheduledOrdersRotViewVM()
        {
            _DemandasDataSource = new BindingList<veDemanda>();
            _ResourceDataSource = new BindingList<pdResource>();
            _JobRotDataSource = new BindingList<pdJobRot>();
        }

        public async Task GeneratedJobSchedule(bool pClearData, bool pSimulate)
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                Errors.Clear();
                
                var jobs = new Task[]
                {
                    LoadResources(_ResourceService, _IdScenario),
                    LoadJobs(_JobRotSchedulerService, _IdScenario)
                };

                await Task.WhenAll(jobs);

                Errors = _JobRotSchedulerService.Errors;
            }
        }

        public async Task LoadScenario()
        {
            using (var _LoadDataRepository = new LoadDataRepository())
            using (var _LoadDataService = new LoadDataService(_LoadDataRepository))
                _Scenarios = new List<pdLoadData>(await _LoadDataService.GetAllAsync());
        }

        private async Task LoadResources(IResourceService _ResourceService, long pIdLoadData)
        {
            _ResourceDataSource = null;
            _ResourceDataSource = new BindingList<pdResource>(
                 (await _ResourceService.GetResourcesByType(1, pIdLoadData)).ToList());
        }

        private async Task LoadJobs(IJobRotSchedulerService _JobRotSchedulerService, long pIdLoadData)
        {
            _JobRotDataSource = null;
            _JobRotDataSource = new BindingList<pdJobRot>(
                 (await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, pIdLoadData)).ToList());
        }


        #region Update has op
        private void UpdateHasOP()
        {
            foreach (var item in _ResourceDataSource)
                item.temops = (_JobRotDataSource.Any(r => r.resourceid == item.idresource)) ? 1 : 0;
            /*
            var noOps = _ResourceDataSource.Where(r => r.temops <= 0).ToArray();

            foreach (var item in noOps)
            {
                _ResourceDataSource.Remove(
                    _ResourceDataSource.First(r => r.idresource == item.idresource));
            }*/
            
                    

        }
        #endregion

        public override async Task LoadData()
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {

                Errors.Clear();

                var jobs = new Task[]
                {
                    LoadJobs(_JobRotSchedulerService, _IdScenario),
                    LoadResources(_ResourceService, _IdScenario)
                };
                await _ResourceService.AttachResourceToOPRot(Parameters.codEstab, _IdScenario);

                UpdateHasOP();
            }
        }

        public async Task SaveChangesAppointments(IList<Appointment> pAppointments)
        {
            using (var _DemandaRepository = new DemandaRepository())
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            using (var _DemandaServiceService = new DemandaService(_DemandaRepository))
            {
                long codOP = 0;
                var NivelOP = "";
                foreach (var item in pAppointments)
                {
                    codOP = Convert.ToInt64(item.CustomFields["Codordprod"].ToString());
                    NivelOP = item.CustomFields["Nivelordprod"].ToString();

                    var Productions = _JobRotDataSource.Where(r => r.codordprod == codOP &&
                                                                  r.nivelordprod == NivelOP);

                    var Appointment = _JobRotDataSource.Where(r => r.idjob == Convert.ToInt32(item.CustomFields["Idjob"].ToString()))
                        .FirstOrDefault();
                    if (Appointment != null)
                    {
                        Appointment.dtiniciooper = item.Start;
                        Appointment.dttermoper = item.End;
                        Appointment.labelop = (int)item.LabelKey;
                        Appointment.statusop = (int)item.StatusKey;
                        Appointment.resourceid = (long)item.ResourceId;
                        Appointment.codmaquina = _ResourceDataSource.First(r => r.idresource == Appointment.resourceid &&
                                                                                r.IdLoadData == _IdScenario).codmaquina;
                        Appointment.datafimop = Productions.Max(r => r.dttermoper);
                        Appointment.datainiop = Productions.Min(r => r.dtiniciooper);

                        Appointment.alterado = 1;
                        _JobRotSchedulerService.Update(Appointment);
                    }
                }
                await _JobRotSchedulerService.SaveChangesAsync();

                await _DemandaServiceService.AtualizaOPDemandaAsync(Parameters.codEstab, _IdScenario);
                UpdateHasOP();
            }
        }

        public async Task<bool> SaveOPChangesToERP()
        {
            using (var _DemandaRepository = new DemandaRepository())
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            using (var _DemandaServiceService = new DemandaService(_DemandaRepository))
            {
                Errors.Clear();
                _JobRotSchedulerService.Errors.Clear();

                await _JobRotSchedulerService.SaveOPChangesToERP(Parameters.codEstab, _IdScenario);
                if (_JobRotSchedulerService.Errors.Count > 0)
                {
                    Errors = _JobRotSchedulerService.Errors;
                    return false;
                }

                await _DemandaServiceService.AtualizaOPDemandaAsync(Parameters.codEstab, _IdScenario);
            }

            return true;
        }

        public void Dispose()
        {
            _JobRotDataSource.Clear();
            _JobRotDataSource = null;

            _ResourceDataSource.Clear();
            _ResourceDataSource = null;

            _DemandasDataSource.Clear();
            _DemandasDataSource = null;

            _Scenarios.Clear();
            _Scenarios = null;

            base.BaseDispose();
        }
    }
}
