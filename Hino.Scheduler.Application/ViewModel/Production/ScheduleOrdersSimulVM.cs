﻿using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class ScheduleOrdersSimulVM : BaseVM, IDisposable
    {
        public int _TimeParts { get; set; }
        public DateTime _DtIntial { get; set; }
        public List<pdLoadData> _Scenarios;
        public long _IdScenario { get; set; }
        public BindingList<pdResource> _ResourceDataSource;
        public BindingList<pdSimulJobsRot> _JobRotDataSource;
        public BindingList<veDemanda> _DemandasDataSource;

        public ScheduleOrdersSimulVM()
        {
            _IdScenario = 0;
            _DemandasDataSource = new BindingList<veDemanda>();
            _ResourceDataSource = new BindingList<pdResource>();
            _JobRotDataSource = new BindingList<pdSimulJobsRot>();

        }

        public async Task LoadScenario()
        {
            using (var _LoadDataRepository = new LoadDataRepository())
            using (var _LoadDataService = new LoadDataService(_LoadDataRepository))
                _Scenarios = new List<pdLoadData>(await _LoadDataService.GetAllAsync());
        }

        private async Task LoadResources(IResourceService _ResourceService, long pIdLoadData)
        {
            _ResourceDataSource = null;
            _ResourceDataSource = new BindingList<pdResource>(
                 (await _ResourceService.GetResourcesByType(1, pIdLoadData)).ToList());
        }

        private async Task LoadJobs(ISimulJobsRotService _JobRotSchedulerService, long pIdLoadData)
        {
            _JobRotDataSource = null;
            _JobRotDataSource = new BindingList<pdSimulJobsRot>(
                 (await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, pIdLoadData)).ToList());
        }

        #region Update has op
        private void UpdateHasOP()
        {
            foreach (var item in _ResourceDataSource)
                item.temops = (_JobRotDataSource.Any(r => r.resourceid == item.idresource)) ? 1 : 0;
        }
        #endregion

        public override async Task LoadData()
        {
            using (var _JobRotSchedulerRepository = new SimulJobsRotRepository())
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new SimulJobsRotService(_JobRotSchedulerRepository))
            {

                Errors.Clear();

                var jobs = new Task[]
                {
                    LoadJobs(_JobRotSchedulerService, _IdScenario),
                    LoadResources(_ResourceService, _IdScenario)
                };

                await Task.WhenAll(jobs);

                await _ResourceService.AttachResourceToOPRot(Parameters.codEstab, _IdScenario);

                UpdateHasOP();
            }
        }

        public async Task<bool> SaveOPChangesToERP()
        {
            using (var _JobRotSchedulerRepository = new SimulJobsRotRepository())
            using (var _JobRotSchedulerService = new SimulJobsRotService(_JobRotSchedulerRepository))
            {
                Errors.Clear();
                _JobRotSchedulerService.Errors.Clear();

                await _JobRotSchedulerService.SaveOPChangesToERP(Parameters.codEstab, _IdScenario);
                if (_JobRotSchedulerService.Errors.Count > 0)
                {
                    Errors = _JobRotSchedulerService.Errors;
                    return false;
                }
            }

            return true;
        }

        public async Task SaveChangesAppointments(IList<Appointment> pAppointments)
        {
            //using (var _DemandaRepository = new DemandaRepository())
            using (var _JobRotSchedulerRepository = new SimulJobsRotRepository())
            using (var _JobRotSchedulerService = new SimulJobsRotService(_JobRotSchedulerRepository))
            //using (var _DemandaServiceService = new DemandaService(_DemandaRepository))
            {
                foreach (var item in pAppointments)
                {
                    var Appointment = _JobRotDataSource.Where(r => r.idjobsimul == Convert.ToInt32(item.CustomFields["Idjobsimul"].ToString()))
                        .FirstOrDefault();
                    if (Appointment != null)
                    {
                        Appointment.dtiniciooper = item.Start;
                        Appointment.dttermoper = item.End;
                        Appointment.labelop = (int)item.LabelKey;
                        Appointment.statusop = (int)item.StatusKey;
                        Appointment.resourceid = (long)item.ResourceId;
                        Appointment.codmaquina = _ResourceDataSource.First(r => r.idresource == Appointment.resourceid &&
                                                                                r.IdLoadData == _IdScenario).codmaquina;
                        Appointment.alterado = 1;
                        _JobRotSchedulerService.Update(Appointment);
                    }
                }
                await _JobRotSchedulerService.SaveChangesAsync();

                UpdateHasOP();
                //await _DemandaServiceService.AtualizaOPDemandaAsync(Parameters.codEstab, _IdScenario);
            }
        }

        public async Task GeneratedJobSchedule()
        {
            using (var _JobRotSchedulerRepository = new SimulJobsRotRepository())
            using (var _JobRotSchedulerService = new SimulJobsRotService(_JobRotSchedulerRepository))
            {
                Errors.Clear();

                await _JobRotSchedulerService.GeneratedJobSchedule(Parameters.codEstab, _IdScenario, _DtIntial, _TimeParts);

                await LoadData();

                /*
                var jobs = new Task[]
                {
                    LoadResources(_ResourceService, _IdScenario),
                    LoadJobs(_JobRotSchedulerService, _IdScenario)
                };

                await Task.WhenAll(jobs);*/

                Errors = _JobRotSchedulerService.Errors;
            }
        }

        public void Dispose()
        {
            _ResourceDataSource = null;
            _ResourceDataSource.Clear();

            _JobRotDataSource = null;
            _JobRotDataSource.Clear();

            _DemandasDataSource = null;
            _DemandasDataSource.Clear();

            _Scenarios = null;
            _Scenarios.Clear();

            base.BaseDispose();
        }
    }
}
