﻿using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.Services.Sales;
using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class ScheduledOrdersViewVM : BaseVM, IDisposable
    {
        public List<pdLoadData> _Scenarios;
        public DateTime _DtInitial { get; set; }
        public DateTime _DtEnd { get; set; }
        public int _TimeParts { get; set; }
        public long _IdScenario { get; set; }
        public BindingList<pdResource> _ResourceDataSource;
        public BindingList<pdJob> _JobDataSource;
        public DataTable _JobShopDataTable;
        public List<ColorOP> _OPColors;

        public ScheduledOrdersViewVM()
        {
            _IdScenario = -1;
            _Scenarios = new List<pdLoadData>();
            _OPColors = new List<ColorOP>();
            _JobShopDataTable = new DataTable();
            _ResourceDataSource = new BindingList<pdResource>();
            _JobDataSource = new BindingList<pdJob>();
        }

        [Obsolete("Criado o sistema de fazer cenários", true)]
        public async Task GeneratedJobSchedule(bool pClearData)
        {
            using (var _JobSchedulerRepository = new JobSchedulerRepository())
            using (var _JobSchedulerService = new JobSchedulerService(_JobSchedulerRepository))
            {
                Errors.Clear();
                await _JobSchedulerService.GeneratedJobSchedule(Parameters.codEstab, false);
                Errors = _JobSchedulerService.Errors;
            }
        }

        public async Task GeneratedJobShopSchedule(bool pClearData)
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                Errors.Clear();
                await _JobRotSchedulerService.GeneratedJobRotSchedule(Parameters.codEstab, _DtInitial, _TimeParts, false, false);
                Errors = _JobRotSchedulerService.Errors;

                await _JobRotSchedulerService.GenerateJobShop(Parameters.codEstab, _DtInitial, _TimeParts);
            }
        }

        public async Task LoadDataJobShop()
        {
            using (var _JobSchedulerRepository = new JobSchedulerRepository())
            using (var _TempoJobPartRepository = new TempoJobPartRepository())
            using (var _TempoJobPartService = new TempoJobPartService(_TempoJobPartRepository))
            using (var _JobSchedulerService = new JobSchedulerService(_JobSchedulerRepository))
            {
                Errors.Clear();

                DataTable dt = new DataTable();
                try
                {

                    dt = (await _TempoJobPartService.QueryAsync(r =>
                       r.codestab == Parameters.codEstab)).ToList().ToDataTable();
                }
                catch (Exception)
                {
                    
                }

                _JobShopDataTable = new DataTable();
                _JobShopDataTable.Columns.Add("CODMAQUINA", typeof(string));

                dt.Select("1=1").ToList()
                    .Select(r => new { TEMPOPART = Convert.ToInt32(r["TEMPOPART"].ToString()) })
                    .GroupBy(s => s.TEMPOPART)
                    .Select(t => new { TEMPOPART = t.Key })
                    .OrderBy(o => o.TEMPOPART)
                    .ToList()
                    .ForEach(r =>
                    {
                        _JobShopDataTable.Columns.Add("TEMPOPART" + r.TEMPOPART, typeof(string));
                    });

                dt.Select("1=1").ToList()
                    .Select(r => new { CODMAQUINA = r["CODMAQUINA"].ToString() })
                    .GroupBy(s => s.CODMAQUINA)
                    .Select(t => new { CODMAQUINA = t.Key })
                    .ToList()
                    .ForEach(r =>
                    {
                        var row = _JobShopDataTable.NewRow();
                        row["CODMAQUINA"] = r.CODMAQUINA;
                        _JobShopDataTable.Rows.Add(row);
                    });

                for (var i = 1; i < _JobShopDataTable.Columns.Count; i++)
                {
                    var col = _JobShopDataTable.Columns[i].ColumnName;
                    if (col != "CODMAQUINA")
                    {
                        var TEMPOPART = Convert.ToInt32(col.Replace("TEMPOPART", " "));
                        dt.Select(string.Format("TEMPOPART = {0}", TEMPOPART)).ToList()
                            .ForEach(r =>
                            {
                                _JobShopDataTable.Select(string.Format("CODMAQUINA='{0}'", r["CODMAQUINA"]))
                                    .ToList()
                                    .ForEach(t =>
                                    {
                                        t[col] = r["TEMPOALOC"].ToString();
                                    });
                            });
                    }
                }

                _OPColors.Clear();

                var qtd = dt.Select("TEMPOALOC IS NOT NULL AND TEMPOALOC <> 'SABADO' AND TEMPOALOC <> 'DOMINGO'").ToList()
                    .Select(r => new { CODORDPROD = r["CODORDPROD"].ToString() })
                    .GroupBy(s => s.CODORDPROD)
                    .Select(t => new { CODORDPROD = t.Key })
                    .Count();

                BaseColors.GenerateRandomHSV(qtd);

                var JobColors = BaseColors.JobColors;

                var cor = 0;
                dt.Select("TEMPOALOC IS NOT NULL AND TEMPOALOC <> 'SABADO' AND TEMPOALOC <> 'DOMINGO'").ToList()
                    .Select(r => new { CODORDPROD = r["CODORDPROD"].ToString() })
                    .GroupBy(s => s.CODORDPROD)
                    .Select(t => new { CODORDPROD = t.Key })
                    .ToList()
                    .ForEach(r =>
                    {
                        if (!string.IsNullOrEmpty(r.CODORDPROD))
                        _OPColors.Add(new ColorOP
                        {
                            Cor = JobColors[cor],
                            OP = Convert.ToInt32(r.CODORDPROD)
                        });
                        cor++;
                    });
            }
        }

        private async Task LoadResources(IResourceService _ResourceService, long pIdLoadData)
        {
            _ResourceDataSource = null;
            _ResourceDataSource = new BindingList<pdResource>(
                 (await _ResourceService.GetResourcesByType(0, pIdLoadData)).ToList());
        }

        private async Task LoadJobsRot(IJobRotSchedulerService _JobRotSchedulerService, long pIdLoadData)
        {
            _JobDataSource = null;

            var lst = await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, pIdLoadData);

            var lstJob = lst.Select(r => new pdJob
            {
                idjob =  Convert.ToInt64($"{r.codordprod}{r.nivelordprod.Replace(".", "")}"),
                codestab = r.codestab,
                codordprod = r.codordprod,
                codestrutura = r.codestrutura,
                nivelordprod = r.nivelordprod,
                nivelordprodpai = r.nivelordprodpai,
                codproduto = r.codproduto,
                codroteiro = r.codroteiro,
                codprograma = r.codprograma,
                datainiop = r.datainiop,
                datafimop = r.datafimop,
                minimaltime = lst.Where(s => s.codordprod == r.codordprod && s.nivelordprod == r.nivelordprod).Sum(t => t.horasprodnec), // r.dttermoper - r.dtiniciooper,
                qtdprog = r.qtdprog,
                qtdapont = r.qtdapont,
                qtdrefugo = r.qtdrefugo,
                percconcluido = r.percconcluido,
                labelop = r.labelop,
                statusop = r.statusop,
                resourceid = r.resourceid,
                codmaquina = r.codmaquina,
                operacao = r.operacao,
                descricao = r.descricao,
                corop = r.corop,
                alterado = r.alterado,
                IdLoadData = r.IdLoadData
            });

            var lstjobDist = lstJob.Distinct().ToList();

            _JobDataSource = new BindingList<pdJob>(lstjobDist);
        }

        public async Task LoadScenario()
        {
            using (var _LoadDataRepository = new LoadDataRepository())
            using (var _LoadDataService = new LoadDataService(_LoadDataRepository))
                _Scenarios = new List<pdLoadData>(await _LoadDataService.GetAllAsync());
        }

        public override async Task LoadData()
        {
            using (var _ResourceRepository = new ResourceRepository())
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                Errors.Clear();

                var jobs = new Task[]
                {
                    LoadResources(_ResourceService, _IdScenario),
                    LoadJobsRot(_JobRotSchedulerService, _IdScenario)
                };

                await Task.WhenAll(jobs);

                var colors = new ColorsVM();
                await colors.GenerateOPColors(_JobDataSource);

                if (colors._HasToReload)
                    await LoadJobsRot(_JobRotSchedulerService, _IdScenario);
            }
        }

        public async Task SaveChangesAppointments(IList<Appointment> pAppointments)
        {
            try
            {
                using (var _DemandaRepository = new DemandaRepository())
                using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
                using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
                using (var _DemandaServiceService = new DemandaService(_DemandaRepository))
                {
                    long codOP = 0;
                    var NivelOP = "";
                    foreach (var item in pAppointments)
                    {
                        codOP = Convert.ToInt64(item.CustomFields["Codordprod"].ToString());
                        NivelOP = item.CustomFields["Nivelordprod"].ToString();

                        var jobsRot = (await _JobRotSchedulerService.QueryAsync(
                            r => r.codordprod == codOP &&
                                 r.nivelordprod == NivelOP &&
                                 r.IdLoadData == _IdScenario));
                        var JobOrigin = (await _JobRotSchedulerService.QueryAsync(
                            r => r.codordprod == codOP &&
                                 r.nivelordprod == NivelOP)).FirstOrDefault();

                        var dtIniOrigin = JobOrigin.datainiop;
                        var dtFimOrigin = JobOrigin.datafimop;
                        foreach (var job in jobsRot)
                        {
                            job.datainiop = item.Start;
                            job.datafimop = item.End;

                            var Interval = Math.Abs((job.datainiop - dtIniOrigin).TotalSeconds);
                            if (job.datainiop < dtIniOrigin)
                                Interval = Interval * (-1);
                            job.dtiniciooper = job.dtiniciooper.AddSeconds(Interval);

                            Interval = Math.Abs((job.datafimop - dtFimOrigin).TotalSeconds);
                            if (job.datafimop < dtFimOrigin)
                                Interval = Interval * (-1);
                            job.dttermoper = job.dttermoper.AddSeconds(Interval);


                            ///job.dtiniciooper = (dtIniOrigin - job.datainiop).

                            job.labelop = (int)item.LabelKey;
                            job.statusop = (int)item.StatusKey;
                            job.alterado = 1;
                            _JobRotSchedulerService.Update(job);
                        }

                    }
                    await _JobRotSchedulerService.SaveChangesAsync();

                    await _DemandaServiceService.AtualizaOPDemandaAsync(Parameters.codEstab, _IdScenario);
                }
            }
            catch (Exception)
            {
                
            }
        }

        public async Task<bool> SaveOPChangesToERP()
        {
            using (var _DemandaRepository = new DemandaRepository())
            using (var _JobSchedulerRepository = new JobSchedulerRepository())
            using (var _JobSchedulerService = new JobSchedulerService(_JobSchedulerRepository))
            using (var _DemandaServiceService = new DemandaService(_DemandaRepository))
            {
                Errors.Clear();
                _JobSchedulerService.Errors.Clear();

                await _JobSchedulerService.SaveOPChangesToERP(Parameters.codEstab, _IdScenario);
                if (_JobSchedulerService.Errors.Count > 0)
                {
                    Errors = _JobSchedulerService.Errors;
                    return false;
                }
                await _DemandaServiceService.AtualizaOPDemandaAsync(Parameters.codEstab, _IdScenario);
            }

            return true;
        }

        public void Dispose()
        {
            _Scenarios.Clear();
            _Scenarios = null;

            _JobDataSource.Clear();
            _JobDataSource = null;

            _ResourceDataSource.Clear();
            _ResourceDataSource = null;

            _OPColors.Clear();
            _OPColors = null;

            base.BaseDispose();
        }
    }
}
