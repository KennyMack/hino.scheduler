﻿using DevExpress.XtraScheduler;
using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Application.Services.Interfaces.Engenharia;
using Hino.Scheduler.Application.Services.Interfaces.Production;
using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Production
{
    public class RoutinesSequenceVM : BaseVM, IDisposable
    {
        public List<pdLoadData> _Scenarios;
        public long _IdScenario { get; set; }
        public BindingList<enRotinas> _RoutineDataSource;
        public BindingList<pdJobRot> _JobRotDataSource;

        public RoutinesSequenceVM()
        {
            _IdScenario = 0;
            _RoutineDataSource = new BindingList<enRotinas>();
            _JobRotDataSource = new BindingList<pdJobRot>();
        }

        public async Task GeneratedJobRoutines(bool pClearData)
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                Errors.Clear();

                await Task.WhenAll(new Task[]
                {
                    _JobRotSchedulerService.GeneratedJobRoutines(Parameters.codEstab, pClearData),
                    _ResourceService.GeneratedResources(Parameters.codEstab, pClearData)
                });

                Errors = _JobRotSchedulerService.Errors;
            }
        }

        public async Task LoadScenario()
        {
            using (var _LoadDataRepository = new LoadDataRepository())
            using (var _LoadDataService = new LoadDataService(_LoadDataRepository))
                _Scenarios = new List<pdLoadData>(await _LoadDataService.GetAllAsync());
        }

        private async Task LoadRoutines(IenRotinasService _ResourceService)
        {
            _RoutineDataSource = null;
            _RoutineDataSource = new BindingList<enRotinas>(
                 (await _ResourceService.GetAllAsync()).ToList());

            foreach (var item in _RoutineDataSource)
            {
                if (_JobRotDataSource.Any(r => r.codrotina == item.codrotina))
                    item.temops = 1;

            }
        }

        private async Task LoadJobs(IJobRotSchedulerService _JobRotSchedulerService)
        {
            _JobRotDataSource = null;
            _JobRotDataSource = new BindingList<pdJobRot>(
                 (await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, _IdScenario)).ToList());
        }

        public override async Task LoadData()
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _enRotinasRepository = new enRotinasRepository())
            using (var _RoutinesService = new enRotinasService(_enRotinasRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {

                Errors.Clear();

                await LoadJobs(_JobRotSchedulerService);
                await LoadRoutines(_RoutinesService);
                
            }
        }

        public async Task SaveChangesAppointmentsRoutines(IList<Appointment> pAppointments)
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                foreach (var item in pAppointments)
                {
                    var Appointment = _JobRotDataSource.Where(r => r.idjob == Convert.ToInt32(item.CustomFields["Idjob"].ToString()))
                        .FirstOrDefault();
                    if (Appointment != null)
                    {
                        var dtInicio = _JobRotDataSource.Where(r =>
                            r.codordprod == Convert.ToInt64(item.CustomFields["Codordprod"]) &&
                            r.nivelordprod == item.CustomFields["Nivelordprod"].ToString()
                        ).Min(r=> r.datainiop);

                        var dtFim = _JobRotDataSource.Where(r =>
                            r.codordprod == Convert.ToInt64(item.CustomFields["Codordprod"]) &&
                            r.nivelordprod == item.CustomFields["Nivelordprod"].ToString()
                        ).Min(r => r.datainiop);

                        if (item.Start < dtInicio)
                            Appointment.datainiop = item.Start;

                        if (item.End > dtFim)
                            Appointment.datafimop = item.End;

                        Appointment.dtiniciooper = item.Start;
                        Appointment.dttermoper = item.End;
                        Appointment.labelop = (int)item.LabelKey;
                        Appointment.statusop = (int)item.StatusKey;
                        Appointment.alterado = 1;
                        _JobRotSchedulerService.Update(Appointment);
                    }
                }
                await _JobRotSchedulerService.SaveChangesAsync();
            }
        }

        public void Dispose()
        {

            base.BaseDispose();
        }
    }
}
