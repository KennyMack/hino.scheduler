﻿using Hino.Scheduler.Application.Services.Engenharia;
using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Engenharia
{
    public class ResourcesHourVM : BaseVM, IDisposable
    {
        public long _IdScenario { get; set; }
        public BindingList<enResourcesHour> _ResourcesHourDataSource;


        public ResourcesHourVM()
        {
            _IdScenario = 0;
        }

        public async override Task LoadData()
        {
            using (var _ResourcesHourRepository = new ResourcesHourRepository())
            using (var _ResourcesHourService = new ResourcesHourService(_ResourcesHourRepository))
            {
                _ResourcesHourDataSource = new BindingList<enResourcesHour>(
                    (await _ResourcesHourService.QueryAsync(r => r.IdLoadData == _IdScenario)).ToList());

            }
        }

        public void Dispose()
        {
            base.BaseDispose();
        }
    }
}
