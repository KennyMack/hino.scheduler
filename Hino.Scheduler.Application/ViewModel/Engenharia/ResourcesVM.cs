﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.Engenharia
{
    public class ResouresVM : BaseVM, IDisposable
    {
        public long _IdScenario { get; set; }
        public List<enMaqDisp> _lstMaqDisp;
        public List<pdJobRot> _lstJobsRot;
        public List<enMaqTurno> _lstMaqTurno;
        public BindingList<pdMachineLoad> _MachineLoad;
        public BindingList<enMaquinas> _MachinesDataSource;

        public ResouresVM()
        {
            _IdScenario = 0;
            _MachineLoad = new BindingList<pdMachineLoad>();
            _MachinesDataSource = new BindingList<enMaquinas>();
        }

        #region Get whether machine hour Working
        private bool GetWorkingTime(DateTime pDate, string pMachine)
        {
            var day = pDate.DayOfWeek;

            var MachineTurno = _lstMaqTurno.FirstOrDefault(r => r.codmaquina == pMachine &&
            r.DayWeekEnum == day);

            if (MachineTurno != null)
            {
                var hrInicio = MachineTurno.horainicio;
                var hrTermino = MachineTurno.horatermino;
                var hrParadaIni = MachineTurno.hrparadaini;
                var hrParadaTermino = MachineTurno.hrparadaterm;

                if (MachineTurno.horatermino < MachineTurno.horainicio)
                {
                    hrInicio = MachineTurno.horatermino;
                    hrTermino = MachineTurno.horainicio;
                }

                if (MachineTurno.hrparadaterm < MachineTurno.hrparadaini)
                {
                    hrParadaIni = MachineTurno.hrparadaterm;
                    hrParadaTermino = MachineTurno.hrparadaini;
                }
                return pDate.ToMinutes().IsBetweenII(hrInicio, hrTermino) &&
                      !pDate.ToMinutes().IsBetweenII(hrParadaIni, hrParadaTermino);
            }

            return false;
        }
        #endregion

        #region Get Disp Hour day of week
        private decimal GetHoursDispDayOfWeek(DateTime pDate, string pMachine)
        {
            var day = pDate.DayOfWeek;

            var Machine = _lstMaqDisp.FirstOrDefault(r => r.codmaquina == pMachine);

            if (Machine != null)
            {
                switch (day)
                {
                    case DayOfWeek.Sunday:
                        return Machine.domingo;
                    case DayOfWeek.Monday:
                        return Machine.segunda;
                    case DayOfWeek.Tuesday:
                        return Machine.terca;
                    case DayOfWeek.Wednesday:
                        return Machine.quarta;
                    case DayOfWeek.Thursday:
                        return Machine.quinta;
                    case DayOfWeek.Friday:
                        return Machine.sexta;
                    case DayOfWeek.Saturday:
                        return Machine.sabado;
                }
            }
            return 0;
        }
        #endregion

        public async Task LoadDataMachines()
        {
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            {
                _MachinesDataSource =
                    new  BindingList<enMaquinas>(
                    await _ResourceService.GetMachinesListAsync(Parameters.codEstab));
            }
        }

        public async override Task LoadData()
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _ResourceRepository = new ResourceRepository())
            using (var _ResourceService = new ResourceService(_ResourceRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                _lstJobsRot = (await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, _IdScenario)).ToList();
                _lstMaqDisp = await _ResourceService.GetMachineDispAsync(Parameters.codEstab, _IdScenario);
                _lstMaqTurno = await _ResourceService.GetMachinesTurnoAsync(Parameters.codEstab, _IdScenario);

                var MachinesHour = _lstMaqDisp.Select(r => new
                {
                    codmaquina = r.codmaquina,
                    dtiniciooper = _lstJobsRot.Where(s => s.codmaquina == r.codmaquina).Min(t => t.dtiniciooper),
                    dttermoper = _lstJobsRot.Where(s => s.codmaquina == r.codmaquina).Max(t => t.dttermoper),
                });

                var lstMachineWorkDays = new List<enMachineWorkDays>();

                _MachineLoad.Clear();

                
                foreach (var item in MachinesHour)
                {
                    var days = 0;
                    var dt = item.dtiniciooper.Date;

                    if (item.dttermoper.ToShortDateString() == item.dtiniciooper.ToShortDateString())
                    {
                        lstMachineWorkDays.Add(new enMachineWorkDays
                        {
                            codmaquina = item.codmaquina,
                            dia = item.dtiniciooper.Date,
                            minutes = (item.dttermoper - item.dtiniciooper).TotalMinutes
                        });
                    }
                    else
                    {
                        while (days != ((item.dttermoper.Date - item.dtiniciooper.Date).TotalDays + 1))
                        {
                            var MachineTurno = _lstMaqTurno.FirstOrDefault(r => r.codmaquina == item.codmaquina &&
                                r.DayWeekEnum == dt.DayOfWeek);

                            if (MachineTurno != null)
                            {
                                var hrInicio = MachineTurno.horainicio;
                                var hrTermino = MachineTurno.horatermino;

                                if (MachineTurno.horatermino < MachineTurno.horainicio)
                                {
                                    hrInicio = MachineTurno.horatermino;
                                    hrTermino = MachineTurno.horainicio;
                                }

                                var totalMin = 0d;

                                if (item.dtiniciooper.Date == dt.Date &&
                                    item.dttermoper.Date != dt.Date &&
                                    item.dtiniciooper.ToMinutes() >= hrInicio)
                                    totalMin = Math.Abs(hrTermino - item.dtiniciooper.ToMinutes());
                                else if (item.dtiniciooper.Date == dt.Date &&
                                    item.dttermoper.Date != dt.Date &&
                                    item.dtiniciooper.ToMinutes() < hrInicio)
                                    totalMin = hrTermino - hrInicio;
                                else if (item.dtiniciooper.Date != dt.Date &&
                                    item.dttermoper.Date != dt.Date)
                                    totalMin = hrTermino - hrInicio;
                                else if (item.dtiniciooper.Date != dt.Date &&
                                    item.dttermoper.Date == dt.Date &&
                                    item.dttermoper.ToMinutes() <= hrTermino)
                                    totalMin = Math.Abs(item.dttermoper.ToMinutes() - hrInicio);
                                else if (item.dtiniciooper.Date != dt.Date &&
                                    item.dttermoper.Date == dt.Date &&
                                    item.dttermoper.ToMinutes() > hrTermino)
                                    totalMin = hrTermino - hrInicio;

                                lstMachineWorkDays.Add(new enMachineWorkDays
                                {
                                    codmaquina = item.codmaquina,
                                    dia = dt,
                                    minutes = totalMin
                                });

                                days++;
                            }
                            dt = dt.AddDays(1);

                        }
                    }
                }

                _MachineLoad = new BindingList<pdMachineLoad>(lstMachineWorkDays.GroupBy(r => new { r.codmaquina, r.dia.Date })
                    .Select(t => new pdMachineLoad
                    {
                        codmaquina = t.Key.codmaquina,
                        dtproducao = t.Key.Date,
                        minsdisponiveis = GetHoursDispDayOfWeek(t.Key.Date, t.Key.codmaquina),
                        minhoraprod = Convert.ToDecimal(
                            lstMachineWorkDays.Where(m => m.codmaquina == t.Key.codmaquina &&
                                                          m.dia.Date == t.Key.Date).Sum(h => h.minutes))
                    })
                    .ToList());
            }
        }


        public void Dispose()
        {
            _MachinesDataSource.Clear();
            _MachinesDataSource = null;

            _MachineLoad.Clear();
            _MachineLoad = null;

            base.BaseDispose();
        }
    }
}
