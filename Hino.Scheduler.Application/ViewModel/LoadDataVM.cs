﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Application.Services.Sales;
using Hino.Scheduler.Application.ViewModel.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel
{
    public class LoadDataVM : BaseVM, IDisposable
    {

        public BindingList<pdJobRot> _JobRotDataSource;
        public BindingList<pdResource> _ResourceDataSource;
        public BindingList<veDemanda> _DemandDataSource;
        public BindingList<pdLoadData> _LoadDataDataSource;
        public List<pdLoadData> _Scenarios;
        public long _IdScenario { get; set; }

        public LoadDataVM()
        {
            _JobRotDataSource = new BindingList<pdJobRot>();
            _ResourceDataSource = new BindingList<pdResource>();
            _DemandDataSource = new BindingList<veDemanda>();
            _LoadDataDataSource = new BindingList<pdLoadData>();
        }

        public async Task LoadScenario()
        {
            using (var _LoadDataRepository = new LoadDataRepository())
            using (var _LoadDataService = new LoadDataService(_LoadDataRepository))
                _Scenarios = new List<pdLoadData>(await _LoadDataService.GetAllAsync());
        }

        public async Task LoadDataSource()
        {
            using (var _pdLoadDataRepository = new LoadDataRepository())
            using (var _pdLoadDataService = new LoadDataService(_pdLoadDataRepository))
            {
                _LoadDataDataSource = new BindingList<pdLoadData>(
                    (await _pdLoadDataService.GetAllAsync()).ToList());
            }
        }

        public async Task LoadDetailDataSource(long pIdDataLoad)
        {
            using (var _pdJobRotRepository = new JobRotSchedulerRepository())
            using (var _pdResourceRepository = new ResourceRepository())
            using (var _veDemandaRepository = new DemandaRepository())
            using (var _pdJobRotService = new JobRotSchedulerService(_pdJobRotRepository))
            using (var _pdResourceService = new ResourceService(_pdResourceRepository))
            using (var _veDemandaService = new DemandaService(_veDemandaRepository))
            {
                _JobRotDataSource = new BindingList<pdJobRot>(
                    (await _pdJobRotService.QueryAsync(r => r.IdLoadData == pIdDataLoad)).ToList());
                _ResourceDataSource = new BindingList<pdResource>(
                    (await _pdResourceService.QueryAsync(r => r.IdLoadData == pIdDataLoad)).ToList());
                _DemandDataSource = new BindingList<veDemanda>(
                    (await _veDemandaService.QueryAsync(r => r.IdLoadData == pIdDataLoad)).ToList());
            }
        }

        public async Task<bool> CreateScenario(DateTime pInitalDate, DateTime pEndDate)
        {
            Errors.Clear();

            if (pInitalDate >= pEndDate)
            {
                Errors.Add("Data inicio deve ser menor que a data de término");
                return false;
            }
            
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _pdLoadDataRepository = new LoadDataRepository())
            using (var _pdLoadDataService = new LoadDataService(_pdLoadDataRepository))
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            using (var colors = new ColorsVM())
            {
                var idLoad = await _pdLoadDataService.CreateScenario(Parameters.codEstab, Parameters.UserLogged, pInitalDate, pEndDate);
                Errors = _pdLoadDataService.Errors;

                if (!Errors.Any() && idLoad > 0)
                {
                    var lstRots = (await _JobRotSchedulerService.GetByEstabIdAndIdLoadAsync(Parameters.codEstab, idLoad)).ToList();

                    await colors.GenerateOPRotColors(lstRots);

                    lstRots.Clear();
                }
            }

            return Errors.Count() <= 0;
        }

        public async Task<bool> DeleteScenario(long pIdDataLoad)
        {
            Errors.Clear();

            using (var _pdLoadDataRepository = new LoadDataRepository())
            using (var _pdLoadDataService = new LoadDataService(_pdLoadDataRepository))
            {
                await _pdLoadDataService.DeleteScenario(Parameters.codEstab, pIdDataLoad);
                Errors = _pdLoadDataService.Errors;
            }

            return Errors.Count() <= 0;
        }

        public void Dispose()
        {
            _JobRotDataSource.Clear();
            _ResourceDataSource.Clear();
            _DemandDataSource.Clear();
            _LoadDataDataSource.Clear();

            _JobRotDataSource = null;
            _ResourceDataSource = null;
            _DemandDataSource = null;
            _LoadDataDataSource = null;

            base.BaseDispose();
        }
    }
}
