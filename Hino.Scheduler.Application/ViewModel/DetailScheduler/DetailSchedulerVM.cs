﻿using Hino.Scheduler.Application.Services.Production;
using Hino.Scheduler.Data.Repositories.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Application.ViewModel.DetailScheduler
{
    public class DetailSchedulerVM : BaseVM, IDisposable
    {
        public BindingList<pdJobRotDetail> _JobRotDataSource;
        public long _IdScenario { get; set; }
        public DetailSchedulerVM()
        {
            _JobRotDataSource = new BindingList<pdJobRotDetail>();

        }

        public async Task LoadDetailRotAsync(long pCodOrdProd, string pNivelOrdProd)
        {
            using (var _JobRotSchedulerRepository = new JobRotSchedulerRepository())
            using (var _JobRotSchedulerService = new JobRotSchedulerService(_JobRotSchedulerRepository))
            {
                _JobRotDataSource = null;
                var lst = await _JobRotSchedulerService.QueryJobsRotDetailAsync(Parameters.codEstab, _IdScenario, pCodOrdProd, pNivelOrdProd);

                _JobRotDataSource = new BindingList<pdJobRotDetail>(
                    lst.Select(r => new pdJobRotDetail
                    {
                        operacao = r.operacao,
                        codrotina = r.codrotina,
                        descoperacao = r.descoperacao,
                        dtiniciooper = r.dtiniciooper,
                        dttermoper = r.dttermoper,
                        codmaquina = r.codmaquina,
                        prodhoraria = r.prodhoraria
                    }).ToList());
            }
        }

        public void Dispose()
        {
            _JobRotDataSource.Clear();
            _JobRotDataSource = null;

            base.BaseDispose();
        }
    }
}
