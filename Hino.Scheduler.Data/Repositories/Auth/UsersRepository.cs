﻿using Hino.Scheduler.Data.Repositories.Interfaces.Auth;
using Hino.Scheduler.Data.Repositories;
using Hino.Scheduler.Model.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Hino.Scheduler.Data.Repositories.Auth
{
    public class UsersRepository : BaseRepository<Users>, IUsersRepository
    {
        public async override Task<Users> GetByIdAsync(Users model, params Expression<Func<Users, object>>[] includeProperties) =>
                await base.DbEntity.AsNoTracking().Where(r => r.codUsuario == model.codUsuario).FirstOrDefaultAsync();

    }
}
