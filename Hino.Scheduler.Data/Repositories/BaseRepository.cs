﻿using Hino.Scheduler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Hino.Scheduler.Data.Context;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Hino.Scheduler.Data.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : BaseEntity
    {
        protected AppDbContext DbConn;
        protected DbSet<T> DbEntity;

        public BaseRepository()
        {
            DbConn = new AppDbContext();
            DbEntity = DbConn.Set<T>();
        }

        protected IQueryable<T> AddQueryProperties(IQueryable<T> query, 
            params Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }

        public virtual T Add(T model)
        {
            DbEntity.Add(model);
            return model;
        }

        public virtual T Update(T model)
        {
            DbConn.Entry(model).State = EntityState.Detached;
            DbConn.Entry(model).State = EntityState.Modified;

            return model;
        }

        public virtual T Remove(T model)
        {
            DbEntity.Remove(model);
            return model;
        }

        public async virtual Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties).ToListAsync();

        public async virtual Task<T> GetByIdAsync(T model, params Expression<Func<T, object>>[] includeProperties) =>
             await DbEntity.AsNoTracking().Where(r => r == model).FirstOrDefaultAsync();

        public async virtual Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, 
            params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties)
                            .Where(predicate)
                            .ToListAsync();

        public virtual void RollBackChanges()
        {
            var changedEntries = DbConn.ChangeTracker.Entries()
                .Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public virtual async Task<int> SaveChangesAsync() =>
            await DbConn.SaveChangesAsync();

        DbRawSqlQuery<decimal> GetNextSequency()
        {
            var sql = $"SELECT SEQ_{typeof(T).Name}.NEXTVAL FROM DUAL";

            return DbConn.Database.SqlQuery<decimal>(sql);
        }

        public virtual async Task<long> NextSequenceAsync()
        {
            try
            {
                return Convert.ToInt64(
                    await GetNextSequency().FirstAsync());
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public void Dispose()
        {
            DbConn.Dispose();
        }
    }
}
