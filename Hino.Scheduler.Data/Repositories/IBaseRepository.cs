﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        T Add(T model);
        Task<T> GetByIdAsync(T model, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        T Remove(T model);
        Task<long> NextSequenceAsync();
        Task<int> SaveChangesAsync();
        void RollBackChanges();
        void Dispose();
    }
}
