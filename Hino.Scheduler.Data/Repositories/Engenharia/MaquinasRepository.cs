﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Hino.Scheduler.Utils;

namespace Hino.Scheduler.Data.Repositories.Engenharia
{
    public class MaquinasRepository : BaseRepository<enMaquinas>, IMaquinasRepository
    {
        public async Task<List<pdMachineCellLoad>> QueryMachineCellLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd)
        {
            var result = DbConn.Database.SqlQuery<pdMachineCellLoad>(@"SELECT TBCELULAS.CODMAQUINA, 
                                                                        TBCELULAS.ENMAQUINASDESCRICAO,
                                                                        TBCELULAS.CODCELULA, 
                                                                        TBCELULAS.ENCELULASDESCRICAO,
                                                                        TBCELULAS.CODROTINA,
                                                                        TBCELULAS.ENROTINASDESCRICAO,
                                                                        TBCELULAS.DIASPERIODO,
                                                                        TBCELULAS.MINUTOSPROD,
                                                                        PCKG_UTEIS.MINTOHORAMIN(TBCELULAS.MINUTOSPROD) HORASPRODMAQ,
                                                                        TBCELULAS.MINPRODCELULA,
                                                                        PCKG_UTEIS.MINTOHORAMIN(TBCELULAS.MINPRODCELULA) HORASPRODCELULA,
                                                                        CASE 
                                                                          WHEN NVL(TBCELULAS.MINUTOSPROD, 0) > 0 THEN
                                                                            CEIL(((NVL(TBCELULAS.MINUTOSPROD, 0) / 60)/ (CEIL(:pDTTERMINO1 - :pDTINICIO1))) * 60)
                                                                          ELSE
                                                                            0
                                                                        END MINSTRABDIAMAQUINA,
                                                                        PCKG_UTEIS.MINTOHORAMIN(
                                                                        CASE 
                                                                          WHEN NVL(TBCELULAS.MINUTOSPROD, 0) > 0 THEN
                                                                            CEIL(((NVL(TBCELULAS.MINUTOSPROD, 0) / 60)/ (CEIL(:pDTTERMINO2 - :pDTINICIO2))) * 60)
                                                                          ELSE
                                                                            0
                                                                        END) HORASTRABDIAMAQUINA,
                                                                        CASE 
                                                                          WHEN NVL(TBCELULAS.MINPRODCELULA, 0) > 0 THEN
                                                                            CEIL(((NVL(TBCELULAS.MINPRODCELULA, 0) / 60)/ (CEIL(:pDTTERMINO3 - :pDTINICIO3))) * 60)
                                                                          ELSE
                                                                            0
                                                                        END MINSTRABDIACELULA,
                                                                        PCKG_UTEIS.MINTOHORAMIN(CASE 
                                                                          WHEN NVL(TBCELULAS.MINPRODCELULA, 0) > 0 THEN
                                                                            CEIL(((NVL(TBCELULAS.MINPRODCELULA, 0) / 60)/ (CEIL(:pDTTERMINO4 - :pDTINICIO4))) * 60)
                                                                          ELSE
                                                                            0
                                                                        END) HORASTRABDIACELULA
                                                                   FROM (SELECT TBCELULAS.CODMAQUINA, 
                                                                                TBCELULAS.ENMAQUINASDESCRICAO,
                                                                                TBCELULAS.CODCELULA, 
                                                                                TBCELULAS.ENCELULASDESCRICAO,
                                                                                TBMAQUINAS.CODROTINA,
                                                                                TBMAQUINAS.ENROTINASDESCRICAO,
                                                                                CEIL(:pDTTERMINO5 - :pDTINICIO5) DIASPERIODO, 
                                                                                NVL(TBMAQUINAS.MINUTOSPROD, 0) MINUTOSPROD,
                                                                                SUM(NVL(TBMAQUINAS.MINUTOSPROD, 0)) OVER (PARTITION BY TBCELULAS.CODCELULA ORDER BY TBCELULAS.CODCELULA) MINPRODCELULA
                                                                           FROM (SELECT TBORDENSPROD.CODMAQUINA,
                                                                                        TBORDENSPROD.CODROTINA,
                                                                                        TBORDENSPROD.CODCELULA, 
                                                                                        TBORDENSPROD.ENCELULASDESCRICAO,
                                                                                        TBORDENSPROD.ENROTINASDESCRICAO,
                                                                                        TBORDENSPROD.CODESTAB,
                                                                                        CEIL(SUM(TBORDENSPROD.HORASPROD)) MINUTOSPROD
                                                                                   FROM (SELECT PDORDEMPROD.CODESTAB, PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD,
                                                                                                PDORDEMPROD.PROGRAMADO, PDORDEMPROD.REALIZADO, PDORDEMPROD.REFUGADO,
                                                                                                PDORDEMPROD.DTINICIO, PDORDEMPROD.DTTERMINO, PDORDEMPRODROTINAS.CODMAQUINA,
                                                                                                PDORDEMPRODROTINAS.OPERACAO,
                                                                                                (PDORDEMPROD.PROGRAMADO - (PDORDEMPROD.REALIZADO - PDORDEMPROD.REFUGADO)) SALDO,
                                                                                                ROUND(PDORDEMPRODROTINAS.PRODHORARIA, 10) PRODHORARIA,
                                                                                                CEIL((CASE 
                                                                                                        WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                          ((PDORDEMPROD.PROGRAMADO /*- (PDORDEMPROD.REALIZADO - PDORDEMPROD.REFUGADO)*/) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                        WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                          60
                                                                                                        ELSE
                                                                                                          1
                                                                                                      END)
                                                                                                 ) HORASPROD,
                                                                                                PCKG_UTEIS.MINTOHORAMIN(
                                                                                                CEIL((CASE 
                                                                                                        WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                          ((PDORDEMPROD.PROGRAMADO /*- (PDORDEMPROD.REALIZADO - PDORDEMPROD.REFUGADO)*/) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                        WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                          60
                                                                                                        ELSE
                                                                                                          1
                                                                                                      END)
                                                                                                 ))
                                                                                                MINHORA, ENROTINAS.CODROTINA, ENROTINAS.DESCRICAO ENROTINASDESCRICAO,
                                                                                                ENCELULAS.CODCELULA, ENCELULAS.DESCRICAO ENCELULASDESCRICAO
                                                                                           FROM PDORDEMPROD,
                                                                                                PDORDEMPRODROTINAS,
                                                                                                ENROTINAS,
                                                                                                ENMAQUINAS,
                                                                                                ENCELULAS
                                                                                          WHERE PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                                            AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                                            AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                                            AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                                            AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                                            AND PDORDEMPROD.CODESTAB              = :pCODESTAB1
                                                                                            AND ENROTINAS.CODROTINA               = PDORDEMPRODROTINAS.CODROTINA
                                                                                            AND ENMAQUINAS.CODMAQUINA             = PDORDEMPRODROTINAS.CODMAQUINA
                                                                                            AND ENMAQUINAS.CODESTAB               = PDORDEMPRODROTINAS.CODESTAB 
                                                                                            AND ENCELULAS.CODCELULA               = ENMAQUINAS.CODCELULA
                                                                                            AND ENCELULAS.CODESTAB                = ENMAQUINAS.CODESTAB
                                                                                            --AND PDORDEMPROD.STATUS              = 'P'
                                                                                            AND PDORDEMPRODROTINAS.PRODHORARIA    > 0
                                                                                            AND TRUNC(PDORDEMPROD.DTINICIO)       >= :pDTINICIO6
                                                                                            AND TRUNC(PDORDEMPROD.DTTERMINO)      <= :pDTTERMINO6
                                                                                            --AND (PDORDEMPROD.PROGRAMADO - (PDORDEMPROD.REALIZADO - PDORDEMPROD.REFUGADO)) > 0
                                                                                            ) TBORDENSPROD
                                                                                 GROUP BY TBORDENSPROD.CODCELULA, 
                                                                                          TBORDENSPROD.ENCELULASDESCRICAO,
                                                                                          TBORDENSPROD.CODROTINA,
                                                                                          TBORDENSPROD.ENROTINASDESCRICAO,
                                                                                          TBORDENSPROD.CODESTAB,
                                                                                          TBORDENSPROD.CODMAQUINA) TBMAQUINAS,
                                                                                (SELECT ENCELULAS.CODCELULA, ENCELULAS.DESCRICAO ENCELULASDESCRICAO,
                                                                                        ENMAQUINAS.CODMAQUINA, ENMAQUINAS.DESCRICAO ENMAQUINASDESCRICAO,
                                                                                        ENMAQUINAS.CODESTAB
                                                                                   FROM ENCELULAS,
                                                                                        ENMAQUINAS
                                                                                  WHERE ENMAQUINAS.CODCELULA   = ENCELULAS.CODCELULA
                                                                                    AND ENMAQUINAS.CODESTAB    = ENCELULAS.CODESTAB
                                                                                    AND ENMAQUINAS.CODESTAB    = :pCODESTAB2
                                                                                    AND ENCELULAS.STATUS       = 1
                                                                                    AND ENMAQUINAS.STATUS      = 1) TBCELULAS
                                                                          WHERE TBMAQUINAS.CODCELULA           (+)= TBCELULAS.CODCELULA
                                                                            AND TBMAQUINAS.CODMAQUINA          (+)= TBCELULAS.CODMAQUINA
                                                                            AND TBMAQUINAS.CODESTAB            (+)= TBCELULAS.CODESTAB) TBCELULAS
                                                                          ORDER BY TBCELULAS.CODCELULA, TBCELULAS.CODMAQUINA",
                   new OracleParameter("pDTTERMINO1", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pDTINICIO1", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),

                   new OracleParameter("pDTTERMINO2", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pDTINICIO2", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),

                   new OracleParameter("pDTTERMINO3", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pDTINICIO3", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),

                   new OracleParameter("pDTTERMINO4", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pDTINICIO4", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),

                   new OracleParameter("pDTTERMINO5", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pDTINICIO5", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),

                   new OracleParameter("pCODESTAB1", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),

                   new OracleParameter("pDTINICIO6", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),
                   new OracleParameter("pDTTERMINO6", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),

                   new OracleParameter("pCODESTAB2", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }

        public async Task ExecLoadCargaMaquinaAsync(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.LOADCARGAMAQUINA(:pCODESTAB, :pDTINICIO, :pDTTERMINO, :pCODUSUARIO); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pDTINICIO", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),
                   new OracleParameter("pDTTERMINO", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                   );
        }

        public async Task<List<pdMachineLoad>> QueryMachineLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario)
        {
            var result = DbConn.Database.SqlQuery<pdMachineLoad>(@"SELECT TBMAQPROD.CODMAQUINA,
                                                                          TBMAQPROD.ENMAQUINASDESCRICAO,
                                                                          TBMAQPROD.CODESTAB,
                                                                          TBMAQPROD.DTPRODUCAO,
                                                                          TO_NUMBER(CEIL(TBMAQPROD.MINSDISPONIVEIS)) MINSDISPONIVEIS,
                                                                          PCKG_UTEIS.MINTOHORAMIN(CEIL(TBMAQPROD.MINSDISPONIVEIS)) DESCMINSDISPONIVEIS,
                                                                          TO_NUMBER(CEIL(TBMAQPROD.MINHORAPROD)) MINHORAPROD,
                                                                          PCKG_UTEIS.MINTOHORAMIN(CEIL(TBMAQPROD.MINHORAPROD)) DESCMINHORA,
                                                                          CASE
                                                                            WHEN TBMAQPROD.MINSDISPONIVEIS > 0 THEN
                                                                              TO_NUMBER(NVL(ROUND(TBMAQPROD.MINHORAPROD / TBMAQPROD.MINSDISPONIVEIS, 2), 0)) 
                                                                            ELSE
                                                                              1
                                                                          END CARGAMAQUINA,
                                                                          CASE
                                                                            WHEN SEMTEMPOSEMANA > 0 THEN
                                                                              'SEM TEMPO DEFINIDO'
                                                                            ELSE
                                                                              ''
                                                                          END SEMTEMPOSEMANA
                                                                     FROM (SELECT TBCARGAMAQ.CODMAQUINA, TBCARGAMAQ.CODESTAB,
                                                                                  TBCARGAMAQ.ENMAQUINASDESCRICAO,
                                                                                  TBCARGAMAQ.DTPRODUCAO,
                                                                                  MIN(TBCARGAMAQ.MINSDISPONIVEIS) MINSDISPONIVEIS,
                                                                                  SUM(TBCARGAMAQ.MINHORAPROD) MINHORAPROD,
                                                                                  MAX(TBCARGAMAQ.SEMTEMPOSEMANA) SEMTEMPOSEMANA
                                                                             FROM (SELECT PDCARGAMAQSIMUL.CODMAQUINA,PDCARGAMAQSIMUL.CODORDPROD,PDCARGAMAQSIMUL.NIVELORDPROD, 
                                                                                          PDCARGAMAQSIMUL.ENMAQUINASDESCRICAO, PDCARGAMAQSIMUL.OPERACAO,
                                                                                          PDCARGAMAQSIMUL.DATAUSO DTPRODUCAO, PDCARGAMAQSIMUL.CODESTAB,
                                                                                          TO_NUMBER(CEIL(NVL(PDCARGAMAQSIMUL.MINUTOSDISP, 0))) MINSDISPONIVEIS,
                                                                                          TO_NUMBER(CEIL(NVL(PDCARGAMAQSIMUL.MINUTOSUSO, 0))) MINHORAPROD,
                                                                                          PDCARGAMAQSIMUL.SEMTEMPOSEMANA
                                                                                     FROM PDCARGAMAQ PDCARGAMAQSIMUL
                                                                                    WHERE PDCARGAMAQSIMUL.CODESTAB    = :pCODESTAB
                                                                                      AND PDCARGAMAQSIMUL.CODUSUARIO  = :pCODUSUARIO) TBCARGAMAQ
                                                                             GROUP BY TBCARGAMAQ.CODMAQUINA, 
                                                                                      TBCARGAMAQ.CODESTAB,
                                                                                      TBCARGAMAQ.ENMAQUINASDESCRICAO,
                                                                                      TBCARGAMAQ.DTPRODUCAO
                                                                             ORDER BY TBCARGAMAQ.DTPRODUCAO,
                                                                                      TBCARGAMAQ.CODMAQUINA) TBMAQPROD",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input));
            return await result.ToListAsync();
            /*
            var result = DbConn.Database.SqlQuery<pdMachineLoad>(@"SELECT TBMAQPROD.CODMAQUINA,
                                                                             TBMAQPROD.ENMAQUINASDESCRICAO,
                                                                          TBMAQPROD.CODESTAB,
                                                                          TBMAQPROD.DTPRODUCAO,
                                                                          TO_NUMBER(CEIL(NVL(TBMAQPROD.MINSDISPONIVEIS, 0))) MINSDISPONIVEIS,
                                                                          TBMAQPROD.DESCMINSDISPONIVEIS,
                                                                          TO_NUMBER(CEIL(NVL(SUM(TBMAQPROD.MINHORA), 0))) MINHORAPROD,
                                                                          PCKG_UTEIS.MINTOHORAMIN(
                                                                          SUM(TBMAQPROD.MINHORA)) DESCMINHORA,
                                                                          TO_NUMBER(NVL(ROUND(SUM(TBMAQPROD.MINHORA) / TBMAQPROD.MINSDISPONIVEIS, 2), 0)) CARGAMAQUINA
                                                                     FROM (SELECT TBMAQPROD.CODMAQUINA,
                                                                                  TBMAQPROD.CODESTAB,
                                                                                  TRUNC(TBMAQPROD.DTPROD) DTPRODUCAO,
                                                                                  CEIL(CASE TO_CHAR(TBMAQPROD.DTPROD, 'D')
                                                                                    WHEN '1' THEN
                                                                                      ENMAQUINADISP.DOMINGO
                                                                                    WHEN '2' THEN
                                                                                      ENMAQUINADISP.SEGUNDA
                                                                                    WHEN '3' THEN
                                                                                      ENMAQUINADISP.TERCA
                                                                                    WHEN '4' THEN
                                                                                      ENMAQUINADISP.QUARTA
                                                                                    WHEN '5' THEN
                                                                                      ENMAQUINADISP.QUINTA
                                                                                    WHEN '6' THEN
                                                                                      ENMAQUINADISP.SEXTA
                                                                                    WHEN '7' THEN
                                                                                      ENMAQUINADISP.SABADO
                                                                                  END * 60) MINSDISPONIVEIS,
                                                                                  PCKG_UTEIS.MINTOHORAMIN(
                                                                                  CEIL(CASE TO_CHAR(TBMAQPROD.DTPROD, 'D')
                                                                                    WHEN '1' THEN
                                                                                      ENMAQUINADISP.DOMINGO
                                                                                    WHEN '2' THEN
                                                                                      ENMAQUINADISP.SEGUNDA
                                                                                    WHEN '3' THEN
                                                                                      ENMAQUINADISP.TERCA
                                                                                    WHEN '4' THEN
                                                                                      ENMAQUINADISP.QUARTA
                                                                                    WHEN '5' THEN
                                                                                      ENMAQUINADISP.QUINTA
                                                                                    WHEN '6' THEN
                                                                                      ENMAQUINADISP.SEXTA
                                                                                    WHEN '7' THEN
                                                                                      ENMAQUINADISP.SABADO
                                                                                  END * 60)) DESCMINSDISPONIVEIS,
                                                                                  TBMAQPROD.MINHORA,
                                                                                  TBMAQPROD.ENMAQUINASDESCRICAO
                                                                             FROM (SELECT PDORDEMPRODROTINAS.CODMAQUINA,
                                                                                          PDORDEMPRODROTINAS.CODESTAB,
                                                                                          ENMAQUINAS.DESCRICAO ENMAQUINASDESCRICAO,
                                                                                          CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                          ) MINHORA,
                                                                                          SUM(CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                           )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)  MINSACUMULADOS,
                                                                                          PDORDEMPRODROTINAS.DTINICIO +
                                                                                          ((SUM(CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                           )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)) / 60 /24) DTPROD
                                                                                     FROM PDORDEMPROD,
                                                                                          PDORDEMPRODROTINAS,
                                                                                          ENROTINAS,
                                                                                          ENMAQUINAS,
                                                                                          ENCELULAS
                                                                                    WHERE PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                                      AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                                      AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                                      AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                                      AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                                      AND PDORDEMPROD.CODESTAB              = :pCODESTAB
                                                                                      AND ENROTINAS.CODROTINA               = PDORDEMPRODROTINAS.CODROTINA
                                                                                      AND ENMAQUINAS.CODMAQUINA             = PDORDEMPRODROTINAS.CODMAQUINA
                                                                                      AND ENMAQUINAS.CODESTAB               = PDORDEMPRODROTINAS.CODESTAB
                                                                                      AND ENCELULAS.CODCELULA               = ENMAQUINAS.CODCELULA
                                                                                      AND ENCELULAS.CODESTAB                = ENMAQUINAS.CODESTAB
                                                                                      AND PDORDEMPRODROTINAS.PRODHORARIA    > 0
                                                                                      AND TRUNC(PDORDEMPROD.DTINICIO)       >= :pDTINICIO
                                                                                      AND TRUNC(PDORDEMPROD.DTTERMINO)      <= :pDTTERMINO) TBMAQPROD,
                                                                                  ENMAQUINADISP
                                                                            WHERE ENMAQUINADISP.CODESTAB    = TBMAQPROD.CODESTAB
                                                                              AND ENMAQUINADISP.CODMAQUINA  = TBMAQPROD.CODMAQUINA) TBMAQPROD
                                                                    WHERE TBMAQPROD.MINSDISPONIVEIS > 0
                                                                    GROUP BY TBMAQPROD.CODMAQUINA,
                                                                             TBMAQPROD.CODESTAB,
                                                                             TBMAQPROD.DTPRODUCAO,
                                                                             TBMAQPROD.DESCMINSDISPONIVEIS,
                                                                             TBMAQPROD.MINSDISPONIVEIS,
                                                                             TBMAQPROD.ENMAQUINASDESCRICAO
                                                                    ORDER BY TBMAQPROD.DTPRODUCAO, TBMAQPROD.CODMAQUINA",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                                      
                   new OracleParameter("pDTINICIO", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),
                   new OracleParameter("pDTTERMINO", OracleDbType.Date, pDtEnd.LastHour(), ParameterDirection.Input)
                   );
            return await result.ToListAsync();*/
        }

        public async Task ExecLoadSimulCargaMaquinaAsync(int pCodEstab, long pIdLoadData, string pCodUsuario)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.LOADSIMULCARGAMAQUINA(:pCODESTAB, :pIDLOADDATA, :pCODUSUARIO); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int32, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                   );
        }

        public async Task<List<pdMachineLoad>> QueryMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodUsuario)
        {
            var result = DbConn.Database.SqlQuery<pdMachineLoad>(@"SELECT TBMAQPROD.CODMAQUINA,
                                                                          TBMAQPROD.ENMAQUINASDESCRICAO,
                                                                          TBMAQPROD.CODESTAB,
                                                                          TBMAQPROD.DTPRODUCAO,
                                                                          TO_NUMBER(CEIL(TBMAQPROD.MINSDISPONIVEIS)) MINSDISPONIVEIS,
                                                                          PCKG_UTEIS.MINTOHORAMIN(CEIL(TBMAQPROD.MINSDISPONIVEIS)) DESCMINSDISPONIVEIS,
                                                                          TO_NUMBER(CEIL(TBMAQPROD.MINHORAPROD)) MINHORAPROD,
                                                                          PCKG_UTEIS.MINTOHORAMIN(CEIL(TBMAQPROD.MINHORAPROD)) DESCMINHORA,
                                                                          CASE
                                                                            WHEN TBMAQPROD.MINSDISPONIVEIS > 0 THEN
                                                                              TO_NUMBER(NVL(ROUND(TBMAQPROD.MINHORAPROD / TBMAQPROD.MINSDISPONIVEIS, 2), 0)) 
                                                                            ELSE
                                                                              1
                                                                          END CARGAMAQUINA,
                                                                          CASE
                                                                            WHEN SEMTEMPOSEMANA > 0 THEN
                                                                              'SEM TEMPO DEFINIDO'
                                                                            ELSE
                                                                              ''
                                                                          END SEMTEMPOSEMANA
                                                                     FROM (SELECT TBCARGAMAQ.CODMAQUINA, TBCARGAMAQ.CODESTAB,
                                                                                  TBCARGAMAQ.ENMAQUINASDESCRICAO,
                                                                                  TBCARGAMAQ.DTPRODUCAO,
                                                                                  MIN(TBCARGAMAQ.MINSDISPONIVEIS) MINSDISPONIVEIS,
                                                                                  SUM(TBCARGAMAQ.MINHORAPROD) MINHORAPROD,
                                                                                  MAX(TBCARGAMAQ.SEMTEMPOSEMANA) SEMTEMPOSEMANA
                                                                             FROM (SELECT PDCARGAMAQSIMUL.CODMAQUINA,PDCARGAMAQSIMUL.CODORDPROD,PDCARGAMAQSIMUL.NIVELORDPROD, 
                                                                                          PDCARGAMAQSIMUL.ENMAQUINASDESCRICAO, PDCARGAMAQSIMUL.OPERACAO,
                                                                                          PDCARGAMAQSIMUL.DATAUSO DTPRODUCAO, PDCARGAMAQSIMUL.CODESTAB,
                                                                                          TO_NUMBER(CEIL(NVL(PDCARGAMAQSIMUL.MINUTOSDISP, 0))) MINSDISPONIVEIS,
                                                                                          TO_NUMBER(CEIL(NVL(PDCARGAMAQSIMUL.MINUTOSUSO, 0))) MINHORAPROD,
                                                                                          PDCARGAMAQSIMUL.SEMTEMPOSEMANA
                                                                                     FROM PDCARGAMAQSIMUL
                                                                                    WHERE PDCARGAMAQSIMUL.CODESTAB    = :pCODESTAB
                                                                                      AND PDCARGAMAQSIMUL.IDLOADDATA  = :pIDLOADDATA
                                                                                      AND PDCARGAMAQSIMUL.CODUSUARIO  = :pCODUSUARIO) TBCARGAMAQ
                                                                             GROUP BY TBCARGAMAQ.CODMAQUINA, 
                                                                                      TBCARGAMAQ.CODESTAB,
                                                                                      TBCARGAMAQ.ENMAQUINASDESCRICAO,
                                                                                      TBCARGAMAQ.DTPRODUCAO
                                                                             ORDER BY TBCARGAMAQ.DTPRODUCAO,
                                                                                      TBCARGAMAQ.CODMAQUINA) TBMAQPROD",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
            /*
            var result = DbConn.Database.SqlQuery<pdMachineLoad>(@"SELECT TBMAQPROD.CODMAQUINA,
                                                                          TBMAQPROD.ENMAQUINASDESCRICAO,
                                                                          TBMAQPROD.CODESTAB,
                                                                          TBMAQPROD.DTPRODUCAO,
                                                                          TO_NUMBER(CEIL(NVL(TBMAQPROD.MINSDISPONIVEIS, 0))) MINSDISPONIVEIS,
                                                                          TBMAQPROD.DESCMINSDISPONIVEIS,
                                                                          TO_NUMBER(CEIL(NVL(SUM(TBMAQPROD.MINHORA), 0))) MINHORAPROD,
                                                                          PCKG_UTEIS.MINTOHORAMIN(
                                                                          SUM(TBMAQPROD.MINHORA)) DESCMINHORA,
                                                                          TO_NUMBER(NVL(ROUND(SUM(TBMAQPROD.MINHORA) / TBMAQPROD.MINSDISPONIVEIS, 2), 0)) CARGAMAQUINA
                                                                     FROM (SELECT TBMAQPROD.CODMAQUINA,
                                                                                  TBMAQPROD.CODESTAB,
                                                                                  TRUNC(TBMAQPROD.DTPROD) DTPRODUCAO,
                                                                                  CEIL(CASE TO_CHAR(TBMAQPROD.DTPROD, 'D')
                                                                                    WHEN '1' THEN
                                                                                      ENMAQUINADISP.DOMINGO
                                                                                    WHEN '2' THEN
                                                                                      ENMAQUINADISP.SEGUNDA
                                                                                    WHEN '3' THEN
                                                                                      ENMAQUINADISP.TERCA
                                                                                    WHEN '4' THEN
                                                                                      ENMAQUINADISP.QUARTA
                                                                                    WHEN '5' THEN
                                                                                      ENMAQUINADISP.QUINTA
                                                                                    WHEN '6' THEN
                                                                                      ENMAQUINADISP.SEXTA
                                                                                    WHEN '7' THEN
                                                                                      ENMAQUINADISP.SABADO
                                                                                  END * 60) MINSDISPONIVEIS,
                                                                                  PCKG_UTEIS.MINTOHORAMIN(
                                                                                  CEIL(CASE TO_CHAR(TBMAQPROD.DTPROD, 'D')
                                                                                    WHEN '1' THEN
                                                                                      ENMAQUINADISP.DOMINGO
                                                                                    WHEN '2' THEN
                                                                                      ENMAQUINADISP.SEGUNDA
                                                                                    WHEN '3' THEN
                                                                                      ENMAQUINADISP.TERCA
                                                                                    WHEN '4' THEN
                                                                                      ENMAQUINADISP.QUARTA
                                                                                    WHEN '5' THEN
                                                                                      ENMAQUINADISP.QUINTA
                                                                                    WHEN '6' THEN
                                                                                      ENMAQUINADISP.SEXTA
                                                                                    WHEN '7' THEN
                                                                                      ENMAQUINADISP.SABADO
                                                                                  END * 60)) DESCMINSDISPONIVEIS,
                                                                                  TBMAQPROD.MINHORA,
                                                                                  TBMAQPROD.ENMAQUINASDESCRICAO
                                                                             FROM (SELECT DISTINCT PDORDEMPROD.CODMAQUINA,
                                                                                          PDORDEMPRODROTINAS.CODESTAB,
                                                                                          ENMAQUINAS.DESCRICAO ENMAQUINASDESCRICAO,
                                                                                          CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                          ) MINHORA,
                                                                                          SUM(CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                           )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)  MINSACUMULADOS,
                                                                                          PDORDEMPROD.DTINICIOOPER +
                                                                                          ((SUM(CEIL((CASE
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                                    ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                                  WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                                    60
                                                                                                  ELSE
                                                                                                    1
                                                                                                END)
                                                                                           )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)) / 60 /24) DTPROD
                                                                                     FROM PDOPJOBSROT PDORDEMPROD,
                                                                                          PDORDEMPRODROTINAS,
                                                                                          ENROTINAS,
                                                                                          ENMAQUINAS,
                                                                                          ENCELULAS
                                                                                    WHERE PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                                      AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                                      AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                                      AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                                      AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                                      AND PDORDEMPRODROTINAS.OPERACAO       = PDORDEMPROD.OPERACAO
                                                                                      AND PDORDEMPROD.CODESTAB              = :pCODESTAB
                                                                                      AND PDORDEMPROD.IDLOADDATA            = :pIDLOADDATA
                                                                                      AND ENROTINAS.CODROTINA               = PDORDEMPRODROTINAS.CODROTINA
                                                                                      AND ENMAQUINAS.CODMAQUINA             = PDORDEMPRODROTINAS.CODMAQUINA
                                                                                      AND ENMAQUINAS.CODESTAB               = PDORDEMPRODROTINAS.CODESTAB
                                                                                      AND ENCELULAS.CODCELULA               = ENMAQUINAS.CODCELULA
                                                                                      AND ENCELULAS.CODESTAB                = ENMAQUINAS.CODESTAB
                                                                                      AND PDORDEMPRODROTINAS.PRODHORARIA    > 0) TBMAQPROD,
                                                                                  ENMAQUINADISP
                                                                            WHERE ENMAQUINADISP.CODESTAB    = TBMAQPROD.CODESTAB
                                                                              AND ENMAQUINADISP.CODMAQUINA  = TBMAQPROD.CODMAQUINA) TBMAQPROD
                                                                    WHERE TBMAQPROD.MINSDISPONIVEIS > 0
                                                                    GROUP BY TBMAQPROD.CODMAQUINA,
                                                                             TBMAQPROD.ENMAQUINASDESCRICAO,
                                                                             TBMAQPROD.CODESTAB,
                                                                             TBMAQPROD.DTPRODUCAO,
                                                                             TBMAQPROD.DESCMINSDISPONIVEIS,
                                                                             TBMAQPROD.MINSDISPONIVEIS
                                                                    ORDER BY TBMAQPROD.DTPRODUCAO, TBMAQPROD.CODMAQUINA",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
            */
        }
    }
}
