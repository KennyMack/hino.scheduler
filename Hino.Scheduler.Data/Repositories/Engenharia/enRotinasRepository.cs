﻿using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;

namespace Hino.Scheduler.Data.Repositories.Engenharia
{
    public class enRotinasRepository : BaseRepository<enRotinas>, IenRotinasRepository
    {
    }
}
