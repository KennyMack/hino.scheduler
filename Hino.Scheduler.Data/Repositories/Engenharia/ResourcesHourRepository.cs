﻿using Hino.Scheduler.Data.Repositories.Interfaces.Engenharia;
using Hino.Scheduler.Model.Engenharia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Engenharia
{
    public class ResourcesHourRepository : BaseRepository<enResourcesHour>, IResourcesHourRepository
    {
    }
}
