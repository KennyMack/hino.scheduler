﻿using Hino.Scheduler.Data.Repositories.Interfaces.Gerais;
using Hino.Scheduler.Model.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Gerais
{
    public class UserDataRepository : BaseRepository<geUserData>, IUserDataRepository
    {
        public async Task<geUserData> GetByCodUsuarioAsync(string pCodUsuario) =>
            await Task.Run(() => DbEntity.FirstOrDefault(r => r.CodUsuario == pCodUsuario));
    }
}
