﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class JobSchedulerRepository : BaseRepository<pdJob>, IJobSchedulerRepository
    {
        public async Task GeneratedJobSchedule(int pCodEstab, bool pClearJobs)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.GERARCALENDARIOOP(:pCODESTAB, :pLIMPARDADOS); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pLIMPARDADOS", OracleDbType.Int32, pClearJobs ? 1 : 0 , ParameterDirection.Input)
                );
        }

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.SALVARALTERACOESNOERP(:pCODESTAB, :pIDLOADDATA); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                   );
        }

        public async Task<List<pdOrdemProdDet>> DetailMachineLoad(int pCodEstab, string pCodMaquina, DateTime pDtProducao, string pCodUsuario)
        {
            var result = DbConn.Database.SqlQuery<pdOrdemProdDet>(@"SELECT TO_NUMBER(TO_CHAR(PDORDEMPROD.CODORDPROD)) CODORDPROD, PDORDEMPROD.NIVELORDPROD, PDORDEMPROD.NIVELORDPRODPAI, PDORDEMPROD.CODESTAB, 
                                                                           PDORDEMPROD.CODPRODUTO, FSPRODUTO.DESCRICAO FSPRODUTODESCRICAO, PDORDEMPROD.DTINICIO, PDORDEMPROD.DTTERMINO,
                                                                           PDORDEMPROD.PROGRAMADO, PDORDEMPROD.REALIZADO, PDORDEMPROD.REFUGADO,
                                                                           PDORDEMPRODROTINAS.CODMAQUINA,
                                                                           PDORDEMPRODROTINAS.OPERACAO,
                                                                           PDORDEMPRODROTINAS.DTINICIO DTINICIOOPER, PDORDEMPRODROTINAS.DTTERMINO DTTERMOPER,
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           PDCARGAMAQSIMUL.MINUTOSUSO) DESCMINHORA
                                                                      FROM PDCARGAMAQ PDCARGAMAQSIMUL,
                                                                           PDORDEMPROD,
                                                                           FSPRODUTO,
                                                                           PDORDEMPRODROTINAS
                                                                     WHERE PDORDEMPROD.CODESTAB              = PDCARGAMAQSIMUL.CODESTAB
                                                                       AND PDORDEMPROD.CODORDPROD            = PDCARGAMAQSIMUL.CODORDPROD
                                                                       AND PDORDEMPROD.NIVELORDPROD          = PDCARGAMAQSIMUL.NIVELORDPROD
                                                                       AND FSPRODUTO.CODPRODUTO              = PDCARGAMAQSIMUL.CODPRODUTO
                                                                       AND PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                       AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                       AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                       AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                       AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                       AND PDORDEMPRODROTINAS.OPERACAO       = PDCARGAMAQSIMUL.OPERACAO
                                                                       AND PDCARGAMAQSIMUL.CODESTAB          = :pCODESTAB
                                                                       AND PDCARGAMAQSIMUL.CODMAQUINA        = :pCODMAQUINA
                                                                       AND TRUNC(PDCARGAMAQSIMUL.DATAUSO)    = :pDTPRODUCAO
                                                                       AND PDCARGAMAQSIMUL.CODUSUARIO        = :pCODUSUARIO",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pCODMAQUINA", OracleDbType.Varchar2, pCodMaquina, ParameterDirection.Input),
                   new OracleParameter("pDTPRODUCAO", OracleDbType.Date, pDtProducao.Date, ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
            /*
            var result = DbConn.Database.SqlQuery<pdOrdemProdDet>(@"SELECT TBORDEMPROD.CODORDPROD, TBORDEMPROD.NIVELORDPROD, TBORDEMPROD.NIVELORDPRODPAI, TBORDEMPROD.CODESTAB, 
                                                                           TBORDEMPROD.CODPRODUTO, TBORDEMPROD.DTINICIO, TBORDEMPROD.DTTERMINO,
                                                                           TBORDEMPROD.PROGRAMADO, TBORDEMPROD.REALIZADO, TBORDEMPROD.REFUGADO,
                                                                           TBORDEMPROD.CODMAQUINA,
                                                                           TBORDEMPROD.OPERACAO,
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           TBORDEMPROD.MINHORA) DESCMINHORA,  
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           TBORDEMPROD.MINSACUMULADOS) DESCMINSACUMULADOS
                                                                      FROM (SELECT PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD, PDORDEMPROD.NIVELORDPRODPAI, PDORDEMPROD.CODESTAB, 
                                                                                   PDORDEMPROD.CODPRODUTO, PDORDEMPROD.DTINICIO, PDORDEMPROD.DTTERMINO,
                                                                                   PDORDEMPROD.PROGRAMADO, PDORDEMPROD.REALIZADO, PDORDEMPROD.REFUGADO,
                                                                                   PDORDEMPRODROTINAS.CODMAQUINA,
                                                                                   PDORDEMPRODROTINAS.OPERACAO,
                                                                                   CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                   ) MINHORA,
                                                                                   SUM(CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                    )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)  MINSACUMULADOS,
                                                                                   PDORDEMPROD.DTINICIO + 
                                                                                   ((SUM(CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.PROGRAMADO) / PDORDEMPRODROTINAS.PRODHORARIA) * 60                             
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                    )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)) / 60 /24) DTPROD
                                                                              FROM PDORDEMPROD,
                                                                                   PDORDEMPRODROTINAS,
                                                                                   ENROTINAS,
                                                                                   ENMAQUINAS,
                                                                                   ENCELULAS
                                                                             WHERE PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                               AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                               AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                               AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                               AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                               AND PDORDEMPROD.CODESTAB              = :pCODESTAB
                                                                               AND ENROTINAS.CODROTINA               = PDORDEMPRODROTINAS.CODROTINA
                                                                               AND ENMAQUINAS.CODMAQUINA             = PDORDEMPRODROTINAS.CODMAQUINA
                                                                               AND ENMAQUINAS.CODESTAB               = PDORDEMPRODROTINAS.CODESTAB 
                                                                               AND ENCELULAS.CODCELULA               = ENMAQUINAS.CODCELULA
                                                                               AND ENCELULAS.CODESTAB                = ENMAQUINAS.CODESTAB
                                                                               AND PDORDEMPRODROTINAS.PRODHORARIA    > 0
                                                                               AND PDORDEMPRODROTINAS.CODMAQUINA     = :pCODMAQUINA) TBORDEMPROD
                                                                     WHERE TRUNC(TBORDEMPROD.DTPROD) = :pDTPRODUCAO
                                                                     ORDER BY TBORDEMPROD.CODORDPROD,
                                                                              TBORDEMPROD.NIVELORDPROD DESC,
                                                                              TBORDEMPROD.OPERACAO",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pCODMAQUINA", OracleDbType.Varchar2, pCodMaquina, ParameterDirection.Input),
                   
                   new OracleParameter("pDTPRODUCAO", OracleDbType.Date, pDtProducao.Date, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
            */
        }

        public async Task<List<pdOrdemProdDet>> DetailMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodMaquina, DateTime pDtProducao, string pCodUsuario)
        {
            var result = DbConn.Database.SqlQuery<pdOrdemProdDet>(@"SELECT TO_NUMBER(TO_CHAR(PDOPJOBSROT.CODORDPROD)) CODORDPROD, PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.NIVELORDPRODPAI, PDOPJOBSROT.CODESTAB, 
                                                                           PDOPJOBSROT.CODPRODUTO, PDOPJOBSROT.FSPRODUTODESCRICAO, PDOPJOBSROT.DATAINIOP DTINICIO, PDOPJOBSROT.DATAFIMOP DTTERMINO,
                                                                           PDOPJOBSROT.QTDPROG PROGRAMADO, PDOPJOBSROT.QTDAPONT REALIZADO, PDOPJOBSROT.QTDREFUGO REFUGADO,
                                                                           PDOPJOBSROT.CODMAQUINA,
                                                                           PDOPJOBSROT.OPERACAO,
                                                                           PDOPJOBSROT.DTINICIOOPER, PDOPJOBSROT.DTTERMOPER,
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           PDCARGAMAQSIMUL.MINUTOSUSO) DESCMINHORA
                                                                      FROM PDCARGAMAQSIMUL,
                                                                           PDOPJOBSROT
                                                                     WHERE PDOPJOBSROT.CODESTAB              = PDCARGAMAQSIMUL.CODESTAB
                                                                       AND PDOPJOBSROT.CODORDPROD            = PDCARGAMAQSIMUL.CODORDPROD
                                                                       AND PDOPJOBSROT.NIVELORDPROD          = PDCARGAMAQSIMUL.NIVELORDPROD
                                                                       AND PDOPJOBSROT.OPERACAO              = PDCARGAMAQSIMUL.OPERACAO
                                                                       AND PDCARGAMAQSIMUL.CODESTAB          = :pCODESTAB
                                                                       AND PDCARGAMAQSIMUL.CODMAQUINA        = :pCODMAQUINA
                                                                       AND PDCARGAMAQSIMUL.IDLOADDATA        = :pIDLOADDATA
                                                                       AND TRUNC(PDCARGAMAQSIMUL.DATAUSO)    = :pDTPRODUCAO
                                                                       AND PDCARGAMAQSIMUL.CODUSUARIO        = :pCODUSUARIO",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pCODMAQUINA", OracleDbType.Varchar2, pCodMaquina, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pDTPRODUCAO", OracleDbType.Date, pDtProducao.Date, ParameterDirection.Input),
                   new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                   );
            return await result.ToListAsync();

            /*
            var result = DbConn.Database.SqlQuery<pdOrdemProdDet>(@"SELECT TO_NUMBER(TO_CHAR(CODORDPROD)) CODORDPROD, TBORDEMPROD.NIVELORDPROD, TBORDEMPROD.NIVELORDPRODPAI, TBORDEMPROD.CODESTAB, 
                                                                           TBORDEMPROD.CODPRODUTO, FSPRODUTO.DESCRICAO FSPRODUTODESCRICAO, TBORDEMPROD.DTINICIO, TBORDEMPROD.DTTERMINO,
                                                                           TBORDEMPROD.PROGRAMADO, TBORDEMPROD.REALIZADO, TBORDEMPROD.REFUGADO,
                                                                           TBORDEMPROD.CODMAQUINA,
                                                                           TBORDEMPROD.OPERACAO,
                                                                           TBORDEMPROD.DTINICIOOPER, TBORDEMPROD.DTTERMOPER,
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           TBORDEMPROD.MINHORA) DESCMINHORA,  
                                                                           PCKG_UTEIS.MINTOHORAMIN(
                                                                           TBORDEMPROD.MINSACUMULADOS) DESCMINSACUMULADOS
                                                                      FROM (SELECT DISTINCT PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD, PDORDEMPROD.NIVELORDPRODPAI, PDORDEMPROD.CODESTAB, 
                                                                                   PDORDEMPROD.CODPRODUTO, PDORDEMPROD.DTINICIOOPER DTINICIO, PDORDEMPROD.DTTERMOPER DTTERMINO,
                                                                                   PDORDEMPROD.QTDPROG PROGRAMADO, PDORDEMPROD.QTDAPONT REALIZADO, PDORDEMPROD.QTDREFUGO REFUGADO,
                                                                                   PDORDEMPRODROTINAS.CODMAQUINA, PDORDEMPROD.DTINICIOOPER, PDORDEMPROD.DTTERMOPER,
                                                                                   PDORDEMPRODROTINAS.OPERACAO,
                                                                                   CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60                             
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                   ) MINHORA,
                                                                                   SUM(CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60                             
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                    )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)  MINSACUMULADOS,
                                                                                   PDORDEMPROD.DTINICIOOPER + 
                                                                                   ((SUM(CEIL((CASE 
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA >= 1 THEN
                                                                                             ((PDORDEMPROD.QTDPROG) / PDORDEMPRODROTINAS.PRODHORARIA) * 60                             
                                                                                           WHEN PDORDEMPRODROTINAS.PRODHORARIA < 1 THEN
                                                                                             60
                                                                                           ELSE         
                                                                                             1
                                                                                         END)
                                                                                    )) OVER (PARTITION BY PDORDEMPROD.CODORDPROD, PDORDEMPROD.NIVELORDPROD ORDER BY  PDORDEMPRODROTINAS.OPERACAO)) / 60 /24) DTPROD
                                                                              FROM PDOPJOBSROT PDORDEMPROD,
                                                                                   PDORDEMPRODROTINAS,
                                                                                   ENROTINAS,
                                                                                   ENMAQUINAS,
                                                                                   ENCELULAS
                                                                             WHERE PDORDEMPRODROTINAS.CODESTAB       = PDORDEMPROD.CODESTAB
                                                                               AND PDORDEMPRODROTINAS.CODESTRUTURA   = PDORDEMPROD.CODESTRUTURA
                                                                               AND PDORDEMPRODROTINAS.CODORDPROD     = PDORDEMPROD.CODORDPROD
                                                                               AND PDORDEMPRODROTINAS.CODROTEIRO     = PDORDEMPROD.CODROTEIRO
                                                                               AND PDORDEMPRODROTINAS.NIVELORDPROD   = PDORDEMPROD.NIVELORDPROD
                                                                               AND PDORDEMPRODROTINAS.OPERACAO       = PDORDEMPROD.OPERACAO
                                                                               AND PDORDEMPROD.CODESTAB              = :pCODESTAB
                                                                               AND PDORDEMPROD.IDLOADDATA            = :pIDLOADDATA
                                                                               AND ENROTINAS.CODROTINA               = PDORDEMPRODROTINAS.CODROTINA
                                                                               AND ENMAQUINAS.CODMAQUINA             = PDORDEMPRODROTINAS.CODMAQUINA
                                                                               AND ENMAQUINAS.CODESTAB               = PDORDEMPRODROTINAS.CODESTAB 
                                                                               AND ENCELULAS.CODCELULA               = ENMAQUINAS.CODCELULA
                                                                               AND ENCELULAS.CODESTAB                = ENMAQUINAS.CODESTAB
                                                                               AND PDORDEMPRODROTINAS.PRODHORARIA    > 0
                                                                               AND PDORDEMPRODROTINAS.CODMAQUINA     = :pCODMAQUINA) TBORDEMPROD,
                                                                           FSPRODUTO
                                                                     WHERE TRUNC(TBORDEMPROD.DTPROD) = :pDTPRODUCAO
                                                                       AND FSPRODUTO.CODPRODUTO = TBORDEMPROD.CODPRODUTO
                                                                     ORDER BY TBORDEMPROD.CODORDPROD,
                                                                              TBORDEMPROD.NIVELORDPROD DESC,
                                                                              TBORDEMPROD.OPERACAO",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pCODMAQUINA", OracleDbType.Varchar2, pCodMaquina, ParameterDirection.Input),

                   new OracleParameter("pDTPRODUCAO", OracleDbType.Date, pDtProducao.Date, ParameterDirection.Input)
                   );
            return await result.ToListAsync();*/
        }
    }
}
