﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Threading.Tasks;
using System;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class OPColorsRepository : BaseRepository<pdOPColors>, IOPColorsRepository
    {
        public async Task ClearColors() =>
            await DbConn.Database.ExecuteSqlCommandAsync("TRUNCATE TABLE PDOPCOLORS");

        public async Task AttachColorToOP(int pCodEstab) =>
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.ATTACHCOLORTOOP(:pCODESTAB); END;",
                      new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                      );

        public async Task AttachColorToOPRot(int pCodEstab) =>
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.ATTACHCOLORTOOPROT(:pCODESTAB); END;",
                      new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                      );

        
    }
}
