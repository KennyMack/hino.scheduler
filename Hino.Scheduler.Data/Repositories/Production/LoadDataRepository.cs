﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Threading.Tasks;
using System;
using Hino.Scheduler.Model;
using System.ComponentModel;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class LoadDataRepository : BaseRepository<pdLoadData>, ILoadDataRepository
    {
        public async Task<long> CreateScenario(int pCodEstab, string pCodUsuario, DateTime pInitalDate, DateTime pEndDate)
        {
            var idLoad = await NextSequenceAsync();

            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.CREATESCENARIO(:pCODESTAB, :pIDLOAD, :pCODUSUARIO, :pINITALDATE, :pENDDATE); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pIDLOAD", OracleDbType.Int64, idLoad, ParameterDirection.Input),
                new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input),
                new OracleParameter("pINITALDATE", OracleDbType.Date, pInitalDate, ParameterDirection.Input),
                new OracleParameter("pENDDATE", OracleDbType.Date, pEndDate, ParameterDirection.Input)
                );

            return idLoad;
        }
        public async Task DeleteScenario(int pCodEstab, long pIdDataLoad)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.DELETESCENARIO(:pCODESTAB, :pIDDATALOAD); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pIDDATALOAD", OracleDbType.Int64, pIdDataLoad, ParameterDirection.Input)
                );
        }
    }
}
