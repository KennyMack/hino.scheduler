﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class JobRotSchedulerRepository : BaseRepository<pdJobRot>, IJobRotSchedulerRepository
    {
        public async Task GeneratedJobRotSchedule(int pCodEstab, DateTime pDtInitial, int pTimeParts, bool pClearJobs, bool pSimulate)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.GERARCALENDARIOOPROT(:pCODESTAB, :pTIMEPARTS, :pDTINITIAL, :pLIMPARDADOS); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pTIMEPARTS", OracleDbType.Int32, pTimeParts, ParameterDirection.Input),
                new OracleParameter("pDTINITIAL", OracleDbType.Date, pDtInitial, ParameterDirection.Input),
                new OracleParameter("pLIMPARDADOS", OracleDbType.Int32, pClearJobs ? 1 : 0 , ParameterDirection.Input)
                );

            if (pSimulate)
            {
                await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.SCHEDULECALENDAROPROT(:pCODESTAB, :pTIMEPARTS, :pDTINITIAL); END;",
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pTIMEPARTS", OracleDbType.Int32, pTimeParts, ParameterDirection.Input),
                    new OracleParameter("pDTINITIAL", OracleDbType.Date, pDtInitial, ParameterDirection.Input)
                    );
            }
        }

        public async Task GeneratedJobRoutines(int pCodEstab, bool pClearJobs)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.GERARCALENDARIOOPROTINAS(:pCODESTAB, :pLIMPARDADOS); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pLIMPARDADOS", OracleDbType.Int32, pClearJobs ? 1 : 0, ParameterDirection.Input)
                );
        }

        public async Task<IEnumerable<pdJobRot>> QueryScheduledJobsRot(int pCodEstab, DateTime pDtInitial)
        {
            var result = DbConn.Database.SqlQuery<pdJobRot>(@"SELECT PDOPJOBSROT.IDJOB, PDOPJOBSROT.CODESTAB, PDOPJOBSROT.CODORDPROD, PDOPJOBSROT.CODESTRUTURA, 
                                                                     PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.NIVELORDPRODPAI, PDOPJOBSROT.CODPRODUTO, PDOPJOBSROT.CODROTEIRO, 
                                                                     PDOPJOBSROT.CODPROGRAMA, PDOPJOBSROT.DATAINIOP, PDOPJOBSROT.DATAFIMOP, PDOPJOBSROT.QTDPROG, PDOPJOBSROT.QTDAPONT, 
                                                                     PDOPJOBSROT.QTDREFUGO, PDOPJOBSROT.PERCCONCLUIDO, PDOPJOBSROT.LABELOP, PDOPJOBSROT.STATUSOP, 
                                                                     PDOPJOBSROT.RESOURCEID, PDOPJOBSROT.CODMAQUINA, PDOPJOBSROT.OPERACAO, PDOPJOBSROT.DESCRICAO, 
                                                                     PDOPJOBSROT.COROP, PDOPJOBSROT.PRODHORARIA, PDOPJOBSROT.HORASPRODNEC, PDOPJOBSROT.ALTERADO, 
                                                                     PDOPJOBSROT.DESCOPERACAO, PDOPJOBSROT.JOBSIMULADO, PDOPJOBSROT.LEADTIME, PDOPJOBSROT.LTENTREGA
                                                                FROM PDOPJOBSROT
                                                               WHERE PDOPJOBSROT.CODESTAB             = :pCODESTAB
                                                               ORDER BY PDOPJOBSROT.CODORDPROD, 
                                                                        PDOPJOBSROT.NIVELORDPROD DESC",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }

        public async Task<IEnumerable<pdJobRot>> QueryJobsRotDetailAsync(int pCodEstab, long pIdLoadData, long pCodOrdProd, string pNivelOrdProd) =>
            await base.QueryAsync(r => r.codestab == pCodEstab && r.IdLoadData == pIdLoadData && r.codordprod == pCodOrdProd && r.nivelordprod == pNivelOrdProd);
       /* {
            var result = DbConn.Database.SqlQuery<pdJobRot>(@"SELECT PDOPJOBSROT.IDJOB, PDOPJOBSROT.CODESTAB, PDOPJOBSROT.CODORDPROD, PDOPJOBSROT.CODESTRUTURA, 
                                                                     PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.NIVELORDPRODPAI, PDOPJOBSROT.CODPRODUTO, PDOPJOBSROT.CODROTEIRO, 
                                                                     PDOPJOBSROT.CODPROGRAMA, PDOPJOBSROT.DATAINIOP, PDOPJOBSROT.DATAFIMOP, PDOPJOBSROT.QTDPROG, PDOPJOBSROT.QTDAPONT, 
                                                                     PDOPJOBSROT.QTDREFUGO, PDOPJOBSROT.PERCCONCLUIDO, PDOPJOBSROT.LABELOP, PDOPJOBSROT.STATUSOP, 
                                                                     PDOPJOBSROT.RESOURCEID, PDOPJOBSROT.CODMAQUINA, PDOPJOBSROT.OPERACAO, PDOPJOBSROT.DESCRICAO, 
                                                                     PDOPJOBSROT.COROP, PDOPJOBSROT.PRODHORARIA, PDOPJOBSROT.HORASPRODNEC, PDOPJOBSROT.ALTERADO, 
                                                                     PDOPJOBSROT.DESCOPERACAO, PDOPJOBSROT.JOBSIMULADO, PDOPJOBSROT.LEADTIME, PDOPJOBSROT.LTENTREGA
                                                                FROM PDOPJOBSROT
                                                               WHERE PDOPJOBSROT.CODESTAB             = :pCODESTAB
                                                                 AND PDOPJOBSROT.IDLOADDATA           = :pIDLOADDATA
                                                                 AND PDOPJOBSROT.CODORDPROD           = :pCODORDPROD
                                                                 AND PDOPJOBSROT.NIVELORDPROD         = :pNIVELORDPROD",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pCODORDPROD", OracleDbType.Int64, pCodOrdProd, ParameterDirection.Input),
                   new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }*/

        public async Task GenerateJobShop(int pCodEstab, DateTime pDtInitial, int pTimeParts)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.JOBSHOP(:pCODESTAB, :pPRECISAO, :pDATAINICIO); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pTIMEPARTS", OracleDbType.Int32, pTimeParts, ParameterDirection.Input),
                    new OracleParameter("pDTINITIAL", OracleDbType.Date, pDtInitial, ParameterDirection.Input)
                   );
        }

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.SALVARALTERACOESNOERP(:pCODESTAB, :pIDLOADDATA); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                   );
        }

        public async Task<IEnumerable<veDemanda>> QueryDemandas(int pCodEstab, DateTime pDtInitial, DateTime pDtFinal)
        {
            var result = DbConn.Database.SqlQuery<veDemanda>(@"SELECT VEPROGVENDA.CODPEDVENDA,
                                                                       VEPROGVENDA.CODPRODUTO,
                                                                       FSPRODUTO.DESCRICAO,
                                                                       FSPRODUTOPCP.LEADTIME,
                                                                       TRUNC(PCKG_UTEIS.GETDATACALCLEADTIME(VEPROGVENDA.CODESTAB, VEPROGVENDA.DATAPROGRAMA, FSPRODUTOPCP.LEADTIME, 0, 'D', 'S')) DTSUGINIPROD,
                                                                       TRUNC(VEPROGVENDA.DATAPROGRAMA) DATAPROGRAMA,
                                                                       (VEPROGVENDA.QTDEPROGRAMA - VEPROGVENDA.QTDEFATURADA) SLDPROGRAMA,
                                                                       (FSSALDOESTOQUE.ANTERIOR + FSSALDOESTOQUE.ENTRADA) - FSSALDOESTOQUE.SAIDA SALDOESTOQUE,
                                                                       SUM(VEPROGVENDA.QTDEPROGRAMA - VEPROGVENDA.QTDEFATURADA) OVER
                                                                          (PARTITION BY VEPROGVENDA.CODPRODUTO ORDER BY ROWNUM) QTDNECCOMPOSTA,
                                                                       (FSSALDOESTOQUE.ANTERIOR + FSSALDOESTOQUE.ENTRADA) - FSSALDOESTOQUE.SAIDA  -
                                                                       SUM(VEPROGVENDA.QTDEPROGRAMA - VEPROGVENDA.QTDEFATURADA) OVER
                                                                          (PARTITION BY VEPROGVENDA.CODPRODUTO ORDER BY ROWNUM) SLDESTOQUEFINAL,
                                                                       -1 IDDEMANDA, NULL IDJOBSIMU, VEPROGVENDA.CODESTAB
                                                                  FROM VEPEDVENDA,
                                                                       VEITEMPEDIDO,
                                                                       VEPROGVENDA,                        
                                                                       FSPRODUTOPARAMESTAB,
                                                                       FSPRODUTOPCP,
                                                                       FSPRODUTO,
                                                                       FSSALDOESTOQUE                        
                                                                 WHERE VEITEMPEDIDO.CODESTAB          = VEPEDVENDA.CODESTAB 
                                                                   AND VEITEMPEDIDO.CODPEDVENDA       = VEPEDVENDA.CODPEDVENDA
                                                                   AND VEPROGVENDA.CODPEDVENDA        = VEITEMPEDIDO.CODPEDVENDA
                                                                   AND VEPROGVENDA.CODESTAB           = VEITEMPEDIDO.CODESTAB
                                                                   AND VEPROGVENDA.CODPRODUTO         = VEITEMPEDIDO.CODPRODUTO
                                                                   AND FSPRODUTOPARAMESTAB.CODESTAB   = VEITEMPEDIDO.CODESTAB
                                                                   AND FSPRODUTOPARAMESTAB.CODPRODUTO = VEITEMPEDIDO.CODPRODUTO
                                                                   AND FSSALDOESTOQUE.CODESTAB        = FSPRODUTOPARAMESTAB.CODESTAB
                                                                   AND FSSALDOESTOQUE.CODESTOQUE      = FSPRODUTOPARAMESTAB.CODESTOQUE
                                                                   AND FSSALDOESTOQUE.CODPRODUTO      = FSPRODUTOPARAMESTAB.CODPRODUTO
                                                                   AND FSPRODUTO.CODPRODUTO           = FSPRODUTOPARAMESTAB.CODPRODUTO
                                                                   AND FSPRODUTOPCP.CODESTAB          = FSPRODUTOPARAMESTAB.CODESTAB
                                                                   AND FSPRODUTOPCP.CODPRODUTO        = FSPRODUTOPARAMESTAB.CODPRODUTO
                                                                   AND VEPEDVENDA.STATUS             IN ('A','M')
                                                                   AND (VEPROGVENDA.QTDEPROGRAMA - VEPROGVENDA.QTDEFATURADA) > 0
                                                                   AND VEITEMPEDIDO.STATUS            = 1
                                                                   AND VEPROGVENDA.CODESTAB           = :pCODESTAB
                                                                   AND VEPROGVENDA.DATAPROGRAMA       BETWEEN :pDTINICIO
                                                                                                          AND :pDTTERMINO
                                                                   AND VEPROGVENDA.CODPEDVENDA NOT IN (SELECT TBPROG.CODPEDVENDA
                                                                                                         FROM (SELECT PDPROGRAMA.CODESTAB, PDPROGRAMA.CODPEDVENDA
                                                                                                                 FROM PDPROGRAMA 
                                                                                                                WHERE CODPEDVENDA IS NULL
                                                                                                                UNION
                                                                                                               SELECT PDPRGPRODPRGVEN.CODESTAB, PDPRGPRODPRGVEN.CODPEDVENDA 
                                                                                                                 FROM PDPRGPRODPRGVEN) TBPROG                                    
                                                                                                        WHERE TBPROG.CODPEDVENDA IS NOT NULL
                                                                                                          AND TBPROG.CODESTAB    = VEPROGVENDA.CODESTAB)
                                                                   AND EXISTS (SELECT 1
                                                                                 FROM ENESTRUTURAS
                                                                                WHERE ENESTRUTURAS.CODESTAB = VEPROGVENDA.CODESTAB
                                                                                  AND ENESTRUTURAS.CODPRODUTO = VEPROGVENDA.CODPRODUTO  
                                                                                  AND ENESTRUTURAS.STATUS = 'V')
                                                                ORDER BY VEPROGVENDA.DATAPROGRAMA ",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pDTINICIO", OracleDbType.Date, pDtInitial.FirstHour(), ParameterDirection.Input),
                   new OracleParameter("pDTTERMINO", OracleDbType.Date, pDtFinal.LastHour(), ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }

        public async Task<List<pdJobRot>> QueryOperationBottleneck(int pCodEstab, long pIdScenario)
        {
            var sql = @"SELECT PDOPJOBSROT.CODORDPROD, PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.CODPRODUTO, FSPRODUTO.DESCRICAO FSPRODUTODESCRICAO, 
                                                                     PDOPJOBSROT.CODESTRUTURA, PDOPJOBSROT.DATAFIMOP, PDOPJOBSROT.DATAINIOP, PDOPJOBSROT.OPERACAO,
                                                                     PDOPJOBSROT.CODMAQUINA, TBMAXOPER.HORASPRODNEC, PDOPJOBSROT.CODPEDVENDA,
                                                                     PDOPJOBSROT.QTDPROG, PDOPJOBSROT.NIVELORDPRODPAI,
                                                                     PDOPJOBSROT.IDJOB, PDOPJOBSROT.CODESTAB, PDOPJOBSROT.CODROTEIRO, 
                                                                     PDOPJOBSROT.CODPROGRAMA, PDOPJOBSROT.QTDAPONT, PDOPJOBSROT.QTDREFUGO, PDOPJOBSROT.PERCCONCLUIDO, PDOPJOBSROT.LABELOP, 
                                                                     PDOPJOBSROT.STATUSOP, PDOPJOBSROT.RESOURCEID,  
                                                                     NVL(PDOPJOBSROT.DESCRICAO, ' ') DESCRICAO, PDOPJOBSROT.COROP, PDOPJOBSROT.PRODHORARIA,  
                                                                     PDOPJOBSROT.ALTERADO, PDOPJOBSROT.DESCOPERACAO, PDOPJOBSROT.JOBSIMULADO, PDOPJOBSROT.GARGALOOPER, PDOPJOBSROT.CODROTINA, 
                                                                     PDOPJOBSROT.DTINICIOOPER, PDOPJOBSROT.DTTERMOPER, PDOPJOBSROT.IDLOADDATA, PDOPJOBSROT.LEADTIME, PDOPJOBSROT.LTENTREGA
                                                                FROM (SELECT TBOPERMAX.CODORDPROD, TBOPERMAX.NIVELORDPROD, 
                                                                             MIN(TBOPERMAX.OPERACAO) OPERACAO, MAX(TBOPERMAX.HORASPRODNEC) HORASPRODNEC     
                                                                        FROM (SELECT PDOPJOBSROT.CODORDPROD, PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.OPERACAO OPERACAO, PDOPJOBSROT.HORASPRODNEC HORASPRODNEC,
                                                                                     DECODE(PDOPJOBSROT.HORASPRODNEC, (SELECT MAX(MAXROT.HORASPRODNEC) HORASPRODNEC 
                                                                                                                         FROM PDOPJOBSROT MAXROT
                                                                                                                        WHERE MAXROT.CODESTAB     = PDOPJOBSROT.CODESTAB
                                                                                                                          AND MAXROT.CODORDPROD   = PDOPJOBSROT.CODORDPROD
                                                                                                                          AND MAXROT.NIVELORDPROD = PDOPJOBSROT.NIVELORDPROD), 1, 0) MAIOR
                                                                                FROM PDOPJOBSROT) TBOPERMAX
                                                                       WHERE TBOPERMAX.MAIOR > 0
                                                                       GROUP BY TBOPERMAX.CODORDPROD, TBOPERMAX.NIVELORDPROD) TBMAXOPER,
                                                                     PDOPJOBSROT,
                                                                     FSPRODUTO
                                                               WHERE PDOPJOBSROT.CODORDPROD     = TBMAXOPER.CODORDPROD
                                                                 AND PDOPJOBSROT.NIVELORDPROD   = TBMAXOPER.NIVELORDPROD
                                                                 AND PDOPJOBSROT.OPERACAO       = TBMAXOPER.OPERACAO
                                                                 AND FSPRODUTO.CODPRODUTO       = PDOPJOBSROT.CODPRODUTO
                                                                 AND PDOPJOBSROT.CODESTAB       = :pCODESTAB
                                                                 AND PDOPJOBSROT.IDLOADDATA     = :pIDLOADDATA
                                                               ORDER BY PDOPJOBSROT.CODORDPROD DESC, 
                                                                        PDOPJOBSROT.NIVELORDPROD";

            var result = DbConn.Database.SqlQuery<pdJobRot>(sql,
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdScenario, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }
    }
}
