﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class ResourceRepository : BaseRepository<pdResource>, IResourceRepository
    {
        public async Task GeneratedResources(int pCodEstab, bool pClearJobs)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.CARREGARESOURCES(:pCODESTAB, :pLIMPARDADOS); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pLIMPARDADOS", OracleDbType.Int32, pClearJobs ? 1 : 0, ParameterDirection.Input)
                );
        }

        public async Task AttachResourceToOPRot(int pCodEstab, long pIdLoadData)
        {
            try
            {
                await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.ATTACHRESOURCETOOPROT(:pCODESTAB, :pIDLOADDATA); END;",
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                    );
            }
            catch (System.Exception)
            {
                
            }
        }

        public async Task<List<enMaquinas>> GetMachinesListAsync(int pCodEstab)
        {
            var result = new List<enMaquinas>();
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                using (var conn = DbConn.Database.Connection)
                {
                    await conn.OpenAsync();

                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT ENMAQUINAS.CODCELULA, ENMAQUINAS.CODESTAB, ENMAQUINAS.CODIMOB, 
                                       ENMAQUINAS.CODINPUT, ENMAQUINAS.CODMAQUINA, ENMAQUINAS.CODPROCESSO, 
                                       ENMAQUINAS.DESCCALCAPTO, ENMAQUINAS.DESCRICAO, ENMAQUINAS.INCORPORACAO, 
                                       ENMAQUINAS.OBSERVACAO, ENMAQUINAS.QUANTIDADE, ENMAQUINAS.RENDIMENTO, 
                                       ENMAQUINAS.STATUS
                                  FROM ENMAQUINAS
                                 WHERE ENMAQUINAS.CODESTAB = :pCODESTAB";

                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        result = new List<enMaquinas>(
                            reader.Select(r => new enMaquinas
                            {
                                codcelula = Convert.ToInt32(r["CODCELULA"]),
                                codestab = Convert.ToInt32(r["CODESTAB"]),
                                codmaquina = Convert.ToString(r["CODMAQUINA"]),
                                descricao = Convert.ToString(r["DESCRICAO"]),
                                observacao = Convert.ToString(r["OBSERVACAO"]),
                                quantidade = Convert.ToInt32(r["QUANTIDADE"]),
                                rendimento = Convert.ToInt32(r["RENDIMENTO"]),
                                status = Convert.ToInt32(r["STATUS"]) > 0
                            })
                        );
                    }
                }
            }
            return result;
        }

        public async Task<List<enMaqTurno>> GetMachinesTurnoAsync(int pCodEstab, long pIdLoadData)
        {
            var result = DbConn.Database.SqlQuery<enMaqTurno>(@"SELECT ENMAQTURNO.CODESTAB, ENMAQTURNO.CODMAQUINA, ENMAQTURNO.TURNO,
                                                                       PDTURNOSDET.DIASEMANA, PDTURNOSDET.HORAINICIO, PDTURNOSDET.HORATERMINO, 
                                                                       PDTURNOSDET.HRPARADAINI, PDTURNOSDET.HRPARADATERM
                                                                  FROM ENMAQTURNO,
                                                                       PDTURNOSDET,
                                                                       (SELECT DISTINCT PDOPJOBSROT.CODMAQUINA, PDOPJOBSROT.CODESTAB
                                                                          FROM PDOPJOBSROT
                                                                         WHERE PDOPJOBSROT.IDLOADDATA   = :pIDLOADDATA
                                                                           AND PDOPJOBSROT.CODESTAB     = :pCODESTAB) TBMAQUINAS
                                                                 WHERE ENMAQTURNO.CODESTAB    = TBMAQUINAS.CODESTAB
                                                                   AND ENMAQTURNO.CODMAQUINA  = TBMAQUINAS.CODMAQUINA
                                                                   AND PDTURNOSDET.CODESTAB   = ENMAQTURNO.CODESTAB
                                                                   AND PDTURNOSDET.TURNO      = ENMAQTURNO.TURNO
                                                                 ORDER BY ENMAQTURNO.CODMAQUINA",
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }

        public async Task<List<enMaqDisp>> GetMachineDispAsync(int pCodEstab, long pIdLoadData)
        {
            var result = DbConn.Database.SqlQuery<enMaqDisp>(@"SELECT TBHORASDISP.CODESTAB, TBHORASDISP.CODMAQUINA, 
                                                                           TBHORASDISP.DOMINGO, TBHORASDISP.SEGUNDA, TBHORASDISP.TERCA, TBHORASDISP.QUARTA, 
                                                                           TBHORASDISP.QUINTA, TBHORASDISP.SEXTA, TBHORASDISP.SABADO
                                                                      FROM (SELECT DISTINCT ENMAQUINADISP.CODESTAB, ENMAQUINADISP.CODMAQUINA, 
                                                                                   ENMAQUINADISP.DOMINGO, ENMAQUINADISP.SEGUNDA, ENMAQUINADISP.TERCA, ENMAQUINADISP.QUARTA, 
                                                                                   ENMAQUINADISP.QUINTA, ENMAQUINADISP.SEXTA, ENMAQUINADISP.SABADO  
                                                                              FROM PDOPJOBSROT,
                                                                                   ENMAQUINAS,
                                                                                   ENMAQUINADISP
                                                                             WHERE ENMAQUINAS.CODESTAB      = PDOPJOBSROT.CODESTAB
                                                                               AND ENMAQUINAS.CODMAQUINA    = PDOPJOBSROT.CODMAQUINA
                                                                               AND ENMAQUINADISP.CODESTAB   = ENMAQUINAS.CODESTAB
                                                                               AND ENMAQUINADISP.CODMAQUINA = ENMAQUINAS.CODMAQUINA
                                                                               AND PDOPJOBSROT.CODESTAB     = :pCODESTAB
                                                                               AND PDOPJOBSROT.IDLOADDATA   = :pIDLOADDATA) TBHORASDISP
                                                                     ORDER BY TBHORASDISP.CODMAQUINA",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                   );
            return await result.ToListAsync();
        }
    }
}
