﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class TempoJobPartRepository : BaseRepository<pdTempoJobPart>, ITempoJobPartRepository
    {

    }
}
