﻿using Hino.Scheduler.Data.Repositories.Interfaces.Production;
using Hino.Scheduler.Model.Production;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class SimulJobsRotRepository : BaseRepository<pdSimulJobsRot>, ISimulJobsRotRepository
    {
        public async Task<IEnumerable<pdSimulJobsRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData) =>
            await QueryAsync(r => r.codestab == pCodEstab && r.IdLoadData == pIdLoadData);

        public async Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.SALVARALTSIMULACAONOERP(:pCODESTAB, :pIDLOADDATA); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                   );
        }

        public async Task GeneratedJobSchedule(int pCodEstab, long pIdLoadData, DateTime pDateIni, int pPartesTempo)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.LOADSIMULATIONJOBS(:pCODESTAB, :pIDLOADDATA, :pDATEINI, :pPARTESTEMPO); END;",
                   new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                   new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input),
                   new OracleParameter("pDATEINI", OracleDbType.Date, pDateIni, ParameterDirection.Input),
                   new OracleParameter("pPARTESTEMPO", OracleDbType.Int32, pPartesTempo, ParameterDirection.Input)
                   );
        }
    }
}
