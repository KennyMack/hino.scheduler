﻿using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Sales
{
    public interface IDemandaRepository : IBaseRepository<veDemanda>
    {
        Task CreateJobFromDemanda(int pCodEstab, long pIdDemanda);
        Task AtualizaOPDemandaAsync(int pCodEstab, long pIdLoadData);
        Task<List<veSalesProgram>> GetSalesProgramListAsync(int pCodEstab);
    }
}
