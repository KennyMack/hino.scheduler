﻿using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Sales
{
    public interface IDemandaDetRepository: IBaseRepository<veDemandaDet>
    {
        Task<List<veDemandaOP>> QueryDemandaOP(int pCodEstab, long pIdLoadData);
    }
}
