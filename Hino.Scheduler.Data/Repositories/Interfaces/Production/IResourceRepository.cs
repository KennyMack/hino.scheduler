﻿using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Production
{
    public interface IResourceRepository : IBaseRepository<pdResource>
    {
        Task<List<enMaqTurno>> GetMachinesTurnoAsync(int pCodEstab, long pIdLoadData);
        Task<List<enMaqDisp>> GetMachineDispAsync(int pCodEstab, long pIdLoadData);
        Task GeneratedResources(int pCodEstab, bool pClearJobs);
        Task AttachResourceToOPRot(int pCodEstab, long pIdLoadData);
        Task<List<enMaquinas>> GetMachinesListAsync(int pCodEstab);
    }
}
