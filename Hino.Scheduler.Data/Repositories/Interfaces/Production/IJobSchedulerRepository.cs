﻿using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Production
{
    public interface IJobSchedulerRepository : IBaseRepository<pdJob>
    {
        Task GeneratedJobSchedule(int pCodEstab, bool pClearJobs);
        Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData);
        Task<List<pdOrdemProdDet>> DetailMachineLoad(int pCodEstab, string pCodMaquina, DateTime pDtProducao, string pCodUsuario);
        Task<List<pdOrdemProdDet>> DetailMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodMaquina, DateTime pDtProducao, string pCodUsuario);
    }
}
