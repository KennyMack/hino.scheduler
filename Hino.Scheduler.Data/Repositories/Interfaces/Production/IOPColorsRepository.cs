﻿using Hino.Scheduler.Model.Production;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Production
{
    public interface IOPColorsRepository : IBaseRepository<pdOPColors>
    {
        Task ClearColors();
        Task AttachColorToOP(int pCodEstab);
        Task AttachColorToOPRot(int pCodEstab);
    }
}
