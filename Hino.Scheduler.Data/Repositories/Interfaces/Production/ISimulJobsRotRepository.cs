﻿using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Production
{
    public interface ISimulJobsRotRepository : IBaseRepository<pdSimulJobsRot>
    {
        Task<IEnumerable<pdSimulJobsRot>> GetByEstabIdAndIdLoadAsync(int pCodEstab, long pIdLoadData);
        Task SaveOPChangesToERP(int pCodEstab, long pIdLoadData);
        Task GeneratedJobSchedule(int pCodEstab, long pIdLoadData, DateTime pDateIni, int pPartesTempo);
    }
}
