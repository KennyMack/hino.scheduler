﻿using Hino.Scheduler.Model.Production;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Production
{
    public interface ITempoJobPartRepository : IBaseRepository<pdTempoJobPart>
    {
    }
}
