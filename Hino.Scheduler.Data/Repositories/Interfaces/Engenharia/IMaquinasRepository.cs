﻿using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Production;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Engenharia
{
    public interface IMaquinasRepository : IBaseRepository<enMaquinas>
    {
        Task<List<pdMachineCellLoad>> QueryMachineCellLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd);
        Task<List<pdMachineLoad>> QueryMachineLoad(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario);
        Task<List<pdMachineLoad>> QueryMachineLoadSimulated(int pCodEstab, long pIdLoadData, string pCodUsuario);
        Task ExecLoadCargaMaquinaAsync(int pCodEstab, DateTime pDtInitial, DateTime pDtEnd, string pCodUsuario);
        Task ExecLoadSimulCargaMaquinaAsync(int pCodEstab, long pIdLoadData, string pCodUsuario);
    }
}
