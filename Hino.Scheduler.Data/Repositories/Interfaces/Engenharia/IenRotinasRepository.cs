﻿using Hino.Scheduler.Model.Engenharia;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Engenharia
{
    public interface IenRotinasRepository : IBaseRepository<enRotinas>
    {
    }
}
