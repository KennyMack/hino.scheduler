﻿using Hino.Scheduler.Model.Engenharia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Engenharia
{
    public interface IResourcesHourRepository : IBaseRepository<enResourcesHour>
    {
    }
}
