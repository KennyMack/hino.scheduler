﻿using Hino.Scheduler.Data.Repositories;
using Hino.Scheduler.Model.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Auth
{
    public interface IUsersRepository : IBaseRepository<Users>
    {
    }
}
