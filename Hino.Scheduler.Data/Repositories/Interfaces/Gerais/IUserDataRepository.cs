﻿using Hino.Scheduler.Model.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Interfaces.Gerais
{
    public interface IUserDataRepository : IBaseRepository<geUserData>
    {
        Task<geUserData> GetByCodUsuarioAsync(string pCodUsuario);
    }
}
