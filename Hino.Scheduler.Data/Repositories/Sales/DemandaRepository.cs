﻿using Hino.Scheduler.Data.Repositories.Interfaces.Sales;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class DemandaRepository : BaseRepository<veDemanda>, IDemandaRepository
    {
        public async Task CreateJobFromDemanda(int pCodEstab, long pIdDemanda)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.CREATEJOBFROMDEMANDA(:pCODESTAB, :pIDDEMANDA); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pIDDEMANDA", OracleDbType.Int64, pIdDemanda, ParameterDirection.Input)
                );
        }

        public async Task AtualizaOPDemandaAsync(int pCodEstab, long pIdLoadData)
        {
            await DbConn.Database.ExecuteSqlCommandAsync("BEGIN PCKG_SCHEDULER.ATUALIZAOPDEMANDA(:pCODESTAB, :pIDLOADDATA); END;",
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                new OracleParameter("pIDLOADDATA", OracleDbType.Int64, pIdLoadData, ParameterDirection.Input)
                );
        }

        public async Task<List<veSalesProgram>> GetSalesProgramListAsync(int pCodEstab)
        {
            var result = new List<veSalesProgram>();
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                using (var conn = DbConn.Database.Connection)
                {
                    await conn.OpenAsync();

                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT GEEMPRESA.CODEMPRESA, GEEMPRESA.NOMEFANTASIA, GEEMPRESA.RAZAOSOCIAL,
                                               VEITEMPEDIDO.CODPRODUTO, FSPRODUTO.DESCRICAO, VEITEMPEDIDO.QUANTIDADE QTDTOTALITEM,
                                               VEPROGVENDA.DATAPROGRAMA, VEPROGVENDA.QTDEPROGRAMA, VEPROGVENDA.QTDEREALIZADA, 
                                               VEPROGVENDA.QTDEFATURADA,
                                               NVL((SELECT SUM((PDORDEMPROD.PROGRAMADO + PDORDEMPROD.REFUGADO) - PDORDEMPROD.REALIZADO)  
                                                  FROM PDORDEMPROD
                                                 WHERE PDORDEMPROD.CODPRODUTO         = VEPROGVENDA.CODPRODUTO
                                                   AND PDORDEMPROD.STATUS             = 'P'
                                                   AND TRUNC(PDORDEMPROD.DTTERMINO)  <= TRUNC(VEPROGVENDA.DATAPROGRAMA)
                                                   AND (PDORDEMPROD.PROGRAMADO + PDORDEMPROD.REFUGADO) - PDORDEMPROD.REALIZADO > 0), 0) QTDPRODUCAO,
                                               NVL(TBSALDORESERVA.SALDORESERVA, 0) SALDOESTOQUERESERVA,
                                               NVL((FSSALDOESTOQUE.ANTERIOR + FSSALDOESTOQUE.ENTRADA) - FSSALDOESTOQUE.SAIDA, 0) SALDOESTOQUEPADRAO
                                          FROM VEPROGVENDA,
                                               VEITEMPEDIDO,
                                               VEPEDVENDA,
                                               FSPRODUTOPARAMESTAB,
                                               FSPRODUTO,
                                               GEEMPRESA,
                                               FSSALDOESTOQUE,
                                               (SELECT SUM((FSSALDOESTOQUE.ANTERIOR + FSSALDOESTOQUE.ENTRADA) - FSSALDOESTOQUE.SAIDA) SALDORESERVA,
                                                       FSSALDOESTOQUE.CODPRODUTO, FSSALDOESTOQUE.CODESTAB 
                                                  FROM FSSALDOESTOQUE
                                                 WHERE FSSALDOESTOQUE.CODESTOQUE = 'RESERVA'
                                                 GROUP BY FSSALDOESTOQUE.CODPRODUTO, FSSALDOESTOQUE.CODESTAB ) TBSALDORESERVA
                                         WHERE VEPROGVENDA.CODPRODUTO          = FSPRODUTO.CODPRODUTO
                                           AND VEITEMPEDIDO.CODESTAB           = VEPROGVENDA.CODESTAB
                                           AND VEITEMPEDIDO.CODPEDVENDA        = VEPROGVENDA.CODPEDVENDA
                                           AND VEITEMPEDIDO.CODPRODUTO         = VEPROGVENDA.CODPRODUTO
                                           AND VEPEDVENDA.CODESTAB             = VEITEMPEDIDO.CODESTAB
                                           AND VEPEDVENDA.CODPEDVENDA          = VEITEMPEDIDO.CODPEDVENDA
                                           AND GEEMPRESA.CODEMPRESA            = VEPEDVENDA.CODEMPRESA
                                           AND TBSALDORESERVA.CODPRODUTO    (+)= VEITEMPEDIDO.CODPRODUTO
                                           AND TBSALDORESERVA.CODESTAB      (+)= VEITEMPEDIDO.CODESTAB
                                           AND FSPRODUTOPARAMESTAB.CODESTAB    = VEITEMPEDIDO.CODESTAB
                                           AND FSPRODUTOPARAMESTAB.CODPRODUTO  = VEITEMPEDIDO.CODPRODUTO
                                           AND FSSALDOESTOQUE.CODESTAB      (+)= FSPRODUTOPARAMESTAB.CODESTAB
                                           AND FSSALDOESTOQUE.CODPRODUTO    (+)= FSPRODUTOPARAMESTAB.CODPRODUTO
                                           AND FSSALDOESTOQUE.CODESTOQUE    (+)= FSPRODUTOPARAMESTAB.CODESTOQUE
                                           AND VEPROGVENDA.QTDEPROGRAMA - VEPROGVENDA.QTDEFATURADA > 0
                                           AND VEPEDVENDA.STATUS            IN ('A', 'M', 'G')
                                         ORDER BY VEPROGVENDA.DATAPROGRAMA,
                                                  VEPROGVENDA.CODPRODUTO, 
                                                  VEPEDVENDA.CODEMPRESA";

                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        result = new List<veSalesProgram>(
                            reader.Select(r => new veSalesProgram
                            {
                                codempresa = Convert.ToInt32(r["CODEMPRESA"]),
                                nomefantasia = Convert.ToString(r["NOMEFANTASIA"]),
                                razaosocial = Convert.ToString(r["RAZAOSOCIAL"]),
                                codproduto = Convert.ToString(r["CODPRODUTO"]),
                                descricao = Convert.ToString(r["DESCRICAO"]),
                                qtdtotalitem = Convert.ToDecimal(r["QTDTOTALITEM"]),
                                dataprograma = Convert.ToDateTime(r["DATAPROGRAMA"]),
                                qtdeprograma = Convert.ToDecimal(r["QTDEPROGRAMA"]),
                                qtderealizada = Convert.ToDecimal(r["QTDEREALIZADA"]),
                                qtdefaturada = Convert.ToDecimal(r["QTDEFATURADA"]),
                                qtdproducao = Convert.ToDecimal(r["QTDPRODUCAO"]),
                                saldoestoquepadrao = Convert.ToDecimal(r["SALDOESTOQUEPADRAO"]),
                                saldoestoquereserva = Convert.ToDecimal(r["SALDOESTOQUERESERVA"]),
                            })
                        );
                    }
                }
            }
            return result;
        }
    }
}
