﻿using Hino.Scheduler.Data.Repositories.Interfaces.Sales;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Repositories.Production
{
    public class DemandaDetRepository : BaseRepository<veDemandaDet>, IDemandaDetRepository
    {
        public async Task<List<veDemandaOP>> QueryDemandaOP(int pCodEstab, long pIdLoadData)
        {
            var result = new List<veDemandaOP>();
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                using (var conn = DbConn.Database.Connection)
                {
                    await conn.OpenAsync();

                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT DISTINCT PDOPJOBSROT.CODORDPROD, PDOPJOBSROT.NIVELORDPROD, PDOPJOBSROT.NIVELORDPRODPAI, PDOPJOBSROT.CODPRODUTO,
                                               PDOPJOBSROT.FSPRODUTODESCRICAO, PDOPJOBSROT.DATAINIOP, PDOPJOBSROT.DATAFIMOP,
                                               PDOPJOBSROT.QTDPROG, PDOPJOBSROT.QTDAPONT, PDOPJOBSROT.QTDREFUGO, PDOPJOBSROT.LABELOP,
                                               PDOPJOBSROT.STATUSOP, VEDEMANDADET.IDLOADDATA, VEDEMANDADET.IDDEMANDA
                                          FROM VEDEMANDADET,
                                               PDOPJOBSROT
                                         WHERE PDOPJOBSROT.IDJOB      = VEDEMANDADET.IDJOB
                                           AND PDOPJOBSROT.CODESTAB   = :pCODESTAB
                                           AND PDOPJOBSROT.IDLOADDATA = :pIDLOADDATA
                                         ORDER BY PDOPJOBSROT.CODORDPROD";

                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pIDLOADDATA", OracleDbType.Int32, pIdLoadData, ParameterDirection.Input));

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        result = new List<veDemandaOP>(
                            reader.Select(r => new veDemandaOP
                            {
                                codordprod = Convert.ToInt64(r["CODORDPROD"]),
                                nivelordprod = Convert.ToString(r["NIVELORDPROD"]),
                                nivelordprodpai = Convert.ToString(r["NIVELORDPRODPAI"]),
                                codproduto = Convert.ToString(r["CODPRODUTO"]),
                                fsprodutodescricao = Convert.ToString(r["FSPRODUTODESCRICAO"]),
                                datafimop = Convert.ToDateTime(r["DATAFIMOP"]),
                                datainiop = Convert.ToDateTime(r["DATAINIOP"]),
                                qtdprog = Convert.ToDecimal(r["QTDPROG"]),
                                qtdapont = Convert.ToDecimal(r["QTDAPONT"]),
                                qtdrefugo = Convert.ToDecimal(r["QTDREFUGO"]),
                                labelop = Convert.ToInt32(r["LABELOP"]),
                                statusop = Convert.ToInt32(r["STATUSOP"]),
                                IdLoadData = Convert.ToInt64(r["IDLOADDATA"]),
                                iddemanda = Convert.ToInt64(r["IDDEMANDA"])
                            })
                        );
                    }
                }
            }

            return result;
        }
    }
}
