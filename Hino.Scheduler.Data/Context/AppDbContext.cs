﻿using Hino.Scheduler.Data.Repositories.Engenharia;
using Hino.Scheduler.Model;
using Hino.Scheduler.Model.Auth;
using Hino.Scheduler.Model.Engenharia;
using Hino.Scheduler.Model.Gerais;
using Hino.Scheduler.Model.Production;
using Hino.Scheduler.Model.Sales;
using Hino.Scheduler.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hino.Scheduler.Data.Context
{
    [DbConfigurationType(typeof(ContextConfiguration))]
    public class AppDbContext : DbContext
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<pdJob> Jobs { get; set; }
        public DbSet<pdJobRot> JobRot { get; set; }
        public DbSet<pdResource> Resources { get; set; }
        public DbSet<enMaquinas> Machines { get; set; }
        public DbSet<pdOPColors> OPColors { get; set; }
        public DbSet<veDemanda> Demandas { get; set; }
        public DbSet<pdTempoJobPart> TempoJobPart { get; set; }
        public DbSet<enRotinas> Rotinas { get; set; }
        public DbSet<pdLoadData> LoadData { get; set; }
        public DbSet<pdSimulJobsRot> SimulJobsRot { get; set; }
        public DbSet<geUserData> UserData { get; set; }
        public DbSet<enResourcesHour> ResourcesHours { get; set; }
        public DbSet<veDemandaDet> DemandaDet { get; set; }

        public AppDbContext():
            base(Parameters.alias)
        {
            Parameters.connStr = this.Database.Connection.ConnectionString;
            this.Configuration.LazyLoadingEnabled = true;

            if (!string.IsNullOrEmpty(Parameters.connStr))
            {
                var reg = new Regex(@"(Id\=)(.*?)(\;)");
                var match = reg.Match(Parameters.connStr);
                Parameters.dbUser = match.Value.Replace("Id=", "").Replace(";", "");
            }
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(Parameters.dbUser);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Properties().Configure(c =>
            {
                c.HasColumnName(c.ClrPropertyInfo.Name.ToUpper());
            });


            base.OnModelCreating(modelBuilder);
        }
    }
}
